
/*@#common-include#(kod.js)*/

// Ova je metoda namijenjena inicijalizaciji zadatka.
// Dobiva questionConfigData koji postavlja editor konfiguracije
// -------------------------------------------------------------------------------------------------
function questionInitialize(questionConfig) {
  var a = Math.floor(Math.random()*(questionConfig.limitMax - questionConfig.limitMin)) + questionConfig.limitMin;
  var b = Math.floor(Math.random()*(questionConfig.limitMax - questionConfig.limitMin)) + questionConfig.limitMin;
  var tocan = a+b;
  userData.question = {a: a, b: b};
  userData.question.correct = tocan;
  userData.questionState = "";
}

// Ova je metoda namijenjena generiranju varijabli koje se dinamički računaju i nigdje ne pohranjuju.
// Ove varijable bit će vidljive kodu koji HTML-predloške odnosno ServerSide-JavaScript-predloške
// lokalizira.
// -------------------------------------------------------------------------------------------------
function getComputedProperties() {
  return {aa: userData.question.a, bb: userData.question.b};
}

// Ova metoda poziva se kako bi se obavilo vrednovanje zadatka. Pozivatelju treba vratiti objekt:
// {correctness: X, solved: Y}, gdje su X in [0,1], Y in {true,false}.
// -------------------------------------------------------------------------------------------------
function questionEvaluate() {
  function isNumeric(n) {
    if(n=="") return false;
    return !isNaN(n);
  }
  function setCorrectSolution() {
    userData.correctQuestionState = ""+userData.question.correct;
  }

  var res = {correctness: 0.0, solved: false};
  if(!userData.questionState) {
    setCorrectSolution();
    return res;
  }
  res.solved = true;
  if(isNumeric(userData.questionState) && Math.abs(parseInt(userData.questionState, 10)-userData.question.correct)<1E-6) {
    res.correctness = 1.0;
    userData.correctQuestionState = ""+userData.questionState;
  } else {
    res.correctness = 0.0;
    setCorrectSolution();
  }
  return res;
}

// Metoda koja se poziva kako bi izvadila zajednički dio stanja zadatka koje je potrebno za prikaz zadatka.
// -------------------------------------------------------------------------------------------------
function exportCommonState() {
  return {a: userData.question.a, b: userData.question.b};
}

// Metoda koja se poziva kako bi izvadilo stanje zadatka koje je postavio korisnik (njegovo rješenje).
// Razlikuje se svrha prikaza: korisnikovo rješenje ili točno rješenje
// -------------------------------------------------------------------------------------------------
function exportUserState(stateType) {
  if(stateType=="USER") {
    return {qs: userData.questionState};
  }
  if(stateType=="CORRECT") {
    return {qs: userData.correctQuestionState};
  }
  return {};
}

// Metoda koja se poziva kako bi vratila stanje zadatka koje odgovara nerješavanom zadatku.
// -------------------------------------------------------------------------------------------------
function exportEmptyState() {
  return {qs: ""};
}

// Metoda koja se poziva kako bi pohranila stanje zadatka na temelju onoga što je korisnik napravio.
// -------------------------------------------------------------------------------------------------
function importQuestionState(data) {
  userData.questionState = data;
}

