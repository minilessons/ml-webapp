# Minilessons web-application

Minilesson: web-application.

Minilessons web-application is Java Servlet based web-application which can be executed on any
servlet container. It is developed as a platform for distributing minilesson content.

_Minilesson_ is selfcontained educational package which can contain multilingual educational
content with text, graphics, video-materials, Java web-start applications and various kinds of questions.

Currently, this is very-alpha release which showcases technological capabilities and can be run on local
computer in order to start the production of educational materials. A number of minilessons are available
on gitlab at https://gitlab.com/groups/minilessons.
