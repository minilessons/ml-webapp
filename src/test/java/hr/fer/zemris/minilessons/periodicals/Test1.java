package hr.fer.zemris.minilessons.periodicals;

import org.junit.Test;

import hr.fer.zemris.minilessons.periodicals.periods.EachNMinutes;
import hr.fer.zemris.minilessons.periodicals.periods.PeriodicalTimeSpec;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

public class Test1 {

	@Test
	public void testEachNMinutes1() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm01+00:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 31, 17);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 31, 25), t2);
	}
	
	@Test
	public void testEachNMinutes2() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm01+00:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 31, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 32, 25), t2);
	}
	
	@Test
	public void testEachNMinutes3() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm01+00:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 31, 28);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 32, 25), t2);
	}
	
	@Test
	public void testEachNMinutes4() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm01+00:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 59, 28);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 11, 00, 25), t2);
	}
	
	@Test
	public void testEachNMinutes5() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm05+01:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 1, 20);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 1, 25), t2);
	}
	
	@Test
	public void testEachNMinutes6() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm05+01:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 1, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 6, 25), t2);
	}
	
	@Test
	public void testEachNMinutes7() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm05+01:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 6, 24);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 6, 25), t2);
	}
	
	@Test
	public void testEachNMinutes8() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm05+01:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 6, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 11, 25), t2);
	}
	
	@Test
	public void testEachNMinutes9() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm05+01:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 50, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 10, 51, 25), t2);
	}
	
	@Test
	public void testEachNMinutes10() {
		PeriodicalTimeSpec pts = new EachNMinutes().get("xm05+01:25");
		
		LocalDateTime t1 = LocalDateTime.of(2016, 9, 2, 10, 56, 25);
		
		LocalDateTime t2 = pts.nextTime(t1);
		
		assertEquals(LocalDateTime.of(2016, 9, 2, 11, 1, 25), t2);
	}
}
