//alert(ml_base_url);

function mlCommentsSubsystem() {
	this.initialized = false;
	this.threads = null;
	this.show = function() {
		if(this.initialized) {
			for(var i = 0; i < this.threads.length; i++) {
				this.threads[i].initialized = false;
				this.threads[i].displayed = false;
			}
			this.buildView();
		}
		if(!this.initialized) {
			  var me = this;
			  $.ajax({
				  method: 'GET',
				  async: true,
				  url: ml_base_url.replace("REPLACEME", ml_base_mlid+"/threads/"+ml_base_itemid),
				  cache: false,
				  success: function(data) {
						if(data.error==false) {
							me.threads = data.threads;
							me.initialized = true;
							me.buildView();
							me.showView();
						} else {
							alert("Pogreška!");
						}
						return;
				  },
				  error: function(x,e,t) {
						alert("Pogreška! ("+e+") ("+t+")");
				  }
			  });
		} else {
			this.showView();
		}
	}
	
	function htmle(text) {
		return $("<div></div>").text(text).html();
	}
	
	this.buildView = function() {
		this.html = '<div id="mlcmntdialog" title="Poruke i komentari" style="font-size: 0.8em;">';
		this.html += this.headerButtons();
		for(var t = 0; t < this.threads.length; t++) {
			var thr = this.threads[t];
			thr.initialized = false;
			thr.displayed = false;
			this.html += '<div style="margin-top: 5px; margin-bottom: 5px;">';
			this.html += '  <div class="mlThrTitle" style=\"background-color: #ccccff;\" onclick=\'mlCS.toggleThreadVisibility(\"'+thr.id+'\");\'>Naslov: '+htmle(thr.title)+'</div>';
			this.html += '  <div id="mlthrt_'+thr.id+'" style="display: none; padding-left: 10px; border-left: 2px solid #ddddff">Pričekajte dok se komentari dohvaćaju...</div>';
			this.html += '</div>';
		}
		this.html += '</div>';
	}

	this.getThread = function(tid) {
		for(var t = 0; t < this.threads.length; t++) {
			if(this.threads[t].id==tid) return this.threads[t];
		}
		return null;
	}
	
	this.toggleThreadVisibility = function(tid) {
		var thr = this.getThread(tid);
		if(thr.displayed==true) return;
		for(var t = 0; t < this.threads.length; t++) {
			var thr2 = this.threads[t];
			if(thr2!=thr && thr2.displayed==true) {
				thr2.displayed=false;
				$("#mlthrt_"+thr2.id).hide();
			}
		}
		thr.displayed=true;
		$("#mlthrt_"+tid).show();
		/*
		thr.displayed = !thr.displayed;
		if(thr.displayed) {
			$("#mlthrt_"+tid).show();
		} else {
			$("#mlthrt_"+tid).hide();
		}*/
		if(thr.displayed) {
		  if(thr.dataobtained!=true) {
			thr.dataobtained=true;
			var me = this;
			$.ajax({
			  method: 'GET',
			  async: true,
			  url: ml_base_url.replace("REPLACEME", ml_base_mlid+"/comments/"+ml_base_itemid+"/"+tid),
			  cache: false,
			  success: function(data) {
					if(data.error==false) {
						thr.comments = [];
						for(var i = 0; i < data.comments.length; i++) {
							thr.comments.push(data.comments[i]);
						}
						me.buildThreadView(tid, thr);
					} else {
						alert("Pogreška!");
					}
					return;
			  },
			  error: function(x,e,t) {
					alert("Pogreška! ("+e+") ("+t+")");
			  }
			});
		  } else if(thr.initialized != true) {
				this.buildThreadView(tid, thr);
		  }
		}
	}

	this.buildThreadView = function(tid, thr) {
		thr.initialized = true;
		var html = '';
		for(var i = 0; i < thr.comments.length; i++) {
			var c = thr.comments[i];
			html += "<div style=\"border: 1px solid #ccccff;\">";
			html += "<div style=\"border-bottom: 1px dashed #ccccff;\">"+(i+1)+". <span class=\"label label-info\">"+htmle(c.owner.authDomain)+" \\ "+htmle(c.owner.firstName)+" "+htmle(c.owner.lastName)+"</span> <span style=\"font-size: 0.7em;\">("+c.posted+")</span></div>"
			html += "<div>"+c.text+"</div>"
			html += "</div>";
		}
		html += '<div><input type="button" class="btn btn-default btn-sm" value="Dodaj komentar" onclick=\'mlCS.addNewCommentGUI('+tid+'); return false;\'></div>';
		$("#mlthrt_"+tid).html(html);
	}
	
	this.headerButtons = function() {
		var html = '';
		html += '<div class="row" style="font-size: 0.8em;"><div class="col-lg-12"><input type="button" class="form-control" value="Dodaj novu poruku" onclick="mlCS.addNewThreadGUI(); return false;"></div></div>';
		return html;
	}
	
	this.newThreadForm = function() {
		var html = '<div id="mlcmntdialogNT" title="Dodavanje nove poruke">';
		html += '<div style="background-color: #fff0f0; padding: 5px; border: 1px solid black;">';
		html += '<div class="row" style="font-size: 0.8em;"><div class="col-lg-12"><label for="mlcmntnewthreadtitle">Dodavanje nove poruke</label><div class="input-group">';
		html += '<span class="input-group-addon"><input type="checkbox" title="Je li poruka javna?" id="mlcmntnewthreadpublic"></span><input type="text" class="form-control" type="text" id="mlcmntnewthreadtitle" placeholder="Naslov...">';
		html += '<div class="input-group"></div></div>';
		html += '<div class="row" style="font-size: 0.8em;"><div class="col-lg-12"><label for="mlcmntnewthreadtext">Poruka</label><textarea rows="5" class="form-control" id="mlcmntnewthreadtext"></textarea></div></div>';
		html += '<div class="row" style="font-size: 0.8em;"><div class="col-lg-12"><input type="button" class="form-control" value="Dodaj" onclick="mlCS.saveNewThread(); return false;"></div></div>';
		html += '</div>'
		html += '</div>';
		return html;
	}

	this.saveNewThread = function() {
		var title = document.getElementById('mlcmntnewthreadtitle').value.trim();
		var text = document.getElementById('mlcmntnewthreadtext').value.trim();
		var ispublic = $('#mlcmntnewthreadpublic').is(":checked");
		var me = this;
		$.ajax({
		  method: 'POST',
		  async: false,
		  url: ml_base_url.replace("REPLACEME", ml_base_mlid+"/threads/"+ml_base_itemid),
		  data: {
			  title: title,
			  text: text,
			  convIsPublic: ispublic
		  },
		  cache: false,
		  success: function(data) {
				if(data.error==false) {
					for(var i = 0; i < data.threads.length; i++) {
						me.threads.push(data.threads[i]);
					}
					alert("Poruka je dodana.");
					$('#mlcmntdialogNT').dialog("option", "hide", false);
					$('#mlcmntdialog').dialog("option", "hide", false);
					$('#mlcmntdialogNT').dialog("close");
					$('#mlcmntdialog').dialog("close");
					me.buildView();
					me.showView();
				} else {
					alert("Pogreška!");
				}
				return;
		  },
		  error: function(x,e,t) {
				alert("Pogreška! ("+e+") ("+t+")");
		  }
		});

	}
	
	this.addNewThreadGUI = function() {
		var html = this.newThreadForm();
		$(html).appendTo('body');
		$('#mlcmntdialogNT').dialog({
			show: {effect: "blind", duration: 500},
			hide: {effect: "explode", duration: 1000},
			width: 600,
			height: 600,
			modal: true,
			close: function(event, ui) {
				$('#mlcmntdialogNT').remove();
			},
			buttons: {
				"Zatvori": function() {
					$( this ).dialog( "close" );
				}
			}
		});
	}

	this.saveNewComment = function(tid) {
		var text = document.getElementById('mlcmntnewthreadtext').value.trim();
		var thr = this.getThread(tid);
		var me = this;
		$.ajax({
		  method: 'POST',
		  async: false,
		  url: ml_base_url.replace("REPLACEME", ml_base_mlid+"/threads/"+ml_base_itemid+"/"+tid),
		  data: {
			  text: text
		  },
		  cache: false,
		  success: function(data) {
				if(data.error==false) {
					for(var i = 0; i < data.comments.length; i++) {
						thr.comments.push(data.comments[i]);
					}
					thr.initialized = false;
					alert("Komentar je dodan.");
					$('#mlcmntdialogNC').dialog("option", "hide", false);
					$('#mlcmntdialog').dialog("option", "hide", false);
					$('#mlcmntdialogNC').dialog("close");
					$('#mlcmntdialog').dialog("close");
					me.buildView();
					me.showView();
				} else {
					alert("Pogreška!");
				}
				return;
		  },
		  error: function(x,e,t) {
				alert("Pogreška! ("+e+") ("+t+")");
		  }
		});
	}
	
	this.newCommentForm = function(tid) {
		var html = '<div id="mlcmntdialogNC" title="Dodavanje novog komentara">';
		html += '<div style="background-color: #fff0f0; padding: 5px; border: 1px solid black;">';
		html += '<div class="row" style="font-size: 0.8em;"><div class="col-lg-12"><label for="mlcmntnewthreadtext">Komentar</label><textarea rows="5" class="form-control" id="mlcmntnewthreadtext"></textarea></div></div>';
		html += '<div class="row" style="font-size: 0.8em;"><div class="col-lg-12"><input type="button" class="form-control" value="Dodaj" onclick="mlCS.saveNewComment('+tid+'); return false;"></div></div>';
		html += '</div>'
		html += '</div>';
		return html;
	}

	this.addNewCommentGUI = function(tid) {
		var html = this.newCommentForm(tid);
		$(html).appendTo('body');
		$('#mlcmntdialogNC').dialog({
			show: {effect: "blind", duration: 500},
			hide: {effect: "explode", duration: 1000},
			width: 600,
			height: 400,
			modal: true,
			close: function(event, ui) {
				$('#mlcmntdialogNC').remove();
			},
			buttons: {
				"Zatvori": function() {
					$( this ).dialog( "close" );
				}
			}
		});
	}
	
	this.showView = function() {
		var html = this.html;
		$(html).appendTo('body');
		$('#mlcmntdialog').dialog({
			show: {effect: "blind", duration: 500},
			hide: {effect: "explode", duration: 1000},
			width: 600,
			height: 600,
			modal: true,
			close: function(event, ui) {
				$('#mlcmntdialog').remove();
			},
			buttons: {
				"Zatvori": function() {
					$( this ).dialog( "close" );
				}
			}
		});
	}

}

var mlCS = new mlCommentsSubsystem();

function mlOpenComments() {
	mlCS.show();
}
