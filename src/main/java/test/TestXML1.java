package test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.xml.coursedef.CourseDefFactory;

public class TestXML1 {

	public static void main(String[] args) throws IOException {
		Course c = CourseDefFactory.fromReader(Files.newBufferedReader(Paths.get("./xmlimport/coursedefs/digitalLogic.xml")));
		System.out.println("Course names:");
		c.getTitles().forEach((k,v)->System.out.println(k+" => "+v));
		System.out.println("Course descriptions:");
		c.getDescriptions().forEach((k,v)->System.out.println(k+" => "+v));
	}
	
}
