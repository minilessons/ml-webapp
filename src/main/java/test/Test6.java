package test;

public class Test6 {

	public static void main(String[] args) {
		String regex = "^[a-zA-Z]{2,8}(_([a-zA-Z]{2}|[0-9]{3})(_(([0-9][0-9a-zA-Z]{3})|[0-9a-zA-Z]{5,8})([-_]([0-9][0-9a-zA-Z]{3}|[0-9a-zA-Z]{5,8}))*)?)?$";
		
		String sample = "en_123_polyton";
		
		if(sample.matches(regex)) {
			System.out.println("DA");
		} else {
			System.out.println("NE");
		}
	}
	
}
