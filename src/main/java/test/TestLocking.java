package test;

import hr.fer.zemris.minilessons.locking.LockingTree;

public class TestLocking {

	public static void main(String[] args) {
		final LockingTree ltree = new LockingTree();
		
		new Thread(()->{
			ltree.lock(new String[] {"dir1"});
			System.out.println(Thread.currentThread().getName()+": radim...");
			try { Thread.sleep(10000); } catch(Exception ex) {}
			System.out.println(Thread.currentThread().getName()+": gotov...");
			ltree.unlock(new String[] {"dir1"});
		}, "Radnik1").start(); 
		
		new Thread(()->{
			ltree.lock(new String[] {"dir1"});
			System.out.println(Thread.currentThread().getName()+": radim...");
			try { Thread.sleep(10000); } catch(Exception ex) {}
			System.out.println(Thread.currentThread().getName()+": gotov...");
			ltree.unlock(new String[] {"dir1"});
		}, "Radnik2").start(); 
		
		new Thread(()->{
			ltree.lock(new String[] {"dir2", "a"});
			System.out.println(Thread.currentThread().getName()+": radim...");
			try { Thread.sleep(10000); } catch(Exception ex) {}
			System.out.println(Thread.currentThread().getName()+": gotov...");
			ltree.unlock(new String[] {"dir2", "a"});
		}, "Radnik3").start(); 
		
		new Thread(()->{
			ltree.lock(new String[] {"dir2", "a"});
			System.out.println(Thread.currentThread().getName()+": radim...");
			try { Thread.sleep(10000); } catch(Exception ex) {}
			System.out.println(Thread.currentThread().getName()+": gotov...");
			ltree.unlock(new String[] {"dir2", "a"});
		}, "Radnik4").start(); 

		new Thread(()->{
			ltree.lock(new String[] {"dir2", "b"});
			System.out.println(Thread.currentThread().getName()+": radim...");
			try { Thread.sleep(10000); } catch(Exception ex) {}
			System.out.println(Thread.currentThread().getName()+": gotov...");
			ltree.unlock(new String[] {"dir2", "b"});
		}, "Radnik5").start(); 
	}
	
}
