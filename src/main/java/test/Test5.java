package test;

public class Test5 {

	public static void main(String[] args) {
		String regex = "^[a-zA-Z_][a-zA-Z0-9_+[-]]*(\\.[a-zA-Z_][a-zA-Z0-9_+[-]]*)+$";
		
		String sample = "marcupic.diglog.lect.de-02a";
		
		if(sample.matches(regex)) {
			System.out.println("DA");
		} else {
			System.out.println("NE");
		}
	}
	
}
