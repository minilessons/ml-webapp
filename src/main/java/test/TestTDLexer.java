package test;

import hr.fer.zemris.minilessons.testspec.lexer.Lexer;
import hr.fer.zemris.minilessons.testspec.parser.TestSpecParser;

public class TestTDLexer {

	public static void main(String[] args) {
		String str =" //: pri prvom zahtjevu za ispitom kreiram odmah ispit i zadatke za sve studente studenta grupe studenta koji je prvi zatražio ispit\n" + 
				" //: potom zastavicama reguliram što je od zadataka vidljivo/rješivo?\n" + 
				"\n" + 
				"question 1 {\n" + 
				"  for group;\n" + 
				"  id \"bla\";              // qid:configname\n" + 
				"  finish_accept: if_correct;   // if-correct, if-not-empty, always\n" + 
				"  score: proportional 0.5;     // koliko bodova nosi zadatak; druga mogućnost: {0.5, -0.2, 0} za T/N/B\n" + 
				"  score_condition: #2:finished;  // #broj_zadatka:uvjet, uvjet može biti samo finished? \n" + 
				"  message_if_unsolved: @q1u;\n" + 
				"  message_if_correctness_between: [0,0.5) @q1m1;\n" + 
				"  message_if_correctness_between: [0.5,1) @q1m2;\n" + 
				"}\n" + 
				"\n" + 
				"question 2 {\n" + 
				"  enabled_if: #1:finished;\n" + 
				"  for group user;\n" + 
				"  id \"bla\";              // qid:configname\n" + 
				"  finish_accept: if_correct;   // if-correct, if-not-empty, always\n" + 
				"  score: proportional 1.5;     // koliko bodova nosi zadatak; druga mogućnost: {0.5, -0.2, 0} za T/N/B\n" + 
				"  message_if_unsolved: @q2u;\n" + 
				"  message_if_correctness_between: [0,0.5) @q2m1;\n" + 
				"  message_if_correctness_between: [0.5,1) @q2m2;\n" + 
				"}\n" + 
				"\n" + 
				"question 3 {\n" + 
				"  enabled_if: #2:finished:all;   // #broj_zadatka:uvjet, uvjet finished:tko, tko iz {all,any}\n" + 
				"  for group;\n" + 
				"  id \"bla\";              // qid:configname\n" + 
				"  finish_accept: if_correct;   // if-correct, if-not-empty, always\n" + 
				"  score: proportional 0.5;     // koliko bodova nosi zadatak; druga mogućnost: {0.5, -0.2, 0} za T/N/B\n" + 
				"  score_condition: #4:finished;  // #broj_zadatka:uvjet, uvjet može biti samo finished? \n" + 
				"  message_if_unsolved: @q3u;\n" + 
				"  message_if_correctness_between: [0,0.5) @q3m1;\n" + 
				"  message_if_correctness_between: [0.5,1) @q3m2;\n" + 
				"}\n" + 
				"\n" + 
				"question 4 {\n" + 
				"  enabled_if: #3:finished;\n" + 
				"  for group user;\n" + 
				"  id \"bla\";              // qid:configname\n" + 
				"  finish_accept: if_correct;   // if-correct, if-not-empty, always\n" + 
				"  score: proportional 1.5;     // koliko bodova nosi zadatak; druga mogućnost: {0.5, -0.2, 0} za T/N/B\n" + 
				"  message_if_unsolved: @q4u;\n" + 
				"  message_if_correctness_between: [0,0.5) @q4m1;\n" + 
				"  message_if_correctness_between: [0.5,1) @q4m2;\n" + 
				"}\n" + 
				"\n" + 
				"translations {\n" + 
				" @q1u {\n" + 
				"   hr \"\"\"ovo\nje tekst\r\nkroz tri retka\"\"\"\n" + 
				"   en \"...\"\n" + 
				" }\n" + 
				" @q1m1 {\n" + 
				"   hr \"...\"\n" + 
				"   en \"...\"\n" + 
				" }\n" + 
				" @q1m2 {\n" + 
				"   hr \"...\"\n" + 
				"   en \"...\"\n" + 
				" }\n" + 
				"}\n" + 
				"\n" + 
				" //: za ispis poruka, u naredbama message_if_*, argument može biti lokalizacijski ključ koji onda mora pogađati \n" + 
				" //: vanjski blok translation, ili može izravno biti \n" + 
				" //: {\n" + 
				" //:   hr \"\"\"ovo je tekst\"\"\"\n" + 
				" //:   en \"tekst\"\n" + 
				" //: }\n" + 
				" //: gdje unutar teksta možemo koristiti \\\" za escapeanje navodnika. Ako umjesto \" niz započne s \"\"\", onda traje\n" + 
				" //: doslovno dalje potencijalno i kroz više redaka, sve do završnog \"\"\", i unutra nema nikakvih escape sekvenci\n" + 
				" //: (tretiramo kao \"raw\" string).\n" + 
				"\n" + 
				" //: ispit dodatno ima otvoreno od, otvoreno do, otvoreno vremena\n" + 
				" //: pri čemu \"otvoreno vremena\" omogućava da se zada primjerice da je ispit otvoren\n" + 
				" //: najduže u trajanju od 1h od trenutka otvaranja, neovisno o \"otvoreno do\" tako dugo dok\n" + 
				" //: je tako određeni kraj unutar prozora \"otvoreno od, otvoreno do\"; inače \"otvoreno do\"\n" + 
				" //: krati taj interval.\n" + 
				"\n" + 
				" //: for naredba može imati argument \"group\", \"group user\" te \"user\" pri čemu ako se koriste prva dva, tada\n" + 
				" //: ispit mora biti definiran u kontekstu grupe i tada \"group user\" i \"user\" znače isto; ako to nije, onda\n" + 
				" //: se ne smije koristiti \"group\" i \"group user\" već samo \"user\" i to je default ako nema ove naredbe.\n" + 
				"";
		
		Lexer lexer = new Lexer(str);
		while(!lexer.nextToken().isEOF()) {
			System.out.println(lexer.getToken());
		}
		
		TestSpecParser parser = new TestSpecParser(str);
	}
}
