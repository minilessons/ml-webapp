package test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.minilessons.QDescriptorParserUtil;
import hr.fer.zemris.minilessons.service.QDeployer;
import hr.fer.zemris.minilessons.xmlmodel.XMLQuestionDescriptor;

public class TestJSQuestionConfig {

	public static void main(String[] args) throws IOException {
		Path path = Paths.get("questions/unos-teksta/descriptor.xml");
		XMLQuestionDescriptor xmlq = QDescriptorParserUtil.parseInputStream(Files.newInputStream(path));
		QDeployer.checkQuestionDescriptor(path.getParent(), xmlq);
		
	}
}
