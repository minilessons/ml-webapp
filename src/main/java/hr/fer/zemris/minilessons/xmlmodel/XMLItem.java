package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLItem {
	private String id;
	private String branchID;
	private List<XMLString> title;
	private List<String> offerBranches;
	
	public String getId() {
		return id;
	}
	
	public List<XMLString> getTitle() {
		return title;
	}
	
	public String getBranchID() {
		return branchID;
	}
	
	public List<String> getOfferBranches() {
		return offerBranches;
	}
}
