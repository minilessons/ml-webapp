package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLMinilesson {

	private String uid;
	private int version = 1;
	private int minorVersion;
	private XMLAbout about;
	private List<XMLOption> optionKeys;
	private List<XMLItem> items;
	private List<XMLBranch> branches;
	
	public XMLAbout getAbout() {
		return about;
	}
	
	public String getUid() {
		return uid;
	}
	
	public int getVersion() {
		return version;
	}
	
	public List<XMLOption> getOptionKeys() {
		return optionKeys;
	}
	
	public List<XMLItem> getItems() {
		return items;
	}
	
	public int getMinorVersion() {
		return minorVersion;
	}
	
	public List<XMLBranch> getBranches() {
		return branches;
	}
}
