package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLBranch {
	private String id;
	private List<XMLString> title;
	
	public String getId() {
		return id;
	}
	
	public List<XMLString> getTitle() {
		return title;
	}
}
