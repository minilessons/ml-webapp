package hr.fer.zemris.minilessons.xmlmodel;

/**
 * Exception which is thrown if there is some unrecoverable error with data in <code>descriptor.xml</code>
 * (e.g. if item IDs are not unique).
 *  
 * @author marcupic
 */
public class MLDescriptorException extends RuntimeException {

	/**
	 * SerialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	public MLDescriptorException() {
	}

	public MLDescriptorException(String message) {
		super(message);
	}

	public MLDescriptorException(Throwable cause) {
		super(cause);
	}

	public MLDescriptorException(String message, Throwable cause) {
		super(message, cause);
	}

	public MLDescriptorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
