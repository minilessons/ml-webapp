package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLPage extends XMLItem {

	private List<XMLFile> files;
	
	public List<XMLFile> getFiles() {
		return files;
	}
}
