package hr.fer.zemris.minilessons.xmlmodel;

import java.util.List;

public class XMLOption {
	
	private String key;
	private List<XMLString> translations;
	private boolean inherit;
	
	public XMLOption() {
	}

	public String getKey() {
		return key;
	}
	
	public List<XMLString> getTranslations() {
		return translations;
	}
	
	public boolean getInherit() {
		return inherit;
	}
	
	public void setTranslations(List<XMLString> translations) {
		this.translations = translations;
	}
}
