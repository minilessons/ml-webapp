package hr.fer.zemris.minilessons;

/**
 * Constants which determine names of standard course collections.
 * 
 * @author marcupic
 */
public enum CourseCollections {
	DEFAULT("DEFAULT");
	
	private String key;

	private CourseCollections(String key) {
		this.key = key;
	}
	
	public String getKey() {
		return key;
	}
}
