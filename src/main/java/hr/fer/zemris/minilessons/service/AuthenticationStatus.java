package hr.fer.zemris.minilessons.service;

/**
 * Status of authentication.
 * 
 * @author marcupic
 */
public enum AuthenticationStatus {
	/**
	 * If username and/or password is invalid.
	 */
	INVALID,
	/**
	 * If username and password matches.
	 */
	SUCCESSFULL,
	/**
	 * If there was error with authentication service itself.
	 */
	ERROR
}
