package hr.fer.zemris.minilessons.service;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;
import hr.fer.zemris.minilessons.dao.model.User;

public class AuthUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Primary key.
	 */
	private Long id;
	/**
	 * First name.
	 */
	private String firstName;
	/**
	 * Last name.
	 */
	private String lastName;
	/**
	 * Username.
	 */
	private String username;
	/**
	 * JMBAG.
	 */
	private String jmbag;
	/**
	 * E-mail.
	 */
	private String email;
	/**
	 * Authentication domain: who is responsible for checking username and password.
	 */
	private String authDomain;
	/**
	 * Upload policy for this user.
	 */
	private UploadPolicy uploadPolicy;
	/**
	 * Permissions. Values are {@link MLBasicPermission#getPermissonName()}.
	 */
	private Set<String> permissions = new HashSet<>();
	/**
	 * When was this user created.
	 */
	private Date createdOn;
	
	public AuthUser() {
	}


	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getUsername() {
		return username;
	}

	public String getJmbag() {
		return jmbag;
	}

	public String getEmail() {
		return email;
	}

	public String getAuthDomain() {
		return authDomain;
	}

	public UploadPolicy getUploadPolicy() {
		return uploadPolicy;
	}

	public Set<String> getPermissions() {
		return permissions;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public static AuthUser fillFromUser(User user) {
		AuthUser au = new AuthUser();
		
		au.id = user.getId();
		au.jmbag = user.getJmbag();
		au.username = user.getUsername();
		au.firstName = user.getFirstName();
		au.lastName = user.getLastName();
		au.email = user.getEmail();
		au.authDomain = user.getAuthDomain();
		au.createdOn = user.getCreatedOn();
		au.uploadPolicy = user.getUploadPolicy();
		au.permissions = Collections.unmodifiableSet(new HashSet<>(user.getPermissions()));
		
		return au;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AuthUser))
			return false;
		AuthUser other = (AuthUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
}
