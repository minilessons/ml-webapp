package hr.fer.zemris.minilessons.service;

/**
 * This class encapsulates information about authentication attempt.
 * For authentication service which offer automatic user data retrieval
 * used for local user database update, {@link #getUserInfo()} will
 * provide a reference to user data (if authentication was successful).
 * 
 * @author marcupic
 */
public class AuthenticationResult {

	private AuthenticationStatus status;
	private AuthUserInfo userInfo;
	
	public AuthenticationResult() {
	}

	public AuthenticationResult(AuthenticationStatus status, AuthUserInfo userInfo) {
		super();
		this.status = status;
		this.userInfo = userInfo;
	}

	/**
	 * Getter for authentication status.
	 * 
	 * @return authentication status; will not be <code>null</code>
	 */
	public AuthenticationStatus getStatus() {
		return status;
	}

	/**
	 * Getter for user information retrieved through authentication service.
	 * 
	 * @return user info or <code>null</code> if authentication was unsuccessful
	 */
	public AuthUserInfo getUserInfo() {
		return userInfo;
	}
}
