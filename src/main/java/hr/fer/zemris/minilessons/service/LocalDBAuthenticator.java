package hr.fer.zemris.minilessons.service;

import java.util.Map;

import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.util.PasswordUtil;

/**
 * Implementation of authentication service provider which compares user provided
 * password with the password stored in {@link User} object (i.e. hashes).
 * 
 * @author marcupic
 *
 */
public class LocalDBAuthenticator implements AuthenticationServiceProvider {

	/**
	 * Constructor.
	 * 
	 * @param config initialization parameters
	 */
	public LocalDBAuthenticator(Map<String,String> config) {
		if("true".equals(config.get("autocreate.enabled"))) {
			System.out.println("Warning: "+this.getClass().getName()+" does not support 'autocreate' option but was created with 'autocreate.enabled'=true; this will be ignored.");
		}
	}

	@Override
	public String getName() {
		return "Local user database authentication service";
	}
	
	@Override
	public AuthenticationResult authenticate(User user, String username, String password, boolean retrieveUserData) {
		// We do not support user data retrieval since we operate on local user database...
		if(user==null) return new AuthenticationResult(AuthenticationStatus.INVALID,null);
		
		// If we have user, check credentials:
		String pwdEncoded = PasswordUtil.encodePassword(password);
		return username.equals(user.getUsername()) && pwdEncoded.equals(user.getPasswordHash()) ? new AuthenticationResult(AuthenticationStatus.SUCCESSFULL,null) : new AuthenticationResult(AuthenticationStatus.INVALID,null);
	}
}
