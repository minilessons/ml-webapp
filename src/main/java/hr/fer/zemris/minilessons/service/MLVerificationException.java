package hr.fer.zemris.minilessons.service;

/**
 * Exception which is thrown when there is problem with content of <code>descriptor.xml</code>.
 * 
 * @author marcupic
 */
public class MLVerificationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MLVerificationException() {
	}

	public MLVerificationException(String message) {
		super(message);
	}

	public MLVerificationException(Throwable cause) {
		super(cause);
	}

	public MLVerificationException(String message, Throwable cause) {
		super(message, cause);
	}

	public MLVerificationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
