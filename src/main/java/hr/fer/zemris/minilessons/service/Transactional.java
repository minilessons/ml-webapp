/**
 * 
 */
package hr.fer.zemris.minilessons.service;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
/**
 * Označava da metodu treba pozvati pod transakcijom.
 * 
 * @author marcupic
 *
 */
public @interface Transactional {

}
