package hr.fer.zemris.minilessons.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import hr.fer.zemris.minilessons.DescriptorParserUtil;
import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOException;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.IntValueStat;
import hr.fer.zemris.minilessons.dao.model.LocalizedString;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonApproval;
import hr.fer.zemris.minilessons.dao.model.MinilessonStats;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.util.UUIDUtil;
import hr.fer.zemris.minilessons.xmlmodel.XMLAuthor;
import hr.fer.zemris.minilessons.xmlmodel.XMLBranch;
import hr.fer.zemris.minilessons.xmlmodel.XMLFile;
import hr.fer.zemris.minilessons.xmlmodel.XMLItem;
import hr.fer.zemris.minilessons.xmlmodel.XMLLanguage;
import hr.fer.zemris.minilessons.xmlmodel.XMLMinilesson;
import hr.fer.zemris.minilessons.xmlmodel.XMLOffer;
import hr.fer.zemris.minilessons.xmlmodel.XMLOption;
import hr.fer.zemris.minilessons.xmlmodel.XMLPage;
import hr.fer.zemris.minilessons.xmlmodel.XMLQuestion;
import hr.fer.zemris.minilessons.xmlmodel.XMLString;
import hr.fer.zemris.minilessons.xmlmodel.XMLView;

public class MLDeployer {

	/**
	 * @param rootPath
	 * @param owner
	 * @param visible
	 * @param restricted
	 * @throws IOException
	 */
	public static void deploy(Path rootPath, User owner, boolean visible, boolean restricted) throws IOException {
		// Smije li korisnik uploadati minilekciju?
		if(owner.getUploadPolicy()==UploadPolicy.DENIED) {
			throw new RuntimeException("You can not upload minilessons: permission denied.");
		}
		// Smije li uploadati neograničenu minilekciju?
		if(owner.getUploadPolicy()==UploadPolicy.RESTRICTED && !restricted) {
			throw new RuntimeException("You can upload only restricted minilessons! Permission denied.");
		}

		// Dohvati deskriptor:
		Path descriptorPath = rootPath.resolve("descriptor.xml");
		XMLMinilesson xmlml = DescriptorParserUtil.parseInputStream(Files.newInputStream(descriptorPath));
		checkMinilessonDescriptor(rootPath, xmlml);

		// Ako korisnik samo smije uploadati ograničene minilekcije, ili ako je eksplicitno rekao da je ista ograničena: provjeri!
		if(owner.getUploadPolicy()==UploadPolicy.RESTRICTED || restricted) {
			checkMinilessonIsRestricted(rootPath, xmlml);
		}
		
		List<Minilesson> allVersions = DAOProvider.getDao().listAllMinilessonVersions(trim(xmlml.getUid()));
		
		boolean newMajorVersion = false;
		boolean existing = false;
		
		Minilesson useml = null;
		Minilesson lastVersion = null;
		
		// If this is not new minilesson:
		if(!allVersions.isEmpty()) {
			Set<User> owners = allVersions.stream().map(m->m.getOwner()).collect(Collectors.toSet());
			if(!owners.contains(owner)) {
				throw new MLVerificationException("Other user is already owner of minilesson with this public ID. You can not upload minilesson with the same public ID.");
			}
			lastVersion = allVersions.stream().min(Minilesson.CMP_BY_VERSION).get();
			if(lastVersion.getVersion() > xmlml.getVersion()) {
				throw new MLVerificationException("Minilesson which exists on the system is newer (has bigger version) than the one you are uploading. Uploading older versions is not allowed.");
			}
			if(lastVersion.getVersion() == xmlml.getVersion() && lastVersion.getMinorVersion() >= xmlml.getMinorVersion()) {
				throw new MLVerificationException("Minilesson which exists on the system is newer (has bigger version) than the one you are uploading. Uploading older versions is not allowed.");
			}
			newMajorVersion = lastVersion.getVersion() < xmlml.getVersion();

			if(!newMajorVersion) {
				checkForCompatibility(xmlml, DescriptorParserUtil.parseInputStream(Files.newInputStream(DAOProvider.getDao().getRealPathFor(lastVersion.getId(), "descriptor.xml"))));
			}
			
			if(!newMajorVersion) useml = lastVersion;
			existing = true;
		}
		
		final Minilesson ml = useml != null ? useml : new Minilesson();
		if(useml==null) {
			ml.setId(UUIDUtil.nextUUID());
			ml.setPublicID(trim(xmlml.getUid()));
		}
		ml.setVersion(xmlml.getVersion());
		ml.setMinorVersion(xmlml.getMinorVersion());
		ml.setLanguages(new HashSet<>());
		ml.setTitles(new HashMap<>());
		ml.setOwner(owner);
		
		if(owner.getUploadPolicy()==UploadPolicy.RESTRICTED) {
			ml.setApproval(MinilessonApproval.SOFT_WAITING);
			ml.setApprovedBy(null);
			ml.setApprovedOn(null);
		} else if(owner.getUploadPolicy()==UploadPolicy.FOR_APPROVAL) {
			if(restricted) {
				ml.setApproval(MinilessonApproval.SOFT_WAITING);
				ml.setApprovedBy(null);
				ml.setApprovedOn(null);
			} else {
				ml.setApproval(MinilessonApproval.WAITING);
				ml.setApprovedBy(null);
				ml.setApprovedOn(null);
			}
		} else if(owner.getUploadPolicy()==UploadPolicy.ALLOWED) {
			ml.setApproval(MinilessonApproval.APPROVED);
			ml.setApprovedBy(owner);
			ml.setApprovedOn(new Date());
		} else {
			throw new RuntimeException("Security problem: uncovered case detected.");
		}

		ml.setUploadedOn(new Date());
		ml.setVisibility(visible);
		
		if(xmlml.getAbout()!=null && xmlml.getAbout().getTitle()!=null) {
			ml.getTitles().clear();
			xmlml.getAbout().getTitle().forEach(x->{
				ml.getTitles().put(trim(x.getLang()), trim(x.getText()));
			});
		}
		if(xmlml.getAbout()!=null && xmlml.getAbout().getSupportedLanguages()!=null) {
			ml.getLanguages().clear();
			xmlml.getAbout().getSupportedLanguages().forEach(x->{
				ml.getLanguages().add(trim(x.getLang()));
			});
		}
		if(xmlml.getAbout()!=null && xmlml.getAbout().getTags() != null) {
			ml.getTags().clear();
			xmlml.getAbout().getTags().forEach(t->{ml.getTags().add(new LocalizedString(trim(t.getLang()), trim(t.getText())));});
		}

		// Ako je ovo upload nove glavne verzije (postojeće minilekcije), staru verziju arhiviraj!
		if(existing && newMajorVersion) {
			lastVersion.setArchived(true);
		}
		
		DAO dao = DAOProvider.getDao();
		
		dao.saveMinilesson(ml);

		if(useml==null) {
			MinilessonStats mlstats = new MinilessonStats();
			mlstats.setMinilesson(ml);
			mlstats.setQualityStatistics(new IntValueStat());
			mlstats.setDifficultyStatistics(new IntValueStat());
			mlstats.setDurationStatistics(new IntValueStat());
			dao.saveMinilessonStats(mlstats);
		}
		
		try {
			if(useml==null) {
				DAOProvider.getDao().getMLStorage().persistMinilessonPackage(ml.getId(), rootPath);
			} else {
				DAOProvider.getDao().getMLStorage().updateMinilessonPackage(ml.getId(), rootPath);
			}
		} catch(IOException ex) {
			throw new DAOException("Exception while copying minilesson package.");
		}
	}

	public static void checkMinilessonIsRestricted(Path rootPath, XMLMinilesson xmlml) {
		for(XMLItem item : xmlml.getItems()) {
			if(!(item instanceof XMLQuestion)) {
				continue;
			}
			XMLQuestion q = (XMLQuestion)item;
			if("static".equals(q.getType())) {
				continue;
			}
			throw new MLVerificationException("Only questions with type=static are supported by restricted minilessons.");
		}
	}

	public static void checkForCompatibility(XMLMinilesson xmlmlNew, XMLMinilesson xmlmlOld) {
		if(xmlmlNew.getItems().size() != xmlmlOld.getItems().size()) {
			throw new MLVerificationException("Versions do not have the same number of items. This is incompatible change for minor version.");
		}
		
		Set<XMLLanguage> newLangSet = new HashSet<>(xmlmlNew.getAbout().getSupportedLanguages());
		Set<XMLLanguage> oldLangSet = new HashSet<>(xmlmlOld.getAbout().getSupportedLanguages());
		
		Set<XMLLanguage> missing = new HashSet<>(oldLangSet);
		missing.removeAll(newLangSet);
		if(!missing.isEmpty()) {
			throw new MLVerificationException("New version is missing languages which were supported by older version ("+missing+"). This is incompatible change for minor version.");
		}
		
		for(int i = 0, n = xmlmlNew.getItems().size(); i<n; i++) {
			XMLItem oldItem = xmlmlOld.getItems().get(i);
			XMLItem newItem = xmlmlNew.getItems().get(i);
			if(!oldItem.getClass().equals(newItem.getClass())) {
				throw new MLVerificationException("Item number "+(i+1)+" is of different type in new and old versions. This is incompatible change for minor version.");
			}
			if(!oldItem.getId().equals(newItem.getId())) {
				throw new MLVerificationException("Item number "+(i+1)+" has different ID in new and old versions. This is incompatible change for minor version.");
			}
			if(!looslyEquals(oldItem.getBranchID(),newItem.getBranchID())) {
				throw new MLVerificationException("Item number "+(i+1)+" belongs to different branch in new and old versions. This is incompatible change for minor version.");
			}
		}
	}

	private static boolean looslyEquals(String s1, String s2) {
		if(s1==null) {
			if(s2==null) return true;
			return false;
		} else if(s2 == null) {
			return false;
		}
		return s1.equals(s2);
	}

	private static class RequiredFiles {
		private Set<String> pages = new HashSet<>();
		private Set<String> jscustom = new HashSet<>();
		
		void addPageFile(String name) {
			pages.add(name);
		}
		
		void addJSCustomFile(String name) {
			jscustom.add(name);
		}
	}
	
	public static void checkMinilessonDescriptor(Path rootPath, XMLMinilesson xmlml) {
		checkPublicID(xmlml.getUid());
		checkNotNull("about", xmlml.getAbout());
		
		Set<String> declaredLanguages = new HashSet<>();
		
		// Verify about.supportedLanguages section:
		// ----------------------------------------
		checkNotEmpty("about.supportedLanguages", xmlml.getAbout().getSupportedLanguages());
		Set<XMLLanguage> langSet = new HashSet<>();
		for(XMLLanguage l : xmlml.getAbout().getSupportedLanguages()) {
			checkNotEmpty("about.supportedLanguages.language.lang", l.getLang());
			checkLang("about.supportedLanguages", l.getLang());
			if(!langSet.add(l)) {
				throw new MLVerificationException("about.supportedLanguages contains duplicate languages.");
			}
			declaredLanguages.add(l.getLang());
		}
		
		// Verify about.title section:
		// --------------------------------
		checkNotEmpty("title", xmlml.getAbout().getTitle());
		Set<String> titleLangSet = new HashSet<>();
		for(XMLString t : xmlml.getAbout().getTitle()) {
			checkNotEmpty("about.title.string.lang", t.getLang());
			checkNotEmpty("about.title.string.text", t.getText());
			checkLang("about.title", t.getLang());
			if(!titleLangSet.add(t.getLang())) {
				throw new MLVerificationException("about.title.string found multiple titles for same language.");
			}
		}
		verifyLanguages("about.title", xmlml.getAbout().getTitle(), declaredLanguages, XMLString::getLang);
		
		// Verify about.authors section:
		// --------------------------------
		checkNotEmpty("about.authors", xmlml.getAbout().getAuthors());
		Set<XMLAuthor> authorSet = new HashSet<>();
		for(XMLAuthor a : xmlml.getAbout().getAuthors()) {
			checkNotEmpty("about.authors.author.firstName", a.getFirstName());
			checkNotEmpty("about.authors.author.lastName", a.getLastName());
			checkNotEmpty("about.authors.author.email", a.getEmail());
			if(!authorSet.add(a)) {
				throw new MLVerificationException("about.authors contains duplicate entries.");
			}
		}

		// Verify about.tags section:
		// --------------------------------
		checkNotEmpty("about.tags", xmlml.getAbout().getTags());
		Set<String> tagsLangSet = new HashSet<>();
		for(XMLString t : xmlml.getAbout().getTags()) {
			checkNotEmpty("about.tags.string.lang", t.getLang());
			checkNotEmpty("about.tags.string.text", t.getText());
			checkLang("about.tags", t.getLang());
			if(!tagsLangSet.add(t.getLang()+"||"+t.getText())) {
				throw new MLVerificationException("about.tags found duplicate tags for the same language ("+t.getLang()+").");
			}
		}

		// Verify global optionKey section:
		// --------------------------------
		Map<String, Map<String,XMLString>> globalOptionMap = new HashMap<>();
		if(xmlml.getOptionKeys()!=null) {
			checkOptionKeys("optionKeys", xmlml.getOptionKeys(), declaredLanguages, globalOptionMap, null);
		}
		
		Set<String> declaredBranches = new HashSet<>();
		
		// Verify global branches section:
		// --------------------------------
		if(xmlml.getBranches()!=null) {
			for(XMLBranch b : xmlml.getBranches()) {
				checkBranchID("branches", b.getId());
				String field = "branches.branch["+b.getId()+"].lang";
				for(XMLString t : b.getTitle()) {
					checkNotEmpty(field, t.getLang());
					checkNotEmpty(field, t.getText());
				}
				verifyLanguages(field, b.getTitle(), declaredLanguages, XMLString::getLang);
				if(!declaredBranches.add(b.getId())) {
					throw new MLVerificationException("Branch with ID '"+b.getId()+"' has multiple declarations.");
				}
			}
		}

		// Verify items section:
		// --------------------------------
		RequiredFiles reqFiles = new RequiredFiles();
		Set<String> itemIDs = new HashSet<>();
		for(XMLItem item : xmlml.getItems()) {
			checkNotEmpty("item", item.getId());
			checkItemID("item", item.getId());
			if(!itemIDs.add(item.getId())) {
				throw new MLVerificationException("Item with ID '"+item.getId()+"' has multiple declarations.");
			}
			String field1 = "item["+item.getId()+"]";
			if(item.getBranchID()!=null) {
				checkBranchID(field1, item.getBranchID());
			}
			String field2 = field1+".title";
			checkNotEmpty(field2, item.getTitle());
			for(XMLString t : item.getTitle()) {
				checkNotEmpty(field2, t.getLang());
				checkLang(field2, t.getLang());
				checkNotEmpty(field2, t.getText());
			}
			verifyLanguages(field2, item.getTitle(), declaredLanguages, XMLString::getLang);
			if(item.getOfferBranches()!=null) {
				for(String s : item.getOfferBranches()) {
					checkBranchID(field1+".offerBranches", s);
				}
			}
			if(item instanceof XMLPage) {
				checkPageFiles("item["+item.getId()+"]", ((XMLPage)item).getFiles(), declaredLanguages, reqFiles);
			} else if(item instanceof XMLQuestion) {
				checkQuestion("item["+item.getId()+"]", (XMLQuestion)item, declaredLanguages, reqFiles, globalOptionMap);
			}
		}

		// Verify all branches were used section:
		// --------------------------------------
		Set<String> usedBranches = new HashSet<>();
		Set<String> offeredBranches = new HashSet<>();
		boolean noBranchItems = false;
		for(XMLItem item : xmlml.getItems()) {
			if(item.getBranchID()!=null) {
				usedBranches.add(item.getBranchID());
			} else {
				noBranchItems = true;
			}
			if(item.getOfferBranches()!=null) {
				for(String s : item.getOfferBranches()) {
					offeredBranches.add(s);
				}
			}
		}
		
		if(!noBranchItems) {
			throw new MLVerificationException("No items were found which do not belong to some branch. Main thread contains zero items which is not allowed.");
		}
		
		Set<String> undeclaredBranches = new HashSet<>(usedBranches);
		undeclaredBranches.removeAll(declaredBranches);
		if(!undeclaredBranches.isEmpty()) {
			throw new MLVerificationException("Some item(s) declared to belong to a branch which does not exists. Here is full list of such branch IDs: "+undeclaredBranches+".");
		}
		Set<String> unofferedBranches = new HashSet<>(declaredBranches);
		unofferedBranches.removeAll(offeredBranches);
		if(!unofferedBranches.isEmpty()) {
			throw new MLVerificationException("There are branches which are not offered to user by any item. Here is full list of such branch IDs: "+unofferedBranches+".");
		}
	}

	private static void checkOptionKeys(String rootField, List<XMLOption> optionKeys, Set<String> declaredLanguages,
			Map<String, Map<String, XMLString>> currentMap, Map<String, Map<String, XMLString>> parentMap) {
		for(XMLOption o : optionKeys) {
			if(o.getInherit() && parentMap==null) throw new MLVerificationException("Global optionKeys section can not use 'inherit' attribute.");
			checkNotEmpty(rootField+".option.key", o.getKey());
			checkValidOptionKey(rootField+".option.key", o.getKey());
			
			if(o.getInherit()) {
				if(!parentMap.containsKey(o.getKey())) {
					throw new MLVerificationException("Key '"+o.getKey()+"' declared translation inheritance from global options, but such option does not exists among global options. Location is "+rootField+".");
				}
				currentMap.put(o.getKey(), parentMap.get(o.getKey()));
				continue;
			}
			
			Map<String,XMLString> map = currentMap.get(o.getKey());
			if(map==null) {
				map = new HashMap<>();
				currentMap.put(o.getKey(), map);
			}
			String field = rootField+".option["+o.getKey()+"]";
			checkNotEmpty(field, o.getTranslations());
			for(XMLString t : o.getTranslations()) {
				checkNotEmpty(field, t.getLang());
				checkLang(field, t.getLang());
				checkNotEmpty(field, t.getText());
				if(map.containsKey(t.getLang())) {
					throw new MLVerificationException("Key '"+o.getKey()+"' has multiple translations for language "+t.getLang()+" in "+field+".");
				}
				map.put(t.getLang(), t);
			}
			verifyLanguages(field, o.getTranslations(), declaredLanguages, XMLString::getLang);
		}
	}

	private static void checkQuestion(String field, XMLQuestion item, Set<String> declaredLanguages, RequiredFiles reqFiles, Map<String, Map<String,XMLString>> globalOptionMap) {
		if(item.getHandler()!=null) {
			checkScriptName(field+".handler", item.getHandler());
			reqFiles.addJSCustomFile(item.getHandler());
		}
		
		if(!"js-custom".equals(item.getType()) && !"static".equals(item.getType())) {
			throw new MLVerificationException("Type '"+item.getType()+"' is unknown for question. Found in "+field+".");
		}
		
		if("js-custom".equals(item.getType()) && item.getHandler() == null) {
			throw new MLVerificationException("Type 'js-custom' question is expected to specify 'handler' attribute! Found in "+field+".");
		}

		if("static".equals(item.getType()) && item.getHandler() != null) {
			throw new MLVerificationException("Type 'static' question does not support 'handler' attribute! Found in "+field+".");
		}
		
		Map<String, Map<String,XMLString>> localOptionMap = new HashMap<>();
		if(item.getOptionKeys()!=null) {
			checkOptionKeys(field+".optionkeys", item.getOptionKeys(), declaredLanguages, localOptionMap, globalOptionMap);
		}
		
		if("static".equals(item.getType()) && (item.getViews() == null || item.getViews().isEmpty())) {
			throw new MLVerificationException("Type 'static' question must have views subsection! Found in "+field+".");
		}
		if("js-custom".equals(item.getType()) && !(item.getViews() == null || item.getViews().isEmpty())) {
			throw new MLVerificationException("Type 'js-custom' question must not have views subsection! Found in "+field+".");
		}
		
		Set<String> foundViewNames = new HashSet<>();
		Set<String> foundPageID = new HashSet<>();
		Set<String> requiredViewNames = new HashSet<>();
		if(item.getViews() != null) {
			boolean anyCompleted = false;
			boolean anyNext = false;
			for(XMLView v : item.getViews()) {
				checkNotEmpty(field+".views.view.renderPageID", v.getRenderPageId());
				checkItemID(field+".views.view.renderPageID", v.getRenderPageId());
				checkNotEmpty(field+".views.view.name", v.getName());
				checkViewName(field+".views.view.name", v.getName());
				if(!foundViewNames.add(v.getName())) {
					throw new MLVerificationException("Duplicate view name found in "+field+". Name was '"+v.getName()+"'.");
				}
				foundPageID.add(v.getRenderPageId());
				checkNotEmpty(field+".views.view['"+v.getName()+"'].offer", v.getOffers());
				for(XMLOffer o : v.getOffers()) {
					checkNotEmpty(field+".views.view['"+v.getName()+"'].offer.key", o.getKey());
					checkValidOptionKey(field+".views.view['"+v.getName()+"'].offer.key", o.getKey());
					checkNotEmpty(field+".views.view['"+v.getName()+"'].offer.action", o.getAction());
					if(o.isCompleted()) anyCompleted = true;
					if(o.getAction().equals("next")) {
						anyNext = true;
					} else if(!o.getAction().startsWith("view:")) {
						throw new MLVerificationException("Unsupported view option action '"+o.getAction()+"'. Found in "+field+".views.");
					} else {
						String viewName = o.getAction().substring(5);
						checkViewName(field+".views.view['"+v.getName()+"'].offer["+o.getKey()+"].action["+o.getAction()+"]", viewName);
						requiredViewNames.add(viewName);
					}
				}
			}
			if(!anyCompleted) {
				throw new MLVerificationException("No view offers option which can complete the question (has attribute completed=\"true\"! Found in "+field+".views.");
			}
			if(!anyNext) {
				throw new MLVerificationException("No view offers option with action 'next'! Found in "+field+".views.");
			}
			if(!foundViewNames.contains("start")) {
				throw new MLVerificationException("View 'start' was not found in "+field+", but is mandatory.");
			}
			if(!foundViewNames.contains("done")) {
				throw new MLVerificationException("View 'done' was not found in "+field+", but is mandatory.");
			}
			Set<String> missingViews = new HashSet<>(requiredViewNames);
			missingViews.removeAll(foundViewNames);
			if(!missingViews.isEmpty()) {
				throw new MLVerificationException("One or more views are used in view-offer-actions but are not declared. Here is a list of such view names: "+missingViews+".");
			}
		}
		Set<String> declaredPages = new HashSet<>();
		if(item.getTemplates()!=null) {
			for(XMLPage p : item.getTemplates()) {
				if(p.getBranchID() != null) {
					throw new MLVerificationException("BranchID must be null (unspecified) in template page "+field+".templates.page["+p.getId()+"].id.");
				}
				checkNotEmpty(field+".templates.page.id", p.getId());
				checkItemID(field+".templates.page.id", p.getId());
				if(!declaredPages.add(p.getId())) {
					throw new MLVerificationException("Duplicate page ID found in "+field+". ID was '"+p.getId()+"'.");
				}
				checkPageFiles(field+".templates.page["+p.getId()+"]", p.getFiles(), declaredLanguages, reqFiles);
			}
		}
		
		Set<String> missingTemplatePages = new HashSet<>(foundPageID);
		missingTemplatePages.removeAll(declaredPages);
		if(!missingTemplatePages.isEmpty()) {
			throw new MLVerificationException("One or more pages are used in views but are not declared in templates. Here is a list of such page IDs: "+missingTemplatePages+".");
		}
	}

	private static void checkPageFiles(String field, Collection<XMLFile> files, Set<String> declaredLanguages, RequiredFiles reqFiles) {
		checkNotEmpty(field+".file", files);
		for(XMLFile f : files) {
			checkNotEmpty(field+".file.format", f.getFormat());
			checkNotEmpty(field+".file.name", f.getName());
			checkNotEmpty(field+".file.lang", f.getLang());
			checkLang(field+".file.lang", f.getLang());
			checkPageFileName(field+".file.name", f.getName());
			if(!"html-snippet".equals(f.getFormat())) {
				throw new MLVerificationException("Unknown file format '"+f.getFormat()+"' in "+field+".");
			}
			reqFiles.addPageFile(f.getName());
		}
		verifyLanguages(field, files, declaredLanguages, XMLFile::getLang);
	}

	public static void checkViewName(String field, String name) {
		if(!PATTERN_VIEW_NAME.matcher(name).matches()) {
			throw new MLVerificationException("In "+field+": found view name which is not of legal syntax. Name was '"+name+"'.");
		}
	}

	public static void checkPageFileName(String field, String name) {
		if(!PATTERN_FILE_NAME.matcher(name).matches()) {
			throw new MLVerificationException("In "+field+": found file name which is not of legal syntax. Name was '"+name+"'.");
		}
	}

	public static void checkScriptName(String field, String name) {
		if(!PATTERN_SCRIPT_NAME.matcher(name).matches()) {
			throw new MLVerificationException("In "+field+": found script name which is not of legal syntax. Name was '"+name+"'.");
		}
	}

	public static void checkItemID(String field, String id) {
		if(!PATTERN_ITEM_ID.matcher(id).matches()) {
			throw new MLVerificationException("In "+field+": found item ID which is not of legal syntax. ID was '"+id+"'.");
		}
		if("done".equals(id)) {
			throw new MLVerificationException("In "+field+": found item with ID 'done'. This is not allowed.");
		}
	}

	public static void checkBranchID(String field, String id) {
		if(!PATTERN_BRANCH_ID.matcher(id).matches()) {
			throw new MLVerificationException("In "+field+": found branch ID which is not of legal syntax. ID was '"+id+"'.");
		}
	}

	public static void checkValidOptionKey(String field, String key) {
		if(!PATTERN_OPTION_KEY.matcher(key).matches()) {
			throw new MLVerificationException("In "+field+": found option key which is not of legal syntax. Key was '"+key+"'.");
		}
	}

	public static void checkLang(String field, String lang) {
		if(!PATTERN_LANG_TAG.matcher(lang).matches()) {
			throw new MLVerificationException("In "+field+": found language tag which is not of legal syntax. Language tag was '"+lang+"'.");
		}
	}

	private static <T> void verifyLanguages(String field, Collection<T> collection, Set<String> languages, Function<T, String> langExtractor) {
		Set<String> foundLanguages = new HashSet<>();
		for(T t : collection) {
			foundLanguages.add(langExtractor.apply(t));
		}
		
		Set<String> missing = new HashSet<>(languages);
		missing.removeAll(foundLanguages);
		
		if(!missing.isEmpty()) {
			throw new MLVerificationException("Collection "+field+" does not have entries for following declared languages which minilesson supports: "+missing+".");
		}
	}

	private static void checkNotEmpty(String field, Collection<?> collection) {
		if(collection==null || collection.isEmpty()) throw new MLVerificationException("Collection "+field+" must not be null or empty!");
	}

	public static final Pattern PATTERN_VIEW_NAME = Pattern.compile("^[a-zA-Z][-a-zA-Z0-9_]*$");
	public static final Pattern PATTERN_SCRIPT_NAME = Pattern.compile("^[a-zA-Z][-a-zA-Z0-9_]*(\\.[a-zA-Z][-a-zA-Z0-9_]*)+$");
	public static final Pattern PATTERN_FILE_NAME = Pattern.compile("^[a-zA-Z][-a-zA-Z0-9_]*(\\.[a-zA-Z][-a-zA-Z0-9_]*)+$");
	public static final Pattern PATTERN_ITEM_ID = Pattern.compile("^[a-zA-Z0-9][-a-zA-Z0-9_]*(\\.[a-zA-Z0-9][-a-zA-Z0-9_]*)*$");
	public static final Pattern PATTERN_BRANCH_ID = Pattern.compile("^[a-zA-Z0-9][-a-zA-Z0-9_]*(\\.[a-zA-Z0-9][-a-zA-Z0-9_]*)*$");
	public static final Pattern PATTERN_OPTION_KEY = Pattern.compile("^[a-zA-Z][a-zA-Z0-9_]*([-][a-zA-Z][a-zA-Z0-9_]*)*$");
	public static final Pattern PATTERN_PUBLICID = Pattern.compile("^[a-zA-Z_][a-zA-Z0-9_+[-]]*(\\.[a-zA-Z_][a-zA-Z0-9_+[-]]*)+$");
	public static final Pattern PATTERN_LANG_TAG = Pattern.compile("^[a-zA-Z]{2,8}(_([a-zA-Z]{2}|[0-9]{3})(_(([0-9][0-9a-zA-Z]{3})|[0-9a-zA-Z]{5,8})([-_]([0-9][0-9a-zA-Z]{3}|[0-9a-zA-Z]{5,8}))*)?)?$");
	
	public static void checkPublicID(String uid) {
		checkNotEmpty("publicID", uid);
		if(!PATTERN_PUBLICID.matcher(uid).matches()) {
			throw new MLVerificationException("Syntax of public ID is invalid.");
		}
	}

	/**
	 * Check that <code>value</code> is not <code>null</code>.
	 * 
	 * @param field field name to include in message in thrown exception is constraint is breached
	 * @param value value to check
	 * @throws MLVerificationException if constraint is breached
	 */
	private static void checkNotNull(String field, Object value) {
		if(value==null) throw new MLVerificationException("Field "+field+" must not be null!");
	}
	
	/**
	 * Check that <code>value</code> is not <code>null</code> or empty (zero-length) string.
	 * 
	 * @param field field name to include in message in thrown exception is constraint is breached
	 * @param value value to check
	 * @throws MLVerificationException if constraint is breached
	 */
	private static void checkNotEmpty(String field, String value) {
		if(value==null || value.isEmpty()) throw new MLVerificationException("Field "+field+" must not be null or empty!");
	}
	
	/**
	 * Helper method which trims non-null strings.
	 * 
	 * @param value string to trim
	 * @return <code>null</code> if <code>value</code> is <code>null</code>, trimmed <code>value</code> otherwise
	 */
	private static String trim(String value) {
		return value==null ? value : value.trim();
	}
}
