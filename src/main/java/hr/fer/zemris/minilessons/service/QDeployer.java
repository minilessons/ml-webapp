package hr.fer.zemris.minilessons.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import hr.fer.zemris.minilessons.QDescriptorParserUtil;
import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOException;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.JSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.LocalizedString;
import hr.fer.zemris.minilessons.dao.model.QuestionApproval;
import hr.fer.zemris.minilessons.dao.model.QuestionConfigVariability;
import hr.fer.zemris.minilessons.dao.model.QuestionType;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinitionAttachment;
import hr.fer.zemris.minilessons.questions.quizzes.QuestionFactory;
import hr.fer.zemris.minilessons.questions.quizzes.QuestionGroupBase;
import hr.fer.zemris.minilessons.util.UUIDUtil;
import hr.fer.zemris.minilessons.web.controllers.QTester;
import hr.fer.zemris.minilessons.web.controllers.QuestionsController;
import hr.fer.zemris.minilessons.xmlmodel.XMLAuthor;
import hr.fer.zemris.minilessons.xmlmodel.XMLConfiguration;
import hr.fer.zemris.minilessons.xmlmodel.XMLLanguage;
import hr.fer.zemris.minilessons.xmlmodel.XMLQuestionDescriptor;
import hr.fer.zemris.minilessons.xmlmodel.XMLString;

public class QDeployer {

	/**
	 * @param rootPath
	 * @param owner
	 * @param visible
	 * @param restricted
	 * @throws IOException
	 */
	public static void deployJSQuestion(Path rootPath, User owner, boolean visible, boolean publicQuestion) throws IOException {
		// Smije li korisnik uploadati minilekciju?
		if(owner.getUploadPolicy()==UploadPolicy.DENIED) {
			throw new RuntimeException("You can not upload js-questions: permission denied.");
		}
		
		// Dohvati deskriptor:
		Path descriptorPath = rootPath.resolve("descriptor.xml");
		XMLQuestionDescriptor xmlq = QDescriptorParserUtil.parseInputStream(Files.newInputStream(descriptorPath));
		checkQuestionDescriptor(rootPath, xmlq);

		for(XMLConfiguration cfg : xmlq.getConfigurations()) {
			if(!Files.isRegularFile(rootPath.resolve("scripts").resolve("config-"+cfg.getName()+".json"))) {
				throw new RuntimeException("Configuration '"+cfg.getName()+"' was declared, but file /scripts/config-"+cfg.getName()+".json was not provided.");
			}
		}

		String publicID = trim(xmlq.getUid());
		
		List<JSQuestionDefinition> allVersions = DAOProvider.getDao().listAllJSQuestionDefinitionVersions(publicID);
		
		boolean newMajorVersion = false;
		boolean existing = false;
		
		JSQuestionDefinition useml = null;
		JSQuestionDefinition lastVersion = null;
		
		// If this is not new question:
		if(!allVersions.isEmpty()) {
			Set<User> owners = allVersions.stream().map(m->m.getOwner()).collect(Collectors.toSet());
			if(!owners.contains(owner)) {
				throw new MLVerificationException("Other user is already owner of question with this public ID. You can not upload question with the same public ID.");
			}
			lastVersion = allVersions.stream().min(JSQuestionDefinition.CMP_BY_VERSION).get();
			if(lastVersion.getVersion() > xmlq.getVersion()) {
				throw new MLVerificationException("JSQuestionDefinition which exists on the system is newer (has bigger version) than the one you are uploading. Uploading older versions is not allowed.");
			}
			if(lastVersion.getVersion() == xmlq.getVersion() && lastVersion.getMinorVersion() >= xmlq.getMinorVersion()) {
				throw new MLVerificationException("JSQuestionDefinition which exists on the system is newer (has bigger version) than the one you are uploading. Uploading older versions is not allowed.");
			}
			newMajorVersion = lastVersion.getVersion() < xmlq.getVersion();

			if(!newMajorVersion) {
				//checkForCompatibility(xmlml, DescriptorParserUtil.parseInputStream(Files.newInputStream(DAOProvider.getDao().getRealPathFor(lastVersion.getId(), "descriptor.xml"))));
			}
			
			if(!newMajorVersion) useml = lastVersion;
			existing = true;
		}
		
		final JSQuestionDefinition ml = useml != null ? useml : new JSQuestionDefinition();
		if(useml==null) {
			ml.setId(UUIDUtil.nextUUID());
			ml.setPublicID(trim(xmlq.getUid()));
			ml.setQuestionType(QuestionType.JS);
		}
		ml.setApiLevel(xmlq.getApiLevel());
		ml.setVersion(xmlq.getVersion());
		ml.setMinorVersion(xmlq.getMinorVersion());
		ml.setLanguages(new HashSet<>());
		ml.setTitles(new HashMap<>());
		ml.setOwner(owner);
		ml.getConfigVariability().clear();
		xmlq.getConfigurations().forEach(c->ml.getConfigVariability().put(c.getName(), c.getVariability()));
		
		if(owner.getUploadPolicy()==UploadPolicy.RESTRICTED || owner.getUploadPolicy()==UploadPolicy.FOR_APPROVAL) {
			ml.setApproval(QuestionApproval.WAITING);
			ml.setApprovedBy(null);
			ml.setApprovedOn(null);
		} else if(owner.getUploadPolicy()==UploadPolicy.ALLOWED) {
			ml.setApproval(QuestionApproval.APPROVED);
			ml.setApprovedBy(owner);
			ml.setApprovedOn(new Date());
		} else {
			throw new RuntimeException("Security problem: uncovered case detected.");
		}

		ml.setUploadedOn(new Date());
		ml.setVisible(visible);
		ml.setPublicQuestion(publicQuestion);
		
		if(xmlq.getAbout()!=null && xmlq.getAbout().getTitle()!=null) {
			ml.getTitles().clear();
			xmlq.getAbout().getTitle().forEach(x->{
				ml.getTitles().put(trim(x.getLang()), trim(x.getText()));
			});
		}
		if(xmlq.getAbout()!=null && xmlq.getAbout().getSupportedLanguages()!=null) {
			ml.getLanguages().clear();
			xmlq.getAbout().getSupportedLanguages().forEach(x->{
				ml.getLanguages().add(trim(x.getLang()));
			});
		}
		if(xmlq.getAbout()!=null && xmlq.getAbout().getTags() != null) {
			ml.getTags().clear();
			xmlq.getAbout().getTags().forEach(t->{ml.getTags().add(new LocalizedString(trim(t.getLang()), trim(t.getText())));});
		}

		// Ako je ovo upload nove glavne verzije (postojeće minilekcije), staru verziju arhiviraj!
		if(existing && newMajorVersion) {
			lastVersion.setArchived(true);
		}
		
		DAO dao = DAOProvider.getDao();
		if(useml==null) dao.saveJSQuestionDefinition(ml);

		try {
			if(useml==null) {
				DAOProvider.getDao().getQStorage().persistQuestionPackage(ml.getId(), rootPath);
			} else {
				DAOProvider.getDao().getQStorage().updateQuestionPackage(ml.getId(), rootPath);
			}
		} catch(IOException ex) {
			throw new DAOException("Exception while copying js-question package.");
		}
		
	}

	public static void deployXMLQuestion(User owner, QuestionsController.XMLQuestionUploadData data) throws IOException {
		// Smije li korisnik uploadati minilekciju?
		if(owner.getUploadPolicy()==UploadPolicy.DENIED) {
			throw new RuntimeException("You can not upload js-questions: permission denied.");
		}

		QuestionGroupBase questionGroup = QuestionFactory.fromText(data.getXml());

		String publicID = trim(questionGroup.getPublicID());
		
		List<XMLQuestionDefinition> allVersions = DAOProvider.getDao().listAllXMLQuestionDefinitionVersions(publicID);
		
		boolean newMajorVersion = false;
		boolean existing = false;
		
		XMLQuestionDefinition useml = null;
		XMLQuestionDefinition lastVersion = null;
		
		// If this is not new question:
		if(!allVersions.isEmpty()) {
			Set<User> owners = allVersions.stream().map(m->m.getOwner()).collect(Collectors.toSet());
			if(!owners.contains(owner)) {
				throw new MLVerificationException("Other user is already owner of question with this public ID. You can not upload question with the same public ID.");
			}
			lastVersion = allVersions.stream().min(JSQuestionDefinition.CMP_BY_VERSION).get();
			if(lastVersion.getVersion() > questionGroup.getVersion()) {
				throw new MLVerificationException("XMLQuestionDefinition which exists on the system is newer (has bigger version) than the one you are uploading. Uploading older versions is not allowed.");
			}
			if(lastVersion.getVersion() == questionGroup.getVersion() && lastVersion.getMinorVersion() >= questionGroup.getMinorVersion()) {
				throw new MLVerificationException("XMLQuestionDefinition which exists on the system is newer (has bigger version) than the one you are uploading. Uploading older versions is not allowed.");
			}
			newMajorVersion = lastVersion.getVersion() < questionGroup.getVersion();

			if(!newMajorVersion) {
				//checkForCompatibility(xmlml, DescriptorParserUtil.parseInputStream(Files.newInputStream(DAOProvider.getDao().getRealPathFor(lastVersion.getId(), "descriptor.xml"))));
			}
			
			if(!newMajorVersion) useml = lastVersion;
			existing = true;
		}
		
		final XMLQuestionDefinition ml = useml != null ? useml : new XMLQuestionDefinition();
		if(useml==null) {
			ml.setId(UUIDUtil.nextUUID());
			ml.setPublicID(trim(questionGroup.getPublicID()));
			ml.setQuestionType(QuestionType.XML);
		}
		ml.setVersion(questionGroup.getVersion());
		ml.setMinorVersion(questionGroup.getMinorVersion());
		ml.setLanguages(new HashSet<>());
		ml.setTitles(new HashMap<>());
		ml.setTags(new ArrayList<>());
		ml.setOwner(owner);
		ml.getConfigVariability().clear();
		for(int i = 0, n = questionGroup.size(); i<n; i++) {
			ml.getConfigVariability().put(questionGroup.getQuestion(i).getId(), QuestionConfigVariability.NONE);
		}
		if(owner.getUploadPolicy()==UploadPolicy.RESTRICTED || owner.getUploadPolicy()==UploadPolicy.FOR_APPROVAL) {
			ml.setApproval(QuestionApproval.WAITING);
			ml.setApprovedBy(null);
			ml.setApprovedOn(null);
		} else if(owner.getUploadPolicy()==UploadPolicy.ALLOWED) {
			ml.setApproval(QuestionApproval.APPROVED);
			ml.setApprovedBy(owner);
			ml.setApprovedOn(new Date());
		} else {
			throw new RuntimeException("Security problem: uncovered case detected.");
		}

		ml.setXml(data.getXml());
		ml.setUploadedOn(new Date());
		ml.setVisible(data.isPublicQuestion());
		ml.setPublicQuestion(data.isPublicQuestion());
		ml.getLanguages().addAll(questionGroup.getSupportedLanguages());
		ml.setXMLQuestionType(questionGroup.getQuestionType().name());
		questionGroup.getTitle().getTranslations().forEach((l,t)->ml.getTitles().put(l, t.getText()));
		questionGroup.getTags().forEach(t->ml.getTags().add(new LocalizedString(t.getLang(), t.getText())));
		
		// Ako je ovo upload nove glavne verzije (postojeće minilekcije), staru verziju arhiviraj!
		if(existing && newMajorVersion) {
			lastVersion.setArchived(true);
		}
		
		DAO dao = DAOProvider.getDao();
		if(useml==null) dao.saveXMLQuestionDefinition(ml);

		List<XMLQuestionDefinitionAttachment> existingAttachments = useml==null ? new ArrayList<>() : dao.findAllXMLQuestionDefinitionAttachments(ml);
		
		Set<String> newNames = data.getAttachments().stream().map(m->m.getName()).collect(Collectors.toSet());
		Iterator<XMLQuestionDefinitionAttachment> it = existingAttachments.iterator();
		while(it.hasNext()) {
			XMLQuestionDefinitionAttachment a = it.next();
			if(!newNames.contains(a.getName())) {
				dao.removeXMLQuestionDefinitionAttachment(a);
				it.remove();
			}
		}
		
		Map<String, XMLQuestionDefinitionAttachment> map = existingAttachments.stream().collect(Collectors.toMap(x->x.getName(), x->x));
		data.getAttachments().forEach(a->{
			XMLQuestionDefinitionAttachment e = map.get(a.getName());
			if(e!=null) {
				if(!Arrays.equals(a.getBody(), e.getBody())) {
					e.setBody(a.getBody());
				}
			} else {
				e = new XMLQuestionDefinitionAttachment();
				e.setQuestion(ml);
				e.setName(a.getName());
				e.setBody(a.getBody());
				dao.saveXMLQuestionDefinitionAttachment(e);
			}
		});
	}

	public static void checkQuestionDescriptor(Path rootPath, XMLQuestionDescriptor xmlq) {
		checkPublicID(xmlq.getUid());
		checkNotNull("about", xmlq.getAbout());

		if(xmlq.getApiLevel()<1 || xmlq.getApiLevel()>2) {
			throw new MLVerificationException("API Level "+xmlq.getApiLevel()+" is invalid.");
		}
		
		if(xmlq.getConfigurations()==null || xmlq.getConfigurations().isEmpty()) {
			throw new MLVerificationException("No configurations found for question. You must define at least one configuration: default.");
		}
		Set<String> configNamesSet = new HashSet<>();
		for(XMLConfiguration s : xmlq.getConfigurations()) {
			configNamesSet.add(s.getName());
			if(!QTester.checkConfigurationName(s.getName())) {
				throw new MLVerificationException("Configuration name '"+s.getName()+"' is invalid.");
			}
			if(s.getVariability()==null) {
				throw new MLVerificationException("Configuration variability for configuration '"+s.getName()+"' is missing: please specify one.");
			}
		}
		if(configNamesSet.size() != xmlq.getConfigurations().size()) {
			throw new MLVerificationException("Duplicate configuration names found. Each configuration must be unique for given question.");
		}
		if(!configNamesSet.contains("default")) {
			throw new MLVerificationException("A configuration named 'default' is mandatory, but is not declared in description.xml.");
		}
		
		Set<String> declaredLanguages = new HashSet<>();
		
		// Verify about.supportedLanguages section:
		// ----------------------------------------
		checkNotEmpty("about.supportedLanguages", xmlq.getAbout().getSupportedLanguages());
		Set<XMLLanguage> langSet = new HashSet<>();
		for(XMLLanguage l : xmlq.getAbout().getSupportedLanguages()) {
			checkNotEmpty("about.supportedLanguages.language.lang", l.getLang());
			checkLang("about.supportedLanguages", l.getLang());
			if(!langSet.add(l)) {
				throw new MLVerificationException("about.supportedLanguages contains duplicate languages.");
			}
			declaredLanguages.add(l.getLang());
		}
		
		// Verify about.title section:
		// --------------------------------
		checkNotEmpty("title", xmlq.getAbout().getTitle());
		Set<String> titleLangSet = new HashSet<>();
		for(XMLString t : xmlq.getAbout().getTitle()) {
			checkNotEmpty("about.title.string.lang", t.getLang());
			checkNotEmpty("about.title.string.text", t.getText());
			checkLang("about.title", t.getLang());
			if(!titleLangSet.add(t.getLang())) {
				throw new MLVerificationException("about.title.string found multiple titles for same language.");
			}
		}
		verifyLanguages("about.title", xmlq.getAbout().getTitle(), declaredLanguages, XMLString::getLang);
		
		// Verify about.authors section:
		// --------------------------------
		checkNotEmpty("about.authors", xmlq.getAbout().getAuthors());
		Set<XMLAuthor> authorSet = new HashSet<>();
		for(XMLAuthor a : xmlq.getAbout().getAuthors()) {
			checkNotEmpty("about.authors.author.firstName", a.getFirstName());
			checkNotEmpty("about.authors.author.lastName", a.getLastName());
			checkNotEmpty("about.authors.author.email", a.getEmail());
			if(!authorSet.add(a)) {
				throw new MLVerificationException("about.authors contains duplicate entries.");
			}
		}

		// Verify about.tags section:
		// --------------------------------
		checkNotEmpty("about.tags", xmlq.getAbout().getTags());
		Set<String> tagsLangSet = new HashSet<>();
		for(XMLString t : xmlq.getAbout().getTags()) {
			checkNotEmpty("about.tags.string.lang", t.getLang());
			checkNotEmpty("about.tags.string.text", t.getText());
			checkLang("about.tags", t.getLang());
			if(!tagsLangSet.add(t.getLang()+"||"+t.getText())) {
				throw new MLVerificationException("about.tags found duplicate tags for the same language ("+t.getLang()+").");
			}
		}
	}

	public static void checkLang(String field, String lang) {
		if(!PATTERN_LANG_TAG.matcher(lang).matches()) {
			throw new MLVerificationException("In "+field+": found language tag which is not of legal syntax. Language tag was '"+lang+"'.");
		}
	}

	private static <T> void verifyLanguages(String field, Collection<T> collection, Set<String> languages, Function<T, String> langExtractor) {
		Set<String> foundLanguages = new HashSet<>();
		for(T t : collection) {
			foundLanguages.add(langExtractor.apply(t));
		}
		
		Set<String> missing = new HashSet<>(languages);
		missing.removeAll(foundLanguages);
		
		if(!missing.isEmpty()) {
			throw new MLVerificationException("Collection "+field+" does not have entries for following declared languages which minilesson supports: "+missing+".");
		}
	}

	private static void checkNotEmpty(String field, Collection<?> collection) {
		if(collection==null || collection.isEmpty()) throw new MLVerificationException("Collection "+field+" must not be null or empty!");
	}

	public static final Pattern PATTERN_PUBLICID = Pattern.compile("^[a-zA-Z_][a-zA-Z0-9_+[-]]*(\\.[a-zA-Z_][a-zA-Z0-9_+[-]]*)+$");
	public static final Pattern PATTERN_LANG_TAG = Pattern.compile("^[a-zA-Z]{2,8}(_([a-zA-Z]{2}|[0-9]{3})(_(([0-9][0-9a-zA-Z]{3})|[0-9a-zA-Z]{5,8})([-_]([0-9][0-9a-zA-Z]{3}|[0-9a-zA-Z]{5,8}))*)?)?$");
	
	public static void checkPublicID(String uid) {
		checkNotEmpty("publicID", uid);
		if(!PATTERN_PUBLICID.matcher(uid).matches()) {
			throw new MLVerificationException("Syntax of public ID is invalid.");
		}
	}

	/**
	 * Check that <code>value</code> is not <code>null</code>.
	 * 
	 * @param field field name to include in message in thrown exception is constraint is breached
	 * @param value value to check
	 * @throws MLVerificationException if constraint is breached
	 */
	private static void checkNotNull(String field, Object value) {
		if(value==null) throw new MLVerificationException("Field "+field+" must not be null!");
	}
	
	/**
	 * Check that <code>value</code> is not <code>null</code> or empty (zero-length) string.
	 * 
	 * @param field field name to include in message in thrown exception is constraint is breached
	 * @param value value to check
	 * @throws MLVerificationException if constraint is breached
	 */
	private static void checkNotEmpty(String field, String value) {
		if(value==null || value.isEmpty()) throw new MLVerificationException("Field "+field+" must not be null or empty!");
	}
	
	/**
	 * Helper method which trims non-null strings.
	 * 
	 * @param value string to trim
	 * @return <code>null</code> if <code>value</code> is <code>null</code>, trimmed <code>value</code> otherwise
	 */
	private static String trim(String value) {
		return value==null ? value : value.trim();
	}
}
