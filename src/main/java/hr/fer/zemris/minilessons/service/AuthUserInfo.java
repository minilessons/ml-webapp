package hr.fer.zemris.minilessons.service;

import java.util.Set;

import hr.fer.zemris.minilessons.dao.model.UploadPolicy;

/**
 * This class encapsulates data about user retrieved from authentication service when the service
 * is used to automatically create locally non-existent users.
 * 
 * @author marcupic
 *
 */
public class AuthUserInfo {

	private String username;
	private String jmbag;
	private String email;
	private String lastName;
	private String firstName;
	private UploadPolicy uploadPolicy;
	private Set<String> permissions;

	public AuthUserInfo() {
	}

	public String getUsername() {
		return username;
	}
	public String getJmbag() {
		return jmbag;
	}
	public String getEmail() {
		return email;
	}
	public String getLastName() {
		return lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setJmbag(String jmbag) {
		this.jmbag = jmbag;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public UploadPolicy getUploadPolicy() {
		return uploadPolicy;
	}
	public Set<String> getPermissions() {
		return permissions;
	}
	public void setUploadPolicy(UploadPolicy uploadPolicy) {
		this.uploadPolicy = uploadPolicy;
	}
	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}
}
