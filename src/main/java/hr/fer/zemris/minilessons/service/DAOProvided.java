/**
 * 
 */
package hr.fer.zemris.minilessons.service;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
/**
 * Označava da metodi pri pozivu treba otvoriti vezu prema DAO a po izlasku je treba automatski zatvoriti.
 * 
 * @author marcupic
 */
public @interface DAOProvided {

}
