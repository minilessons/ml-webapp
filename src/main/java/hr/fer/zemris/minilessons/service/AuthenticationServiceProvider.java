package hr.fer.zemris.minilessons.service;

import hr.fer.zemris.minilessons.dao.model.User;

/**
 * This is authentication service provider: an object which can be used to
 * authenticate user.
 * 
 * @author marcupic
 *
 */
public interface AuthenticationServiceProvider {

	/**
	 * Getter for authentication service implementation name.
	 * 
	 * @return name
	 */
	String getName();
	
	/**
	 * Performs user authentication. Only if <code>retrieveUserData</code> is set to <code>true</code>, an attempt will be made to obtain
	 * user data from authentication service and store it in result. This data will be retrieved only if authentication service supports
	 * such process and if user authentication is successful.
	 * 
	 * @param user user
	 * @param username user provided username
	 * @param password user provided password
	 * @param retrieveUserData should user data be retrieved from this authentication service (for purpose of automatic local user creation)
	 * @return authentication status
	 */
	AuthenticationResult authenticate(User user, String username, String password, boolean retrieveUserData);
}
