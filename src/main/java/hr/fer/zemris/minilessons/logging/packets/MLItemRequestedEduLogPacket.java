package hr.fer.zemris.minilessons.logging.packets;

import hr.fer.zemris.minilessons.logging.AbstractEduLogPacket;

public class MLItemRequestedEduLogPacket extends AbstractEduLogPacket {

	private static final int PACKET_ID = 2;
	private byte[] content;
	
	public MLItemRequestedEduLogPacket(String username, String minilessonUID, String itemID, String viewID) {
		super(PACKET_ID, username);
		content = new PacketBuilder().writeString(minilessonUID).writeString(itemID).writeString(viewID).getContent();
	}

	@Override
	public byte[] getContent() {
		return content;
	}
}
