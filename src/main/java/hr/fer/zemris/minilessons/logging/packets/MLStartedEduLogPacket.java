package hr.fer.zemris.minilessons.logging.packets;

import hr.fer.zemris.minilessons.logging.AbstractEduLogPacket;

public class MLStartedEduLogPacket extends AbstractEduLogPacket {

	private static final int PACKET_ID = 1;
	private byte[] content;
	
	public MLStartedEduLogPacket(String username, String minilessonUID) {
		super(PACKET_ID, username);
		content = new PacketBuilder().writeString(minilessonUID).getContent();
	}

	@Override
	public byte[] getContent() {
		return content;
	}
}
