package hr.fer.zemris.minilessons.logging.packets;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

class PacketBuilder {

	private ByteArrayOutputStream bos;
	private DataOutputStream dos;
	private SimpleDateFormat sdf;
	
	public PacketBuilder() {
		bos = new ByteArrayOutputStream();
		dos = new DataOutputStream(bos);
	}

	public PacketBuilder writeString(String str) {
		try {
			if(str==null || str.isEmpty()) {
				dos.writeInt(0);
			} else {
				byte[] data = str.getBytes(StandardCharsets.UTF_8);
				dos.writeInt(data.length);
				dos.write(data);
			}
		} catch(Exception ex) {
			throw new RuntimeException("Could not prepare log data!", ex);
		}
		return this;
	}

	public PacketBuilder writeDate(Date date) {
		if(sdf==null) {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
		try {
			byte[] data = sdf.format(date).getBytes(StandardCharsets.UTF_8);
			dos.writeByte(data.length);
			dos.write(data);
		} catch(Exception ex) {
			throw new RuntimeException("Could not prepare log data!", ex);
		}
		return this;
	}
	
	public byte[] getContent() {
		try {
			dos.flush();
			dos.close();
		} catch(Exception ex) {
			throw new RuntimeException("Could not prepare log data!", ex);
		}
		return bos.toByteArray();
	}
}
