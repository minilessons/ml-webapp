package hr.fer.zemris.minilessons.logging.packets;

import hr.fer.zemris.minilessons.logging.AbstractEduLogPacket;

public class MLJSCAnswerEduLogPacket extends AbstractEduLogPacket {

	private static final int PACKET_ID = 3;
	private byte[] content;
	
	public MLJSCAnswerEduLogPacket(String username, String minilessonUID, String itemID, String viewID, String keyID, String sentData) {
		super(PACKET_ID, username);
		content = new PacketBuilder().writeString(minilessonUID).writeString(itemID).writeString(viewID).writeString(keyID).writeString(sentData).getContent();
	}

	@Override
	public byte[] getContent() {
		return content;
	}
}
