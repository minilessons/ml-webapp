package hr.fer.zemris.minilessons.util;

/**
 * This class represents a single video-subtitle. Video-subtitle is 
 * specified by filename, language tag and label.
 * 
 * @author marcupic
 */
public class VideoSubtitleSpec {
	
	/**
	 * Name of subtitles file.
	 */
	private String fileName;
	/**
	 * Language tag specifying the language of subtitles.
	 */
	private String language;
	/**
	 * Label for subtitles.
	 */
	private String label;
	
	/**
	 * Constructor.
	 * 
	 * @param fileName file name
	 * @param language language tag
	 * @param label label
	 */
	public VideoSubtitleSpec(String fileName, String language, String label) {
		super();
		this.fileName = fileName;
		this.language = language;
		this.label = label;
	}

	/**
	 * Getter for filename.
	 * 
	 * @return filename.
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * Getter for label.
	 * @return label
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * Getter for language.
	 * 
	 * @return language
	 */
	public String getLanguage() {
		return language;
	}
}
