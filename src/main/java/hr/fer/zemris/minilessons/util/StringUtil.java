package hr.fer.zemris.minilessons.util;

/**
 * String utility class.
 * 
 * @author marcupic
 */
public class StringUtil {

	/**
	 * Helper method for trimming non-null strings. If argument is <code>null</code>, so is the result. If argument is
	 * not <code>null</code>, it is trimmed and returned, unless after trimming an empty string is obtained; in that case,
	 * <code>null</code> is returned. In other words, this method will never return empty string.
	 * @param s string
	 * @return trimmed string
	 */
	public static String trim(String s) {
		if(s==null) return null;
		s = s.trim();
		return s.isEmpty() ? null : s;
	}

}
