package hr.fer.zemris.minilessons.periodicals.periods;

import java.time.LocalDateTime;

public interface PeriodicalTimeSpec {
	boolean isFormatOK(String str);
	PeriodicalTimeSpec get(String str);
	LocalDateTime nextTime(LocalDateTime now);
}