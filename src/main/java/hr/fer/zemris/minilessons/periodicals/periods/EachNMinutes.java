package hr.fer.zemris.minilessons.periodicals.periods;

import java.time.LocalDateTime;

public class EachNMinutes extends AbstractPeriodicalTimeSpec {
	private int m;
	private int mOffset;
	private int sOffset;
	
	@Override
	public boolean isFormatOK(String str) {
		if(str.length() != 10) return false;
		if(str.charAt(0)!='x' || str.charAt(1)!='m' || str.charAt(4)!='+' || str.charAt(7)!=':') return false;
		if(!digits(str,2,3) || !digits(str,5,6) || !digits(str,8,9)) return false;
		if(number(str,2,3)==0 || number(str,2,3)>59 || number(str,5,6)>59 || number(str,8,9)>59) return false;
		return true;
	}

	@Override
	public PeriodicalTimeSpec get(String str) {
		if(!isFormatOK(str)) throw new IllegalArgumentException("'"+str+"' is not properly formatted for EachNMinutes.");
		EachNMinutes s = new EachNMinutes();
		s.m = number(str,2,3);
		s.mOffset = number(str,5,6);
		s.sOffset = number(str,8,9);
		return s;
	}

	@Override
	public LocalDateTime nextTime(LocalDateTime now) {
		int nowH = now.getHour();
		int nowM = now.getMinute();
		int nowS = now.getSecond();
		
		int nowTime = nowM*60+nowS;
		int offsetTime = mOffset*60+sOffset;

		if(nowTime < offsetTime) {
			return LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), nowH, mOffset, sOffset);
		}
		
		int periodTime = m*60;
		
		int difference = nowTime - offsetTime;
		int k = difference / periodTime;
		
		k++;
		
		if(offsetTime + k*periodTime >= 3600) {
			return LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), nowH, mOffset, sOffset).plusHours(1);
		}

		return LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), nowH, mOffset+k*m, sOffset);
	}
}