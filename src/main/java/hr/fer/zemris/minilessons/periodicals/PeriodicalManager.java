package hr.fer.zemris.minilessons.periodicals;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import hr.fer.zemris.minilessons.periodicals.periods.EachHour;
import hr.fer.zemris.minilessons.periodicals.periods.EachNHours;
import hr.fer.zemris.minilessons.periodicals.periods.EachNMinutes;
import hr.fer.zemris.minilessons.periodicals.periods.PeriodicalTimeSpec;
import hr.fer.zemris.minilessons.periodicals.periods.SpecificMoment;

/**
 * <p>Podsustav koji je zadužen za periodičko izvođenje poslova u sustavu.</p>
 * 
 * <p>Specifikacija poslova zadaje se kao jedan redak sljedećeg formata.</p>
 * <pre>
 * Syntax: ml.periodicals=Entry Entry Entry ...
 * Entry = FQCN@Timespec,TimeSpec,TimeSpec,...
 * FQCN: Fully-Qualified Class Name of Runnable service to execute.
 * Timespec = HH:MM:SS   - each day, in given time (e.g. 17:50:00)
 *             *:MM:SS   - each day, each hour, in given time (e.g. *:30:00)
 *            xhHH+HH:MM:SS - each HH hours, offseted by MM:SS (e.g. 'xh02+01:10:00' - each two hours, in *:10:00, but starting from hour 1: 01:10:00, 03:10:00, ...)
 *            xmMM+MM:SS    - each MM minutes, offseted by MM:SS (e.g. 'xh03+01:30' - each three minutes with given offset: 01:30,04:30,07:30,10:30,...)
 * </pre>
 * 
 * @author marcupic
 *
 */
public class PeriodicalManager {

	/**
	 * Je li zatraženo gašenje podsustava?
	 */
	private volatile boolean shutdownRequested = false;
	/**
	 * Je li gašenje završeno?
	 */
	private boolean shutdownCompleted = false;
	/**
	 * Pomoćna dretva koja izvršava periodičke poslove.
	 */
	private Thread serviceThread;
	
	/**
	 * Red poslova sortiran po vremenu izvođenja; najraniji posao je prvi.
	 */
	private PriorityQueue<PeriodicalSpecification> queue = new PriorityQueue<>();
	
	/**
	 * Pomoćno polje podržanih specifikatora vremenskih oznaka u kojima se mogu pokretati poslovi.
	 */
	private static final PeriodicalTimeSpec[] supportedPeriodSpecification = {
		new SpecificMoment(),
		new EachHour(),
		new EachNHours(),
		new EachNMinutes()
	};
	
	/**
	 * Privatni razred koji čuva informacije o jednom konkretnom poslu. Pamti puno ime razreda koji predstavlja posao,
	 * listu vremenskih oznaka, referencu na primjerak posla te prvi sljedeći trenutak kada treba pokrenuti taj posao. 
	 * 
	 * @author marcupic
	 *
	 */
	private static class PeriodicalSpecification implements Comparable<PeriodicalSpecification> {
		/**
		 * Puno ime periodičkog posla (odnosno javinog razreda koji predstavlja taj posao).
		 */
		private String classFQN;
		/**
		 * Lista vremenskih specifikatora.
		 */
		private List<PeriodicalTimeSpec> list;
		/**
		 * Primjerak posla.
		 */
		private Runnable service;
		/**
		 * Kada prvi sljedeći puta treba izvesti ovaj posao?
		 */
		private LocalDateTime next;
		
		/**
		 * Konstruktor.
		 * 
		 * @param classFQN puno ime razreda koji predstavlja periodički posao
		 * @param list lista vremenskih specifikatora
		 */
		public PeriodicalSpecification(String classFQN, List<PeriodicalTimeSpec> list) {
			super();
			this.classFQN = classFQN;
			this.list = list;
			try {
				this.service = (Runnable)this.getClass().getClassLoader().loadClass(this.classFQN).newInstance();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				throw new RuntimeException("Could not instantiate "+classFQN+".", e);
			}
		}
		
		///**
		// * Getter za puno ime razreda koji predstavlja posao.
		// * 
		// * @return puno ime razreda
		// */
		//public String getClassFQN() {
		//	return classFQN;
		//}

		/**
		 * Pomoćna metoda koja s obzirom na predano vrijeme (argument <code>now</code>) određuje prvi sljedeći
		 * vremenski trenutak veći od predanog vremena u kojem posao treba pokrenuti.
		 * 
		 * @param now referentni vremenski trenutak
		 */
		public void determineNext(LocalDateTime now) {
			LocalDateTime candidate = null;
			for(PeriodicalTimeSpec pts : list) {
				LocalDateTime tmp = pts.nextTime(now);
				if(candidate==null || tmp.isBefore(candidate)) {
					candidate = tmp;
				}
			}
			next = candidate;
		}
		
		@Override
		public int compareTo(PeriodicalSpecification o) {
			return this.next.compareTo(o.next);
		}
	}

	/**
	 * Konstruktor. Prima specifikaciju svih poslova koje treba obavljati kao i vremenske specifikatore koji govore kada treba obavljati poslove.
	 * Pogledajte dokumentaciju ovog razreda za više detalja.
	 * 
	 * @param configKey specifikacija zadatka s vremenskim trenutcima
	 */
	public PeriodicalManager(String configKey) {
		List<PeriodicalSpecification> specs = new ArrayList<>();

		String[] services = configKey.split(" ");
		for(String service : services) {
			service = service.trim();
			if(service.isEmpty()) continue;
			
			int atPos = service.indexOf('@');
			if(atPos==-1 || atPos<1 || atPos==service.length()-1) throw new IllegalArgumentException("Specification "+service+" is invalid.");
			String classFQN = service.substring(0, atPos);
			String rest = service.substring(atPos+1);
			
			String[] timeSpecs = rest.split(",");
			List<PeriodicalTimeSpec> list = new ArrayList<>();
			
			for(String s : timeSpecs) {
				PeriodicalTimeSpec pts = null;
				for(PeriodicalTimeSpec candidateFormat : supportedPeriodSpecification) {
					if(candidateFormat.isFormatOK(s)) {
						pts = candidateFormat.get(s);
						break;
					}
				}
				if(pts==null) throw new IllegalArgumentException("Specification "+s+" is not recognized.");
				list.add(pts);
			}
			
			specs.add(new PeriodicalSpecification(classFQN, list));
		}
	
		LocalDateTime now = LocalDateTime.now();
		for(PeriodicalSpecification s : specs) {
			s.determineNext(now);
			queue.add(s);
		}

		if(!queue.isEmpty()) {
			serviceThread = new Runner("Periodicals executor thread");
			serviceThread.start();
		} else {
			shutdownCompleted = true;
			System.out.println("Nije pronađen niti jedan periodički zadatak. Sustav za izvođenje periodičkih poslova se neće pokrenuti.");
		}
	}

	/**
	 * Pokreće gašenje ovog podsustava. Metoda omogućava da se pozivatelj zablokira dok gašenje ne završi (argument <code>shouldWait</code>). Čekanje
	 * pri tome može biti vremenski ograničeno. Ako je <code>waitFor</code> nula ili negativan, ako je traženo čekanje, bit će vremenski neograničeno.
	 * Ako je pozitivan, tada će pozivatelj čekati najviše toliko milisekundi.
	 * 
	 * @param shouldWait treba li pozivatelj biti zablokiran dok gašenje ne završi: <code>true</code> ako treba, <code>false</code> ako ne smije
	 * @param waitFor ograničenje čekanja
	 */
	public void shutdown(boolean shouldWait, long waitFor) {
		shutdownRequested = true;
		serviceThread.interrupt();
		if(!shouldWait) return;

		long leftWaiting = waitFor;
		long started = System.currentTimeMillis();
		synchronized(this) {
			while(!shutdownCompleted) {
				if(waitFor>0 && leftWaiting <= 0) break;
				try {
					if(waitFor<=0) {
						this.wait();
					} else {
						this.wait(leftWaiting);
					}
				} catch(InterruptedException ignorable) {
				}
				if(waitFor>0) {
					long waited = System.currentTimeMillis()-started;
					leftWaiting = waitFor - waited;
				}
			}
		}
	}

	/**
	 * metoda provjerava je li gašenje podsustava završeno.
	 * @return <code>true</code> ako je gašenje završeno, <code>false</code> inače.
	 */
	public synchronized boolean isShutdownCompleted() {
		return shutdownCompleted;
	}
	
	/**
	 * Pomoćna dretva koja obavlja pozivanje poslova.
	 * 
	 * @author marcupic
	 */
	private class Runner extends Thread {
		
		/**
		 * Konstruktor.
		 * 
		 * @param name ime dretve
		 */
		public Runner(String name) {
			super(name);
		}

		@Override
		public void run() {
			
			while(!shutdownRequested) {
				
				PeriodicalSpecification spec = null;
				LocalDateTime now = LocalDateTime.now();
				while(true) {
					spec = queue.peek();
					
					if(spec.next.isAfter(now)) {
						break;
					}
					
					spec = queue.poll();
					//System.out.println("Executing periodical "+spec.getClassFQN()+" scheduled for: " + spec.next);
					try {
						spec.service.run();
					} catch(Exception ex) {
						ex.printStackTrace();
					}
					
					now = LocalDateTime.now();
					spec.determineNext(now);
					queue.add(spec);
					
					//System.out.println("Execution of periodical "+spec.classFQN+" completed; next is scheduled for: " + spec.next);
				}
				
				Duration toWait = Duration.between(now, spec.next);
				long milis = toWait.toMillis() + 500;
				//System.out.println("Periodicals: will sleep for: " + milis);
				try {
					Thread.sleep(milis);
				} catch (InterruptedException e) {
					if(Thread.interrupted()) continue;
				}
			}
			System.out.println(Thread.currentThread().getName()+": gotova.");
			synchronized(PeriodicalManager.this) {
				shutdownCompleted = true;
				PeriodicalManager.this.notifyAll();
			}
		}
		
	}
}
