package hr.fer.zemris.minilessons.periodicals.periods;

public abstract class AbstractPeriodicalTimeSpec implements PeriodicalTimeSpec {
	protected boolean digits(String str, int from, int to) {
		for(int i = from; i <= to; i++) {
			if(!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	protected int number(String str, int from, int to) {
		int n = 0;
		for(int i = from; i <= to; i++) {
			char c = str.charAt(i);
			if(!Character.isDigit(c)) {
				throw new NumberFormatException("Character " + c + " is not digit.");
			}
			n *= 10;
			n += c-'0';
		}
		return n;
	}
}