package hr.fer.zemris.minilessons.periodicals.periods;

import java.time.LocalDateTime;

public class EachNHours extends AbstractPeriodicalTimeSpec {
	private int h;
	private int hOffset;
	private int mOffset;
	private int sOffset;
	
	@Override
	public boolean isFormatOK(String str) {
		if(str.length() != 13) return false;
		if(str.charAt(0)!='x' || str.charAt(1)!='h' || str.charAt(4)!='+' || str.charAt(7)!=':' || str.charAt(10)!=':') return false;
		if(!digits(str,2,3) || !digits(str,5,6) || !digits(str,8,9) || !digits(str,11,12)) return false;
		if(number(str,2,3)==0 || number(str,2,3)>23 || number(str,5,6)>23 || number(str,8,9)>59 || number(str,11,12)>59) return false;
		return true;
	}

	@Override
	public PeriodicalTimeSpec get(String str) {
		if(!isFormatOK(str)) throw new IllegalArgumentException("'"+str+"' is not properly formatted for EachNHours.");
		EachNHours s = new EachNHours();
		s.h = number(str,2,3);
		s.hOffset = number(str,5,6);
		s.mOffset = number(str,8,9);
		s.sOffset = number(str,11,12);
		return s;
	}

	@Override
	public LocalDateTime nextTime(LocalDateTime now) {
		int nowH = now.getHour();
		int nowM = now.getMinute();
		int nowS = now.getSecond();
		
		int nowTime = ((nowH*60)+nowM)*60+nowS;
		int offsetTime = ((hOffset*60)+mOffset)*60+sOffset;

		if(nowTime < offsetTime) {
			return LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hOffset, mOffset, sOffset);
		}
		
		int periodTime = h*3600;
		
		int difference = nowTime - offsetTime;
		int k = difference / periodTime;
		
		k++;
		
		if(offsetTime + k*periodTime >= 24*3600) {
			return LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hOffset, mOffset, sOffset).plusDays(1);
		}

		return LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hOffset+k*h, mOffset, sOffset);
	}
}