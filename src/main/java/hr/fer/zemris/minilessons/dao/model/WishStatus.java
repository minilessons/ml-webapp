package hr.fer.zemris.minilessons.dao.model;

public enum WishStatus {

	OPEN,
	CLOSED,
	DUPLICATE,
	WONTIMPL
}
