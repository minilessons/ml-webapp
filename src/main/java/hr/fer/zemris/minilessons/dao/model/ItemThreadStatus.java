package hr.fer.zemris.minilessons.dao.model;

/**
 * Status of Item Comment.
 * 
 * @author marcupic
 *
 */
public enum ItemThreadStatus {
	/**
	 * When conversation is active.
	 */
	ACTIVE,
	/**
	 * When conversation is postponed for later.
	 */
	POSTPONED,
	/**
	 * When conversation is closed with positive status (i.e. agreement has been made).
	 */
	CLOSED,
	/**
	 * When conversation is closed with negative status (e.g. what user commented is not acceptable; suggestion won't be considered, ...).
	 */
	REJECTED;
}
