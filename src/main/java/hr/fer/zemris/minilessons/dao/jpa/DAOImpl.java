package hr.fer.zemris.minilessons.dao.jpa;

import java.nio.file.Path;
import java.util.List;

import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.model.CommonQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.dao.model.CourseCollection;
import hr.fer.zemris.minilessons.dao.model.ItemComment;
import hr.fer.zemris.minilessons.dao.model.ItemStatus;
import hr.fer.zemris.minilessons.dao.model.ItemStatusPK;
import hr.fer.zemris.minilessons.dao.model.ItemThread;
import hr.fer.zemris.minilessons.dao.model.JSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.JSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.MLStatWithUserGrade;
import hr.fer.zemris.minilessons.dao.model.MLUserStat;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonApproval;
import hr.fer.zemris.minilessons.dao.model.MinilessonStats;
import hr.fer.zemris.minilessons.dao.model.MinilessonStatus;
import hr.fer.zemris.minilessons.dao.model.MinilessonStatusPK;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserGrade;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserGradePK;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserSharedData;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.dao.model.UserCourseRole;
import hr.fer.zemris.minilessons.dao.model.UserRegistration;
import hr.fer.zemris.minilessons.dao.model.WSActivity;
import hr.fer.zemris.minilessons.dao.model.WSActivityScore;
import hr.fer.zemris.minilessons.dao.model.WSActivityScorePK;
import hr.fer.zemris.minilessons.dao.model.WSActivitySource;
import hr.fer.zemris.minilessons.dao.model.WSGroup;
import hr.fer.zemris.minilessons.dao.model.WSGrouping;
import hr.fer.zemris.minilessons.dao.model.WSJoinRequestStatus;
import hr.fer.zemris.minilessons.dao.model.WSQDWorkflowState;
import hr.fer.zemris.minilessons.dao.model.WSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.WSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.Wish;
import hr.fer.zemris.minilessons.dao.model.WishUserVote;
import hr.fer.zemris.minilessons.dao.model.WishUserVotePK;
import hr.fer.zemris.minilessons.dao.model.WishWithUserVote;
import hr.fer.zemris.minilessons.dao.model.Workspace;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinPolicy;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinRequest;
import hr.fer.zemris.minilessons.dao.model.WorkspacePage;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinitionAttachment;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DWSQuestionDefinition;
import hr.fer.zemris.minilessons.domain.model.DXMLQDef;
import hr.fer.zemris.minilessons.qstorage.QStorage;
import hr.fer.zemris.minilessons.storage.MLStorage;

public class DAOImpl implements DAO {

	private MLStorage storage;
	private QStorage qstorage;
	
	public DAOImpl(MLStorage storage, QStorage qstorage) {
		this.storage = storage;
		this.qstorage = qstorage;
	}
	
	@Override
	public MLStorage getMLStorage() {
		return storage;
	}
	
	@Override
	public QStorage getQStorage() {
		return qstorage;
	}
	
	@Override
	public User findUserByID(long id) {
		return JPA.getEntityManager().find(User.class, id);
	}

	@Override
	public void saveUser(User user) {
		JPA.getEntityManager().persist(user);
	}

	@Override
	public void saveMinilesson(Minilesson minilesson) {
		JPA.getEntityManager().persist(minilesson);
	}

	@Override
	public void saveJSQuestionDefinition(JSQuestionDefinition jSQuestionDefinition) {
		JPA.getEntityManager().persist(jSQuestionDefinition);
	}
	
	@Override
	public void saveJSQuestionInstance(JSQuestionInstance jSQuestionInstance) {
		JPA.getEntityManager().persist(jSQuestionInstance);
	}
		
	@Override
	public void saveMinilessonStats(MinilessonStats minilessonStats) {
		JPA.getEntityManager().persist(minilessonStats);
	}
	
	@Override
	public User findUserByUsername(String username, String authDomain) {
		List<User> list = JPA.getEntityManager()
			.createQuery("SELECT u FROM User u WHERE u.username=:username AND u.authDomain=:authDomain", User.class)
			.setParameter("username", username)
			.setParameter("authDomain", authDomain)
			.getResultList();
		return list.isEmpty() ? null : list.get(0);
	}
	
	@Override
	public User findUserByJMBAG(String jmbag) {
		List<User> list = JPA.getEntityManager()
			.createQuery("SELECT u FROM User u WHERE u.jmbag=:jmbag", User.class)
			.setParameter("jmbag", jmbag)
			.getResultList();
		return list.isEmpty() ? null : list.get(0);
	}
	
	@Override
	public List<Minilesson> listMinilessons() {
		return JPA.getEntityManager().createQuery("SELECT m FROM Minilesson m", Minilesson.class).getResultList();
	}

	@Override
	public Minilesson findMinilessonByID(String id) {
		return JPA.getEntityManager().find(Minilesson.class, id);
	}

	@Override
	public Path getRealPathFor(String minilessonID, String path) {
		return storage.getRealPathFor(minilessonID, path);
	}
	
	@Override
	public Path getRealQPathFor(String questionID, String path) {
		return qstorage.getRealPathFor(questionID, path);
	}

	@Override
	public ItemStatus findItemStatus(Minilesson minilesson, User user, String itemID) {
		return JPA.getEntityManager().find(ItemStatus.class, new ItemStatusPK(minilesson.getId(), user.getId(), itemID));
	}

	@Override
	public void saveItemStatus(ItemStatus itemStatus) {
		JPA.getEntityManager().persist(itemStatus);
	}

	@Override
	public List<String> listCompletedItems(Minilesson minilesson, User user) {
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>)JPA.getEntityManager()
				.createQuery("SELECT it.itemID FROM ItemStatus it where it.minilesson.id=:mid and it.user.id=:uid")
				.setParameter("mid", minilesson.getId())
				.setParameter("uid", user.getId())
				.getResultList();
		return list;
	}

	@Override
	public MinilessonStatus findMinilessonStatus(Minilesson minilesson, User user) {
		return JPA.getEntityManager().find(MinilessonStatus.class, new MinilessonStatusPK(minilesson.getId(), user.getId()));
	}

	@Override
	public void saveMinilessonStatus(MinilessonStatus minilessonStatus) {
		JPA.getEntityManager().persist(minilessonStatus);
	}

	@Override
	public List<MinilessonStats> listAllVisibleMLStats() {
		List<MinilessonStats> list = JPA.getEntityManager().createQuery("SELECT s FROM MinilessonStats s JOIN s.minilesson m WHERE m.visible=true AND m.archived=false AND m.approval in (:ap1, :ap2) ORDER BY m.uploadedOn", MinilessonStats.class)
				.setParameter("ap1", MinilessonApproval.APPROVED)
				.setParameter("ap2", MinilessonApproval.SOFT_WAITING)
				.getResultList();
		return list;
	}

	@Override
	public List<MinilessonStats> listAllVisibleMLStatsForOwner(User owner) {
		List<MinilessonStats> list = JPA.getEntityManager().createQuery("SELECT s FROM MinilessonStats s JOIN s.minilesson m WHERE m.visible=true AND m.archived=false AND m.approval in (:ap1, :ap2) and m.owner.id=:uid ORDER BY m.uploadedOn", MinilessonStats.class)
				.setParameter("ap1", MinilessonApproval.APPROVED)
				.setParameter("ap2", MinilessonApproval.SOFT_WAITING)
				.setParameter("uid", owner.getId())
				.getResultList();
		return list;
	}
	
	@Override
	public List<MLStatWithUserGrade> listAllVisibleMLStatsEx(Long userID) {
		List<MLStatWithUserGrade> list = JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.dao.model.MLStatWithUserGrade(s,mug,ms) FROM MinilessonStats s JOIN s.minilesson m LEFT OUTER JOIN MinilessonUserGrade mug ON mug.minilesson.id=m.id AND mug.user.id=:uid LEFT OUTER JOIN MinilessonStatus ms ON ms.minilesson.id=m.id AND ms.user.id=:uid WHERE m.visible=true AND m.archived=false AND m.approval in (:ap1, :ap2) ORDER BY m.uploadedOn", MLStatWithUserGrade.class)
				.setParameter("ap1", MinilessonApproval.APPROVED)
				.setParameter("ap2", MinilessonApproval.SOFT_WAITING)
				.setParameter("uid", userID)
				.getResultList();
		return list;
	}
	
	@Override
	public List<MLStatWithUserGrade> listMLStatsExForCourseMinilessons(Long userID, Course course) {
		List<MLStatWithUserGrade> list = JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.dao.model.MLStatWithUserGrade(s,mug,ms) FROM Course c, MinilessonStats s JOIN s.minilesson m LEFT OUTER JOIN MinilessonUserGrade mug ON mug.minilesson.id=m.id AND mug.user.id=:uid LEFT OUTER JOIN MinilessonStatus ms ON ms.minilesson.id=m.id AND ms.user.id=:uid WHERE c=:co AND m MEMBER OF c.minilessons AND m.visible=true AND m.archived=false AND m.approval in (:ap1, :ap2) ORDER BY m.uploadedOn", MLStatWithUserGrade.class)
				.setParameter("ap1", MinilessonApproval.APPROVED)
				.setParameter("ap2", MinilessonApproval.SOFT_WAITING)
				.setParameter("uid", userID)
				.setParameter("co", course)
				.getResultList();
		return list;
	}

	@Override
	public List<MLStatWithUserGrade> listMLStatsExForMinilesson(Long userID, Minilesson ml) {
		List<MLStatWithUserGrade> list = JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.dao.model.MLStatWithUserGrade(s,mug,ms) FROM MinilessonStats s JOIN s.minilesson m LEFT OUTER JOIN MinilessonUserGrade mug ON mug.minilesson.id=m.id AND mug.user.id=:uid LEFT OUTER JOIN MinilessonStatus ms ON ms.minilesson.id=m.id AND ms.user.id=:uid WHERE m=:ml AND m.visible=true AND m.archived=false AND m.approval in (:ap1, :ap2) ORDER BY m.uploadedOn", MLStatWithUserGrade.class)
				.setParameter("ap1", MinilessonApproval.APPROVED)
				.setParameter("ap2", MinilessonApproval.SOFT_WAITING)
				.setParameter("uid", userID)
				.setParameter("ml", ml)
				.getResultList();
		return list;
	}

	@Override
	public List<DQuestionInstance> findPublicUsersJSQuestionInstances(User user, JSQuestionDefinition qd) {
		return JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.domain.model.DQuestionInstance(jsi.id, jsi.status, jsi.correctness, jsi.solved, jsi.configName, jsi.createdOn, jsi.evaluatedOn) FROM JSQuestionInstance jsi WHERE jsi.user=:user AND jsi.question=:qd AND jsi.context IS NULL", DQuestionInstance.class)
				.setParameter("qd", qd)
				.setParameter("user", user)
				.getResultList();
	}
	
	@Override
	public List<DQuestionInstance> findPublicUsersXMLQuestionInstances(User user, XMLQuestionDefinition qd) {
		return JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.domain.model.DQuestionInstance(jsi.id, jsi.status, jsi.correctness, jsi.solved, jsi.configName, jsi.createdOn, jsi.evaluatedOn) FROM XMLQuestionInstance jsi WHERE jsi.user=:user AND jsi.question=:qd AND jsi.context IS NULL", DQuestionInstance.class)
				.setParameter("qd", qd)
				.setParameter("user", user)
				.getResultList();
	}

	@Override
	public List<MinilessonStats> listMLStatsForCourseMinilessons(Course course) {
		List<MinilessonStats> list = JPA.getEntityManager().createQuery("SELECT s FROM Course c, MinilessonStats s JOIN s.minilesson m WHERE c=:co AND m MEMBER OF c.minilessons AND m.visible=true AND m.archived=false AND m.approval in (:ap1, :ap2) ORDER BY m.uploadedOn", MinilessonStats.class)
				.setParameter("ap1", MinilessonApproval.APPROVED)
				.setParameter("ap2", MinilessonApproval.SOFT_WAITING)
				.setParameter("co", course)
				.getResultList();
		return list;
	}

	@Override
	public List<MinilessonStats> listMLStatsForMinilesson(Minilesson ml) {
		List<MinilessonStats> list = JPA.getEntityManager().createQuery("SELECT s FROM MinilessonStats s JOIN s.minilesson m WHERE m=:ml AND m.visible=true AND m.archived=false AND m.approval in (:ap1, :ap2) ORDER BY m.uploadedOn", MinilessonStats.class)
				.setParameter("ap1", MinilessonApproval.APPROVED)
				.setParameter("ap2", MinilessonApproval.SOFT_WAITING)
				.setParameter("ml", ml)
				.getResultList();
		return list;
	}

	@Override
	public Minilesson findNewestVisibleMinilesson(String publicID) {
		List<Minilesson> list = JPA.getEntityManager().createQuery("SELECT m FROM Minilesson m WHERE m.publicID=:pid AND m.archived=false AND m.visible=true", Minilesson.class)
				.setParameter("pid", publicID)
				.getResultList();
		if(list.isEmpty()) return null;
		if(list.size()>1) {
			list.sort((o1,o2)->{int r = Long.compare(o2.getVersion(), o1.getVersion()); if(r!=0) return r;  return Long.compare(o2.getMinorVersion(), o1.getMinorVersion()); });
		}
		return list.get(0);
	}

	@Override
	public CommonQuestionDefinition findNewestVisibleQuestionDefinition(String publicID) {
		List<CommonQuestionDefinition> list = JPA.getEntityManager().createQuery("SELECT m FROM CommonQuestionDefinition m WHERE m.publicID=:pid AND m.archived=false AND m.visible=true AND m.publicQuestion=true", CommonQuestionDefinition.class)
				.setParameter("pid", publicID)
				.getResultList();
		if(list.isEmpty()) return null;
		if(list.size()>1) {
			list.sort((o1,o2)->{int r = Long.compare(o2.getVersion(), o1.getVersion()); if(r!=0) return r;  return Long.compare(o2.getMinorVersion(), o1.getMinorVersion()); });
		}
		return list.get(0);
	}
	
	@Override
	public List<Minilesson> listAllMinilessonsForOwner(User user) {
		return JPA.getEntityManager().createQuery("SELECT m FROM Minilesson m WHERE m.owner.id=:uid AND m.archived=false", Minilesson.class)
				.setParameter("uid", user.getId())
				.getResultList();
	}

	@Override
	public List<Minilesson> listAllMinilessonVersions(String publicID) {
		return JPA.getEntityManager().createQuery("SELECT m FROM Minilesson m WHERE m.publicID=:pid", Minilesson.class)
				.setParameter("pid", publicID)
				.getResultList();
	}
	
	@Override
	public List<JSQuestionDefinition> listAllJSQuestionDefinitionVersions(String publicID) {
		return JPA.getEntityManager().createQuery("SELECT q FROM JSQuestionDefinition q WHERE q.publicID=:pid", JSQuestionDefinition.class)
				.setParameter("pid", publicID)
				.getResultList();
	}

	@Override
	public List<XMLQuestionDefinition> listAllXMLQuestionDefinitionVersions(String publicID) {
		return JPA.getEntityManager().createQuery("SELECT q FROM XMLQuestionDefinition q WHERE q.publicID=:pid", XMLQuestionDefinition.class)
				.setParameter("pid", publicID)
				.getResultList();
	}
	
	@Override
	public JSQuestionDefinition findJSQuestionDefinitionByID(String id) {
		return JPA.getEntityManager().find(JSQuestionDefinition.class, id);
	}
	
	@Override
	public CommonQuestionDefinition findCommonQuestionDefinitionByID(String id) {
		return JPA.getEntityManager().find(CommonQuestionDefinition.class, id);
	}
	
	@Override
	public JSQuestionInstance findJSQuestionInstanceByID(Long id) {
		return JPA.getEntityManager().find(JSQuestionInstance.class, id);
	}
	
	@Override
	public List<JSQuestionDefinition> findVisibleJSQuestionDefinitions() {
		return JPA.getEntityManager().createQuery("SELECT q FROM JSQuestionDefinition q WHERE q.visible=true AND q.archived=false", JSQuestionDefinition.class)
				.getResultList();
	}
	
	@Override
	public List<JSQuestionDefinition> findVisiblePublicJSQuestionDefinitions(User owner) {
		if(owner==null) {
			return JPA.getEntityManager().createQuery("SELECT q FROM JSQuestionDefinition q WHERE q.visible=true AND q.archived=false AND q.publicQuestion=true", JSQuestionDefinition.class)
					.getResultList();
		} else {
			return JPA.getEntityManager().createQuery("SELECT q FROM JSQuestionDefinition q WHERE q.visible=true AND q.archived=false AND (q.publicQuestion=true OR q.owner=:owner)", JSQuestionDefinition.class)
					.setParameter("owner", owner)
					.getResultList();
		}
	}
	
	@Override
	public List<CommonQuestionDefinition> findVisiblePublicCommonQuestionDefinitions(User owner) {
		if(owner==null) {
			return JPA.getEntityManager().createQuery("SELECT q FROM CommonQuestionDefinition q WHERE q.visible=true AND q.archived=false AND q.publicQuestion=true", CommonQuestionDefinition.class)
					.getResultList();
		} else {
			return JPA.getEntityManager().createQuery("SELECT q FROM CommonQuestionDefinition q WHERE q.visible=true AND q.archived=false AND (q.publicQuestion=true OR q.owner=:owner)", CommonQuestionDefinition.class)
					.setParameter("owner", owner)
					.getResultList();
		}
	}
	
	@Override
	public List<String> findUnprocessedMinilessons() {
		return JPA.getEntityManager().createQuery("SELECT DISTINCT ml.id FROM MinilessonUserGrade mug JOIN mug.minilesson ml WHERE mug.unprocessed=true", String.class).getResultList();
	}
	
	@Override
	public List<MinilessonUserGrade> findMinilessonUserGrades(Long userID) {
		return JPA.getEntityManager().createQuery("SELECT mug FROM MinilessonUserGrade mug where mug.user.id=:uid", MinilessonUserGrade.class)
			.setParameter("uid", userID)
			.getResultList();
	}
	
	@Override
	public MinilessonUserGrade findMinilessonUserGrades(Minilesson minilesson, User user) {
		return JPA.getEntityManager().find(MinilessonUserGrade.class, new MinilessonUserGradePK(minilesson.getId(), user.getId()));
	}
	
	@Override
	public List<MinilessonUserGrade> findMinilessonUserGrades(String minilessonID) {
		return JPA.getEntityManager().createQuery("SELECT mug FROM MinilessonUserGrade mug where mug.minilesson.id=:mid", MinilessonUserGrade.class)
				.setParameter("mid", minilessonID)
				.getResultList();
	}

	@Override
	public void saveMinilessonUserGrade(MinilessonUserGrade mug) {
		JPA.getEntityManager().persist(mug);
	}
	
	@Override
	public MinilessonStats findMinilessonStats(Minilesson minilesson) {
		List<MinilessonStats> list = JPA.getEntityManager().createQuery("SELECT s FROM MinilessonStats s WHERE s.minilesson.id=:mlid", MinilessonStats.class)
				.setParameter("mlid", minilesson.getId())
				.getResultList();
		return list.isEmpty() ? null : list.get(0);
	}
	
	@Override
	public Wish findWish(Long id) {
		return JPA.getEntityManager().find(Wish.class, id);
	}
	
	@Override
	public List<Long> findUnprocessedWishes() {
		return JPA.getEntityManager().createQuery("SELECT DISTINCT w.id FROM WishUserVote wuv JOIN wuv.wish w WHERE wuv.unprocessed=true", Long.class).getResultList();
	}
	
	@Override
	public List<WishUserVote> findWishVotes(Long wishID) {
		return JPA.getEntityManager().createQuery("SELECT wuv FROM WishUserVote wuv WHERE wuv.wish.id=:id", WishUserVote.class).setParameter("id", wishID).getResultList();
	}
	
	@Override
	public void saveNewWish(Wish wish) {
		JPA.getEntityManager().persist(wish);
	}
	
	@Override
	public List<Wish> listAllWishes() {
		return JPA.getEntityManager().createQuery("SELECT w FROM Wish w", Wish.class).getResultList();
	}
	
	@Override
	public List<WishWithUserVote> listAllWishesEx(Long userID) {
		return JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.dao.model.WishWithUserVote(w,v) FROM Wish w LEFT OUTER JOIN WishUserVote v ON w.id=v.wish.id and v.user.id=:uid", WishWithUserVote.class).setParameter("uid", userID).getResultList();
	}
	
	@Override
	public WishUserVote findWishUserVote(Long wishID, Long userID) {
		return JPA.getEntityManager().find(WishUserVote.class, new WishUserVotePK(wishID, userID));
	}
	
	@Override
	public void saveWishUserVote(WishUserVote wuv) {
		JPA.getEntityManager().persist(wuv);
	}
	
	@Override
	public int countRegistrationsForDate(String date) {
		Number number = JPA.getEntityManager().createQuery("SELECT COUNT(*) FROM UserRegistration ur where ur.dateKey=:dk", Number.class)
				.setParameter("dk", date)
				.getSingleResult();
		return number.intValue();
	}

	@Override
	public int countRegistrationsForDate(String date, String ip) {
		Number number = JPA.getEntityManager().createQuery("SELECT COUNT(*) FROM UserRegistration ur where ur.dateKey=:dk and ur.requestIP=:ip", Number.class)
				.setParameter("dk", date)
				.setParameter("ip", ip)
				.getSingleResult();
		return number.intValue();
	}
	
	@Override
	public void saveUserRegistration(UserRegistration ureg) {
		JPA.getEntityManager().persist(ureg);
	}
	
	@Override
	public UserRegistration findUserRegistration(Long id) {
		return JPA.getEntityManager().find(UserRegistration.class, id);
	}
	
	@Override
	public List<MLUserStat> getUserMinilessonBasicStats(String minilessonID) {
		return JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.dao.model.MLUserStat(u.jmbag, u.firstName, u.lastName, m.openedAt, m.completedOn) FROM MinilessonStatus m INNER JOIN m.user u where m.minilesson.id=:mid", MLUserStat.class)
				.setParameter("mid", minilessonID)
				.getResultList();
	}
	
	@Override
	public ItemComment findItemComment(Long commentID) {
		return JPA.getEntityManager().find(ItemComment.class, commentID);
	}
	
	@Override
	public ItemThread findItemThread(Long threadID) {
		return JPA.getEntityManager().find(ItemThread.class, threadID);
	}
	
	@Override
	public void saveItemComment(ItemComment itemComment) {
		JPA.getEntityManager().persist(itemComment);
	}
	
	@Override
	public void saveItemThread(ItemThread thread) {
		JPA.getEntityManager().persist(thread);
	}
	
	@Override
	public List<ItemComment> listItemComments(Long threadID) {
		return JPA.getEntityManager().createQuery("SELECT c FROM ItemComment c WHERE c.itemThread.id=:tid ORDER BY c.postedOn", ItemComment.class)
				.setParameter("tid", threadID)
				.getResultList();
	}
	
	@Override
	public List<ItemThread> listMinilessonItemThreads(String minilessonID, String itemID) {
		return JPA.getEntityManager().createQuery("SELECT t FROM ItemThread t WHERE t.minilesson.id=:mid AND t.item=:iid ORDER BY t.postedOn", ItemThread.class)
				.setParameter("mid", minilessonID)
				.setParameter("iid", itemID)
				.getResultList();
	}
	
	@Override
	public void saveMinilessonUserSharedData(MinilessonUserSharedData sharedData) {
		JPA.getEntityManager().persist(sharedData);
	}
	
	@Override
	public List<CourseCollection> findAllCourseCollections() {
		return JPA.getEntityManager().createQuery("SELECT cc FROM CourseCollection cc", CourseCollection.class).getResultList();
	}
	
	@Override
	public Course findCourseByID(String id) {
		return JPA.getEntityManager().find(Course.class, id);
	}
	
	@Override
	public CourseCollection findCourseCollectionByID(String id) {
		return JPA.getEntityManager().find(CourseCollection.class, id);
	}
	
	@Override
	public List<UserCourseRole> findUserCourseRoleFor(Course course) {
		return JPA.getEntityManager().createQuery("SELECT cc FROM UserCourseRole cc where cc.id.course_id=:cid", UserCourseRole.class).setParameter("cid", course.getId()).getResultList();
	}
	
	@Override
	public List<UserCourseRole> findUserCourseRoleFor(User user) {
		return JPA.getEntityManager().createQuery("SELECT cc FROM UserCourseRole cc where cc.id.user_id=:uid", UserCourseRole.class).setParameter("uid", user.getId()).getResultList();
	}
	
	@Override
	public void saveCourse(Course course) {
		JPA.getEntityManager().persist(course);
	}
	
	@Override
	public void saveCourseCollection(CourseCollection cc) {
		JPA.getEntityManager().persist(cc);
	}
	
	@Override
	public void saveUserCourseRole(UserCourseRole ucr) {
		JPA.getEntityManager().persist(ucr);
	}
	
	@Override
	public List<Course> listAllCourses() {
		return JPA.getEntityManager().createQuery("SELECT c FROM Course c", Course.class).getResultList();
	}
	
	@Override
	public Workspace findWorkspaceByID(Long id) {
		return JPA.getEntityManager().find(Workspace.class, id);
	}
	
	@Override
	public WorkspaceJoinRequest findWorkspaceJoinRequestByID(Long id) {
		return JPA.getEntityManager().find(WorkspaceJoinRequest.class, id);
	}
	
	@Override
	public void saveWorkspace(Workspace ws) {
		JPA.getEntityManager().persist(ws);
	}
	
	@Override
	public void saveWorkspaceJoinRequest(WorkspaceJoinRequest req) {
		JPA.getEntityManager().persist(req);
	}
	
	@Override
	public List<WorkspaceJoinRequest> findWorkspaceJoinRequests(Workspace ws, User user) {
		return JPA.getEntityManager().createQuery("SELECT req FROM WorkspaceJoinRequest req WHERE req.workspace=:ws AND req.user=:user", WorkspaceJoinRequest.class)
				.setParameter("ws", ws)
				.setParameter("user", user)
				.getResultList();
	}
	
	@Override
	public List<Workspace> findUserWorkspaces(User user) {
		return JPA.getEntityManager().createQuery("SELECT ws FROM Workspace ws WHERE :user MEMBER OF ws.users OR ws.owner=:user", Workspace.class)
				.setParameter("user", user)
				.getResultList();
	}
	
	@Override
	public List<Workspace> findJoinableWorkspaces(User user) {
		return JPA.getEntityManager().createQuery("SELECT ws FROM Workspace ws WHERE :user NOT MEMBER OF ws.users AND ws.joinPolicy <> :jps", Workspace.class)
				.setParameter("user", user)
				.setParameter("jps", WorkspaceJoinPolicy.CLOSED)
				.getResultList();
	}
	
	@Override
	public void removeWorkspaceJoinRequest(WorkspaceJoinRequest req) {
		JPA.getEntityManager().remove(req);
	}
	
	@Override
	public List<WorkspaceJoinRequest> findUnsolvedWorkspaceJoinRequests(Workspace ws) {
		return JPA.getEntityManager().createQuery("SELECT wsjr FROM WorkspaceJoinRequest wsjr WHERE wsjr.status=:wsjrstat AND wsjr.workspace=:ws", WorkspaceJoinRequest.class)
				.setParameter("ws", ws)
				.setParameter("wsjrstat", WSJoinRequestStatus.OPEN)
				.getResultList();
	}
	
	@Override
	public void saveWSGroup(WSGroup gr) {
		JPA.getEntityManager().persist(gr);
	}
	
	@Override
	public void saveWSGrouping(WSGrouping wsg) {
		JPA.getEntityManager().persist(wsg);
	}

	@Override
	public WSGrouping findWSGroupingByID(Long id) {
		return JPA.getEntityManager().find(WSGrouping.class, id);
	}
	
	@Override
	public WSGroup findWSGroupByID(Long id) {
		return JPA.getEntityManager().find(WSGroup.class, id);
	}
	
	@Override
	public List<WSGrouping> findGroupingsForWorkspace(Workspace ws) {
		return JPA.getEntityManager().createQuery("SELECT gr FROM WSGrouping gr WHERE gr.workspace=:ws", WSGrouping.class)
			.setParameter("ws", ws)
			.getResultList();
	}
	
	@Override
	public List<WSGroup> findWSGroupsForWSGrouping(WSGrouping wsg, boolean onlyActive) {
		if(onlyActive) {
			return JPA.getEntityManager().createQuery("SELECT gr FROM WSGroup gr WHERE gr.grouping=:wsg AND gr.active=true", WSGroup.class)
					.setParameter("wsg", wsg)
					.getResultList();
		} else {
			return JPA.getEntityManager().createQuery("SELECT gr FROM WSGroup gr WHERE gr.grouping=:wsg", WSGroup.class)
					.setParameter("wsg", wsg)
					.getResultList();
		}
	}
	
	@Override
	public List<WSGroup> findActiveWSGroupsForWSGroupingAndUser(WSGrouping wsg, User user) {
		return JPA.getEntityManager().createQuery("SELECT gr FROM WSGroup gr WHERE gr.grouping=:wsg AND gr.active=true AND :us MEMBER OF gr.users", WSGroup.class)
				.setParameter("wsg", wsg)
				.setParameter("us", user)
				.getResultList();
	}
	
	@Override
	public List<WSGroup> findEmptyActiveNonfixedWSGroups(WSGrouping gr) {
		return JPA.getEntityManager().createQuery("SELECT gr FROM WSGroup gr WHERE gr.grouping=:wsg AND gr.fixed=false AND gr.active=true AND gr.users IS EMPTY", WSGroup.class)
				.setParameter("wsg", gr)
				.getResultList();
	}
	
	@Override
	public List<WSActivity> findActivitiesForWorkspace(Workspace ws) {
		return JPA.getEntityManager().createQuery("SELECT ac FROM WSActivity ac WHERE ac.workspace=:ws", WSActivity.class)
				.setParameter("ws", ws)
				.getResultList();
	}
	
	@Override
	public void saveWSActivity(WSActivity activity) {
		JPA.getEntityManager().persist(activity);
	}
	
	@Override
	public void saveXMLQuestionDefinition(XMLQuestionDefinition def) {
		JPA.getEntityManager().persist(def);
	}
	
	@Override
	public List<DXMLQDef> listXMLQuestionsForOwner(User user, String lang) {
		return JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.domain.model.DXMLQDef(qd.id, qdt, qd.uploadedOn, qd.owner.id, qd.owner.lastName, qd.owner.firstName, qd.owner.authDomain) FROM XMLQuestionDefinition qd, in (qd.titles) qdt WHERE qd.owner=:u and key(qdt)=:lang", DXMLQDef.class)
				.setParameter("u", user)
				.setParameter("lang", lang)
				.getResultList();
	}
	
	@Override
	public WSActivity findWSActivityByID(Long id) {
		return JPA.getEntityManager().find(WSActivity.class, id);
	}
	
	@Override
	public XMLQuestionDefinition findXMLQuestionDefinition(String id) {
		return JPA.getEntityManager().find(XMLQuestionDefinition.class, id);
	}
	
	@Override
	public void saveWSQuestionDefinition(WSQuestionDefinition wsqd) {
		JPA.getEntityManager().persist(wsqd);
	}
	
	@Override
	public List<DWSQuestionDefinition> listDWSQuestionDefinitions(Workspace ws, WorkspacePage pg, String lang) {
//		return JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.domain.model.DWSQuestionDefinition(qd.id, qdt, qd.createdAt, qd.maxScore, qd.workflow, qd.workflowCurrentState, qd.subpage, grt, act, qd.groupAssignment) FROM WSQuestionDefinition qd LEFT OUTER JOIN qd.activity a LEFT OUTER JOIN qd.grouping g LEFT OUTER JOIN qd.titles qdt LEFT OUTER JOIN a.titles act, in (g.titles) grt WHERE qd.workspace=:ws and qd.subpage=:pg and key(qdt)=:lang and key(act)=:lang and key(grt)=:lang", DWSQuestionDefinition.class)
		return JPA.getEntityManager().createQuery("SELECT new hr.fer.zemris.minilessons.domain.model.DWSQuestionDefinition(qd.id, qdt, qd.createdAt, qd.maxScore, qd.workflow, qd.workflowCurrentState, qd.subpage, qd.groupAssignment, s.id, s.maxScore, s.invalid) FROM WSQuestionDefinition qd LEFT OUTER JOIN qd.source s, IN (qd.titles) qdt WHERE qd.workspace=:ws and qd.subpage=:pg and key(qdt)=:lang", DWSQuestionDefinition.class)
				.setParameter("ws", ws)
				.setParameter("pg", pg)
				.setParameter("lang", lang)
				.getResultList();
	}
	
	@Override
	public WSQuestionDefinition findWSQuestionDefinition(Long id) {
		return JPA.getEntityManager().find(WSQuestionDefinition.class, id);
	}
	
	@Override
	public List<WSQuestionDefinition> findActiveQuestions(Workspace ws, WorkspacePage subpage) {
		return JPA.getEntityManager().createQuery("SELECT qd FROM WSQuestionDefinition qd WHERE qd.workspace=:ws and qd.subpage=:pg and qd.workflowCurrentState in (:st1,:st2,:st3,:st4,:st5,:st6)", WSQuestionDefinition.class)
				.setParameter("ws", ws)
				.setParameter("pg", subpage)
				.setParameter("st1", WSQDWorkflowState.OPEN)
				.setParameter("st2", WSQDWorkflowState.VERIFY)
				.setParameter("st3", WSQDWorkflowState.INSPECT)
				.setParameter("st4", WSQDWorkflowState.AUTO)
				.setParameter("st5", WSQDWorkflowState.AUTOREP)
				.setParameter("st6", WSQDWorkflowState.AUTOSUB)
				.getResultList();
	}
	
	@Override
	public List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, User u) {
		return JPA.getEntityManager().createQuery("SELECT qi FROM WSQuestionInstance qi WHERE qi.definition=:qd and qi.user=:u", WSQuestionInstance.class)
				.setParameter("qd", qd)
				.setParameter("u", u)
				.getResultList();
	}
	
	@Override
	public List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, WSGroup g) {
		return JPA.getEntityManager().createQuery("SELECT qi FROM WSQuestionInstance qi WHERE qi.definition=:qd and qi.group=:g", WSQuestionInstance.class)
				.setParameter("qd", qd)
				.setParameter("g", g)
				.getResultList();
	}
	
	@Override
	public void saveWSQuestionInstance(WSQuestionInstance wsqi) {
		JPA.getEntityManager().persist(wsqi);
	}
	
	@Override
	public void saveXMLQuestionInstance(XMLQuestionInstance qi) {
		JPA.getEntityManager().persist(qi);
	}
	
	@Override
	public WSQuestionInstance findWSQuestionInstanceByID(Long id) {
		return JPA.getEntityManager().find(WSQuestionInstance.class, id);
	}
	
	@Override
	public XMLQuestionInstance findXMLQuestionInstanceByID(Long id) {
		return JPA.getEntityManager().find(XMLQuestionInstance.class, id);
	}
	
	@Override
	public List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, boolean additional) {
		return JPA.getEntityManager().createQuery("SELECT qi FROM WSQuestionInstance qi WHERE qi.definition=:qd and qi.additional=:ad", WSQuestionInstance.class)
				.setParameter("qd", qd)
				.setParameter("ad", additional)
				.getResultList();
	}
	
	@Override
	public void saveWSActivityScore(WSActivityScore score) {
		JPA.getEntityManager().persist(score);
	}
	
	@Override
	public void saveWSActivitySource(WSActivitySource src) {
		JPA.getEntityManager().persist(src);
	}
	
	@Override
	public WSActivityScore findWSActivityScore(WSActivityScorePK pk) {
		return JPA.getEntityManager().find(WSActivityScore.class, pk);
	}
	
	@Override
	public List<WSActivityScore> findWSActivityScores(Workspace ws, User user) {
		return JPA.getEntityManager().createQuery("SELECT sc FROM WSActivityScore sc WHERE sc.source.activity.workspace=:ws and sc.user=:user", WSActivityScore.class)
				.setParameter("ws", ws)
				.setParameter("user", user)
				.getResultList();
	}
	
	@Override
	public List<WSActivitySource> findAllWSActivitySources(Workspace ws) {
		return JPA.getEntityManager().createQuery("SELECT src FROM WSActivitySource src WHERE src.activity.workspace=:ws", WSActivitySource.class)
				.setParameter("ws", ws)
				.getResultList();
	}
	
	@Override
	public void saveXMLQuestionDefinitionAttachment(XMLQuestionDefinitionAttachment a) {
		JPA.getEntityManager().persist(a);
	}
	
	@Override
	public void removeXMLQuestionDefinitionAttachment(XMLQuestionDefinitionAttachment a) {
		JPA.getEntityManager().remove(a);
	}
	
	@Override
	public XMLQuestionDefinitionAttachment findXMLQuestionDefinitionAttachment(XMLQuestionDefinition definition, String filename) {
		List<XMLQuestionDefinitionAttachment> list = JPA.getEntityManager().createQuery("SELECT a FROM XMLQuestionDefinitionAttachment a WHERE a.question=:def and a.name=:name", XMLQuestionDefinitionAttachment.class)
				.setParameter("def", definition)
				.setParameter("name", filename)
				.getResultList();
		if(list.isEmpty() || list.size()>1) return null;
		return list.get(0);
	}
	
	@Override
	public List<XMLQuestionDefinitionAttachment> findAllXMLQuestionDefinitionAttachments(
			XMLQuestionDefinition definition) {
		List<XMLQuestionDefinitionAttachment> list = JPA.getEntityManager().createQuery("SELECT a FROM XMLQuestionDefinitionAttachment a WHERE a.question=:def", XMLQuestionDefinitionAttachment.class)
				.setParameter("def", definition)
				.getResultList();
		return list;
	}
	
	@Override
	public List<WSActivityScore> listAllWorkspaceValidActivityUserScores(Workspace ws) {
		return JPA.getEntityManager().createQuery("SELECT sco FROM WSActivityScore sco JOIN sco.source src JOIN src.activity act JOIN act.workspace ws, IN(ws.users) u WHERE sco.user=u AND ws=:ws AND sco.pending=false AND src.invalid=false", WSActivityScore.class)
				.setParameter("ws", ws)
				.getResultList();
	}
	
	@Override
	public List<JSQuestionDefinition> listAllJSQuestionsForOwner(User owner) {
		return JPA.getEntityManager().createQuery("SELECT q FROM JSQuestionDefinition q WHERE q.owner=:owner", JSQuestionDefinition.class)
				.setParameter("owner", owner)
				.getResultList();
	}
	
	@Override
	public List<CommonQuestionDefinition> listAllCommonQuestionsForOwner(User owner) {
		return JPA.getEntityManager().createQuery("SELECT q FROM CommonQuestionDefinition q WHERE q.owner=:owner", CommonQuestionDefinition.class)
				.setParameter("owner", owner)
				.getResultList();
	}
}
