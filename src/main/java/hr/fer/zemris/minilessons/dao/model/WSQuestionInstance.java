package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="wsq_instances")
public class WSQuestionInstance {

	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private WSQuestionDefinition definition;
	
	@Column(length=40, nullable=false)
	private String actualInstance;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=true)
	private WSGroup group;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=true)
	private User user;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=true)
	private WSQuestionInstance toControl;
	
	@Enumerated(EnumType.STRING)
	@Column(length=10)
	private QuestionControlResult controlResult;
	
	private Boolean controllerCorrect;

	private boolean additional;
	
	@Enumerated(EnumType.STRING)
	@Column(length=10, nullable=false)
	private QIStatus status;

	private double correctnessMeasure;
	
	private boolean solved;
	
	public WSQuestionInstance() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getControllerCorrect() {
		return controllerCorrect;
	}
	public void setControllerCorrect(Boolean controllerCorrect) {
		this.controllerCorrect = controllerCorrect;
	}
	
	public QuestionControlResult getControlResult() {
		return controlResult;
	}
	public void setControlResult(QuestionControlResult controlResult) {
		this.controlResult = controlResult;
	}
	
	public boolean isSolved() {
		return solved;
	}
	public void setSolved(boolean solved) {
		this.solved = solved;
	}
	
	public double getCorrectnessMeasure() {
		return correctnessMeasure;
	}
	public void setCorrectnessMeasure(double correctnessMeasure) {
		this.correctnessMeasure = correctnessMeasure;
	}
	
	public QIStatus getStatus() {
		return status;
	}
	public void setStatus(QIStatus status) {
		this.status = status;
	}
	
	public WSQuestionDefinition getDefinition() {
		return definition;
	}

	public void setDefinition(WSQuestionDefinition definition) {
		this.definition = definition;
	}

	public String getActualInstance() {
		return actualInstance;
	}

	public void setActualInstance(String actualInstance) {
		this.actualInstance = actualInstance;
	}

	public WSGroup getGroup() {
		return group;
	}

	public void setGroup(WSGroup group) {
		this.group = group;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public WSQuestionInstance getToControl() {
		return toControl;
	}

	public void setToControl(WSQuestionInstance toControl) {
		this.toControl = toControl;
	}

	public boolean isAdditional() {
		return additional;
	}

	public void setAdditional(boolean additional) {
		this.additional = additional;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WSQuestionInstance))
			return false;
		WSQuestionInstance other = (WSQuestionInstance) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
