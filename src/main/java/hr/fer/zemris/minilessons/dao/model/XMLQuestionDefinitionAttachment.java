package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="xmlquestion_atts")
public class XMLQuestionDefinitionAttachment {

	@Id @GeneratedValue
	private Long id;
	
	@Column(length=100, nullable=false)
	private String name;
	
	@Lob
	@Column(length=128*1024, nullable=false)
	private byte[] body;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private XMLQuestionDefinition question;
	
	public XMLQuestionDefinitionAttachment() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public XMLQuestionDefinition getQuestion() {
		return question;
	}
	public void setQuestion(XMLQuestionDefinition question) {
		this.question = question;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof XMLQuestionDefinitionAttachment))
			return false;
		XMLQuestionDefinitionAttachment other = (XMLQuestionDefinitionAttachment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
