package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * This class is used to track user given grade for minilesson
 * (i.e. users can grade the quality of minilessons).
 * 
 * @author marcupic
 */
@Entity
@Table(name="ml_user_grades")
@IdClass(MinilessonUserGradePK.class)
public class MinilessonUserGrade {

	/**
	 * Minilesson which grade is tracked.
	 */
	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	private Minilesson minilesson;
	/**
	 * User whose grade is tracked.
	 */
	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	private User user;
	/**
	 * User-given quality grade. Zero means: not graded. Grades can be from 1 (bad) to 5 (excellent).
	 */
	private int qualityGrade;
	/**
	 * User-given difficulty grade. Zero means: not graded. Grades can be from 1 (easy) to 5 (extremely hard).
	 */
	private int difficultyGrade;
	/**
	 * Time, in minutes, that user entered (this is user-perceived entry) as the time
	 * it took him to complete this minilesson. Used for statistics. 
	 */
	private int duration;
	/**
	 * Boolean flag which is set to true each time grade or duration is changed.
	 * System demon service will periodically check for all unprocessed records,
	 * recalculate statistics and clear this flag.
	 */
	private boolean unprocessed;
	/**
	 * Optimistic-lock version.
	 */
	@Version
	private int olversion;
	
	/**
	 * Constructor.
	 */
	public MinilessonUserGrade() {
	}

	/**
	 * Constructor.
	 * 
	 * @param minilesson minilesson
	 * @param user user
	 */
	public MinilessonUserGrade(Minilesson minilesson, User user) {
		super();
		this.minilesson = minilesson;
		this.user = user;
	}

	/**
	 * Getter for minilesson which status is tracked.
	 * 
	 * @return minilesson
	 */
	public Minilesson getMinilesson() {
		return minilesson;
	}
	/**
	 * Setter for minilesson which status is tracked.
	 * 
	 * @param minilesson minilesson
	 */
	public void setMinilesson(Minilesson minilesson) {
		this.minilesson = minilesson;
	}

	/**
	 * Getter for user whose status is tracked.
	 * 
	 * @return user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * Setter for user whose status is tracked.
	 * 
	 * @param user user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Getter for user-given quality grade. Grades range from 1 to 5. 0 is interpreted as: not set.
	 * @return grade
	 */
	public int getQualityGrade() {
		return qualityGrade;
	}
	/**
	 * Setter for user-given quality grade.
	 * @param qualityGrade grade
	 */
	public void setQualityGrade(int qualityGrade) {
		this.qualityGrade = qualityGrade;
	}
	
	/**
	 * Getter for user-given difficulty grade. Grades range from 1 to 5. 0 is interpreted as: not set.
	 * @return grade
	 */
	public int getDifficultyGrade() {
		return difficultyGrade;
	}
	/**
	 * Setter for user-given difficulty grade.
	 * @param difficultyGrade grade
	 */
	public void setDifficultyGrade(int difficultyGrade) {
		this.difficultyGrade = difficultyGrade;
	}

	/**
	 * Getter for duration. Zero is interpreted as not set. Time is in minutes.
	 * @return duration duration
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * Setter for duration.
	 * @param duration duration
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * Getter for unprocessed flag.
	 * @return unprocessed flag
	 */
	public boolean isUnprocessed() {
		return unprocessed;
	}
	/**
	 * Setter for unprocessed flag.
	 * @param unprocessed unprocessed
	 */
	public void setUnprocessed(boolean unprocessed) {
		this.unprocessed = unprocessed;
	}
	
	/**
	 * Getter for optimistic lock version.
	 * @return version
	 */
	public int getOlversion() {
		return olversion;
	}
	/**
	 * Setter for optimistic lock version.
	 * @param olversion version
	 */
	public void setOlversion(int olversion) {
		this.olversion = olversion;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((minilesson == null) ? 0 : minilesson.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinilessonUserGrade other = (MinilessonUserGrade) obj;
		if (minilesson == null) {
			if (other.minilesson != null)
				return false;
		} else if (!minilesson.equals(other.minilesson))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	
}
