package hr.fer.zemris.minilessons.dao.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="wsgroups")
public class WSGroup implements Comparable<WSGroup> {

	@Id @GeneratedValue
	private Long id;
	
	@Column(length=50, nullable=false)
	private String name;

	@Column(length=10, nullable=false)
	private String secret;
	/**
	 * Je li grupa zaključana za izmjene. 
	 */
	private boolean fixed;
	
	/**
	 * Je li ovo aktivna grupa.
	 */
	private boolean active;

	/**
	 * Grupa od strane vlasnika radnog prostora može biti označena kao
	 * validirana, pa se u tom slučaju uzima u obzir u situacijama u kojima
	 * se inače nevaljale grupe ne uzimaju u obzir.
	 */
	private boolean validated;
	
	@Version
	private int version;

	@ManyToMany
	@JoinTable(
		name="wsgroup_users",
		joinColumns=@JoinColumn(name="wsg_id", referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="u_id", referencedColumnName="id")
	)
	private Set<User> users = new HashSet<>();

	@ManyToOne
	@JoinColumn(nullable=false)
	private WSGrouping grouping;
	
	public WSGroup() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isValidated() {
		return validated;
	}
	
	public void setValidated(boolean validated) {
		this.validated = validated;
	}
	
	public WSGrouping getGrouping() {
		return grouping;
	}
	public void setGrouping(WSGrouping grouping) {
		this.grouping = grouping;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WSGroup))
			return false;
		WSGroup other = (WSGroup) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(WSGroup o) {
		return this.name.compareTo(o.name);
	}
}
