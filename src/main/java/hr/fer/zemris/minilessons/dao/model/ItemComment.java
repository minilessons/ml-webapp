package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name="item_comments")
public class ItemComment {

	/**
	 * ID.
	 */
	@Id @GeneratedValue
	private Long id;
	
	/**
	 * User who posted this comment.
	 */
	@ManyToOne
	@JoinColumn(name="owner_id", nullable=false)
	private User owner;
	
	/**
	 * When was this comment posted?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date postedOn;

	/**
	 * In which thread does this comment belongs?
	 * Can not be <code>null</code>.
	 */
	@ManyToOne
	@JoinColumn(nullable=false)
	private ItemThread itemThread;
	
	/**
	 * Optimistic-lock version.
	 */
	@Version
	private long olversion;

	@Lob
	@Column(length=1024*10,nullable=false)
	private String text;

	/**
	 * How to interpret comment text. Must have a value
	 * which is recognized by the system and for which
	 * system offers format renderer. Legal values
	 * should be obtained by {@link SupportedTextFormat#getFormatName()}.
	 */
	@Column(length=10,nullable=false)
	private String textFormat;
	
	/**
	 * Is this comment hidden? Only owner of minilesson (or admin) can make comment hidden.
	 */
	private boolean hidden;
	
	/**
	 * Is this comment marked as deleted? Only owner of minilesson (or admin) can make comment hidden.
	 */
	private boolean deleted;
	
	public ItemComment() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Date getPostedOn() {
		return postedOn;
	}

	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	public ItemThread getItemThread() {
		return itemThread;
	}

	public void setItemThread(ItemThread itemThread) {
		this.itemThread = itemThread;
	}

	public long getOlversion() {
		return olversion;
	}

	public void setOlversion(long olversion) {
		this.olversion = olversion;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTextFormat() {
		return textFormat;
	}
	
	public void setTextFormat(String textFormat) {
		this.textFormat = textFormat;
	}
	
	public boolean isHidden() {
		return hidden;
	}
	
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ItemComment))
			return false;
		ItemComment other = (ItemComment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
