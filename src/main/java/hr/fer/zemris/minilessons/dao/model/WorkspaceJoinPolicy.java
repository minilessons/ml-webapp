package hr.fer.zemris.minilessons.dao.model;

public enum WorkspaceJoinPolicy {

	/**
	 * Prijava se automatski obrađuje i prihvaća.
	 */
	AUTOMATIC,
	/**
	 * Prijava se prihvaća ili odbija kako i kada to vlasnik radnog prostora odluči.
	 */
	APPROVAL,
	/**
	 * Prijave nisu moguće.
	 */
	CLOSED
}
