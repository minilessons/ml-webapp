package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ml_shared_statuses")
public class MinilessonUserSharedData {

	/**
	 * Minilesson User Shared Data ID (primary key).
	 */
	@Id @GeneratedValue
	private Long id;

	@Column(length=1024*128, nullable=true)
	private String sharedData;
	
	public MinilessonUserSharedData() {
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSharedData() {
		return sharedData;
	}
	public void setSharedData(String sharedData) {
		this.sharedData = sharedData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MinilessonUserSharedData))
			return false;
		MinilessonUserSharedData other = (MinilessonUserSharedData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
