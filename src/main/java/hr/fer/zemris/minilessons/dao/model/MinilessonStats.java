package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Statistics for this minilesson.
 * 
 * @author marcupic
 *
 */
/**
 * @author marcupic
 *
 */
@Entity
@Table(name="ml_stats")
@Cacheable(true)
public class MinilessonStats {
	/**
	 * ID.
	 */
	@Id @GeneratedValue
	private Long id;
	/**
	 * Minilesson for which this is statistics. Field is unique.
	 */
	@OneToOne(fetch=FetchType.EAGER)
	private Minilesson minilesson;

	/**
	 * Statistical data for minilesson quality.
	 */
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="median", column=@Column(name="qualMed")),
		@AttributeOverride(name="average", column=@Column(name="qualAvg")),
		@AttributeOverride(name="stddev", column=@Column(name="qualStd")),
		@AttributeOverride(name="n", column=@Column(name="qualN"))
	})
	private IntValueStat qualityStatistics;

	/**
	 * Statistical data for minilesson difficulty.
	 */
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="median", column=@Column(name="diffMed")),
		@AttributeOverride(name="average", column=@Column(name="diffAvg")),
		@AttributeOverride(name="stddev", column=@Column(name="diffStd")),
		@AttributeOverride(name="n", column=@Column(name="diffN"))
	})
	private IntValueStat difficultyStatistics;

	/**
	 * Statistical data for minilesson duration.
	 */
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="median", column=@Column(name="duraMed")),
		@AttributeOverride(name="average", column=@Column(name="duraAvg")),
		@AttributeOverride(name="stddev", column=@Column(name="duraStd")),
		@AttributeOverride(name="n", column=@Column(name="duraN"))
	})
	private IntValueStat durationStatistics;
	
	/**
	 * Date when this calculation occurred.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastCalculatedOn;
	/**
	 * Version field for optimistic locking.
	 */
	@Version
	private int olversion;
	
	/**
	 * Constructor.
	 */
	public MinilessonStats() {
	}

	/**
	 * Getter for ID.
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Setter for ID.
	 * @param id id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Getter for minilesson
	 * @return minilesson
	 */
	public Minilesson getMinilesson() {
		return minilesson;
	}

	/**
	 * Setter for minilesson.
	 * @param minilesson minilesson
	 */
	public void setMinilesson(Minilesson minilesson) {
		this.minilesson = minilesson;
	}

	/**
	 * Getter for date of average calculation.
	 * @return date
	 */
	public Date getLastCalculatedOn() {
		return lastCalculatedOn;
	}

	/**
	 * Setter for calculation date.
	 * @param lastCalculatedOn date
	 */
	public void setLastCalculatedOn(Date lastCalculatedOn) {
		this.lastCalculatedOn = lastCalculatedOn;
	}

	/**
	 * Getter for optimistic-lock version
	 * @return version
	 */
	public int getOlversion() {
		return olversion;
	}
	/**
	 * Setter for optimistic-lock version
	 * @param olversion version
	 */
	public void setOlversion(int olversion) {
		this.olversion = olversion;
	}

	/**
	 * Getter for quality statistics.
	 * @return statistics
	 */
	public IntValueStat getQualityStatistics() {
		return qualityStatistics;
	}
	
	/**
	 * Setter for quality statistics.
	 * @param qualityStatistics statistics
	 */
	public void setQualityStatistics(IntValueStat qualityStatistics) {
		this.qualityStatistics = qualityStatistics;
	}
	
	/**
	 * Getter for difficulty statistics.
	 * @return statistics
	 */
	public IntValueStat getDifficultyStatistics() {
		return difficultyStatistics;
	}
	
	/**
	 * Setter for difficulty statistics.
	 * @param difficultyStatistics statistics
	 */
	public void setDifficultyStatistics(IntValueStat difficultyStatistics) {
		this.difficultyStatistics = difficultyStatistics;
	}
	
	/**
	 * Getter for duration statistics.
	 * @return statistics
	 */
	public IntValueStat getDurationStatistics() {
		return durationStatistics;
	}
	
	/**
	 * Setter for duration statistics.
	 * @param durationStatistics statistics
	 */
	public void setDurationStatistics(IntValueStat durationStatistics) {
		this.durationStatistics = durationStatistics;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinilessonStats other = (MinilessonStats) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
