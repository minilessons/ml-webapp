package hr.fer.zemris.minilessons.dao.testing;

import javax.servlet.http.HttpSession;

/**
 * Helper data object for prototype DAO implementation.
 * 
 * @author marcupic
 */
public class TestingDAOData {

	/**
	 * ThreadLocal variable for users HTTP session object.
	 */
	private static final ThreadLocal<HttpSession> sessions = new ThreadLocal<>();
	
	/**
	 * Setter for users HTTP session object
	 * 
	 * @param session users HTTP session object
	 */
	public static void set(HttpSession session) {
		sessions.set(session);
	}
	
	/**
	 * Getter for users HTTP session object
	 * 
	 * @return thread-bound HTTP session object 
	 */
	public static HttpSession get() {
		return sessions.get();
	}
	
	/**
	 * Used to remove thread-bound HTTP session object.
	 */
	public static void remove() {
		sessions.remove();
	}
}
