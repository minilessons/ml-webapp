package hr.fer.zemris.minilessons.dao.testing;

import hr.fer.zemris.minilessons.dao.DAOBoot;

public class DAOBootImpl implements DAOBoot {

	@Override
	public void startup() {
		// No Op.
	}

	@Override
	public void destroy() {
		// No Op.
	}

}
