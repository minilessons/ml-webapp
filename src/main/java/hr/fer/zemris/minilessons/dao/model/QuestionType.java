package hr.fer.zemris.minilessons.dao.model;

/**
 * Supported CommonQuestionType implementations.
 * 
 * @author marcupic
 *
 */
public enum QuestionType {

	XML,
	JS;
	
	
}
