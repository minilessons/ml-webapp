package hr.fer.zemris.minilessons.dao.model;

public enum WSQDWorkflowState {

	UNINIT("Uninitialized"),
	AUTO("Automatic"),
	AUTOREP("Automatic with repeating"),
	OPEN("Open"),
	CLOSED("Closed"),
	VERIFY("Verify"),
	AUTOSUB("Automatic with subquestion followup"),
	INSPECT("Inspect");
	
	private String title;

	private WSQDWorkflowState(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
}
