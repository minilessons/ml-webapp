package hr.fer.zemris.minilessons.dao.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Object which stores relevant parts of minilesson from <code>descriptor.xml</code>.
 * 
 * @author marcupic
 *
 */
/**
 * @author marcupic
 *
 */
@Entity
@Table(name="minilessons")
@Cacheable(true)
public class Minilesson {
	
	/**
	 * Minilesson ID (primary key).
	 */
	@Id
	@Column(length=32)
	private String id; // will be of UUID-format
	/**
	 * Public Minilesson ID (alternate key when used with version), as
	 * declared with attribute <code>uid</code> in <code>minilesson</code>-tag.
	 */
	@Column(length=200,unique=false,nullable=false)
	private String publicID;
	/**
	 * Minilesson declared version.
	 */
	private long version;
	/**
	 * Minilesson declared minor version.
	 */
	private long minorVersion;
	/**
	 * Is this minilesson visible?
	 */
	private boolean visible;
	/**
	 * Is this minilesson archived?
	 */
	private boolean archived;
	/**
	 * Approval status of this minilesson.
	 */
	@Enumerated(EnumType.STRING)
	@Column(length=20,nullable=false)
	private MinilessonApproval approval;
	/**
	 * Owner of this minilesson (user that uploaded it).
	 */
	@ManyToOne
	@JoinColumn(nullable=false)
	private User owner;
	/**
	 * When was this minilesson uploaded.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date uploadedOn;
	/**
	 * When was this minilesson last modified?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	private Date lastModifiedOn;
	/**
	 * Who approved this minilesson?
	 */
	@ManyToOne
	@JoinColumn(nullable=true)
	private User approvedBy;
	/**
	 * When was this minilesson approved?
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	private Date approvedOn;
	/**
	 * Localized titles of minilesson.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=230)
	@CollectionTable(name="ml_titles",joinColumns={@JoinColumn(name="ml_id")})
	private Map<String,String> titles = new HashMap<>();
	/**
	 * Localized tags of minilesson.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="ml_tags",joinColumns={@JoinColumn(name="ml_id")})
	@OrderColumn(name="idx")
	private List<LocalizedString> tags = new ArrayList<>();
	/**
	 * Set of language tags on which this minilesson is offered.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="ml_langs",joinColumns={@JoinColumn(name="ml_id")})
	@Column(length=10)
	private Set<String> languages = new HashSet<>();

	@Version
	private int olversion;
	
	/**
	 * Getter for primary key.
	 * @return primary key
	 */
	public String getId() {
		return id;
	}
	/**
	 * Setter for primary key
	 * @param id primary key
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Getter for public minilesson id declared in <code>descriptor.xml</code> 
	 * @return public minilesson id
	 */
	public String getPublicID() {
		return publicID;
	}
	/**
	 * Setter for public minilesson id.
	 * @param publicID public minilesson id
	 */
	public void setPublicID(String publicID) {
		this.publicID = publicID;
	}
	
	/**
	 * Getter for minilesson version.
	 * @return version
	 */
	public long getVersion() {
		return version;
	}
	/**
	 * Setter for minilesson version.
	 * @param version version
	 */
	public void setVersion(long version) {
		this.version = version;
	}
	
	/**
	 * Getter for minor version.
	 * @return minor version
	 */
	public long getMinorVersion() {
		return minorVersion;
	}
	/**
	 * Setter for minor version.
	 * @param minorVersion minor version
	 */
	public void setMinorVersion(long minorVersion) {
		this.minorVersion = minorVersion;
	}
	
	/**
	 * Getter for visible.
	 * @return <code>true</code> if minilesson is visible, <code>false</code> otherwise
	 */
	public boolean isVisible() {
		return visible;
	}
	/**
	 * Setter for visibility.
	 * @param visible visibility
	 */
	public void setVisibility(boolean visible) {
		this.visible = visible;
	}
	
	/**
	 * Getter for archival status.
	 * @return <code>true</code> if this minilesson is archived, <code>false</code> otherwise
	 */
	public boolean isArchived() {
		return archived;
	}
	/**
	 * Setter for archival status.
	 * @param archived archived
	 */
	public void setArchived(boolean archived) {
		this.archived = archived;
	}
	
	/**
	 * Getter for approval status.
	 * @return approval status
	 */
	public MinilessonApproval getApproval() {
		return approval;
	}
	/**
	 * Setter for approval status.
	 * @param approval approval status
	 */
	public void setApproval(MinilessonApproval approval) {
		this.approval = approval;
	}
	
	/**
	 * Getter for owner.
	 * @return minilesson owner
	 */
	public User getOwner() {
		return owner;
	}
	/**
	 * Setter for owner.
	 * @param owner owner
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}
	
	/**
	 * Getter for date when minilesson was uploaded.
	 * @return upload date
	 */
	public Date getUploadedOn() {
		return uploadedOn;
	}
	/**
	 * Setter for date when minilesson was uploaded.
	 * @param uploadedOn upload date
	 */
	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}
	
	/**
	 * Getter for date when minilesson was last modified (e.g. uploaded new minor version).
	 * @return last modification date; can be <code>null</code> if minilesson was not modified since upload
	 */
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	/**
	 * Setter for last modificiation date. Can be <code>null</code>.
	 * @param lastModifiedOn last modification date
	 */
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	
	/**
	 * Getter for user who set approval status of this minilesson.
	 * @return user
	 */
	public User getApprovedBy() {
		return approvedBy;
	}
	/**
	 * Setter for user who set approval status of this minilesson.
	 * @param approvedBy user
	 */
	public void setApprovedBy(User approvedBy) {
		this.approvedBy = approvedBy;
	}
	
	/**
	 * Getter for date when approval status was last set.
	 * @return date
	 */
	public Date getApprovedOn() {
		return approvedOn;
	}
	/**
	 * Setter for date when approval status was last set.
	 * @param approvedOn
	 */
	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	/**
	 * Getter for localized titles.
	 * 
	 * @return localized titles
	 */
	public Map<String,String> getTitles() {
		return titles;
	}
	
	/**
	 * Setter for localized titles.
	 * 
	 * @param titles localized titles
	 */
	public void setTitles(Map<String,String> titles) {
		this.titles = titles;
	}
	
	/**
	 * Getter for localized tags.
	 * 
	 * @return set of tags
	 */
	public List<LocalizedString> getTags() {
		return tags;
	}
	/**
	 * Setter for localized tags.
	 * 
	 * @param tags localized tags
	 */
	public void setTags(List<LocalizedString> tags) {
		this.tags = tags;
	}
	
	/**
	 * Getter for languages offered by this minilesson.
	 * 
	 * @return set of language tags
	 */
	public Set<String> getLanguages() {
		return languages;
	}
	/**
	 * Setter for languages offered by this minilesson.
	 * 
	 * @param languages set of langauge tags
	 */
	public void setLanguages(Set<String> languages) {
		this.languages = languages;
	}

	/**
	 * Getter for optimistic-lock version
	 * @return version
	 */
	public int getOlversion() {
		return olversion;
	}
	/**
	 * Setter for optimistic-lock version
	 * @param olversion version
	 */
	public void setOlversion(int olversion) {
		this.olversion = olversion;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Minilesson other = (Minilesson) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	/**
	 * Public comparator which compares minilessons by version.minorversion. Ordering performed by
	 * this comparator will have minilesson with latest (biggest) version and then minorversion
	 * as first element.
	 */
	public static final Comparator<Minilesson> CMP_BY_VERSION = (m1,m2)->{
		int res = -Long.compare(m1.getVersion(), m2.getVersion());
		if(res != 0) return res;
		return -Long.compare(m1.getMinorVersion(), m2.getMinorVersion());
	};
}
