package hr.fer.zemris.minilessons.dao;

import java.nio.file.Path;
import java.util.List;

import hr.fer.zemris.minilessons.dao.model.CommonQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.dao.model.CourseCollection;
import hr.fer.zemris.minilessons.dao.model.ItemComment;
import hr.fer.zemris.minilessons.dao.model.ItemStatus;
import hr.fer.zemris.minilessons.dao.model.ItemThread;
import hr.fer.zemris.minilessons.dao.model.MLStatWithUserGrade;
import hr.fer.zemris.minilessons.dao.model.MLUserStat;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonStats;
import hr.fer.zemris.minilessons.dao.model.MinilessonStatus;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserGrade;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserSharedData;
import hr.fer.zemris.minilessons.dao.model.JSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.JSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.dao.model.UserCourseRole;
import hr.fer.zemris.minilessons.dao.model.UserRegistration;
import hr.fer.zemris.minilessons.dao.model.WSActivity;
import hr.fer.zemris.minilessons.dao.model.WSActivityScore;
import hr.fer.zemris.minilessons.dao.model.WSActivityScorePK;
import hr.fer.zemris.minilessons.dao.model.WSActivitySource;
import hr.fer.zemris.minilessons.dao.model.WSGroup;
import hr.fer.zemris.minilessons.dao.model.WSGrouping;
import hr.fer.zemris.minilessons.dao.model.WSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.WSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.Wish;
import hr.fer.zemris.minilessons.dao.model.WishUserVote;
import hr.fer.zemris.minilessons.dao.model.WishWithUserVote;
import hr.fer.zemris.minilessons.dao.model.Workspace;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinRequest;
import hr.fer.zemris.minilessons.dao.model.WorkspacePage;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinitionAttachment;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DWSQuestionDefinition;
import hr.fer.zemris.minilessons.domain.model.DXMLQDef;
import hr.fer.zemris.minilessons.qstorage.QStorage;
import hr.fer.zemris.minilessons.storage.MLStorage;

/**
 * DAO interface for data-access layer operations.
 * 
 * @author marcupic
 *
 */
public interface DAO {
	/**
	 * Retrieves storage engine which is in charge for question file storage.
	 * 
	 * @return storage
	 */
	QStorage getQStorage();
	/**
	 * Retrieves storage engine which is in charge for minilesson file storage.
	 * 
	 * @return storage
	 */
	MLStorage getMLStorage();
	/**
	 * Returns user for given id or <code>null</code> if user does not exists.
	 * 
	 * @param id primary key
	 * @return user if such one exists or <code>null</code> otherwise
	 */
	User findUserByID(long id);
	/**
	 * Returns user for given JMBAG or <code>null</code> if user does not exists.
	 * 
	 * @param JMBAG JMBAG
	 * @return user if such one exists or <code>null</code> otherwise
	 */
	User findUserByJMBAG(String jmbag);
	/**
	 * Returns user for given username and authentication domain or <code>null</code> if user does not exists.
	 * 
	 * @param username username
	 * @param authDomain in which authentication domain
	 * @return user if such one exists or <code>null</code> otherwise
	 */
	User findUserByUsername(String username, String authDomain);
	/**
	 * Persists new user into data storage.
	 * 
	 * @param user user to persist
	 */
	void saveUser(User user);
	
	/**
	 * Retrieves a list of available minilessons. If no minilessons exists, empty list
	 * will be returned.
	 * 
	 * @return list of minilessons
	 */
	List<Minilesson> listMinilessons();

	/**
	 * Searches for newest minilesson with given publicID which is not archived and is visible.
	 * @param publicID minilesson public id
	 * @return minilesson
	 */
	Minilesson findNewestVisibleMinilesson(String publicID);
	/**
	 * Searches for newest question definition with given publicID which is not archived and is visible.
	 * @param publicID question definition public id
	 * @return question definition
	 */
	CommonQuestionDefinition findNewestVisibleQuestionDefinition(String publicID);
	/**
	 * Retrieves a list of all {@link MinilessonStats} for minilessons which are not
	 * archived, not hidden and are accessible for usage.
	 * 
	 * @return list
	 */
	List<MinilessonStats> listAllVisibleMLStats();
	
	/**
	 * Retrieves a list of all {@link MinilessonStats} with paired {@link MinilessonUserGrade} (if present) for minilessons which are not
	 * archived, not hidden and are accessible for usage. Result is packed as {@link MLStatWithUserGrade}.
	 * 
	 * @param userID user ID
	 * @return list
	 */
	List<MLStatWithUserGrade> listAllVisibleMLStatsEx(Long userID);
	
	/**
	 * Retrieves a list of all {@link MinilessonStats} for minilessons which are not
	 * archived, not hidden and are accessible for usage, and which are owned by given user.
	 * 
	 * @param owner owner
	 * @return list of minilesson statistics object
	 */
	List<MinilessonStats> listAllVisibleMLStatsForOwner(User owner);
	
	/**
	 * Returns minilesson with given primary key.
	 * 
	 * @param id primary key
	 * @return minilesson if such one exists or <code>null</code> otherwise
	 */
	Minilesson findMinilessonByID(String id);

	/**
	 * Returns question with given primary key.
	 * 
	 * @param id primary key
	 * @return question if such one exists or <code>null</code> otherwise
	 */
	JSQuestionDefinition findJSQuestionDefinitionByID(String id);
	
	/**
	 * Returns question instance with given primary key.
	 * 
	 * @param id primary key
	 * @return question instance if such one exists or <code>null</code> otherwise
	 */
	JSQuestionInstance findJSQuestionInstanceByID(Long id);

	/**
	 * Returns a list of visible questions.
	 * 
	 * @return list of visible questions
	 */
	List<JSQuestionDefinition> findVisibleJSQuestionDefinitions();
	
	/**
	 * Returns a list of visible questions which are public or owned by given user.
	 * 
	 * @param owner owner
	 * @return list of visible questions
	 */
	List<JSQuestionDefinition> findVisiblePublicJSQuestionDefinitions(User owner);

	/**
	 * Method resolves a path into the web-server filesystem where a resource
	 * with given relative path for minilesson with given primary key can be
	 * accessed. Argument <code>path</code> must be relative with respect to
	 * minilesson package (e.g. <code>files/page1-en.html</code>).
	 * 
	 * @param minilessonID primary key of minilesson
	 * @param path relative path to resolve
	 * @return resolved path into the local filesystem or <code>null</code> if error occurred
	 */
	Path getRealPathFor(String minilessonID, String path);

	/**
	 * Method resolves a path into the web-server filesystem where a resource
	 * with given relative path for question with given primary key can be
	 * accessed. Argument <code>path</code> must be relative with respect to
	 * question package (e.g. <code>files/page1-en.html</code>).
	 * 
	 * @param questionID primary key of question
	 * @param path relative path to resolve
	 * @return resolved path into the local filesystem or <code>null</code> if error occurred
	 */
	Path getRealQPathFor(String questionID, String path);
	
	/**
	 * Retrieves a status for given item and user in given minilesson.
	 * 
	 * @param minilesson minilesson which contains item
	 * @param user user whose status is requested
	 * @param itemID item for which status is requested
	 * @return status object is one exists, <code>null</code> otherwise
	 */
	ItemStatus findItemStatus(Minilesson minilesson, User user, String itemID);
	/**
	 * Persists given status.
	 * 
	 * @param itemStatus status to persist
	 */
	void saveItemStatus(ItemStatus itemStatus);

	/**
	 * Retrieves a list of item-IDs which user has completed on given minilesson.
	 * 
	 * @param minilesson
	 * @param user
	 * @return
	 */
	List<String> listCompletedItems(Minilesson minilesson, User user);
	
	/**
	 * Retrieves status of minilesson for given user.
	 * 
	 * @param minilesson minilesson which status is inquired
	 * @param user user for which a status is inquired
	 * @return status object or <code>null</code> if no status is recorded yet
	 */
	MinilessonStatus findMinilessonStatus(Minilesson minilesson, User user);
	/**
	 * Retrieves minilesson stats object for given minilesson.
	 * 
	 * @param minilesson minilesson
	 * @return statistics object
	 */
	MinilessonStats findMinilessonStats(Minilesson minilesson);
	/**
	 * Persists minilesson status.
	 * 
	 * @param minilessonStatus minilesson status
	 */
	void saveMinilessonStatus(MinilessonStatus minilessonStatus);
	/**
	 * Persists minilesson.
	 * 
	 * @param minilesson minilesson
	 */
	void saveMinilesson(Minilesson minilesson);
	/**
	 * Persists minilesson statistics.
	 * 
	 * @param minilessonStats minilesson statistics
	 */
	void saveMinilessonStats(MinilessonStats minilessonStats);
	/**
	 * Returns a list of minilessonID-s which have unprocessed votes.
	 * 
	 * @return list of IDs
	 */
	List<String> findUnprocessedMinilessons();
	/**
	 * Returns a list of user-grades for given minilesson.
	 * 
	 * @param minilessonID minilesson id
	 * @return list
	 */
	List<MinilessonUserGrade> findMinilessonUserGrades(String minilessonID);
	/**
	 * Save minilesson user grade object.
	 * 
	 * @param mug user grade
	 */
	void saveMinilessonUserGrade(MinilessonUserGrade mug);
	/**
	 * Retrieves minilesson user grade objects for given user.
	 * 
	 * @param userID user id
	 * @return list
	 */
	List<MinilessonUserGrade> findMinilessonUserGrades(Long userID);
	/**
	 * For given user and given minilesson retrieves user-grade object.
	 * 
	 * @param minilesson minilesson
	 * @param user user
	 * @return user-grade object, if such one exists
	 */
	MinilessonUserGrade findMinilessonUserGrades(Minilesson minilesson, User user);
	/**
	 * Save new wish.
	 * 
	 * @param wish wish
	 */
	void saveNewWish(Wish wish);
	/**
	 * Returns a list of wishes which have unprocessed votes.
	 * 
	 * @return list of IDs
	 */
	List<Long> findUnprocessedWishes();
	/**
	 * Finds a wish with given id.
	 * 
	 * @param id wish id
	 * @return wish or <code>null</code> if no such wish exists
	 */
	Wish findWish(Long id);
	/**
	 * Retrieves a list of votes for selected wish.
	 * @param wishID wish id
	 * @return list of existing user votes
	 */
	List<WishUserVote> findWishVotes(Long wishID);
	/**
	 * Retrieves a list of all wishes.
	 * 
	 * @return list
	 */
	List<Wish> listAllWishes();
	/**
	 * Retrieves a list of all wishes combined with user votes.
	 * 
	 * @param userID id of user whose votes are retrieved
	 * @return list
	 */
	List<WishWithUserVote> listAllWishesEx(Long userID);
	/**
	 * Retrieves user vote for given wish.
	 * 
	 * @param wishID wish id
	 * @param userID user id
	 * @return
	 */
	WishUserVote findWishUserVote(Long wishID, Long userID);
	/**
	 * Persists new wish user vote.
	 * 
	 * @param wuv wishUserVote
	 */
	void saveWishUserVote(WishUserVote wuv);
	
	/**
	 * Method retrieves all minilessons for given user which are not
	 * archived.
	 * 
	 * @param user owner
	 * @return list of minilessons
	 */
	List<Minilesson> listAllMinilessonsForOwner(User user);
	
	/**
	 * List all minilessons with given publicID (with all possible statuses).
	 * 
	 * @param publicID minilesson publicID
	 * @return list of minilessons
	 */
	List<Minilesson> listAllMinilessonVersions(String publicID);
	
	/**
	 * List all questions with given publicID (with all possible statuses).
	 * 
	 * @param publicID question publicID
	 * @return list of questions
	 */
	List<JSQuestionDefinition> listAllJSQuestionDefinitionVersions(String publicID);

	/**
	 * List all questions with given publicID (with all possible statuses).
	 * 
	 * @param publicID question publicID
	 * @return list of questions
	 */
	List<XMLQuestionDefinition> listAllXMLQuestionDefinitionVersions(String publicID);

	/**
	 * Returns the number of registration requests for given date. Date MUST be of format yyyy-MM-dd.
	 * @param date date
	 * @return number of registrations
	 */
	int countRegistrationsForDate(String date);
	
	/**
	 * Returns the number of registration requests for given date from given IP address. Date MUST be of format yyyy-MM-dd.
	 * @param date date
	 * @param ip ip address
	 * @return number of registrations
	 */
	int countRegistrationsForDate(String date, String ip);
	
	/**
	 * Save new user registration.
	 * 
	 * @param ureg user registration
	 */
	void saveUserRegistration(UserRegistration ureg);
	
	/**
	 * Finds UserRegistration with given ID.
	 * @param id ID
	 * @return UserRegistration
	 */
	UserRegistration findUserRegistration(Long id);
	
	/**
	 * For each user who opened minilesson returns opening and closing date.
	 * @param minilessonID minilesson ID
	 * @return list of {@link MLUserStat} objects
	 */
	List<MLUserStat> getUserMinilessonBasicStats(String minilessonID);
	
	/**
	 * List all item threads for given minilesson and item.
	 * @param minilessonID minilesson ID
	 * @param itemID item
	 * @return list of threads
	 */
	List<ItemThread> listMinilessonItemThreads(String minilessonID, String itemID);
	
	/**
	 * List all comments for given thread.
	 * @param threadID thread ID
	 * @return list of comments
	 */
	List<ItemComment> listItemComments(Long threadID);
	/**
	 * Loads ItemThread with given id.
	 * @param threadID thread id
	 * @return loaded thread
	 */
	ItemThread findItemThread(Long threadID);
	/**
	 * Persist new item comment.
	 * @param itemComment item comment
	 */
	void saveItemComment(ItemComment itemComment);
	/**
	 * Persist new item thread.
	 * @param thread thread
	 */
	void saveItemThread(ItemThread thread);
	/**
	 * Loads ItemComment with given id.
	 * @param commentID comment id
	 * @return item comment
	 */
	ItemComment findItemComment(Long commentID);
	
	/**
	 * Save question.
	 * 
	 * @param jSQuestionDefinition question
	 */
	void saveJSQuestionDefinition(JSQuestionDefinition jSQuestionDefinition);
	
	/**
	 * Save question instance.
	 * 
	 * @param jSQuestionInstance question instance
	 */
	void saveJSQuestionInstance(JSQuestionInstance jSQuestionInstance);
	
	/**
	 * Saves minilesson user shared data.
	 * 
	 * @param sharedData data
	 */
	void saveMinilessonUserSharedData(MinilessonUserSharedData sharedData);
	
	void saveCourse(Course course);

	Course findCourseByID(String id);

	List<Course> listAllCourses();
	
	List<CourseCollection> findAllCourseCollections();
	
	CourseCollection findCourseCollectionByID(String id);
	
	void saveCourseCollection(CourseCollection cc);
	
	void saveUserCourseRole(UserCourseRole ucr);
	
	List<UserCourseRole> findUserCourseRoleFor(User user);
	
	List<UserCourseRole> findUserCourseRoleFor(Course course);
	
	List<MLStatWithUserGrade> listMLStatsExForCourseMinilessons(Long userID, Course course);
	
	List<MinilessonStats> listMLStatsForCourseMinilessons(Course course);
	
	List<MLStatWithUserGrade> listMLStatsExForMinilesson(Long userID, Minilesson ml);
	
	List<MinilessonStats> listMLStatsForMinilesson(Minilesson ml);
	
	Workspace findWorkspaceByID(Long id);
	void saveWorkspace(Workspace ws);
	List<WorkspaceJoinRequest> findWorkspaceJoinRequests(Workspace ws, User user);

	void saveWorkspaceJoinRequest(WorkspaceJoinRequest req);
	WorkspaceJoinRequest findWorkspaceJoinRequestByID(Long id);
	
	List<Workspace> findUserWorkspaces(User user);
	List<Workspace> findJoinableWorkspaces(User user);
	
	void removeWorkspaceJoinRequest(WorkspaceJoinRequest req);
	
	List<WorkspaceJoinRequest> findUnsolvedWorkspaceJoinRequests(Workspace ws);
	
	List<WSGrouping> findGroupingsForWorkspace(Workspace ws);
	void saveWSGrouping(WSGrouping wsg);
	WSGrouping findWSGroupingByID(Long id);
	
	List<WSGroup> findWSGroupsForWSGrouping(WSGrouping wsg, boolean onlyActive);
	List<WSGroup> findActiveWSGroupsForWSGroupingAndUser(WSGrouping wsg, User user);
	void saveWSGroup(WSGroup gr);
	WSGroup findWSGroupByID(Long id);
	
	List<WSGroup> findEmptyActiveNonfixedWSGroups(WSGrouping gr);
	
	List<WSActivity> findActivitiesForWorkspace(Workspace ws);
	void saveWSActivity(WSActivity activity);
	void saveXMLQuestionDefinition(XMLQuestionDefinition def);
	List<DXMLQDef> listXMLQuestionsForOwner(User user, String lang);
	XMLQuestionDefinition findXMLQuestionDefinition(String id);
	void saveWSQuestionDefinition(WSQuestionDefinition wsqd);
	WSActivity findWSActivityByID(Long id);
	List<DWSQuestionDefinition> listDWSQuestionDefinitions(Workspace ws, WorkspacePage pg, String lang);
	WSQuestionDefinition findWSQuestionDefinition(Long id);
	List<WSQuestionDefinition> findActiveQuestions(Workspace ws, WorkspacePage subpage);
	List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, WSGroup g);
	List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, User u);
	void saveXMLQuestionInstance(XMLQuestionInstance qi);
	void saveWSQuestionInstance(WSQuestionInstance wsqi);
	XMLQuestionInstance findXMLQuestionInstanceByID(Long id);
	WSQuestionInstance findWSQuestionInstanceByID(Long id);
	List<WSQuestionInstance> findWSQuestionInstances(WSQuestionDefinition qd, boolean additional);
	void saveWSActivitySource(WSActivitySource src);
	WSActivityScore findWSActivityScore(WSActivityScorePK pk);
	void saveWSActivityScore(WSActivityScore score);
	List<WSActivityScore> findWSActivityScores(Workspace ws, User user);
	List<WSActivitySource> findAllWSActivitySources(Workspace ws);
	void saveXMLQuestionDefinitionAttachment(XMLQuestionDefinitionAttachment a);
	XMLQuestionDefinitionAttachment findXMLQuestionDefinitionAttachment(XMLQuestionDefinition definition, String filename);
	List<XMLQuestionDefinitionAttachment> findAllXMLQuestionDefinitionAttachments(XMLQuestionDefinition definition);
	List<WSActivityScore> listAllWorkspaceValidActivityUserScores(Workspace ws);
	List<JSQuestionDefinition> listAllJSQuestionsForOwner(User owner);
	void removeXMLQuestionDefinitionAttachment(XMLQuestionDefinitionAttachment a);
	List<CommonQuestionDefinition> listAllCommonQuestionsForOwner(User owner);
	CommonQuestionDefinition findCommonQuestionDefinitionByID(String id);
	List<CommonQuestionDefinition> findVisiblePublicCommonQuestionDefinitions(User owner);
	List<DQuestionInstance> findPublicUsersJSQuestionInstances(User user, JSQuestionDefinition qd);
	List<DQuestionInstance> findPublicUsersXMLQuestionInstances(User user, XMLQuestionDefinition qd);
}
