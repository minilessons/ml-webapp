package hr.fer.zemris.minilessons.dao.model;

/**
 * Data transfer object for DAO select.
 * 
 * @author marcupic
 *
 */
public class MLStatWithUserGrade {

	private MinilessonStats stats;
	private MinilessonUserGrade userGrade;
	private MinilessonStatus status;

	public MLStatWithUserGrade() {
	}

	public MLStatWithUserGrade(MinilessonStats stats, MinilessonUserGrade userGrade, MinilessonStatus status) {
		super();
		this.stats = stats;
		this.userGrade = userGrade;
		this.status = status;
	}

	public MinilessonStats getStats() {
		return stats;
	}
	
	public MinilessonUserGrade getUserGrade() {
		return userGrade;
	}
	
	public MinilessonStatus getStatus() {
		return status;
	}
}
