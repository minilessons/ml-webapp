package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="course_user_roles")
public class UserCourseRole {

	@EmbeddedId
	private UserCourseRoleKey id;

	@ManyToOne
	@MapsId("user_id")
	private User user;
	
	@ManyToOne
	@MapsId("course_id")
	private Course course;
	
	public UserCourseRole() {
	}

	public UserCourseRole(UserCourseRoleKey id) {
		super();
		this.id = id;
	}

	public UserCourseRoleKey getId() {
		return id;
	}
	
	public void setId(UserCourseRoleKey id) {
		this.id = id;
	}
	
	public Course getCourse() {
		return course;
	}
	
	public void setCourse(Course course) {
		this.course = course;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}

	public CourseRole getRole() {
		return id.getRole();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserCourseRole))
			return false;
		UserCourseRole other = (UserCourseRole) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
