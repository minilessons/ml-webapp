package hr.fer.zemris.minilessons.dao.model;

/**
 * User's permission regarding minilesson uploading.
 * 
 * @author marcupic
 */
public enum UploadPolicy {
	/**
	 * Can not upload any minilesson.
	 */
	DENIED,
	/**
	 * Can upload only restricted minilessons.
	 */
	RESTRICTED,
	/**
	 * Can upload more complex minilessons but these will have to be approved.
	 */
	FOR_APPROVAL,
	/**
	 * Can upload all kinds of minilessons with no limitations.
	 */
	ALLOWED
}
