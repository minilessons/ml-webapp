package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * This is a vote record for a single user for a single wish.
 * 
 * @author marcupic
 */
@IdClass(WishUserVotePK.class)
@Entity
@Table(name="wish_user_votes",indexes={
	@Index(columnList="wish_id, user_id", unique=true)
})
public class WishUserVote {

	/**
	 * Wish which is being voted for.
	 */
	@Id @ManyToOne(fetch=FetchType.LAZY)
	private Wish wish;
	
	/**
	 * User whose vote this is.
	 */
	@Id @ManyToOne(fetch=FetchType.LAZY)
	private User user;
	
	/**
	 * <code>true</code> if statistics subsystem did not update statistics to reflect this vote; <code>false</code> otherwise.
	 */
	private boolean unprocessed;
	
	/**
	 * Value of vote: -1, 0 or 1.
	 */
	private int vote;
	
	/**
	 * Optimistic lock version.
	 */
	@Version
	private int olversion;
	
	public WishUserVote() {
	}

	public Wish getWish() {
		return wish;
	}

	public void setWish(Wish wish) {
		this.wish = wish;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isUnprocessed() {
		return unprocessed;
	}

	public void setUnprocessed(boolean unprocessed) {
		this.unprocessed = unprocessed;
	}

	public int getVote() {
		return vote;
	}

	public void setVote(int vote) {
		this.vote = vote;
	}

	public int getOlversion() {
		return olversion;
	}

	public void setOlversion(int olversion) {
		this.olversion = olversion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result + ((wish == null) ? 0 : wish.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WishUserVote))
			return false;
		WishUserVote other = (WishUserVote) obj;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (wish == null) {
			if (other.wish != null)
				return false;
		} else if (!wish.equals(other.wish))
			return false;
		return true;
	}
}
