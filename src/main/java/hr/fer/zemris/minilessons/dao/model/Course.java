package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name="courses")
public class Course {

	/**
	 * Course ID (primary key).
	 */
	@Id
	@Column(length=32)
	private String id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	/**
	 * Localized titles of course.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=230)
	@CollectionTable(name="co_titles",joinColumns={@JoinColumn(name="co_id")})
	private Map<String,String> titles = new HashMap<>();

	/**
	 * Localized descriptions of course.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=4096)
	@CollectionTable(name="co_descriptions",joinColumns={@JoinColumn(name="co_id")})
	private Map<String,String> descriptions = new HashMap<>();

	private boolean active;

	@Version
	private int version;

	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true, optional=false)
	@JoinColumn(name="id", insertable=false, updatable=false)
	private CourseDetails details;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(
		name="course_minilessons", 
		joinColumns=@JoinColumn(name="co_id", referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="ml_id", referencedColumnName="id")
	)
	private Set<Minilesson> minilessons = new HashSet<>();

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(
		name="course_questions", 
		joinColumns=@JoinColumn(name="co_id", referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="q_id", referencedColumnName="id")
	)
	private Set<CommonQuestionDefinition> questions = new HashSet<>();
	
	public Course() {
		details = new CourseDetails();
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
		this.details.setId(id);
	}

	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Map<String,String> getTitles() {
		return titles;
	}
	
	public void setTitles(Map<String,String> titles) {
		this.titles = titles;
	}
	
	public Map<String,String> getDescriptions() {
		return descriptions;
	}
	
	public void setDescriptions(Map<String,String> descriptions) {
		this.descriptions = descriptions;
	}

	public CourseDetails getDetails() {
		return details;
	}
	public void setDetails(CourseDetails details) {
		this.details = details;
		this.details.setId(id);
	}

	public Set<Minilesson> getMinilessons() {
		return minilessons;
	}
	public void setMinilessons(Set<Minilesson> minilessons) {
		this.minilessons = minilessons;
	}

	public Set<CommonQuestionDefinition> getQuestions() {
		return questions;
	}
	public void setQuestions(Set<CommonQuestionDefinition> questions) {
		this.questions = questions;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Course))
			return false;
		Course other = (Course) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
