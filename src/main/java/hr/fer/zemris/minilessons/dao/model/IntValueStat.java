package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Embeddable;

/**
 * Several statistical descriptors.
 *  
 * @author marcupic
 */
@Embeddable
public class IntValueStat {

	private double median;
	private double average;
	private double stddev;
	private int n;
	
	public IntValueStat() {
	}

	public double getMedian() {
		return median;
	}

	public void setMedian(double median) {
		this.median = median;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public double getStddev() {
		return stddev;
	}

	public void setStddev(double stddev) {
		this.stddev = stddev;
	}
	
	public IntValueStat copy() {
		IntValueStat s = new IntValueStat();
		
		s.average = this.average;
		s.median = this.median;
		s.stddev = this.stddev;
		s.n = this.n;
		
		return s;
	}
}
