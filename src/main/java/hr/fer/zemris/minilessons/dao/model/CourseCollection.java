package hr.fer.zemris.minilessons.dao.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

@Entity
@Table(name="course_collections")
public class CourseCollection {

	@Id
	@Column(length=20)
	private String id;
	
	/**
	 * Localized titles for this course collection.
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@MapKeyColumn(name="lang", length=10)
	@Column(name="text", length=230)
	@CollectionTable(name="cc_titles",joinColumns={@JoinColumn(name="cc_id")})
	private Map<String,String> titles = new HashMap<>();

	@ManyToMany
	@JoinTable(
		name="cc_courses", 
		joinColumns=@JoinColumn(name="cc_id", referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="c_id", referencedColumnName="id")
	)
	@OrderColumn(name="c_order")
	private List<Course> courses = new ArrayList<>();

	public CourseCollection() {
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public List<Course> getCourses() {
		return courses;
	}
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	
	public Map<String,String> getTitles() {
		return titles;
	}
	public void setTitles(Map<String,String> titles) {
		this.titles = titles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CourseCollection))
			return false;
		CourseCollection other = (CourseCollection) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
