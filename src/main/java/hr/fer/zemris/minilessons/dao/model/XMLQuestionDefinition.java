package hr.fer.zemris.minilessons.dao.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Entity
@DiscriminatorValue("XML")
public class XMLQuestionDefinition extends CommonQuestionDefinition {
	
	@Lob
	@Column(length=32*1024, nullable=true)
	private String xml;
	
	@Column(length=20, nullable=true)
	private String xmlQuestionType;
		
	public XMLQuestionDefinition() {
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getXMLQuestionType() {
		return xmlQuestionType;
	}

	public void setXMLQuestionType(String questionType) {
		this.xmlQuestionType = questionType;
	}

	@PrePersist @PreUpdate
	private void checkConstraints() {
		if(xml==null) {
			throw new RuntimeException("XMLQuestionDefinition.xml must not be null.");
		}
		if(xmlQuestionType==null) {
			throw new RuntimeException("XMLQuestionDefinition.questionType must not be null.");
		}
	}

}
