package hr.fer.zemris.minilessons.dao.jpa;

import javax.persistence.EntityManager;

import hr.fer.zemris.minilessons.dao.DAOException;

public class JPA {

	public static EntityManager getEntityManager() {
		EntityManager em = JPAData.managers.get();
		if(em==null) {
			throw new DAOException("DAO provider was not open!");
		}
		return em;
	}

}
