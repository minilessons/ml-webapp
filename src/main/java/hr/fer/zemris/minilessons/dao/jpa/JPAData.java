package hr.fer.zemris.minilessons.dao.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class JPAData {

	private static EntityManagerFactory emf;
	static final ThreadLocal<EntityManager> managers = new ThreadLocal<>();

	static void setEntityManager(EntityManager em) {
		managers.set(em);
	}
	
	static void removeEntityManager() {
		managers.remove();
	}

	static EntityManager getEntityManager() {
		return managers.get();
	}
	
	public static EntityManagerFactory getEmf() {
		return emf;
	}
	
	public static void setEmf(EntityManagerFactory emf) {
		JPAData.emf = emf;
	}
}
