package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * This class is used to track status of user on minilesson
 * (i.e. did user accessed the minilesson; did user completed the minilesson).
 * 
 * @author marcupic
 */
@Entity
@Table(name="ml_statuses")
@IdClass(MinilessonStatusPK.class)
@Cacheable(true)
public class MinilessonStatus {

	
	/**
	 * Minilesson which status is tracked.
	 */
	@Id
	@ManyToOne
	private Minilesson minilesson;
	/**
	 * User whose status is tracked.
	 */
	@Id
	@ManyToOne
	private User user;
	
	/**
	 * Completion date.
	 */
	private Date completedOn;
	/**
	 * Opening date.
	 */
	private Date openedAt;

	@OneToOne(fetch=FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.REMOVE}, optional=true, orphanRemoval=true)
	private MinilessonUserSharedData sharedData;
	
	/**
	 * Constructor.
	 */
	public MinilessonStatus() {
	}

	/**
	 * Constructor.
	 * 
	 * @param minilesson minilesson
	 * @param user user
	 */
	public MinilessonStatus(Minilesson minilesson, User user) {
		super();
		this.minilesson = minilesson;
		this.user = user;
	}

	/**
	 * Getter for minilesson which status is tracked.
	 * 
	 * @return minilesson
	 */
	public Minilesson getMinilesson() {
		return minilesson;
	}

	/**
	 * Setter for minilesson which status is tracked.
	 * 
	 * @param minilesson minilesson
	 */
	public void setMinilesson(Minilesson minilesson) {
		this.minilesson = minilesson;
	}

	/**
	 * Getter for user whose status is tracked.
	 * 
	 * @return user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Setter for user whose status is tracked.
	 * 
	 * @param user user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Getter for completion date.
	 * 
	 * @return completion date or <code>null</code> is minilesson was not yet completed
	 */
	public Date getCompletedOn() {
		return completedOn;
	}

	/**
	 * Setter for completion date.
	 * 
	 * @param completedOn completion date
	 */
	public void setCompletedOn(Date completedOn) {
		this.completedOn = completedOn;
	}

	/**
	 * Getter for opening date.
	 * 
	 * @return opening date; will not be <code>null</code>
	 */
	public Date getOpenedAt() {
		return openedAt;
	}
	
	/**
	 * Setter for opening date. Must not be <code>null</code>.
	 * 
	 * @param openedAt opening date
	 */
	public void setOpenedAt(Date openedAt) {
		this.openedAt = openedAt;
	}
	
	/**
	 * Getter for shared user data.
	 * @return data
	 */
	public MinilessonUserSharedData getSharedData() {
		return sharedData;
	}
	
	/**
	 * Setter for shared user data.
	 * @param sharedData data
	 */
	public void setSharedData(MinilessonUserSharedData sharedData) {
		this.sharedData = sharedData;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((minilesson == null) ? 0 : minilesson.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinilessonStatus other = (MinilessonStatus) obj;
		if (minilesson == null) {
			if (other.minilesson != null)
				return false;
		} else if (!minilesson.equals(other.minilesson))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	
}
