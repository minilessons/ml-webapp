package hr.fer.zemris.minilessons.dao.model;

import java.util.Date;

public class MLUserStat {

	private String jmbag;
	private String firstName;
	private String lastName;
	private Date startedOn;
	private Date completedOn;
	
	public MLUserStat() {
	}

	public MLUserStat(String jmbag, String firstName, String lastName, Date startedOn, Date completedOn) {
		super();
		this.jmbag = jmbag;
		this.firstName = firstName;
		this.lastName = lastName;
		this.startedOn = startedOn;
		this.completedOn = completedOn;
	}

	public Date getCompletedOn() {
		return completedOn;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getJmbag() {
		return jmbag;
	}
	
	public Date getStartedOn() {
		return startedOn;
	}
}
