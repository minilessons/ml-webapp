package hr.fer.zemris.minilessons.dao.model;

/**
 * Minilesson approval status.
 * 
 * @author marcupic
 */
public enum MinilessonApproval {
	/**
	 * Minilesson is waiting for approval. It is unavailable for regular usage.
	 */
	WAITING,
	/**
	 * Minilesson is waiting for approval, but is restricted. It is available for regular usage
	 * but will have visible indication that it is not verified yet.
	 */
	SOFT_WAITING,
	/**
	 * Minilesson is rejected.
	 */
	REJECTED,
	/**
	 * Minilesson is approved for regular usage.
	 */
	APPROVED
}
