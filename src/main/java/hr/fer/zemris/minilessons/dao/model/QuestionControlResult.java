package hr.fer.zemris.minilessons.dao.model;

public enum QuestionControlResult {

	NOTSOLVED,
	CORRECT,
	INCORRECT;
	
}
