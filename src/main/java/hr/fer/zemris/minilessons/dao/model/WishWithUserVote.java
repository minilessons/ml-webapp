package hr.fer.zemris.minilessons.dao.model;

public class WishWithUserVote {

	private Wish wish;
	private WishUserVote vote;
	
	public WishWithUserVote() {
	}

	public WishWithUserVote(Wish wish, WishUserVote vote) {
		super();
		this.wish = wish;
		this.vote = vote;
	}

	public WishUserVote getVote() {
		return vote;
	}
	
	public Wish getWish() {
		return wish;
	}
}
