package hr.fer.zemris.minilessons.dao.jpa;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import hr.fer.zemris.minilessons.dao.DAOBoot;
import hr.fer.zemris.minilessons.dao.DAOException;
import hr.fer.zemris.minilessons.dao.DAOProvider;

public class DAOBootImpl implements DAOBoot {

	@Override
	public void startup() {
		String persistenceUnitName = null;
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("jpa.properties");
			if(is==null) {
				throw new DAOException("File not found: jpa.properties. Could not initialize DAO layer.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			persistenceUnitName = prop.getProperty("jpa.persistenceUnitName");
			if(persistenceUnitName==null || persistenceUnitName.isEmpty()) {
				throw new IOException("jpa.persistenceUnitName is not set.");
			}
		} catch(IOException ex) {
			throw new DAOException(ex);
		}
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnitName);
		JPAData.setEmf(emf);
	}

	@Override
	public void destroy() {
		EntityManagerFactory emf = JPAData.getEmf();
		emf.close();
	}

}
