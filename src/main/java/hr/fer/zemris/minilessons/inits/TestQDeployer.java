package hr.fer.zemris.minilessons.inits;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import hr.fer.zemris.minilessons.AuthDomain;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.DAOUtil;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.service.QDeployer;

public class TestQDeployer implements Runnable {

	@Override
	public void run() {
		Path root = null;
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-testqdeployer.properties");
			if(is==null) {
				throw new IOException("File not found: ml-testqdeployer.properties. Could not run TestQDeployer.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			String dbi = prop.getProperty("ml.testqdeployer.root");
			if(dbi==null || dbi.isEmpty()) {
				throw new IOException("ml.testqdeployer.root is not set.");
			}
			root = Paths.get(dbi);
			if(!Files.isDirectory(root)) {
				throw new RuntimeException("Directory "+root+" specified for q-storage is not present.");
			}
			if(!Files.isReadable(root)) {
				throw new RuntimeException("Directory "+root+" specified for q-storage could not be read.");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		List<Path> list = new ArrayList<>();
		try(DirectoryStream<Path> ds = Files.newDirectoryStream(root)) {
			for(Path p : ds) {
				String name = p.getFileName().toString();
				if(name.equals(".") || name.equals("..")) continue;
				if(!Files.isDirectory(p)) {
					continue;
				}
				if(Files.exists(p.resolve("descriptor.xml"))) {
					list.add(p);
				}
			}
		} catch(IOException ex) {
			throw new RuntimeException("Error while scanning folders.", ex);
		}
		
		User user = DAOUtil.transactional(()->{
			return DAOProvider.getDao().findUserByUsername("admin", AuthDomain.LOCAL.getKey());
		});
		if(user==null) {
			throw new RuntimeException("User admin is not found.");
		}
		for(Path p : list) {
			final Path qroot = p;
			System.out.println("Will deploy question: " + p);
			DAOUtil.transactional(()->{
				QDeployer.deployJSQuestion(qroot, user, true, true);
				return null;
			});
		}
	}
}
