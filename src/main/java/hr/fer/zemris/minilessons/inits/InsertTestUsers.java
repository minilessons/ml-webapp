package hr.fer.zemris.minilessons.inits;

import java.util.Date;
import java.util.HashSet;

import hr.fer.zemris.minilessons.AuthDomain;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.DAOUtil;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.util.PasswordUtil;

/**
 * This is helper task which inserts test accounts. Use it for private testing purposes only.
 * 
 * @author marcupic
 *
 */
public class InsertTestUsers implements Runnable {

	@Override
	public void run() {
		DAOUtil.transactional(()->{
			
			insertUser("student1", "Ivo1", "Ivić1", "#0000000002", UploadPolicy.ALLOWED, new String[] {}, "student1");
			insertUser("student2", "Ivo2", "Ivić2", "#0000000003", UploadPolicy.ALLOWED, new String[] {}, "student2");
			insertUser("student3", "Ivo3", "Ivić3", "#0000000004", UploadPolicy.ALLOWED, new String[] {}, "student3");
			insertUser("student4", "Ivo4", "Ivić4", "#0000000005", UploadPolicy.ALLOWED, new String[] {}, "student4");
			insertUser("student5", "Ivo5", "Ivić5", "#0000000006", UploadPolicy.ALLOWED, new String[] {}, "student5");
			insertUser("student6", "Ivo6", "Ivić6", "#0000000007", UploadPolicy.ALLOWED, new String[] {}, "student6");

			insertUser("teacher1", "Ante1", "Antić1", "#0000000008", UploadPolicy.ALLOWED, new String[] {MLBasicPermission.CAN_CREATE_WORKSPACE.getPermissonName()}, "teacher1");
			insertUser("teacher2", "Ante2", "Antić2", "#0000000009", UploadPolicy.ALLOWED, new String[] {MLBasicPermission.CAN_CREATE_WORKSPACE.getPermissonName()}, "teacher2");
			insertUser("teacher3", "Ante3", "Antić3", "#0000000010", UploadPolicy.ALLOWED, new String[] {MLBasicPermission.CAN_CREATE_WORKSPACE.getPermissonName()}, "teacher3");

			return null;
		});
	}
	
	private void insertUser(String userName, String firstName, String lastName, String jmbag, UploadPolicy uploadPolicy, String[] permissions, String password) {
		User user = DAOProvider.getDao().findUserByUsername(userName, AuthDomain.LOCAL.getKey());
		if(user!=null) return;
		
		Date now = new Date();
		
		user = new User();
		user.setUsername(userName);
		user.setAuthDomain(AuthDomain.LOCAL.getKey());
		user.setEmail("");
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setJmbag(jmbag);
		user.setUploadPolicy(UploadPolicy.ALLOWED);
		user.setPermissions(new HashSet<>());
		for(String perm : permissions) {
			user.getPermissions().add(perm);
		}
		user.setPasswordHash(PasswordUtil.encodePassword(password));
		user.setCreatedOn(now);
		
		DAOProvider.getDao().saveUser(user);
	}
}
