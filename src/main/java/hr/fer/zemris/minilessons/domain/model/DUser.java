package hr.fer.zemris.minilessons.domain.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.UploadPolicy;
import hr.fer.zemris.minilessons.dao.model.User;

public class DUser implements Comparable<DUser> {

	/**
	 * Primary key.
	 */
	private Long id;
	/**
	 * First name.
	 */
	private String firstName;
	/**
	 * Last name.
	 */
	private String lastName;
	/**
	 * Username.
	 */
	private String username;
	/**
	 * JMBAG.
	 */
	private String jmbag;
	/**
	 * E-mail.
	 */
	private String email;
	/**
	 * Authentication domain: who is responsible for checking username and password.
	 */
	private String authDomain;
	/**
	 * Upload policy for this user.
	 */
	private UploadPolicy uploadPolicy;
	/**
	 * Permissions. Values are {@link MLBasicPermission#getPermissonName()}.
	 */
	private Set<String> permissions = new HashSet<>();
	/**
	 * When was this user created.
	 */
	private Date createdOn;

	public static DUser fromUser(User u, boolean initPerms) {
		DUser d = new DUser();
		d.id = u.getId();
		d.firstName = u.getFirstName();
		d.lastName = u.getLastName();
		d.jmbag = u.getJmbag();
		d.email = u.getEmail();
		d.authDomain = u.getAuthDomain();
		d.uploadPolicy = u.getUploadPolicy();
		d.permissions = initPerms ? new HashSet<>(u.getPermissions()) : new HashSet<>();
		d.createdOn = u.getCreatedOn();
		return d;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getJmbag() {
		return jmbag;
	}

	public void setJmbag(String jmbag) {
		this.jmbag = jmbag;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuthDomain() {
		return authDomain;
	}

	public void setAuthDomain(String authDomain) {
		this.authDomain = authDomain;
	}

	public UploadPolicy getUploadPolicy() {
		return uploadPolicy;
	}

	public void setUploadPolicy(UploadPolicy uploadPolicy) {
		this.uploadPolicy = uploadPolicy;
	}

	public Set<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DUser))
			return false;
		DUser other = (DUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(DUser o) {
		if(this.firstName==null) {
			if(o.firstName != null) return -1;
		} else {
			if(o.firstName == null) return 1;
			int r = this.firstName.compareTo(o.firstName);
			if(r!=0) return r;
		}
		if(this.lastName==null) {
			if(o.lastName != null) return -1;
		} else {
			if(o.lastName == null) return 1;
			int r = this.lastName.compareTo(o.lastName);
			if(r!=0) return r;
		}
		return this.id.compareTo(o.id);
	}
}
