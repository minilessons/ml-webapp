package hr.fer.zemris.minilessons.domain.model;

public class DOption {

	private String key;
	private String text;
	
	public DOption(String key, String text) {
		super();
		this.key = key;
		this.text = text;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getText() {
		return text;
	}
}
