package hr.fer.zemris.minilessons.domain.model;

import java.util.Date;

import hr.fer.zemris.minilessons.dao.model.Course;

public class DCourse {
	private String id;
	private String title;
	private String description;
	private boolean active;
	private Date createdOn;
	
	public DCourse() {
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public static DCourse from(Course c, String lang) {
		DCourse dc = new DCourse();
		dc.setActive(c.isActive());
		dc.setId(c.getId());
		dc.setTitle(c.getTitles().getOrDefault(lang, "???"));
		dc.setDescription(c.getDescriptions().getOrDefault(lang, "???"));
		dc.setCreatedOn(c.getCreatedOn());
		return dc;
	}
}
