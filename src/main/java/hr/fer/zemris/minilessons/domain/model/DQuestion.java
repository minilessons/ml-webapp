package hr.fer.zemris.minilessons.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import hr.fer.zemris.minilessons.dao.model.CommonQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.QuestionApproval;
import hr.fer.zemris.minilessons.dao.model.QuestionConfigVariability;
import hr.fer.zemris.minilessons.dao.model.QuestionType;

public class DQuestion {
	
	private String id;
	private String publicID;
	private long version;
	private long minorVersion;
	private boolean visible;
	private boolean archived;
	private String title;
	private Date uploadedOn;
	private QuestionApproval approval;
	private DUser approvedBy;
	private Date approvedOn;
	private Set<DLocalizedString> titles = new HashSet<>();
	private Set<DLocalizedString> tags = new HashSet<>();
	private Set<String> tagsForCurrentLanguage = new LinkedHashSet<>();
	private Set<String> languages = new HashSet<>();
	private boolean publicQuestion;
	private Map<String,QuestionConfigVariability> configVariability = new LinkedHashMap<>();
	private List<String> configurations = new ArrayList<>();
	private QuestionType questionType;
	private DUser owner;
	
	/**
	 * Can current user manager approval?
	 */
	private boolean approvalManageable;
	
	public DQuestion() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPublicID() {
		return publicID;
	}

	public void setPublicID(String publicID) {
		this.publicID = publicID;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public long getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(long minorVersion) {
		this.minorVersion = minorVersion;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public QuestionApproval getApproval() {
		return approval;
	}

	public void setApproval(QuestionApproval approval) {
		this.approval = approval;
	}

	public DUser getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(DUser approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public Set<DLocalizedString> getTitles() {
		return titles;
	}

	public void setTitles(Set<DLocalizedString> titles) {
		this.titles = titles;
	}

	public Set<DLocalizedString> getTags() {
		return tags;
	}

	public void setTags(Set<DLocalizedString> tags) {
		this.tags = tags;
	}

	public Set<String> getTagsForCurrentLanguage() {
		return tagsForCurrentLanguage;
	}

	public void setTagsForCurrentLanguage(Set<String> tagsForCurrentLanguage) {
		this.tagsForCurrentLanguage = tagsForCurrentLanguage;
	}

	public Set<String> getLanguages() {
		return languages;
	}

	public void setLanguages(Set<String> languages) {
		this.languages = languages;
	}

	public boolean isPublicQuestion() {
		return publicQuestion;
	}

	public void setPublicQuestion(boolean publicQuestion) {
		this.publicQuestion = publicQuestion;
	}

	public List<String> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<String> configurations) {
		this.configurations = configurations;
	}

	public boolean isApprovalManageable() {
		return approvalManageable;
	}
	public void setApprovalManageable(boolean approvalManageable) {
		this.approvalManageable = approvalManageable;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}
	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public DUser getOwner() {
		return owner;
	}
	public void setOwner(DUser owner) {
		this.owner = owner;
	}

	public Map<String, QuestionConfigVariability> getConfigVariability() {
		return configVariability;
	}
	public void setConfigVariability(Map<String, QuestionConfigVariability> configVariability) {
		this.configVariability = configVariability;
	}
	
	public static DQuestion fromDAOModel(CommonQuestionDefinition q, String lang) {
		DQuestion dq = new DQuestion();
		dq.id = q.getId();
		dq.publicID = q.getPublicID();
		dq.version = q.getVersion();
		dq.minorVersion = q.getMinorVersion();
		dq.visible = q.isVisible();
		dq.archived = q.isArchived();
		dq.title = q.getTitles().getOrDefault(lang, "???");
		dq.setUploadedOn(q.getUploadedOn());
		dq.setApproval(q.getApproval());
		if(q.getApprovedBy()!=null) dq.setApprovedBy(DUser.fromUser(q.getApprovedBy(), false));
		dq.setApprovedOn(q.getApprovedOn());
		dq.setTitles(q.getTitles().entrySet().stream().map(e->new DLocalizedString(e.getKey(), e.getValue())).collect(Collectors.toSet()));
		dq.setTags(q.getTags().stream().map(e->new DLocalizedString(e.getLang(), e.getText())).collect(Collectors.toSet()));
		dq.setTagsForCurrentLanguage(q.getTags().stream().filter(e->e.getLang().equals(lang)).map(e->e.getText()).collect(Collectors.toSet()));
		dq.setLanguages(new LinkedHashSet<>(q.getLanguages()));
		dq.setPublicQuestion(q.isPublicQuestion());
		dq.setConfigVariability(new LinkedHashMap<>(q.getConfigVariability()));
		dq.setQuestionType(q.getQuestionType());
		dq.setOwner(DUser.fromUser(q.getOwner(), false));
		return dq;
	}
}
