package hr.fer.zemris.minilessons.domain.model;

import java.util.Date;

import hr.fer.zemris.minilessons.dao.model.Wish;
import hr.fer.zemris.minilessons.dao.model.WishStatus;
import hr.fer.zemris.minilessons.dao.model.WishUserVote;
import hr.fer.zemris.minilessons.web.controllers.MarkDownUtil;

public class DWish {

	private Long id;
	private String area;
	private String topic;
	private String description;
	private String htmlDescription;
	private Date openedAt;
	private DUser openedBy;
	private Date statusChangedOn;
	private String statusText;
	private WishStatus status;
	private int positiveVotes;
	private int negativeVotes;
	private int userVote;
	
	public DWish() {
	}

	public static DWish fromWish(Wish wish) {
		DWish dw = new DWish();
		
		dw.id = wish.getId();
		dw.area = wish.getArea();
		dw.topic = wish.getTopic();
		dw.description = wish.getDescription();
		dw.openedAt = wish.getOpenedAt();
		dw.openedBy = DUser.fromUser(wish.getOpenedBy(), false);
		dw.statusChangedOn = wish.getStatusChangedOn();
		dw.statusText = wish.getStatusText();
		dw.status = wish.getStatus();
		dw.positiveVotes = wish.getPositiveVotes();
		dw.negativeVotes = wish.getNegativeVotes();
		dw.htmlDescription = MarkDownUtil.convert(wish.getDescription());
		return dw;
	}

	public DWish fillUserVote(WishUserVote vote) {
		if(vote!=null) {
			this.userVote = vote.getVote();
		}
		return this;
	}

	public int getUserVote() {
		return userVote;
	}
	
	public Long getId() {
		return id;
	}

	public String getArea() {
		return area;
	}

	public String getTopic() {
		return topic;
	}

	public String getDescription() {
		return description;
	}

	public Date getOpenedAt() {
		return openedAt;
	}

	public DUser getOpenedBy() {
		return openedBy;
	}

	public Date getStatusChangedOn() {
		return statusChangedOn;
	}

	public String getStatusText() {
		return statusText;
	}

	public WishStatus getStatus() {
		return status;
	}

	public int getPositiveVotes() {
		return positiveVotes;
	}

	public int getNegativeVotes() {
		return negativeVotes;
	}
	
	public String getHtmlDescription() {
		return htmlDescription;
	}
}
