package hr.fer.zemris.minilessons.domain.model;

import hr.fer.zemris.minilessons.dao.model.WSGrouping;

public class DWSGrouping {

	private Long id;
	private String title;
	private Integer minMembers;
	private Integer maxMembers;
	private boolean managementEnabled;
	
	public DWSGrouping() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getMinMembers() {
		return minMembers;
	}

	public void setMinMembers(Integer minMembers) {
		this.minMembers = minMembers;
	}

	public Integer getMaxMembers() {
		return maxMembers;
	}

	public void setMaxMembers(Integer maxMembers) {
		this.maxMembers = maxMembers;
	}

	public boolean isManagementEnabled() {
		return managementEnabled;
	}

	public void setManagementEnabled(boolean managementEnabled) {
		this.managementEnabled = managementEnabled;
	}

	public static DWSGrouping fromDAOModel(WSGrouping gr, String lang) {
		DWSGrouping d = new DWSGrouping();
		d.setId(gr.getId());
		d.setManagementEnabled(gr.isManagementEnabled());
		d.setMaxMembers(gr.getMaxMembers());
		d.setMinMembers(gr.getMaxMembers());
		d.setTitle(gr.getTitles().getOrDefault(lang, "???"));
		return d;
	}
}
