package hr.fer.zemris.minilessons.xml.coursedef;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import hr.fer.zemris.minilessons.ILocalizedString;
import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.dao.model.LocalizedMediumString;
import hr.fer.zemris.minilessons.dao.model.LocalizedString;

public class CourseDefFactory {

	private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
	private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
	
	private static class MyErrorHandler implements ErrorHandler {
		private List<String> messages = new ArrayList<>();
		
		private String getParseExceptionInfo(SAXParseException spe) {
	        String systemId = spe.getSystemId();
	        if (systemId == null) {
	            systemId = "null";
	        }

	        String info = "URI=" + systemId + " Line=" + spe.getLineNumber() +
	                      ": " + spe.getMessage();
	        return info;
	    }
		@Override
		public void warning(SAXParseException exception) throws SAXException {
			messages.add("Warning: " + getParseExceptionInfo(exception));
		}
		@Override
		public void error(SAXParseException exception) throws SAXException {
			messages.add("Error: " + getParseExceptionInfo(exception));
		}
		@Override
		public void fatalError(SAXParseException exception) throws SAXException {
			messages.add("Fatal: " + getParseExceptionInfo(exception));
		}
		public List<String> getMessages() {
			return messages;
		}
	}

	private static class MyEntityResolver implements EntityResolver {
		@Override
		public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
			if(systemId != null && systemId.endsWith("/course-def.xsd")) {
				InputStream is = CourseDefFactory.class.getResourceAsStream("course-def.xsd");
				return new InputSource(is);
			}
			return null;
		}
	}

	public static Course fromText(String text) {
		return fromReader(new StringReader(text));
	}
	
	public static Course fromReader(Reader reader) {
		return fromInputSource(new InputSource(reader));
	}

	public static Course fromInputSource(InputSource source) {
		MyErrorHandler errorHandler = new MyErrorHandler();
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			dbf.setValidating(true);
			dbf.setCoalescing(true);
			dbf.setExpandEntityReferences(true);
			dbf.setIgnoringComments(true);
			dbf.setIgnoringElementContentWhitespace(true);
			dbf.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
			dbf.setAttribute(JAXP_SCHEMA_SOURCE, new String[] {"course-def.xsd"});
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			db.setErrorHandler(errorHandler);
			db.setEntityResolver(new MyEntityResolver());
			Document doc = db.parse(source);
			Course c = build(doc.getFirstChild());
			if(errorHandler.getMessages().isEmpty()) {
				return c;
			}
		} catch(Exception ex) {
			throw new RuntimeException("Could not parse course definition. " + ex.getMessage(), ex);
		}
		throw new RuntimeException("Could not parse course definition. " + errorHandler.getMessages());
	}

	private static Course build(Node root) {
		NodeList children = root.getChildNodes();
		Set<String> languages = buildLanguages(children.item(0));
		Map<String,String> titles = buildTitles(children.item(1), languages);
		Map<String,String> descriptions = buildDescriptions(children.item(2), languages);
		Course c = new Course();
		c.setCreatedOn(new Date());
		c.setTitles(titles);
		c.setDescriptions(descriptions);
		return c;
	}
	
	private static Map<String,String> buildDescriptions(Node item, Set<String> languages) {
		return toMap(buildText(item, LocalizedMediumString.class, languages));
	}

	private static Map<String,String> buildTitles(Node item, Set<String> languages) {
		return toMap(buildText(item, LocalizedString.class, languages));
	}

	private static Map<String,String> toMap(Set<? extends ILocalizedString> set) {
		Map<String,String> map = new LinkedHashMap<>();
		if(set!=null) {
			set.forEach(e->map.put(e.getLang(), e.getText()));
		}
		return map;
	}
	
	private static <T extends ILocalizedString> Set<T> buildText(Node item, Class<T> clazz, Set<String> languages) {
		NodeList children = item.getChildNodes();
		Set<String> foundLangs = new HashSet<>();
		Map<String,T> translations = new LinkedHashMap<>();
		
		Constructor<T> c = null;
		
		try {
			c = clazz.getConstructor(String.class, String.class);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("Could not build localized strings.", e);
		}
		
		for(int i = 0, n = children.getLength(); i < n; i++) {
			Node strNode = children.item(i);
			Node langAttr = strNode.getAttributes().getNamedItem("lang");
			if(langAttr==null) throw new RuntimeException("Tag str missing lang attribute.");
			String lang = langAttr.getNodeValue();
			if(!languages.contains(lang)) {
				throw new RuntimeException("Found lang="+lang+" which is not in declared supported languages.");
			}
			if(translations.containsKey(lang)) {
				throw new RuntimeException("Found multiple translations for lang="+lang+".");
			}
			foundLangs.add(lang);
			String text = strNode.getFirstChild().getNodeValue();
			T t = null;
			try {
				t = c.newInstance(lang, text);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				throw new RuntimeException("Could not build localized strings.", e);
			}
			translations.put(lang, t);
		}
		
		if(!languages.equals(foundLangs)) {
			throw new RuntimeException("Set of declared supported languages "+languages+" and provided translations "+foundLangs+" are mismatched.");
		}
		return new LinkedHashSet<>(translations.values());
	}
	
	private static Set<String> buildLanguages(Node item) {
		Set<String> set = new LinkedHashSet<>();
		if(!item.getNodeName().equals("supported-languages")) throw new RuntimeException("supported-languages expected.");
		NodeList children = item.getChildNodes();
		for(int i = 0, n = children.getLength(); i < n; i++) {
			Node langNode = children.item(i);
			Node langAttr = langNode.getAttributes().getNamedItem("name");
			if(langAttr==null) throw new RuntimeException("Tag language missing name attribute.");
			set.add(langAttr.getNodeValue());
		}
		return set;
	}
}
