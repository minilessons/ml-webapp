package hr.fer.zemris.minilessons.qstorage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of frozen storage: minilessons are detected during boot time.
 * 
 * @author marcupic
 *
 */
public class QStorageFrozen implements QStorage {

	private Map<String,Path> rootMap = new HashMap<>();
	
	public QStorageFrozen() {
	}

	@Override
	public List<Path> allQuestionRoots() throws IOException {
		return new ArrayList<>(rootMap.values());
	}
	
	@Override
	public Path getRealPathFor(String questionKey, String path) {
		if(path==null) return null;
		return rootMap.get(questionKey).resolve(path);
	}

	@Override
	public void persistQuestionPackage(String questionID, Path srcDir) throws IOException {
		System.out.println("[QStorageFrozen] Warning: question "+questionID+" deployed in original directory "+srcDir+". If this directory is deleted, question won't be available! This mode is FOR TESTING ONLY. Do not use in production!");
		rootMap.put(questionID, srcDir);
	}

	@Override
	public void updateQuestionPackage(String questionID, Path srcDir) throws IOException {
		throw new IOException("Package update is not supported by this storage.");
	}
}
