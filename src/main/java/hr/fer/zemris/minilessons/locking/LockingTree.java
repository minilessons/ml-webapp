package hr.fer.zemris.minilessons.locking;

public class LockingTree {

	private LockTreeNode root = new LockTreeNode(null, "root");

	private static final LockingTree defaultInstance = new LockingTree();
	
	public static LockingTree getDefaultInstance() {
		return defaultInstance;
	}
	
	public LockingTree() {
	}

	public void lock(String[] path) {
		if(path==null) throw new RuntimeException("Locking path was null.");
		LockTreeNode[] nodes = new LockTreeNode[path.length];

		// Najprije stvori stazu; ovo bi trebalo ići bez iznimaka...
		synchronized(this) {
			LockTreeNode parent = root;
			for(int i = 0; i < path.length; i++) {
				String p = path[i];
				LockTreeNode node = parent.getOrCreateChild(p);
				node.incrementRefCount();
				nodes[i] = node;
				parent = node;
			}
		}
		
		int index = 0;
		try {
			while(index < path.length) {
				if(index < path.length-1) {
					nodes[index].acquireReadLock();
				} else {
					nodes[index].acquireWriteLock();
				}
				index++;
			}
		} catch(Throwable ex) {
			cleanUp(nodes, index-1);
			throw new RuntimeException("Exception while locking path.", ex);
		}
	}

	private void cleanUp(LockTreeNode[] nodes, int index) {
		Exception e = null;
		for(int i = index; i >= 0; i--) {
			if(i==nodes.length-1) {
				try { nodes[i].releaseWriteLock(); } catch(Exception ex) {if(e==null) e = ex;}
			} else {
				try { nodes[i].releaseReadLock(); } catch(Exception ex) {if(e==null) e = ex;}
			}
		}
		
		synchronized(this) {
			for(int i = nodes.length-1; i >= 0; i--) {
				nodes[i].decrementRefCount();
			}
		}
		
		if(e != null) throw new RuntimeException("Problem while cleaning up locking path.", e);
	}

	public void unlock(String[] path) {
		if(path==null) throw new RuntimeException("Locking path was null.");
		LockTreeNode[] nodes = new LockTreeNode[path.length];

		// Najprije stvori stazu; ovo bi trebalo ići bez iznimaka...
		synchronized(this) {
			LockTreeNode parent = root;
			for(int i = 0; i < path.length; i++) {
				String p = path[i];
				LockTreeNode node = parent.getChild(p);
				if(node==null) throw new RuntimeException("Unlocking path is invalid - contains missing child.");
				nodes[i] = node;
				parent = node;
			}
		}
		
		cleanUp(nodes, nodes.length-1);
	}
}
