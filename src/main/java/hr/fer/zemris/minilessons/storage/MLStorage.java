package hr.fer.zemris.minilessons.storage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * This is abstraction of storage for minilesson files. Storage can be read-only.
 * 
 * @author marcupic
 */
public interface MLStorage {
	/**
	 * Returns a path into the filesystem where minilesson-relative
	 * resource can be found.
	 * 
	 * @param minilessonKey unique identifier of minilesson; depending on DAO implementation this could be minilesson ID
	 * @param path minilesson-relative path of some resource; if <code>null</code>, path to minilesson root directory will be returned
	 * @return path on filesystem for requested resource
	 */
	Path getRealPathFor(String minilessonKey, String path);
	/**
	 * Get a list of all minilesson root directories which are found on filesystem.
	 * 
	 * @return list of roots
	 * @throws IOException if folders could not be listed
	 */
	List<Path> allMinilessonRoots() throws IOException;
	/**
	 * <p>Storage engine will copy given package from temporary location which is
	 * given as <code>srcDir</code>. After that, temporary location can be deleted.</p>
	 * 
	 * @param minilessonID minilesson identifier for package being copied
	 * @param srcDir source directory (root directory of package to be deployed)
	 * @throws IOException if problem occures while persisting
	 */
	void persistMinilessonPackage(String minilessonID, Path srcDir) throws IOException;
	/**
	 * <p>Storage engine will copy given package from temporary location which is
	 * given as <code>srcDir</code>. After that, temporary location can be deleted.
	 * Since this operation assumes that previous version of files already exists,
	 * to prevent errors in middle of copying, implementation is expected to first
	 * copy content into new temporary location on the same filesystem as is the repository
	 * and then delete old directory and rename fresh copy (or some similar strategy).</p>
	 * 
	 * @param minilessonID minilesson identifier for package being copied
	 * @param srcDir source directory (root directory of package to be deployed)
	 * @throws IOException if problem occures while persisting
	 */
	void updateMinilessonPackage(String minilessonID, Path srcDir) throws IOException;
}
