package hr.fer.zemris.minilessons;

import java.util.Locale;

import org.thymeleaf.TemplateEngine;

/**
 * Abstract configuration of this web-application. Concrete implementations
 * will be used for HTTP request processing.
 * 
 * @author marcupic
 *
 */
public interface Configuration {
	/**
	 * Obtains the {@link Locale} which should be used for content generation in request processing.
	 * 
	 * @return locale for content generation; will not be <code>null</code>
	 */
	public Locale getLocale();
	/**
	 * Returns thymeleaf template engine to be used for content generation
	 * @return template engine; will not be <code>null</code>
	 */
	public TemplateEngine getTemplateEngine();
}
