package hr.fer.zemris.minilessons.localization;

import java.util.Objects;

public class I18NText {

	private String text;
	private String lang;
	
	public I18NText(String text, String lang) {
		this.text = Objects.requireNonNull(text);
		this.lang = Objects.requireNonNull(lang);
	}
	
	public String getLang() {
		return lang;
	}
	
	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		return String.format("%s => %s", lang, text);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof I18NText))
			return false;
		I18NText other = (I18NText) obj;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
}
