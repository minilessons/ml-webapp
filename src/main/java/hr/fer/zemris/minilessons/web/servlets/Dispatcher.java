package hr.fer.zemris.minilessons.web.servlets;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.web.handlers.StreamRetValHandler;
import hr.fer.zemris.minilessons.web.handlers.TemplateRetValHandler;
import hr.fer.zemris.minilessons.web.mappings.HttpMethod;
import hr.fer.zemris.minilessons.web.mappings.Mapping;
import hr.fer.zemris.minilessons.web.mappings.Mappings;
import hr.fer.zemris.minilessons.web.mappings.ResponseHandler;
import hr.fer.zemris.minilessons.web.mappings.ResultCallback;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;

/**
 * This is the main dispatcher servlet for most of minilessons web-application. This servlet intercepts a number of
 * URLs used by this web-application, forwards processing to appropriate controller and then generates content the way
 * controller has specified.
 * 
 * @author marcupic
 */
@MultipartConfig(fileSizeThreshold=1024*100, maxFileSize=10*1024*1024, maxRequestSize=11*1024*1024)
public class Dispatcher extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * A list of URL mappings which we serve.
	 */
	private Mappings urlMappings = new Mappings()
			.add(new Mapping(HttpMethod.GET,  "home", "Home", "home"))
			.add(new Mapping(HttpMethod.GET,  "index", "Home", "mainIndex"))
			.add(new Mapping(HttpMethod.GET,  "minilessons", "LessonsBrowser", "process"))
			.add(new Mapping(HttpMethod.GET,  "minilessons/$apctx", "LessonsBrowser", "process"))
			.add(new Mapping(HttpMethod.GET,  "minilessons/$apctx/ml/$uid", "LessonsBrowser", "process"))
			.add(new Mapping(HttpMethod.GET,  "minilesson/$apctx/$uid/item", "ItemController", "openItem"))
			.add(new Mapping(HttpMethod.GET,  "minilesson/$apctx/$uid/item/$iid/$vid", "ItemController", "openItemView"))
			.add(new Mapping(HttpMethod.GET,  "minilesson/$apctx/$uid/item/$iid/$vid/$key", "ItemController", "openItemViewKey"))
			.add(new Mapping(HttpMethod.GET,  "minilesson/$apctx/$uid/file/$fid", "MinilessonFiles", "process"))
			.add(new Mapping(HttpMethod.GET,  "minilesson/$apctx/$mid/comments/$iid/$tid", "ItemCommentThreads", "listThread"))
			.add(new Mapping(HttpMethod.GET,  "minilesson/$apctx/$mid/threads/$iid", "ItemCommentThreads", "listThreads"))
			.add(new Mapping(HttpMethod.POST, "minilesson/$apctx/$mid/threads/$iid", "ItemCommentThreads", "addThread"))
			.add(new Mapping(HttpMethod.POST, "minilesson/$apctx/$mid/threads/$iid/$tid", "ItemCommentThreads", "addComment"))
			.add(new Mapping(HttpMethod.POST, "minilesson/$apctx/$mid/threads/$iid/$tid/$cid/hidden", "ItemCommentThreads", "changeCommentVisibility"))
			.add(new Mapping(HttpMethod.POST, "minilesson/$apctx/$mid/threads/$iid/$tid/$cid/deleted", "ItemCommentThreads", "changeCommentDeleted"))
			.add(new Mapping(HttpMethod.POST, "minilesson/$apctx/$mid/threads/$iid/$tid/public", "ItemCommentThreads", "changeThreadPublic"))

			.add(new Mapping(HttpMethod.GET,  "questions", "QuestionsController", "listQuestions"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx", "QuestionsController", "listQuestions"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/details/$qid", "QuestionsController", "listQuestions"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/details/$qid/$config", "QuestionsController", "listQuestions"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/new/$qid/$cid", "QuestionsController", "createNewQuestionInstance"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/upload", "QuestionsController", "showUpload"))
			.add(new Mapping(HttpMethod.POST, "questions/$apctx/upload", "QuestionsController", "doUpload"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/solving/$rqi", "QuestionsController", "showSolving"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/solving/$rqi/$filename", "QuestionsController", "serveQuestionFile"))
			.add(new Mapping(HttpMethod.POST, "questions/$apctx/solving/$rqi", "QuestionsController", "saveSolving"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/inspect/$rqi", "QuestionsController", "inspectSolving"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/inspect/$rqi/$filename", "QuestionsController", "serveQuestionFile"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/verify/$rqi", "QuestionsController", "verifySolving"))
			.add(new Mapping(HttpMethod.GET,  "questions/$apctx/verify/$rqi/$filename", "QuestionsController", "serveQuestionFile"))

			.add(new Mapping(HttpMethod.GET,  "workspaces/$wid/useractivities", "WorkspaceController", "showUserActivities"))
			.add(new Mapping(HttpMethod.GET,  "workspaces/$wid/room/$rid", "WorkspaceController", "showRoom"))
			.add(new Mapping(HttpMethod.GET,  "workspaces/$wid/room/$rid/defnew", "WorkspaceController", "showDefNewQuestion"))
			.add(new Mapping(HttpMethod.POST, "workspaces/$wid/room/$rid/defnew", "WorkspaceController", "saveDefNewQuestion"))
			.add(new Mapping(HttpMethod.POST, "workspaces/$wid/room/$rid/qchangestate", "WorkspaceController", "updateQuestionState"))
			.add(new Mapping(HttpMethod.GET,  "workspaces", "WorkspaceController", "list"))
			.add(new Mapping(HttpMethod.GET,  "workspaces/join/$wid", "WorkspaceController", "processJoinRequest"))
			.add(new Mapping(HttpMethod.GET,  "workspaces/activities/show/$wid", "WorkspaceController", "showActivities"))
			.add(new Mapping(HttpMethod.POST, "workspaces/activities/saveNew/$wid", "WorkspaceController", "saveNewActivity"))
			.add(new Mapping(HttpMethod.GET,  "workspaces/$wid", "WorkspaceController", "show"))
			.add(new Mapping(HttpMethod.GET,  "workspaces/members/$gid", "WorkspaceController", "showGroupForMember"))
			.add(new Mapping(HttpMethod.POST, "workspaces/members/$gid/join", "WorkspaceController", "joinGroupForMember"))
			.add(new Mapping(HttpMethod.POST, "workspaces/members/$gid/leave", "WorkspaceController", "leaveGroupForMember"))
			.add(new Mapping(HttpMethod.POST, "workspaces/members/$gid/createNew", "WorkspaceController", "createNewGroupForMember"))
			.add(new Mapping(HttpMethod.GET,  "admin/workspaces/new", "WorkspaceController", "showNew"))
			.add(new Mapping(HttpMethod.GET,  "admin/workspaces/edit/$wid", "WorkspaceController", "showEdit"))
			.add(new Mapping(HttpMethod.POST, "admin/workspaces/edit", "WorkspaceController", "processUpdate"))
			.add(new Mapping(HttpMethod.GET,  "admin/workspaces/jreqlist/$wid", "WorkspaceController", "listJoinRequests"))
			.add(new Mapping(HttpMethod.POST, "admin/workspaces/jreqlist/$wjrid", "WorkspaceController", "resolveJoinRequest"))

			.add(new Mapping(HttpMethod.GET,  "admin/workspaces/wgrs/new/$wsid", "WorkspaceController", "showNewGrouping"))
			.add(new Mapping(HttpMethod.GET,  "admin/workspaces/wgrs/edit/$gid", "WorkspaceController", "showEditGrouping"))
			.add(new Mapping(HttpMethod.GET,  "admin/workspaces/wgrs/view/$gid", "WorkspaceController", "showViewGrouping"))
			.add(new Mapping(HttpMethod.POST, "admin/workspaces/wgrs/edit", "WorkspaceController", "processUpdateGrouping"))
			.add(new Mapping(HttpMethod.POST, "admin/workspaces/wgrs/validate/$gid", "WorkspaceController", "validateGroup"))
			.add(new Mapping(HttpMethod.POST, "admin/workspaces/wgrs/invalidate/$gid", "WorkspaceController", "invalidateGroup"))
			.add(new Mapping(HttpMethod.POST, "admin/workspaces/wsqd/$wsqdid/invalidateScore", "WorkspaceController", "wsqdInvalidateScore"))
			.add(new Mapping(HttpMethod.POST, "admin/workspaces/wsqd/$wsqdid/validateScore", "WorkspaceController", "wsqdValidateScore"))
			
			.add(new Mapping(HttpMethod.GET,  "courses", "CourseCollections", "showDefault"))
			.add(new Mapping(HttpMethod.GET,  "courses/show/$ccid", "CourseCollections", "showSpecified"))

			.add(new Mapping(HttpMethod.GET,  "course/$apctx/$cid", "CourseController", "show"))
			.add(new Mapping(HttpMethod.GET,  "admin/course/$apctx/form/$cid", "CourseController", "showEdit"))
			.add(new Mapping(HttpMethod.POST, "admin/course/$apctx/form/$cid", "CourseController", "performEdit"))
			
			.add(new Mapping(HttpMethod.GET,  "admin/courseCollection/form/$ccid", "CourseCollections", "showAdminList"))
			.add(new Mapping(HttpMethod.POST, "admin/courseCollection/form/$ccid", "CourseCollections", "updateAdminList"))

			.add(new Mapping(HttpMethod.GET,  "login", "Login", "showLogin"))
			.add(new Mapping(HttpMethod.POST, "login", "Login", "login"))
			.add(new Mapping(HttpMethod.GET,  "logout", "Login", "logout"))
			.add(new Mapping(HttpMethod.GET,  "rules", "Rules", "rules"))
			.add(new Mapping(HttpMethod.POST, "mdu", "MarkDownUtil", "process"))
			.add(new Mapping(HttpMethod.POST, "textFormat", "MarkDownUtil", "format"))
			.add(new Mapping(HttpMethod.GET,  "info/users/$uid", "UserInfo", "infoPage"))
			.add(new Mapping(HttpMethod.GET,  "info/help/$hid", "Help", "helpPage"))
			.add(new Mapping(HttpMethod.GET,  "wishlist", "Wishes", "showWishes"))
			.add(new Mapping(HttpMethod.GET,  "wish/new", "Wishes", "showNewWishEditor"))
			.add(new Mapping(HttpMethod.POST, "wish/save", "Wishes", "saveNewWish"))
			.add(new Mapping(HttpMethod.POST, "wish/vote/save", "Wishes", "saveWishVote"))
			.add(new Mapping(HttpMethod.GET,  "mladmin", "MLAdministration", "showList"))
			.add(new Mapping(HttpMethod.POST, "mladmin/upload", "MLAdministration", "upload"))
			.add(new Mapping(HttpMethod.POST, "mladmin/toggle-visibility/$uid", "MLAdministration", "toggleVisibility"))
			.add(new Mapping(HttpMethod.POST, "mladmin/set-approval/$uid/$status", "MLAdministration", "setApproval"))

			.add(new Mapping(HttpMethod.GET,  "qadmin", "QJSAdministration", "showList"))
			.add(new Mapping(HttpMethod.POST, "qadmin/uploadjs", "QJSAdministration", "uploadjs"))
			.add(new Mapping(HttpMethod.POST, "qadmin/uploadxml", "QJSAdministration", "uploadxml"))
			.add(new Mapping(HttpMethod.POST, "qadmin/uploadxmlz", "QJSAdministration", "uploadxmlz"))
			.add(new Mapping(HttpMethod.POST, "qadmin/toggle-visibility/$uid", "QJSAdministration", "toggleVisibility"))
			.add(new Mapping(HttpMethod.POST, "qadmin/set-approval/$uid/$status", "QJSAdministration", "setApproval"))
			
			
			.add(new Mapping(HttpMethod.GET,  "minilesson/$apctx/$uid/vote", "ItemController", "getUserMLVotes"))
			.add(new Mapping(HttpMethod.POST, "minilesson/$apctx/$uid/vote/$qual/$dif/$dur", "ItemController", "saveUserMLVotes"))
			.add(new Mapping(HttpMethod.POST, "minilesson/$apctx/$uid/dlstats", "ItemController", "dlStats"))
			.add(new Mapping(HttpMethod.GET,  "admin/users/search", "UserAccount", "searchForm"))
			.add(new Mapping(HttpMethod.POST, "admin/users/search", "UserAccount", "search"))
			.add(new Mapping(HttpMethod.GET,  "admin/courses/import", "CourseImporter", "importForm"))
			.add(new Mapping(HttpMethod.POST, "admin/courses/import", "CourseImporter", "importAction"))
			.add(new Mapping(HttpMethod.GET,  "admin/user", "UserAccount", "newUser"))
			.add(new Mapping(HttpMethod.GET,  "admin/user/$id", "UserAccount", "edit"))
			.add(new Mapping(HttpMethod.POST, "admin/user", "UserAccount", "save"))
			.add(new Mapping(HttpMethod.POST, "register", "Registration", "register"))
			.add(new Mapping(HttpMethod.GET,  "activate", "Registration", "activate"))
			.add(new Mapping(HttpMethod.GET,  "admin/logs/flush", "Logs", "logsFlush"))
			.add(new Mapping(HttpMethod.GET,  "test/question/$q/new", "QTester", "newTest"))
			.add(new Mapping(HttpMethod.GET,  "test/question/$q/new/$conf", "QTester", "newTest"))
			.add(new Mapping(HttpMethod.GET,  "test/question/list", "QTester", "list"))
			.add(new Mapping(HttpMethod.POST, "test/question/qupdate", "QTester", "qupdate"))
			.add(new Mapping(HttpMethod.POST, "test/question/qupdatef", "QTester", "qupdatefinish"))
			.add(new Mapping(HttpMethod.GET,  "question/$qid/file/$fid", "QuestionFiles", "process"))
			;

	/**
	 * Map of response handlers.
	 */
	private static Map<Class<?>, ResponseHandler> responseHandlers = new HashMap<>();
	static {
		responseHandlers.put(StreamRetVal.class, new StreamRetValHandler());
		responseHandlers.put(TemplateRetVal.class, new TemplateRetValHandler());
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGetPost(req, resp, HttpMethod.GET);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGetPost(req, resp, HttpMethod.POST);
	}
	
	/**
	 * Method which processes http request for this application. A matching mapping is searched among registered
	 * mappings. If no mapping matches, 404 is returned to client. If mapping is found, its registered controller
	 * method is called. If method returned result which is not <code>null</code>, appropriate result handler is
	 * looked up and executed. If no handler is found, 500 is returned to client.
	 * 
	 * @param req request
	 * @param resp response
	 * @param httpMethod http method
	 * @throws ServletException
	 * @throws IOException
	 */
	private void doGetPost(HttpServletRequest req, HttpServletResponse resp, HttpMethod httpMethod) throws ServletException, IOException {
		boolean matched = urlMappings.match(req.getPathInfo(), req, resp, httpMethod, new ResultCallback() {
			@Override
			public void accept(Object result) throws ServletException, IOException {
				handleResult(req, resp, result);
			}
		});
		
		if(matched) {
			return;
		}

		resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
		new StreamRetValHandler().handle(req, resp, new StreamRetVal("Requested URL was not found.", StandardCharsets.UTF_8, "text/html; charset=UTF-8", null));
	}

	/**
	 * Method which handles the result generated by controller. Exact processing is specified in {@link #doGetPost(HttpServletRequest, HttpServletResponse, HttpMethod)}.
	 * 
	 * @param req request
	 * @param resp response
	 * @param result result
	 * @throws ServletException
	 * @throws IOException
	 */
	private void handleResult(HttpServletRequest req, HttpServletResponse resp, Object result) throws ServletException, IOException {
		ResponseHandler handler = responseHandlers.get(result.getClass());
		if(handler != null) {
			handler.handle(req, resp, result);
		} else {
			System.out.println("ERROR: got result of class "+result.getClass().getName()+" which does not have output handler!");
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			new StreamRetValHandler().handle(req, resp, new StreamRetVal("msg", StandardCharsets.UTF_8, "text/html; charset=UTF-8", null));
		}
	}

}
