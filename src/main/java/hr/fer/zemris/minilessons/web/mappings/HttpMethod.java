package hr.fer.zemris.minilessons.web.mappings;

/**
 * Http methods we support in mapping specification.
 * 
 * @author marcupic
 */
public enum HttpMethod {
	/**
	 * GET method
	 */
	GET, 
	/**
	 * POST method
	 */
	POST, 
	/**
	 * ANY method
	 */
	ANY;
}