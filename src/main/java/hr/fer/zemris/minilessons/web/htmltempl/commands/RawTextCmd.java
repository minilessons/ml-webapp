package hr.fer.zemris.minilessons.web.htmltempl.commands;

import java.io.IOException;

import hr.fer.zemris.minilessons.VariableMapping;
import hr.fer.zemris.minilessons.web.htmltempl.CommandEnvironment;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;

public class RawTextCmd implements HtmlTemplateCommand {

	private VariableMapping mapping;
	
	public RawTextCmd(VariableMapping mapping) {
		super();
		this.mapping = mapping;
	}

	@Override
	public void execute(CommandEnvironment env) throws IOException {
		if(!mapping.containsKey(env.getCommandArgs())) {
			throw new RuntimeException("Variable '" + env.getCommandArgs() + "' is not found for inclusion.");
		}
		Object content = mapping.get(env.getCommandArgs());
		String varContext = content==null ? "" : content.toString();
		env.getWriter().append(varContext);
	}

}
