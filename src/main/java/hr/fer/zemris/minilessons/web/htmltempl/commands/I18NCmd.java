package hr.fer.zemris.minilessons.web.htmltempl.commands;

import java.io.IOException;
import java.util.Map;

import hr.fer.zemris.minilessons.web.WebUtil;
import hr.fer.zemris.minilessons.web.htmltempl.CommandEnvironment;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;

public class I18NCmd implements HtmlTemplateCommand {

	public static enum I18NEncode {
		HTML,
		RAW,
		JS,
		HTML_ATTRIB
	}
	
	private Map<String,String> translations;
	
	public I18NCmd(Map<String,String> translations) {
		super();
		this.translations = translations;
	}

	@Override
	public void execute(CommandEnvironment env) throws IOException {
		String varValue = env.getCommandArgs().trim();
		I18NEncode encode = I18NEncode.HTML;
		
		// Utvrdi je li prisutan encoding; encoding se piše između dvije zvjezdice: *raw*
		char[] chars = varValue.toCharArray();
		int curr = 0;
		while(curr < chars.length && (chars[curr]==' '||chars[curr]=='\t'||chars[curr]=='\r'||chars[curr]=='\n')) curr++;
		if(curr<chars.length && chars[curr]=='*') {
			curr++;
			int start = curr;
			while(curr < chars.length && chars[curr]!='*') curr++;
			if(curr < chars.length) {
				// Imam encode!
				String encName = new String(chars, start, curr-start).trim();
				encode = determineEncodeFromName(encName);
				curr++;
				while(curr < chars.length && (chars[curr]==' '||chars[curr]=='\t'||chars[curr]=='\r'||chars[curr]=='\n')) curr++;
				if(curr >= chars.length) {
					varValue = "";
				} else {
					varValue = new String(chars, curr, chars.length-curr);
				}
			}
		}

		String value = translations.getOrDefault(varValue, "???"+varValue+"???");
		switch(encode) {
		case HTML: WebUtil.htmlEncode(env.getWriter(), value); break;
		case HTML_ATTRIB: WebUtil.htmlAttribEncode(env.getWriter(), value); break;
		case JS: WebUtil.jsEncode(env.getWriter(), value); break;
		case RAW: env.getWriter().append(value); break;
		}
	}

	private I18NEncode determineEncodeFromName(String encName) {
		if(encName.equalsIgnoreCase("html")) return I18NEncode.HTML;
		if(encName.equalsIgnoreCase("html-attrib")) return I18NEncode.HTML_ATTRIB;
		if(encName.equalsIgnoreCase("js")) return I18NEncode.JS;
		if(encName.equalsIgnoreCase("raw")) return I18NEncode.RAW;
		throw new RuntimeException("Invalid echo encoding found: "+encName+".");
	}
}
