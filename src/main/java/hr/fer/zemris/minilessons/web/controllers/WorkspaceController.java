package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.BaseQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.QIStatus;
import hr.fer.zemris.minilessons.dao.model.QuestionControlResult;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.dao.model.WSActivity;
import hr.fer.zemris.minilessons.dao.model.WSActivityScore;
import hr.fer.zemris.minilessons.dao.model.WSActivityScorePK;
import hr.fer.zemris.minilessons.dao.model.WSActivitySource;
import hr.fer.zemris.minilessons.dao.model.WSGroup;
import hr.fer.zemris.minilessons.dao.model.WSGrouping;
import hr.fer.zemris.minilessons.dao.model.WSJoinRequestStatus;
import hr.fer.zemris.minilessons.dao.model.WSQDWorkflow;
import hr.fer.zemris.minilessons.dao.model.WSQDWorkflowState;
import hr.fer.zemris.minilessons.dao.model.WSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.WSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.Workspace;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinPolicy;
import hr.fer.zemris.minilessons.dao.model.WorkspaceJoinRequest;
import hr.fer.zemris.minilessons.dao.model.WorkspacePage;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DUser;
import hr.fer.zemris.minilessons.domain.model.DWSActivity;
import hr.fer.zemris.minilessons.domain.model.DWSGroup;
import hr.fer.zemris.minilessons.domain.model.DWSGrouping;
import hr.fer.zemris.minilessons.domain.model.DWSQuestionDefinition;
import hr.fer.zemris.minilessons.domain.model.DWorkspace;
import hr.fer.zemris.minilessons.domain.model.DWorkspaceJoinRequest;
import hr.fer.zemris.minilessons.domain.model.DXMLQDef;
import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.questions.quizzes.AbcSingleQuestion;
import hr.fer.zemris.minilessons.questions.quizzes.AbcSingleQuestionGroup;
import hr.fer.zemris.minilessons.questions.quizzes.AbstractQuestion;
import hr.fer.zemris.minilessons.questions.quizzes.QuestionFactory;
import hr.fer.zemris.minilessons.questions.quizzes.QuestionGroupBase;
import hr.fer.zemris.minilessons.questions.quizzes.SupportedXMLQuestions;
import hr.fer.zemris.minilessons.questions.quizzes.TextAnswer;
import hr.fer.zemris.minilessons.questions.quizzes.TextQuestion;
import hr.fer.zemris.minilessons.questions.quizzes.TextQuestionGroup;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.CallAfterLockException;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.controllers.CourseController.KeyValue;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

public class WorkspaceController {

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal list(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		boolean canCreate = canAdmin || MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_CREATE_WORKSPACE.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());

		List<Workspace> myAllWs = dao.findUserWorkspaces(user);
		List<Workspace> joinableWs = dao.findJoinableWorkspaces(user);
		
		List<Workspace> myActiveWs = myAllWs.stream().filter(ws->!ws.isArchived()).collect(Collectors.toList());
		List<Workspace> myArchivedWs = myAllWs.stream().filter(ws->ws.isArchived()).collect(Collectors.toList());
		
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		req.setAttribute("myActiveWs", myActiveWs.stream().map(ws->DWorkspace.fromDAOModel(ws, lang)).sorted((ws1,ws2)->ws1.getTitle().compareTo(ws2.getTitle())).collect(Collectors.toList()));
		req.setAttribute("myArchivedWs", myArchivedWs.stream().map(ws->DWorkspace.fromDAOModel(ws, lang)).sorted((ws1,ws2)->ws1.getTitle().compareTo(ws2.getTitle())).collect(Collectors.toList()));
		req.setAttribute("joinableWs", joinableWs.stream().map(ws->DWorkspace.fromDAOModel(ws, lang)).sorted((ws1,ws2)->ws1.getTitle().compareTo(ws2.getTitle())).collect(Collectors.toList()));
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("canCreate", canCreate);
		
		return new TemplateRetVal("wslist", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public TemplateRetVal processJoinRequest(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String sid = StringUtil.trim(variables.get("wid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		boolean canAccess = ws.getUsers().contains(user);
		if(canAccess) {
			req.setAttribute("errorMsgKey", "wsJoinAlreadyMember");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(ws.getJoinPolicy()==WorkspaceJoinPolicy.CLOSED) {
			req.setAttribute("errorMsgKey", "wsJoiningIsClosed");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<WorkspaceJoinRequest> reqs = dao.findWorkspaceJoinRequests(ws, user);
		List<WorkspaceJoinRequest> openreqs = new ArrayList<>();
		for(WorkspaceJoinRequest r : reqs) {
			switch(r.getStatus()) {
			case BANNED:
				req.setAttribute("errorMsgKey", "bannedWSJoinState");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			case OPEN:
				openreqs.add(r);
				break;
			case ACCEPTED:
				req.setAttribute("errorMsgKey", "inconsistentWSJoinState");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			case REJECTED:
				break;
			}
		}
		if(!openreqs.isEmpty()) {
			if(ws.getJoinPolicy()==WorkspaceJoinPolicy.AUTOMATIC) {
				openreqs.sort((r1,r2)->r1.getCreatedAt().compareTo(r2.getCreatedAt()));
				WorkspaceJoinRequest creq = openreqs.get(0);
				openreqs.remove(0);
				openreqs.forEach(dao::removeWorkspaceJoinRequest);
				
				creq.setResolutionDate(new Date());
				creq.setStatus(WSJoinRequestStatus.ACCEPTED);
				ws.getUsers().add(user);
			}
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+ws.getId());
			return null;
		}
		if(ws.getJoinPolicy()==WorkspaceJoinPolicy.AUTOMATIC) {
			WorkspaceJoinRequest creq = new WorkspaceJoinRequest();
			creq.setCreatedAt(new Date());
			creq.setResolutionDate(creq.getCreatedAt());
			creq.setStatus(WSJoinRequestStatus.ACCEPTED);
			creq.setUser(user);
			creq.setWorkspace(ws);
			dao.saveWorkspaceJoinRequest(creq);
			ws.getUsers().add(user);
		} else {
			WorkspaceJoinRequest creq = new WorkspaceJoinRequest();
			creq.setCreatedAt(new Date());
			creq.setStatus(WSJoinRequestStatus.OPEN);
			creq.setUser(user);
			creq.setWorkspace(ws);
			dao.saveWorkspaceJoinRequest(creq);
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+ws.getId());
		return null;
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal listJoinRequests(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String sid = StringUtil.trim(variables.get("wid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		DAO dao = DAOProvider.getDao();
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canEdit = ws.getOwner().equals(user);
		if(!canAdmin && !canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<WorkspaceJoinRequest> unsolvedJReqs = dao.findUnsolvedWorkspaceJoinRequests(ws);
		DWorkspace dws = DWorkspace.fromDAOModel(ws, lang);
		req.setAttribute("reqs", unsolvedJReqs.stream().map(r->DWorkspaceJoinRequest.fromDAOModel(r, dws)).sorted((r1,r2)->r1.getCreatedAt().compareTo(r2.getCreatedAt())).collect(Collectors.toList()));
		req.setAttribute("statuses", Arrays.asList(WSJoinRequestStatus.values()));
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("canEdit", canEdit);
		req.setAttribute("workspace", dws);
		
		return new TemplateRetVal("wsjrlist", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public Object resolveJoinRequest(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String sid = StringUtil.trim(variables.get("wjrid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			String ret = "{\"err\": true, \"data\": \"Error.\"}";
			return new StreamRetVal(ret, StandardCharsets.UTF_8, "application/json", null);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		WorkspaceJoinRequest jreq = dao.findWorkspaceJoinRequestByID(id);
		if(jreq==null) {
			String ret = "{\"err\": true, \"data\": \"Error.\"}";
			return new StreamRetVal(ret, StandardCharsets.UTF_8, "application/json", null);
		}
		
		Workspace ws = jreq.getWorkspace();
		boolean canEdit = ws.getOwner().equals(user);
		if(!canAdmin && !canEdit) {
			String ret = "{\"err\": true, \"data\": \"No permissions.\"}";
			return new StreamRetVal(ret, StandardCharsets.UTF_8, "application/json", null);
		}
		if(jreq.getStatus()==WSJoinRequestStatus.ACCEPTED) {
			String ret = "{\"err\": true, \"data\": \"Can not revoke granted access.\"}";
			return new StreamRetVal(ret, StandardCharsets.UTF_8, "application/json", null);
		}
		String st = req.getParameter("status");
		WSJoinRequestStatus status = null;
		try {
			if(st!=null) status = WSJoinRequestStatus.valueOf(st);
		} catch(Exception ignorable) {
		}
		if(status==null) {
			String ret = "{\"err\": true, \"data\": \"Error.\"}";
			return new StreamRetVal(ret, StandardCharsets.UTF_8, "application/json", null);
		}
		if(status==WSJoinRequestStatus.OPEN && jreq.getStatus()==WSJoinRequestStatus.OPEN) {
			String ret = "{\"err\": true, \"data\": \"No OP.\"}";
			return new StreamRetVal(ret, StandardCharsets.UTF_8, "application/json", null);
		}

		if(status==WSJoinRequestStatus.ACCEPTED) {
			ws.getUsers().add(jreq.getUser());
		}
		jreq.setResolutionDate(new Date());
		jreq.setStatus(status);
		jreq.setMessage(StringUtil.trim(req.getParameter("message")));
		
		String ret = "{\"err\": false, \"data\": \"OK.\", \"disable\": "+(status==WSJoinRequestStatus.ACCEPTED ? "true" : "false")+"}";
		return new StreamRetVal(ret, StandardCharsets.UTF_8, "application/json", null);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public Object joinGroupForMember(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String sid = StringUtil.trim(variables.get("gid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		String sjgid = StringUtil.trim(req.getParameter("gidToJoin"));
		Long gid = null;
		try {
			if(sjgid!=null) gid = Long.valueOf(sjgid);
		} catch(Exception ignorable) {
		}
		if(gid==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		WSGrouping gr = dao.findWSGroupingByID(id);
		WSGroup g = dao.findWSGroupByID(gid);
		if(gr==null || g==null || !gr.isManagementEnabled() || !g.getGrouping().equals(gr) || !gr.getWorkspace().getUsers().contains(user)) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		if(!g.getSecret().equals(req.getParameter("fgrkey"))) {
			req.setAttribute("errorMsgKey", "wsgroupSecretMismatch");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		// Ako već sada ima previše studenata, nećemo dopustiti ulazak!
		if(gr.getMaxMembers()!=null && g.getUsers().size() >= gr.getMaxMembers()) {
			req.setAttribute("errorMsgKey", "wsgroupIsFull");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(!g.isActive() || g.getUsers().contains(user)) {
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/members/"+g.getGrouping().getId());
			return null;
		}

		String[] lockKey = new String[] {"wsid="+gr.getWorkspace().getId()};
		if(!"1".equals(variables.get("@lockObtained"))) {
			throw new CallAfterLockException(lockKey);
		} else {
			String path = Arrays.asList(lockKey).stream().collect(Collectors.joining("/"));
			if(!path.equals(variables.get("@lockPath"))) {
				throw new RuntimeException("Pogreška pri zaključavanju; očekivani ključ nije jednak stvarnom.");
			}
		}
		
		if(g.isFixed()) {
			WSGroup ng = new WSGroup();
			ng.setActive(true);
			ng.setFixed(false);
			ng.setGrouping(g.getGrouping());
			ng.setName(g.getName());
			ng.setSecret(g.getSecret());
			ng.getUsers().addAll(g.getUsers());
			ng.getUsers().add(user);
			g.setActive(false);
			dao.saveWSGroup(ng);
		} else {
			g.getUsers().add(user);
			g.setValidated(false);
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/members/"+g.getGrouping().getId());
		return null;
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=false) @Transactional
	public Object wsqdInvalidateScore(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return wsqdChangeValidityOfScore(req, resp, variables, false);
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=false) @Transactional
	public Object wsqdValidateScore(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return wsqdChangeValidityOfScore(req, resp, variables, true);
	}

	private Object wsqdChangeValidityOfScore(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, boolean valid) throws IOException {
		Optional<Long> wsqdid = stringToLong(variables.get("wsqdid"));
		if(wsqdid==null || !wsqdid.isPresent()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		WSQuestionDefinition qd = dao.findWSQuestionDefinition(wsqdid.get());
		if(qd==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		if(!canAdmin && !qd.getWorkspace().getOwner().equals(user)) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(qd.getSource() != null && qd.getSource().isInvalid()!=!valid) {
			qd.getSource().setInvalid(!valid);
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+qd.getWorkspace().getId()+"/room/"+qd.getSubpage().getNumID());
		return null;
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public Object createNewGroupForMember(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String sid = StringUtil.trim(variables.get("gid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		WSGrouping gr = dao.findWSGroupingByID(id);
		if(gr==null || !gr.isManagementEnabled()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(!gr.getWorkspace().getUsers().contains(user)) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		String name = StringUtil.trim(req.getParameter("fgrname"));
		if(name==null || !checkGroupName(name)) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		String[] lockKey = new String[] {"wsid="+gr.getWorkspace().getId()};
		if(!"1".equals(variables.get("@lockObtained"))) {
			throw new CallAfterLockException(lockKey);
		} else {
			String path = Arrays.asList(lockKey).stream().collect(Collectors.joining("/"));
			if(!path.equals(variables.get("@lockPath"))) {
				throw new RuntimeException("Pogreška pri zaključavanju; očekivani ključ nije jednak stvarnom.");
			}
		}
		
		List<WSGroup> candidates = dao.findEmptyActiveNonfixedWSGroups(gr);
		
		WSGroup g = null;
		if(!candidates.isEmpty()) {
			g = candidates.get(0);
			g.setValidated(false);
		} else {
			g = new WSGroup();
			g.setActive(true);
			g.setFixed(false);
			g.setGrouping(gr);
		}
		g.setName(name);
		g.setSecret(newGroupSecretKey());
		g.getUsers().add(user);
		
		if(g.getId()==null) {
			dao.saveWSGroup(g);
		}
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/members/"+gr.getId());
		return null;
	}


	private static String newGroupSecretKey() {
		char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		Random r = new Random();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < 10; i++) {
			sb.append(letters[r.nextInt(letters.length)]);
		}
		return sb.toString();
	}
	
	private static boolean checkGroupName(String name) {
		if(name.length()<1 || name.length()>25) return false;
		for(char c : name.toCharArray()) {
			if(Character.isLetterOrDigit(c)) continue;
			if(c==' ' || c=='-') continue;
			return false;
		}
		return true;
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public Object leaveGroupForMember(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String sid = StringUtil.trim(variables.get("gid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		WSGroup gr = dao.findWSGroupByID(id);
		if(gr==null || !gr.getGrouping().isManagementEnabled()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		if(!gr.isActive() || !gr.getUsers().contains(user)) {
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/members/"+gr.getGrouping().getId());
			return null;
		}
		
		String[] lockKey = new String[] {"wsid="+gr.getGrouping().getWorkspace().getId()};
		if(!"1".equals(variables.get("@lockObtained"))) {
			throw new CallAfterLockException(lockKey);
		} else {
			String path = Arrays.asList(lockKey).stream().collect(Collectors.joining("/"));
			if(!path.equals(variables.get("@lockPath"))) {
				throw new RuntimeException("Pogreška pri zaključavanju; očekivani ključ nije jednak stvarnom.");
			}
		}
		
		if(gr.isFixed()) {
			WSGroup ng = new WSGroup();
			ng.setActive(true);
			ng.setFixed(false);
			ng.setGrouping(gr.getGrouping());
			ng.setName(gr.getName());
			ng.setSecret(gr.getSecret());
			ng.getUsers().addAll(gr.getUsers());
			ng.getUsers().remove(user);
			gr.setActive(false);
			dao.saveWSGroup(ng);
		} else {
			gr.getUsers().remove(user);
			gr.setValidated(false);
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/members/"+gr.getGrouping().getId());
		return null;
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showGroupForMember(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String sid = StringUtil.trim(variables.get("gid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		WSGrouping grouping = dao.findWSGroupingByID(id);
		if(grouping==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = grouping.getWorkspace();
		
		if(!ws.getUsers().contains(user)) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));
		req.setAttribute("grouping", DWSGrouping.fromDAOModel(grouping, lang));

		List<WSGroup> memberGroups = dao.findActiveWSGroupsForWSGroupingAndUser(grouping, user);
		if(memberGroups.isEmpty()) {
			req.setAttribute("myGroup", null);
			req.setAttribute("canCreateNewGroup", true);
			req.setAttribute("canLeaveGroup", false);
			req.setAttribute("canJoinGroup", true);
			List<WSGroup> allActiveGroups = dao.findWSGroupsForWSGrouping(grouping, true);
			// filtriraj samo neprazne grupe
			req.setAttribute("groups", allActiveGroups.stream().filter(g->!g.getUsers().isEmpty()).map(g->DWSGroup.fromDAOModel(g, true)).sorted().collect(Collectors.toList()));
		} else if(memberGroups.size()==1) {
			WSGroup g = memberGroups.get(0);
			req.setAttribute("myGroup", g);
			req.setAttribute("canCreateNewGroup", false);
			req.setAttribute("canLeaveGroup", true);
			req.setAttribute("canJoinGroup", false);
			req.setAttribute("groups", null);
			List<DUser> users = g.getUsers().stream().map(u->DUser.fromUser(u, false)).sorted().collect(Collectors.toList());
			req.setAttribute("users", users);
			boolean constrSatisfied = true;
			if(grouping.getMinMembers()!=null && g.getUsers().size() < grouping.getMinMembers()) {
				constrSatisfied = false;
			}
			if(grouping.getMaxMembers()!=null && g.getUsers().size() > grouping.getMaxMembers()) {
				constrSatisfied = false;
			}
			req.setAttribute("constrSatisfied", constrSatisfied);
		} else {
			// TODO: U više je aktivnih grupa; to se nije smjelo dogoditi! Ponudi mu brisanje iz neke od njih?
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		return new TemplateRetVal("wsgroupingm", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=false) @Transactional
	public TemplateRetVal updateQuestionState(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String sid = StringUtil.trim(variables.get("wid"));
		String rid = StringUtil.trim(variables.get("rid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null || (!"1".equals(rid) && !"2".equals(rid) && !"3".equals(rid))) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		Optional<Long> qdid = stringToLong(req.getParameter("qdid"));
		if(qdid==null || !qdid.isPresent()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		Optional<WSQDWorkflowState> wfs = stringToState(req.getParameter("ns"));
		if(wfs==null || !wfs.isPresent()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		//String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canEdit = ws.getOwner().equals(user);

		if(!canAdmin && !canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		WSQuestionDefinition qd = dao.findWSQuestionDefinition(qdid.get());
		if(!qd.getWorkspace().equals(ws)) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		WSQDWorkflowState requestedState = wfs.get();
		if(!nextStatesAfter(qd.getWorkflow(), qd.getWorkflowCurrentState()).contains(requestedState) || (qd.getGrouping()!=null && qd.getGrouping().isManagementEnabled())) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Zadatak je grupni a izmjene grupa su omogućene. Onemogućite izmjene grupa pa zatim aktivirajte pitanje.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(qd.getWorkflow()==WSQDWorkflow.WF3 || qd.getWorkflow()==WSQDWorkflow.WF4) {
			if(qd.getWorkflowCurrentState()==WSQDWorkflowState.UNINIT) {
				qd.setWorkflowCurrentState(requestedState);
			} else if((qd.getWorkflow()==WSQDWorkflow.WF3 && qd.getWorkflowCurrentState()==WSQDWorkflowState.OPEN && requestedState==WSQDWorkflowState.INSPECT) || (qd.getWorkflow()==WSQDWorkflow.WF4 && qd.getWorkflowCurrentState()==WSQDWorkflowState.OPEN && requestedState==WSQDWorkflowState.VERIFY)) {
				// Zaključaj pa vrednuj sva stvorena pitanja...
				String[] lockKey = new String[] {"wsid="+ws.getId()};
				if(!"1".equals(variables.get("@lockObtained"))) {
					throw new CallAfterLockException(lockKey);
				} else {
					String path = Arrays.asList(lockKey).stream().collect(Collectors.joining("/"));
					if(!path.equals(variables.get("@lockPath"))) {
						throw new RuntimeException("Pogreška pri zaključavanju; očekivani ključ nije jednak stvarnom.");
					}
				}
				List<WSQuestionInstance> wsinstances = evalQuestions(user, qd, false);
				updateActivities(qd, wsinstances, false, false);
				if(qd.getWorkflow()==WSQDWorkflow.WF4) {
					assignControlQuestions(dao, qd, wsinstances);
				}
				qd.setWorkflowCurrentState(requestedState);
			} else if(qd.getWorkflow()==WSQDWorkflow.WF3 && qd.getWorkflowCurrentState()==WSQDWorkflowState.INSPECT && requestedState==WSQDWorkflowState.CLOSED) {
				qd.setWorkflowCurrentState(requestedState);
			} else if(qd.getWorkflow()==WSQDWorkflow.WF4 && qd.getWorkflowCurrentState()==WSQDWorkflowState.VERIFY && requestedState==WSQDWorkflowState.INSPECT) {
				// Treba pribrojiti bodove ako je dobro napravio provjeru točnosti dodijeljenog zadatka
				evalQuestionControl(qd);
				qd.setWorkflowCurrentState(requestedState);
			} else if(qd.getWorkflow()==WSQDWorkflow.WF4 && qd.getWorkflowCurrentState()==WSQDWorkflowState.INSPECT && requestedState==WSQDWorkflowState.CLOSED) {
				qd.setWorkflowCurrentState(requestedState);
			}
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+ws.getId()+"/room/"+rid);
		return null;
	}
	
	private void assignControlQuestions(DAO dao, WSQuestionDefinition qd, List<WSQuestionInstance> wsinstances) {
		// Ako imamo manje od dva sudionika, ne možemo napraviti dodjele...
		if(wsinstances.size()<2) return;
		// Inače, da vidimo što trebamo:
		if(qd.getGrouping()==null) {
			// each user got his own question; shuffle among all users
			makeAssignments(wsinstances);
		} else if(qd.isGroupAssignment()) {
			// each group got its own question; shuffle between groups; implementation is same as above
			makeAssignments(wsinstances);
		} else {
			// each user got his own question but users are divided into groups; shuffle among users from the same group if possible
			List<WSGroup> activeGroups = dao.findWSGroupsForWSGrouping(qd.getGrouping(), true);
			Map<User, WSGroup> userToGroupMap = new HashMap<>();
			activeGroups.forEach(g->{
				g.getUsers().forEach(u->userToGroupMap.put(u, g));
			});
			Map<WSGroup, List<WSQuestionInstance>> map = wsinstances.stream().collect(Collectors.groupingBy(qi->userToGroupMap.get(qi.getUser())));
			List<WSQuestionInstance> singles = new ArrayList<>();
			Set<WSGroup> nonsingles = new HashSet<>();
			map.forEach((g,l)->{
				if(l.size()>1) {
					nonsingles.add(g);
				} else {
					singles.add(l.get(0));
				}
			});
			if(singles.size()==1) {
				WSGroup g = nonsingles.iterator().next();
				nonsingles.remove(g);
			}
			if(!singles.isEmpty()) {
				makeAssignments(singles);
			}
			nonsingles.forEach(g->makeAssignments(map.get(g)));
		}
	}

	private void makeAssignments(List<WSQuestionInstance> list) {
		List<WSQuestionInstance> list1 = new ArrayList<>(list);
		Collections.shuffle(list1);
		List<WSQuestionInstance> list2 = new ArrayList<>(list1.subList(1, list1.size()));
		list2.add(list1.get(0));
		for(int i = 0, n = list1.size(); i < n; i++) {
			list1.get(i).setToControl(list2.get(i));
		}
	}
	
	private void updateActivities(WSQuestionDefinition qd, List<WSQuestionInstance> wsinstances, boolean fromAdditional, boolean setAsPending) {
		DAO dao = DAOProvider.getDao();
		if(qd.getActivity()==null) {
			return;
		}
		
		WSActivitySource src = qd.getSource();
		if(src==null ) {
			double maxScore = 0;
			if(qd.getWorkflow()==WSQDWorkflow.WF3) {
				maxScore = qd.getMaxScore();
			} else if(qd.getWorkflow()==WSQDWorkflow.WF4) {
				maxScore = qd.getMaxScore() + qd.getMaxControlScore();
			} else if(qd.getWorkflow()==WSQDWorkflow.WF5) {
				maxScore = qd.getMaxScore() + qd.getMaxAdditionalScore();
			}
			src = new WSActivitySource();
			src.setActivity(qd.getActivity());
			src.setInvalid(false);
			src.setMaxScore(maxScore);
			src.setContext(buildContext(qd));
			dao.saveWSActivitySource(src);
			qd.setSource(src);
		}
		
		for(WSQuestionInstance wsqi : wsinstances) {
			if(qd.isGroupAssignment()) {
				for(User u : wsqi.getGroup().getUsers()) {
					WSActivityScorePK pk = new WSActivityScorePK(u.getId(), src.getId());
					WSActivityScore score = dao.findWSActivityScore(pk);
					boolean shoudCreate = score==null;
					if(shoudCreate) {
						score = new WSActivityScore();
						score.setId(pk);
						score.setSource(src);
						score.setUser(u);
					}
					score.setAssignedAt(new Date());
					score.setPending(setAsPending);
					if(fromAdditional) {
						score.setScore(score.getScore() + src.getMaxScore()*wsqi.getCorrectnessMeasure());
					} else {
						score.setScore(src.getMaxScore()*wsqi.getCorrectnessMeasure());
					}
					if(shoudCreate) {
						dao.saveWSActivityScore(score);
					}
				}
			} else {
				User u = wsqi.getUser();
				WSActivityScorePK pk = new WSActivityScorePK(u.getId(), src.getId());
				WSActivityScore score = dao.findWSActivityScore(pk);
				boolean shoudCreate = score==null;
				if(shoudCreate) {
					score = new WSActivityScore();
					score.setId(pk);
					score.setSource(src);
					score.setUser(u);
				}
				score.setAssignedAt(new Date());
				score.setPending(setAsPending);
				if(fromAdditional) {
					score.setScore(score.getScore() + src.getMaxScore()*wsqi.getCorrectnessMeasure());
				} else {
					score.setScore(src.getMaxScore()*wsqi.getCorrectnessMeasure());
				}
				if(shoudCreate) {
					dao.saveWSActivityScore(score);
				}
			}
			
			
		}
	}

	private void evalQuestionControl(WSQuestionDefinition qd) {
		DAO dao = DAOProvider.getDao();
		List<WSQuestionInstance> wsinstances = dao.findWSQuestionInstances(qd, false);
		for(WSQuestionInstance wsqi : wsinstances) {
			if(wsqi.getToControl()==null) continue;
			boolean ok = false;
			if(wsqi.getToControl().getStatus()==QIStatus.ERROR) {
				if(wsqi.getControlResult()==QuestionControlResult.NOTSOLVED) {
					ok = true;
				}
			} else if(wsqi.getToControl().getStatus()==QIStatus.EVALUATED) {
				if(wsqi.getControlResult()==QuestionControlResult.CORRECT && wsqi.getToControl().getCorrectnessMeasure()>0.99999) {
					ok = true;
				} else if(wsqi.getControlResult()==QuestionControlResult.INCORRECT && wsqi.getToControl().getCorrectnessMeasure()<0.999995) {
					ok = true;
				}
			}
			wsqi.setControllerCorrect(ok);
			if(!ok || wsqi.getDefinition().getSource()==null) continue;
			
			// Ako je zadatak grupni, bodove dobivaju svi članovi grupe:
			if(wsqi.getGroup()!=null) {
				for(User u : wsqi.getGroup().getUsers()) {
					WSActivityScorePK pk = new WSActivityScorePK(u.getId(), wsqi.getDefinition().getSource().getId());
					WSActivityScore sc = dao.findWSActivityScore(pk);
					if(sc != null) {
						sc.setScore(sc.getScore() + wsqi.getDefinition().getMaxControlScore());
					}
				}
			} else {
				WSActivityScorePK pk = new WSActivityScorePK(wsqi.getUser().getId(), wsqi.getDefinition().getSource().getId());
				WSActivityScore sc = dao.findWSActivityScore(pk);
				if(sc != null) {
					sc.setScore(sc.getScore() + wsqi.getDefinition().getMaxControlScore());
				}
			}
		}
	}

	private List<WSQuestionInstance> evalQuestions(User user, WSQuestionDefinition qd, boolean additional) {
		DAO dao = DAOProvider.getDao();
		List<WSQuestionInstance> wsinstances = dao.findWSQuestionInstances(qd, additional);
		String ctx = additional ? qd.getAdditionalQuestionDefinition() : qd.getQuestionDefinition();
		if(ctx.startsWith("xmld=")) {
			evalXMLQuestions(dao, user, qd, additional, ctx.substring(5), wsinstances);
		} else {
			throw new RuntimeException("Nepodržana vrsta zadatka...");
		}
		return wsinstances;
	}

	private void evalXMLQuestions(DAO dao, User user, WSQuestionDefinition qd, boolean additional, String xmldid, List<WSQuestionInstance> wsinstances) {
		XMLQuestionDefinition xmlqd = dao.findXMLQuestionDefinition(xmldid);
		QuestionGroupBase qgb = QuestionFactory.fromText(xmlqd.getXml());
		for(WSQuestionInstance wsqi : wsinstances) {
			if(!wsqi.getActualInstance().startsWith("xmli=")) throw new RuntimeException("Invalid actualInstance found.");
			XMLQuestionInstance qi = dao.findXMLQuestionInstanceByID(Long.valueOf(wsqi.getActualInstance().substring(5)));
			if(qgb.getQuestionType()==SupportedXMLQuestions.ABCSINGLE) {
				evalXMLABCSQuestion(dao, user, xmlqd, (AbcSingleQuestionGroup)qgb, qi);
			} else if(qgb.getQuestionType()==SupportedXMLQuestions.TEXTUAL) {
				evalXMLTextualQuestion(dao, user, xmlqd, (TextQuestionGroup)qgb, qi);
			} else {
				throw new RuntimeException("Evaluacija odabranog tipa pitanja nije podržana.");
			}
			wsqi.setStatus(qi.getStatus());
			wsqi.setCorrectnessMeasure(qi.getCorrectness());
			wsqi.setSolved(qi.isSolved());
		}
	}

	public static void evalXMLABCSQuestion(DAO dao, User user, XMLQuestionDefinition xmlqd, AbcSingleQuestionGroup qg, XMLQuestionInstance xmlqi) {
		String[] sordering = xmlqi.getqData().split(",");
		int[] indexes = new int[sordering.length];
		for(int i = 0; i < sordering.length; i++) {
			indexes[i] = Integer.parseInt(sordering[i]);
		}
		String conf = xmlqi.getConfigName();
		AbcSingleQuestion q = qg.getForID(conf);
		
		Optional<Long> selIndex = WorkspaceController.stringToLong(StringUtil.trim(xmlqi.getuData()));
		int index = selIndex!=null && selIndex.isPresent() ? selIndex.get().intValue() : -1;
		
		boolean correct = index>=0 && index<q.getAnswers().size() && q.getAnswers().get(indexes[index]).isCorrect();

		xmlqi.setEvaluatedOn(new Date());
		xmlqi.setStatus(QIStatus.EVALUATED);
		xmlqi.setCorrectness(correct ? 1.0 : 0.0);
		xmlqi.setSolved(index!=-1);
	}

	public static void evalXMLTextualQuestion(DAO dao, User user, XMLQuestionDefinition xmlqd, TextQuestionGroup qg, XMLQuestionInstance xmlqi) {
		String conf = xmlqi.getConfigName();
		TextQuestion q = qg.getForID(conf);
		String usersAnswer = StringUtil.trim(xmlqi.getuData());
		if(usersAnswer==null) {
			xmlqi.setEvaluatedOn(new Date());
			xmlqi.setStatus(QIStatus.EVALUATED);
			xmlqi.setCorrectness(0.0);
			xmlqi.setSolved(false);
		} else {
			boolean correct = false;
			for(TextAnswer ta : q.getAnswers()) {
				if(ta.isCaseSensitive()) {
					for(I18NText t : ta.getText().getTranslations().values()) {
						if(t.getText().equals(usersAnswer)) {
							correct = true;
							break;
						}
					}
				} else {
					for(I18NText t : ta.getText().getTranslations().values()) {
						if(t.getText().equalsIgnoreCase(usersAnswer)) {
							correct = true;
							break;
						}
					}
				}
				if(correct) break;
			}
			xmlqi.setEvaluatedOn(new Date());
			xmlqi.setStatus(QIStatus.EVALUATED);
			xmlqi.setCorrectness(correct ? 1.0 : 0.0);
			xmlqi.setSolved(true);
		}
	}

	private Optional<WSQDWorkflowState> stringToState(String str) {
		str = StringUtil.trim(str);
		if(str==null) return Optional.empty();
		try {
			return Optional.of(WSQDWorkflowState.valueOf(str));
		} catch(Exception ex) {
			return null;
		}
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public TemplateRetVal showRoom(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String sid = StringUtil.trim(variables.get("wid"));
		String rid = StringUtil.trim(variables.get("rid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null || (!"1".equals(rid) && !"2".equals(rid) && !"3".equals(rid))) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canEdit = ws.getOwner().equals(user);
		boolean canAccess = ws.getUsers().contains(user);

		if(!canAdmin && !canEdit && !canAccess) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("canEdit", canEdit);
		req.setAttribute("canAccess", canAccess);
		req.setAttribute("ws", ws);
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));
		req.setAttribute("rid", rid);

		//String lang = MLCurrentContext.getLocale().getLanguage();

		if("1".equals(rid)) {
			return showRoom1(req, resp, variables, dao, lang, ws, user, canAdmin, canEdit, canAccess);
		} else if("2".equals(rid)) {
			return showRoom2(req, resp, dao, lang, ws, user, canAdmin, canEdit, canAccess);
		} else if("3".equals(rid)) {
			return showRoom3(req, resp, dao, lang, ws, user, canAdmin, canEdit, canAccess);
		} else {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
	}
	
	private TemplateRetVal showRoom1(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, DAO dao, String lang, Workspace ws, User user, boolean canAdmin, boolean canEdit, boolean canAccess) throws IOException {
		if(canAdmin || canEdit) {
			List<DWSQuestionDefinition> qdefs = dao.listDWSQuestionDefinitions(ws, WorkspacePage.QUIZ, lang);
			for(DWSQuestionDefinition qd : qdefs) {
				qd.setNextStates(nextStatesAfter(qd.getWorkflow(), qd.getWorkflowCurrentState()));
			}
			req.setAttribute("qdefs", qdefs);
			return new TemplateRetVal("room1", Technology.THYMELEAF);
		}
		if(canAccess) { // Ako sam student...
			// Vidi ima li koje aktivno pitanje...
			List<WSQuestionDefinition> active = dao.findActiveQuestions(ws,WorkspacePage.QUIZ);
			if(active.size() != 1) {
				req.setAttribute("nothingToDo", true);
				req.setAttribute("nothingToDoKey", 1); // stvarni nothing to do
				return new TemplateRetVal("room1", Technology.THYMELEAF);
			}
			WSQuestionDefinition qd = active.get(0);
			
			if(qd.getWorkflow()==WSQDWorkflow.WF4) {
				if(qd.getWorkflowCurrentState()==WSQDWorkflowState.OPEN) {
					UserQuestionContext wsqiContext = loadWSQuestionInstances(dao, user, qd, req);
					if(wsqiContext.retval != null) return wsqiContext.retval;
					WSQuestionInstance wsqi = wsqiContext.findBasicInstance();
					// Ako nema pitanja - treba ga stvoriti:
					wsqi = createWSQuestionInstance(variables, dao, ws, user, qd, wsqiContext, wsqi);
					if("idle".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 6); // pohranjeno...
					} else if("idlee".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 7); // nije pohranjeno jer se zadatak više ne može rješavati...
					} else {
						// redirekt na stranicu za rjesavanje zadatka...
						resp.sendRedirect(req.getContextPath()+"/ml/questions/-/solving/"+wsqi.getActualInstance().replace('=', '-'));
						return null;
					}
				} else if(qd.getWorkflowCurrentState()==WSQDWorkflowState.INSPECT) {
					UserQuestionContext wsqiContext = loadWSQuestionInstances(dao, user, qd, req);
					if(wsqiContext.retval != null) return wsqiContext.retval;
					WSQuestionInstance wsqi = wsqiContext.findBasicInstance();
					if(wsqi==null) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 1); // stvarni nothing to do
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					}
					if("idle".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 1); // stvarni nothing to do
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					} else if("idlee".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 9); // ne možete više pregledavati zadatak
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					} else {
						// redirekt na stranicu za rjesavanje zadatka...
						resp.sendRedirect(req.getContextPath()+"/ml/questions/-/inspect/"+wsqi.getActualInstance().replace('=', '-'));
						return null;
					}
				} else if(qd.getWorkflowCurrentState()==WSQDWorkflowState.VERIFY) {
					UserQuestionContext wsqiContext = loadWSQuestionInstances(dao, user, qd, req);
					if(wsqiContext.retval != null) return wsqiContext.retval;
					WSQuestionInstance wsqi = wsqiContext.findBasicInstance();
					if(wsqi==null || wsqi.getToControl()==null) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 1); // stvarni nothing to do
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					}
					if("idle".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 1); // stvarni nothing to do
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					} else if("idlee".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 9); // ne možete više pregledavati zadatak
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					} else {
						// redirekt na stranicu za rjesavanje zadatka...
						resp.sendRedirect(req.getContextPath()+"/ml/questions/-/verify/"+wsqi.getActualInstance().replace('=', '-'));
						return null;
					}
				} else {
					req.setAttribute("nothingToDo", true);
					req.setAttribute("nothingToDoKey", 5); // pitanje je u nepodržanom stanju
				}
			} else if(qd.getWorkflow()==WSQDWorkflow.WF3) {
				System.out.println(" ========= DEBUG =========: WF3");
				if(qd.getWorkflowCurrentState()==WSQDWorkflowState.INSPECT) {
					UserQuestionContext wsqiContext = loadWSQuestionInstances(dao, user, qd, req);
					if(wsqiContext.retval != null) return wsqiContext.retval;
					WSQuestionInstance wsqi = wsqiContext.findBasicInstance();
					if(wsqi==null) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 1); // stvarni nothing to do
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					}
					if("idle".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 1); // stvarni nothing to do
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					} else if("idlee".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 9); // ne možete više pregledavati zadatak
						return new TemplateRetVal("room1", Technology.THYMELEAF);
					} else {
						// redirekt na stranicu za rjesavanje zadatka...
						resp.sendRedirect(req.getContextPath()+"/ml/questions/-/inspect/"+wsqi.getActualInstance().replace('=', '-'));
						return null;
					}
				} else if(qd.getWorkflowCurrentState()==WSQDWorkflowState.OPEN) {
					UserQuestionContext wsqiContext = loadWSQuestionInstances(dao, user, qd, req);
					if(wsqiContext.retval != null) return wsqiContext.retval;
					WSQuestionInstance wsqi = wsqiContext.findBasicInstance();
					// Ako nema pitanja - treba ga stvoriti:
					wsqi = createWSQuestionInstance(variables, dao, ws, user, qd, wsqiContext, wsqi);
					if("idle".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 6); // pohranjeno...
					} else if("idlee".equals(req.getParameter("op"))) {
						req.setAttribute("nothingToDo", true);
						req.setAttribute("nothingToDoKey", 7); // nije pohranjeno jer se zadatak više ne može rješavati...
					} else {
						// redirekt na stranicu za rjesavanje zadatka...
						resp.sendRedirect(req.getContextPath()+"/ml/questions/-/solving/"+wsqi.getActualInstance().replace('=', '-'));
						return null;
					}
				} else {
					req.setAttribute("nothingToDo", true);
					req.setAttribute("nothingToDoKey", 5); // pitanje je u nepodržanom stanju
				}
			} else {
				req.setAttribute("nothingToDo", true);
				req.setAttribute("nothingToDoKey", 4); // nepodržan workflow za pitanje u ovoj sobi
			}
		}
		return new TemplateRetVal("room1", Technology.THYMELEAF);
	}

	private WSQuestionInstance createWSQuestionInstance(Map<String, String> variables, DAO dao, Workspace ws, User user,
			WSQuestionDefinition qd, UserQuestionContext wsqiContext, WSQuestionInstance wsqi) {
		if(wsqi==null) {
			String[] lockKey = new String[] {"wsid="+ws.getId(), wsqiContext.group!=null ? "gid="+wsqiContext.group.getId() : "uid="+user.getId()};
			if(!"1".equals(variables.get("@lockObtained"))) {
				throw new CallAfterLockException(lockKey);
			} else {
				String path = Arrays.asList(lockKey).stream().collect(Collectors.joining("/"));
				if(!path.equals(variables.get("@lockPath"))) {
					throw new RuntimeException("Pogreška pri zaključavanju; očekivani ključ nije jednak stvarnom.");
				}
			}
			if(wsqiContext.group!=null) {
				wsqiContext.group.setFixed(true);
			}
			// Sada je zaključano i sigurno; idemo stvoriti...
			if(qd.isGroupAssignment()) {
				// Stvori jedan primjerak za grupu...
				WSInstancesCreatorJob job = null;
				if(qd.getQuestionDefinition().startsWith("xmld=")) {
					job = new XMLWSInstancesCreatorJob(dao, qd.getQuestionDefinition().substring(5));
				} else {
					throw new RuntimeException("Nepodržana vrsta zadatka...");
				}
				List<WSQuestionInstance> wsqis = job.create(qd, wsqiContext.group, Collections.singletonList(null), qd.getQuestionDefinitionConfig());
				wsqi = wsqis.get(0);
			} else if(qd.getGrouping()!=null){
				// Stvori po jedan primjerak za svakog člana grupe, ali s različitim konfiguracijama...
				WSInstancesCreatorJob job = null;
				if(qd.getQuestionDefinition().startsWith("xmld=")) {
					job = new XMLWSInstancesCreatorJob(dao, qd.getQuestionDefinition().substring(5));
				} else {
					throw new RuntimeException("Nepodržana vrsta zadatka...");
				}
				List<WSQuestionInstance> wsqis = job.create(qd, null, new ArrayList<>(wsqiContext.group.getUsers()), qd.getQuestionDefinitionConfig());
				wsqi = wsqis.stream().filter(x->user.equals(x.getUser())).findFirst().get();
			} else {
				// Stvori zadatak za pojedinačnog korisnika...
				WSInstancesCreatorJob job = null;
				if(qd.getQuestionDefinition().startsWith("xmld=")) {
					job = new XMLWSInstancesCreatorJob(dao, qd.getQuestionDefinition().substring(5));
				} else {
					throw new RuntimeException("Nepodržana vrsta zadatka...");
				}
				List<WSQuestionInstance> wsqis = job.create(qd, null, Collections.singletonList(user), qd.getQuestionDefinitionConfig());
				wsqi = wsqis.get(0);
			}
		}
		return wsqi;
	}

	private abstract static class WSInstancesCreatorJob {
		protected DAO dao;
		protected String realqdid;

		public WSInstancesCreatorJob(DAO dao, String realqdid) {
			super();
			this.dao = dao;
			this.realqdid = realqdid;
		}

		public final List<WSQuestionInstance> create(WSQuestionDefinition qd, WSGroup group, List<User> users, String configurations) {
			List<WSQuestionInstance> result = new ArrayList<>();
			loadQDef();
			List<String> configList = prepareConfigurations(users.size(), configurations);
			for(int i = 0; i < users.size(); i++) {
				User u = users.get(i);
				String conf = configList.get(i);
				BaseQuestionInstance realqi = prepareQuestion(u, conf);
				WSQuestionInstance wsqi = new WSQuestionInstance();
				wsqi.setActualInstance(buildContext(realqi));
				wsqi.setAdditional(false);
				wsqi.setDefinition(qd);
				wsqi.setGroup(group);
				wsqi.setUser(group==null ? u : null);
				wsqi.setToControl(null);
				wsqi.setStatus(QIStatus.SOLVABLE);
				dao.saveWSQuestionInstance(wsqi);
				realqi.setContext(buildContext(wsqi));
				result.add(wsqi);
			}
			return result;
		}

		protected abstract BaseQuestionInstance prepareQuestion(User u, String conf);

		protected abstract List<String> prepareConfigurations(int size, String configurations);

		protected abstract void loadQDef();
	}
	
	private class XMLWSInstancesCreatorJob extends WSInstancesCreatorJob {

		private XMLQuestionDefinition realqd;
		private QuestionGroupBase qgb;
		
		public XMLWSInstancesCreatorJob(DAO dao, String realqdid) {
			super(dao, realqdid);
		}

		@Override
		protected void loadQDef() {
			realqd = dao.findXMLQuestionDefinition(realqdid);
			qgb = QuestionFactory.fromText(realqd.getXml());
		}

		@Override
		protected BaseQuestionInstance prepareQuestion(User u, String conf) {
			if(qgb.getQuestionType()==SupportedXMLQuestions.ABCSINGLE) {
				AbcSingleQuestion selected = null;
				for(int i = 0, n = qgb.size(); i < n; i++) {
					if(qgb.getQuestion(i).getId().equals(conf)) {
						selected = (AbcSingleQuestion)qgb.getQuestion(i);
						break;
					}
				}
				if(selected==null) throw new RuntimeException("No question with selected configuration found.");

				// Trenutno radimo bez permutacija opcija, pa fiksno pišemo da biramo opcije 0,1,2,...
				// Kasnije se i ovo može mijenjati / konfigurirati...
				String ordering = IntStream.range(0, selected.getAnswers().size()).mapToObj(Integer::toString).collect(Collectors.joining(","));
				XMLQuestionInstance realqi = new XMLQuestionInstance();
				realqi.setContext(null);
				realqi.setQuestion(realqd);
				realqi.setqData(ordering);
				realqi.setConfigName(conf);
				realqi.setCreatedOn(new Date());
				realqi.setStatus(QIStatus.SOLVABLE);
				realqi.setUser(u);
				dao.saveXMLQuestionInstance(realqi);
				return realqi;
			} else if(qgb.getQuestionType()==SupportedXMLQuestions.TEXTUAL) {
				TextQuestion selected = null;
				for(int i = 0, n = qgb.size(); i < n; i++) {
					if(qgb.getQuestion(i).getId().equals(conf)) {
						selected = (TextQuestion)qgb.getQuestion(i);
						break;
					}
				}
				if(selected==null) throw new RuntimeException("No question with selected configuration found.");
				XMLQuestionInstance realqi = new XMLQuestionInstance();
				realqi.setContext(null);
				realqi.setQuestion(realqd);
				realqi.setqData(null);
				realqi.setConfigName(conf);
				realqi.setCreatedOn(new Date());
				realqi.setStatus(QIStatus.SOLVABLE);
				realqi.setUser(u);
				dao.saveXMLQuestionInstance(realqi);
				return realqi;
			} else {
				throw new RuntimeException("Unsupported question type.");
			}
		}
		
		@Override
		protected List<String> prepareConfigurations(int size, String configurations) {
			String[] givenConfigs = readConfigs(configurations, true);
			Set<String> allowedConfigs = new HashSet<>();
			for(String s : givenConfigs) {
				allowedConfigs.add(s);
			}
			List<String> candidateConfigs = new ArrayList<>();
			for(int i = 0, n = qgb.size(); i < n; i++) {
				AbstractQuestion aq = qgb.getQuestion(i);
				if(givenConfigs.length==0 || allowedConfigs.contains(aq.getId())) {
					candidateConfigs.add(aq.getId());
				}
			}
			if(candidateConfigs.isEmpty()) throw new RuntimeException("Question could not be generated - configuration set is empty.");
			List<String> resultList = new ArrayList<>();
			while(resultList.size() < size) {
				resultList.addAll(candidateConfigs);
			}
			Collections.shuffle(resultList, new Random());
			return resultList.subList(0, size);
		}
	}
	
	private static String buildContext(Object obj) {
		if(obj instanceof XMLQuestionInstance) {
			return "xmli="+((XMLQuestionInstance)obj).getId();
		}
		if(obj instanceof XMLQuestionDefinition) {
			return "xmld="+((XMLQuestionDefinition)obj).getId();
		}
		if(obj instanceof WSQuestionInstance) {
			return "wsqi="+((WSQuestionInstance)obj).getId();
		}
		if(obj instanceof WSQuestionDefinition) {
			return "wsqd="+((WSQuestionDefinition)obj).getId();
		}
		throw new RuntimeException("Unsupported object type for context.");
	}
	
	private UserQuestionContext loadWSQuestionInstances(DAO dao, User user, WSQuestionDefinition qd, HttpServletRequest req) {
		WSGroup g = null;
		if(qd.getGrouping() != null) {
			List<WSGroup> myGroups = dao.findActiveWSGroupsForWSGroupingAndUser(qd.getGrouping(), user);
			if(myGroups.size() != 1) {
				req.setAttribute("nothingToDo", true);
				req.setAttribute("nothingToDoKey", 2); // korisnik nije u grupi ili je u više grupa
				return new UserQuestionContext(null, null, new TemplateRetVal("room1", Technology.THYMELEAF), null);
			}
			g = myGroups.get(0);
			if((g.getGrouping().getMinMembers()!=null && g.getUsers().size()<g.getGrouping().getMinMembers())||(g.getGrouping().getMaxMembers()!=null && g.getUsers().size()<g.getGrouping().getMaxMembers())) {
				if(!g.isValidated()) {
					req.setAttribute("nothingToDo", true);
					req.setAttribute("nothingToDoKey", 3); // korisnik je u grupi koja ima nedopušten broj članova
					return new UserQuestionContext(null, null, new TemplateRetVal("room1", Technology.THYMELEAF), null);
				}
			}
		}
		if(qd.isGroupAssignment()) {
			if(g==null) {
				req.setAttribute("nothingToDo", true);
				req.setAttribute("nothingToDoKey", 8); // zadan je grupni zadatak ali korisnik nije niti u jednoj grupi...
				return new UserQuestionContext(null, null, new TemplateRetVal("room1", Technology.THYMELEAF), null);
			}
			List<WSQuestionInstance> qinsts = dao.findWSQuestionInstances(qd,g);
			return new UserQuestionContext(g, user, null, qinsts);
		} else {
			List<WSQuestionInstance> qinsts = dao.findWSQuestionInstances(qd,user);
			return new UserQuestionContext(qd.getGrouping()!=null ? g : null, user, null, qinsts);
		}
	}

	private static class UserQuestionContext {
		private WSGroup group;
		//private User user;
		private TemplateRetVal retval;
		private List<WSQuestionInstance> qinsts;
		public UserQuestionContext(WSGroup group, User user, TemplateRetVal retval,
				List<WSQuestionInstance> qinsts) {
			super();
			this.group = group;
			//this.user = user;
			this.retval = retval;
			this.qinsts = qinsts;
		}
		private WSQuestionInstance findBasicInstance() {
			if(qinsts==null) return null;
			List<WSQuestionInstance> list = qinsts.stream().filter(q->!q.isAdditional()).collect(Collectors.toList());
			if(list.isEmpty()) return null;
			return list.get(0);
		}
	}
	
	private TemplateRetVal showRoom2(HttpServletRequest req, HttpServletResponse resp, DAO dao, String lang, Workspace ws, User user, boolean canAdmin, boolean canEdit, boolean canAccess) {
		req.setAttribute("errorMsgKey", "notImplemented.soon");
		return new TemplateRetVal("errorPage", Technology.THYMELEAF);
	}
	
	private TemplateRetVal showRoom3(HttpServletRequest req, HttpServletResponse resp, DAO dao, String lang, Workspace ws, User user, boolean canAdmin, boolean canEdit, boolean canAccess) {
		req.setAttribute("errorMsgKey", "notImplemented.soon");
		return new TemplateRetVal("errorPage", Technology.THYMELEAF);
	}
	
	private List<WSQDWorkflowState> nextStatesAfter(WSQDWorkflow workflow, WSQDWorkflowState workflowCurrentState) {
		List<WSQDWorkflowState> list = new ArrayList<>();
		switch(workflow) {
		case WF1:
			if(workflowCurrentState==WSQDWorkflowState.UNINIT) {
				list.add(WSQDWorkflowState.AUTO);
			} else if(workflowCurrentState==WSQDWorkflowState.AUTO) {
				list.add(WSQDWorkflowState.CLOSED);
			} else if(workflowCurrentState==WSQDWorkflowState.CLOSED) {
				list.add(WSQDWorkflowState.AUTO);
			}
			break;
		case WF2:
			if(workflowCurrentState==WSQDWorkflowState.UNINIT) {
				list.add(WSQDWorkflowState.AUTOREP);
			} else if(workflowCurrentState==WSQDWorkflowState.AUTOREP) {
				list.add(WSQDWorkflowState.CLOSED);
			} else if(workflowCurrentState==WSQDWorkflowState.CLOSED) {
				list.add(WSQDWorkflowState.AUTOREP);
			}
			break;
		case WF3:
			if(workflowCurrentState==WSQDWorkflowState.UNINIT) {
				list.add(WSQDWorkflowState.OPEN);
			} else if(workflowCurrentState==WSQDWorkflowState.OPEN) {
				list.add(WSQDWorkflowState.INSPECT);
			} else if(workflowCurrentState==WSQDWorkflowState.INSPECT) {
				list.add(WSQDWorkflowState.CLOSED);
			}
			break;
		case WF4:
			if(workflowCurrentState==WSQDWorkflowState.UNINIT) {
				list.add(WSQDWorkflowState.OPEN);
			} else if(workflowCurrentState==WSQDWorkflowState.OPEN) {
				list.add(WSQDWorkflowState.VERIFY);
			} else if(workflowCurrentState==WSQDWorkflowState.VERIFY) {
				list.add(WSQDWorkflowState.INSPECT);
			} else if(workflowCurrentState==WSQDWorkflowState.INSPECT) {
				list.add(WSQDWorkflowState.CLOSED);
			}
			break;
		case WF5:
			if(workflowCurrentState==WSQDWorkflowState.UNINIT) {
				list.add(WSQDWorkflowState.AUTOSUB);
			} else if(workflowCurrentState==WSQDWorkflowState.AUTOSUB) {
				list.add(WSQDWorkflowState.CLOSED);
			} else if(workflowCurrentState==WSQDWorkflowState.CLOSED) {
				list.add(WSQDWorkflowState.AUTOSUB);
			}
			break;
		}
		return list;
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showDefNewQuestion(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String sid = StringUtil.trim(variables.get("wid"));
		String rid = StringUtil.trim(variables.get("rid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null || (!"1".equals(rid) && !"2".equals(rid) && !"3".equals(rid))) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canEdit = ws.getOwner().equals(user);
		boolean canAccess = false;

		if(!canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("canEdit", canEdit);
		req.setAttribute("canAccess", canAccess);
		req.setAttribute("ws", ws);
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));
		req.setAttribute("rid", rid);
		req.setAttribute("subpage", WorkspacePage.fromNumID(rid));

		List<DXMLQDef> qdefs = new ArrayList<>(); 
		qdefs.add(new DXMLQDef());
		qdefs.addAll(dao.listXMLQuestionsForOwner(user, lang));
		req.setAttribute("qdefs", qdefs);

		List<WSGrouping> grlist = dao.findGroupingsForWorkspace(ws);
		List<DWSGrouping> dgrlist = new ArrayList<>();
		dgrlist.add(new DWSGrouping());
		dgrlist.addAll(grlist.stream().map(g->DWSGrouping.fromDAOModel(g, lang)).collect(Collectors.toList()));
		req.setAttribute("groupings", dgrlist);
		
		req.setAttribute("workflows", Stream.of(WSQDWorkflow.values()).map(w->new KeyValue(w.name(), w.getTitle())).collect(Collectors.toList()));

		List<WSActivity> activities = dao.findActivitiesForWorkspace(ws);
		List<DWSActivity> dactivities = new ArrayList<>();
		dactivities.add(new DWSActivity());
		dactivities.addAll(activities.stream().map(a->new DWSActivity(a.getId(), a.getTitles().getOrDefault(lang, "???"))).collect(Collectors.toList()));
		req.setAttribute("activities", dactivities);
		
		return new TemplateRetVal("wsnewqdef", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public TemplateRetVal saveDefNewQuestion(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String sid = StringUtil.trim(variables.get("wid"));
		String rid = StringUtil.trim(variables.get("rid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null || (!"1".equals(rid) && !"2".equals(rid) && !"3".equals(rid))) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canEdit = ws.getOwner().equals(user);
		boolean canAccess = false;

		if(!canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		WorkspacePage subpage = WorkspacePage.fromNumID(rid);
		
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("canEdit", canEdit);
		req.setAttribute("canAccess", canAccess);
		req.setAttribute("ws", ws);
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));
		req.setAttribute("rid", rid);
		req.setAttribute("subpage", subpage);

		WSQDWorkflow wf = null;
		try {
			wf = WSQDWorkflow.valueOf(req.getParameter("workflows"));
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Workflow nije odabran.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(wf!=WSQDWorkflow.WF3 && wf!=WSQDWorkflow.WF4) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Trenutno su podržani samo {"+WSQDWorkflow.WF3.getTitle()+","+WSQDWorkflow.WF4.getTitle()+"}.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		Optional<Long> grgid = stringToLong(req.getParameter("grouping"));
		if(grgid == null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Predana je nevaljala grupacija.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		WSGrouping grouping = grgid.isPresent() ? dao.findWSGroupingByID(grgid.get()) : null;
		if(grgid.isPresent() && (grouping==null || !grouping.getWorkspace().equals(ws))) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Predana je nevaljala grupacija.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		Optional<Long> actid = stringToLong(req.getParameter("activity"));
		if(actid == null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Predana je nevaljala aktivnost.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		WSActivity activity = actid.isPresent() ? dao.findWSActivityByID(actid.get()) : null;
		if(actid.isPresent() && (activity==null || !activity.getWorkspace().equals(ws))) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Predana je nevaljala aktivnost.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		List<String> errors = new ArrayList<>();
		
		List<KeyValue> translations = readTranslations(req, "1", errors);
		if(!errors.isEmpty()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", errors.toString());
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(translations.isEmpty()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Naslov mora biti predan na barem jednom jeziku.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		Optional<String> xmldefid = stringToID(req.getParameter("qdef"));
		if(xmldefid==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Pogreška pri odabiru zadatka.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(!xmldefid.isPresent()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Niste odabrali zadatak.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		XMLQuestionDefinition qdef = dao.findXMLQuestionDefinition(xmldefid.get());
		if(qdef==null || (!qdef.isPublicQuestion() && !qdef.getOwner().equals(user))) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Pogreška pri odabiru zadatka.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String questionDefinition = "xmld="+qdef.getId();
		
		String[] configs = readConfigs(req.getParameter("questionDefinitionConfig"), true);
		if(configs==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Nešto nije u redu s danim konfiguracijama osnovnog zadatka.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(qdef!=null) {
			checkXMLQConfig(qdef, configs, errors);
		} /*else {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Nepodržana vrsta zadatka. Ne mogu provjeriti konfiguracije.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}*/
		if(!errors.isEmpty()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", errors.toString());
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean groupAssignment = "1".equals(req.getParameter("groupAssignment"));
		boolean acceptIncorrectSolution = "1".equals(req.getParameter("acceptIncorrectSolution"));

		if(groupAssignment==true && grouping==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Ne možete podesiti da je zadatak grupni, a ne zadati grupaciju za valjane grupe.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		Optional<Double> maxScore = readDecimal(req.getParameter("maxScore"));
		Optional<Double> maxAdditionalScore = readDecimal(req.getParameter("maxAdditionalScore"));
		Optional<Double> maxControlScore = readDecimal(req.getParameter("maxControlScore"));

		if(maxScore==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "maxScore se ne da protumačiti kao decimalni broj.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(maxAdditionalScore==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "maxAdditionalScore se ne da protumačiti kao decimalni broj.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(maxControlScore==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "maxControlScore se ne da protumačiti kao decimalni broj.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		Optional<Date> solvableSince = readDateTime(req.getParameter("solvableSince"));
		Optional<Date> solvableUntil = readDateTime(req.getParameter("solvableUntil"));
		Optional<Date> additionalSolvableUntil = readDateTime(req.getParameter("additionalSolvableUntil"));
		if(solvableSince==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "solvableSince se ne da protumačiti kao datum i vrijeme.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(solvableUntil==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "solvableUntil se ne da protumačiti kao datum i vrijeme.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(additionalSolvableUntil==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "additionalSolvableUntil se ne da protumačiti kao datum i vrijeme.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		boolean additionalGroupAssignment = "1".equals(req.getParameter("additionalGroupAssignment"));
		if(additionalGroupAssignment==true && grouping==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Ne možete podesiti da je dodatni zadatak grupni, a ne zadati grupaciju za valjane grupe.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		Optional<Long> axmldefid = stringToLong(req.getParameter("aqdef"));
		if(axmldefid==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Pogreška pri odabiru dodatnog zadatka.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		XMLQuestionDefinition aqdef = null;
		String[] aconfigs = null;
		String additionalQuestionDefinition = null;
		if(axmldefid.isPresent()) {
			aqdef = dao.findXMLQuestionDefinition(xmldefid.get());

			if(aqdef==null || (!aqdef.isPublicQuestion() && !aqdef.getOwner().equals(user))) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				req.setAttribute("errorDetails", "Pogreška pri odabiru dodatnog zadatka.");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			
			aconfigs = readConfigs(req.getParameter("additionalQuestionDefinitionConfigs"), true);
			if(aconfigs==null) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				req.setAttribute("errorDetails", "Nešto nije u redu s danim konfiguracijama dodatnog zadatka.");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			
			additionalQuestionDefinition = "xmld="+aqdef.getId();
		}
		
		if(aqdef!=null) {
			checkXMLQConfig(aqdef, aconfigs, errors);
		} /*else {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Nepodržana vrsta zadatka. Ne mogu provjeriti konfiguracije.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}*/
		if(!errors.isEmpty()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", errors.toString());
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		if(activity != null && !maxScore.isPresent()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			req.setAttribute("errorDetails", "Niste definirali koliko se bodova dobiva za rješavanje osnovnog zadatka a specificirali ste aktivnost za evidenciju bodova.");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(wf==WSQDWorkflow.WF3) {
			if(additionalQuestionDefinition != null || additionalSolvableUntil.isPresent() || (aconfigs!=null && aconfigs.length!=0) || maxAdditionalScore.isPresent()) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				req.setAttribute("errorDetails", "Workflow "+wf.getTitle()+" ne podržava specificiranje dodatnih zadataka.");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			if(maxControlScore.isPresent()) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				req.setAttribute("errorDetails", "Workflow "+wf.getTitle()+" ne podržava zadavanje zadatka provjere drugog zadatka pa bodovi ne smiju biti specificirani.");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
		if(wf==WSQDWorkflow.WF4) {
			if(additionalQuestionDefinition != null || additionalSolvableUntil.isPresent() || (aconfigs!=null && aconfigs.length!=0) || maxAdditionalScore.isPresent()) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				req.setAttribute("errorDetails", "Workflow "+wf.getTitle()+" ne podržava specificiranje dodatnih zadataka.");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			if(activity != null && !maxControlScore.isPresent()) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				req.setAttribute("errorDetails", "Workflow "+wf.getTitle()+" podržava zadavanje zadatka provjere drugog zadatka pa bodovi moraju biti specificirani.");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
		
		WSQuestionDefinition wsqd = new WSQuestionDefinition();
		wsqd.setAcceptIncorrectSolution(acceptIncorrectSolution);
		wsqd.setActivity(activity);
		wsqd.setAdditionalGroupAssignment(additionalGroupAssignment);
		wsqd.setAdditionalQuestionDefinition(additionalQuestionDefinition);
		wsqd.setAdditionalQuestionDefinitionConfigs(aconfigs==null ? null : Stream.of(aconfigs).collect(Collectors.joining(",")));
		wsqd.setAdditionalSolvableUntil(additionalSolvableUntil.orElse(null));
		wsqd.setGroupAssignment(groupAssignment);
		wsqd.setGrouping(grouping);
		wsqd.setMaxAdditionalScore(maxAdditionalScore.orElse(0.0));
		wsqd.setMaxControlScore(maxControlScore.orElse(0.0));
		wsqd.setMaxScore(maxScore.orElse(0.0));
		wsqd.setQuestionDefinition(questionDefinition);
		wsqd.setQuestionDefinitionConfig(configs == null ? null : Stream.of(configs).collect(Collectors.joining(",")));
		wsqd.setSolvableSince(solvableSince.orElse(null));
		wsqd.setSolvableUntil(solvableUntil.orElse(null));
		wsqd.setSource(null);
		wsqd.setSubpage(subpage);
		translations.forEach(kv->wsqd.getTitles().put(kv.getKey(), kv.getValue()));
		wsqd.setWorkflow(wf);
		wsqd.setWorkflowCurrentState(WSQDWorkflowState.UNINIT);
		wsqd.setWorkspace(ws);
		wsqd.setCreatedAt(new Date());

		dao.saveWSQuestionDefinition(wsqd);
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+ws.getId()+"/room/"+rid);
		return null;
	}

	private Optional<Date> readDateTime(String str) {
		str = StringUtil.trim(str);
		if(str==null) return Optional.empty();
		try {
			LocalDateTime ldt = LocalDateTime.parse(str.length()<19 ? str+":00" : str);
			return Optional.of(Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant()));
		} catch(Exception ex) {
		}
		return null;
	}

	private Optional<Double> readDecimal(String str) {
		str = StringUtil.trim(str);
		if(str==null) return Optional.empty();
		try {
			return Optional.of(Double.valueOf(str));
		} catch(Exception ex) {
		}
		try {
			return Optional.of(Double.valueOf(str.replace(',', '.')));
		} catch(Exception ex) {
		}
		return null;
	}

	private void checkXMLQConfig(XMLQuestionDefinition qdef, String[] configs, List<String> errors) {
		QuestionGroupBase q = null;
		try {
			q = QuestionFactory.fromText(qdef.getXml());
		} catch(Exception ex) {
			errors.add("XML se ne da parsirati. " + ex.getMessage());
			return;
		}
		
		Set<String> legalIDs = new HashSet<>();
		try {
			for(int i = 0, n = q.size(); i < n; i++) {
				AbstractQuestion aq = q.getQuestion(i);
				legalIDs.add(aq.getId());
			}
		} catch(Exception ex) {
			errors.add("Problem s prikupljanjem konfiguracija. " + ex.getMessage());
			return;
		}
		
		for(String s : configs) {
			if(!legalIDs.contains(s)) {
				errors.add("Konfiguracija '" + s +"' ne postoji u zadatku.");
				return;
			}
		}
	}

	private String[] readConfigs(String str, boolean xmlconf) {
		str = StringUtil.trim(str);
		if(str==null) return new String[0];
		List<String> list = new ArrayList<>();
		for(String s : str.split(",")) {
			s = s.trim();
			if(s.isEmpty()) return null;
			if(xmlconf) {
				try {
					Long.parseLong(s);
				} catch(Exception ex) {
					return null;
				}
			} else {
				if(!QTester.checkConfigurationName(s)) return null;
			}
			if(list.contains(s)) return null;
			list.add(s);
		}
		return list.toArray(new String[list.size()]);
	}

	public static Optional<String> stringToID(String str) {
		str = StringUtil.trim(str);
		if(str==null) return Optional.empty();
		char[] chars = str.toCharArray();
		for(char c : chars) {
			if(c>='0' && c<='9') continue;
			if(c>='A' && c<='Z') continue;
			if(c>='a' && c<='z') continue;
			return null;
		}
		return Optional.of(str);
	}
	
	public static Optional<Long> stringToLong(String str) {
		str = StringUtil.trim(str);
		if(str==null) return Optional.empty();
		try {
			return Optional.of(Long.valueOf(str));
		} catch(Exception ex) {
			return null;
		}
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showUserActivities(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String sid = StringUtil.trim(variables.get("wid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canEdit = ws.getOwner().equals(user);
		boolean canAccess = ws.getUsers().contains(user);

		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("canEdit", canEdit);
		req.setAttribute("canAccess", canAccess);
		req.setAttribute("ws", ws);
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));
		
		if(!canAdmin && !canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<WSActivityScore> scoreList = dao.listAllWorkspaceValidActivityUserScores(ws);
		List<WSActivity> activitiesList = dao.findActivitiesForWorkspace(ws);

		List<User> users = new ArrayList<>(ws.getUsers());
		users.sort(Comparator.naturalOrder());
		activitiesList.sort((a,b)->a.getTitles().getOrDefault(lang, "???").compareTo(b.getTitles().getOrDefault(lang, "???")));
		
		Map<User,Map<WSActivity,List<WSActivityScore>>> detailsMap = new LinkedHashMap<>();
		Map<User,ScoreTotals> grandTotalsMap = new LinkedHashMap<>();
		Map<User,Map<WSActivity,ScoreTotals>> totalsMap = new LinkedHashMap<>();
		for(User u : users) {
			Map<WSActivity,List<WSActivityScore>> m = new LinkedHashMap<>();
			Map<WSActivity,ScoreTotals> tm = new LinkedHashMap<>();
			detailsMap.put(u, m);
			grandTotalsMap.put(u, new ScoreTotals());
			totalsMap.put(u, tm);
			for(WSActivity a : activitiesList) {
				m.put(a, new ArrayList<>());
				tm.put(a, new ScoreTotals());
			}
			
		}
		for(WSActivityScore s : scoreList) {
			detailsMap.get(s.getUser()).get(s.getSource().getActivity()).add(s);
			ScoreTotals st = totalsMap.get(s.getUser()).get(s.getSource().getActivity());
			st.score += s.getScore();
			st.maxScore += s.getSource().getMaxScore();
			st.present = true;
			
			ScoreTotals sgt = grandTotalsMap.get(s.getUser());
			sgt.score += s.getScore();
			sgt.maxScore += s.getSource().getMaxScore();
			sgt.present = true;
		}

		req.setAttribute("curlang", lang);
		req.setAttribute("activities", activitiesList);
		req.setAttribute("details", detailsMap);
		req.setAttribute("totals", totalsMap);
		req.setAttribute("grandTotals", grandTotalsMap);
		
		return new TemplateRetVal("wsactscores", Technology.THYMELEAF);
	}
	
	public static class ScoreTotals {
		private boolean present;
		private double score;
		private double maxScore;

		public boolean isPresent() {
			return present;
		}
		public double getScore() {
			return score;
		}
		public double getMaxScore() {
			return maxScore;
		}
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal show(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String sid = StringUtil.trim(variables.get("wid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canEdit = ws.getOwner().equals(user);
		boolean canAccess = ws.getUsers().contains(user);

		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("canEdit", canEdit);
		req.setAttribute("canAccess", canAccess);
		req.setAttribute("ws", ws);
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));

		//String lang = MLCurrentContext.getLocale().getLanguage();

		// Ako nisam niti vlasnik niti pridruženi student (i nisam admin)
		if(!canEdit && !canAccess && !canAdmin) {
			// Vidi ima li zahtjeva za pridruživanjem... Pošalji ga na prikladnu stranicu...
			boolean haveOpen = false;
			boolean haveAccepted = false;
			boolean haveBanned = false;
			//boolean hasRejected = false;
			List<WorkspaceJoinRequest> reqs = dao.findWorkspaceJoinRequests(ws, user);
			for(WorkspaceJoinRequest r : reqs) {
				if(r.getStatus()==WSJoinRequestStatus.OPEN) {
					haveOpen = true;
					continue;
				}
				if(r.getStatus()==WSJoinRequestStatus.ACCEPTED) {
					haveAccepted = true;
					continue;
				}
				if(r.getStatus()==WSJoinRequestStatus.BANNED) {
					haveBanned = true;
					continue;
				}
				if(r.getStatus()==WSJoinRequestStatus.REJECTED) {
					//hasRejected = true;
					continue;
				}
			}
			if(haveAccepted) {
				req.setAttribute("errorMsgKey", "inconsistentWSJoinState");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			if(haveBanned) {
				req.setAttribute("errorMsgKey", "bannedWSJoinState");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			req.setAttribute("canSendJoinRequest", !haveOpen);
			req.setAttribute("joinRequests", reqs);
			req.setAttribute("renderJoinInfo", true);

			return new TemplateRetVal("wshome", Technology.THYMELEAF);
		}

		req.setAttribute("renderJoinInfo", false);
		req.setAttribute("curlang", lang);

		List<WSGrouping> groupings = new ArrayList<>(ws.getGroupings());
		groupings.sort((g1,g2)->g1.getTitles().getOrDefault(lang, "???").compareTo(g2.getTitles().getOrDefault(lang, "???")));
		req.setAttribute("groupings", groupings);

		return new TemplateRetVal("wshome", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=false) @Transactional
	public TemplateRetVal saveNewActivity(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String sid = StringUtil.trim(variables.get("wid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		//String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canEdit = ws.getOwner().equals(user);
		boolean canAccess = ws.getUsers().contains(user);

		if(!canAccess && !canAdmin && !canEdit) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		List<String> errors = new ArrayList<>();

		List<KeyValue> entries1 = readTranslations(req, "1", errors);

		if(!errors.isEmpty()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		WSActivity activity = new WSActivity();
		activity.setWorkspace(ws);
		entries1.forEach(e->activity.getTitles().put(e.getKey(), e.getValue()));
		
		dao.saveWSActivity(activity);

		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/activities/show/"+ws.getId());
		return null;
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showActivities(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String sid = StringUtil.trim(variables.get("wid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		DAO dao = DAOProvider.getDao();
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canEdit = ws.getOwner().equals(user);
		boolean canAccess = ws.getUsers().contains(user);

		if(!canAccess && !canAdmin && !canEdit) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("canEdit", canEdit);
		req.setAttribute("canAccess", canAccess);
		req.setAttribute("ws", ws);
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));

		if(canAdmin || canEdit) {
			List<WSActivity> activities = dao.findActivitiesForWorkspace(ws);
			req.setAttribute("activities", activities);
		}
		if(canAccess) {
			Map<Long, Double> totals = new HashMap<>();
			Map<Long, Double> maxTotals = new HashMap<>();
			List<WSActivityScore> scores = dao.findWSActivityScores(ws, user);
			Set<WSActivity> actSet = new HashSet<>();
			for(WSActivityScore s : scores) {
				if(s.isPending()) continue;
				if(s.getSource().isInvalid()) continue;
				WSActivity act = s.getSource().getActivity();
				totals.merge(act.getId(), s.getScore(), (d1,d2)->d1+d2);
				actSet.add(act);
			}
			List<WSActivitySource> allSources = dao.findAllWSActivitySources(ws);
			for(WSActivitySource src : allSources) {
				if(src.isInvalid()) continue;
				maxTotals.merge(src.getActivity().getId(), src.getMaxScore(), (d1,d2)->d1+d2);
				actSet.add(src.getActivity());
			}
			List<WSActivity> activities = new ArrayList<>(actSet);
			activities.sort((a1,a2)->a1.getTitles().getOrDefault(lang, "???").compareTo(a2.getTitles().getOrDefault(lang, "???")));
			req.setAttribute("activities", activities);
			req.setAttribute("scores", scores);
			req.setAttribute("totals", totals);
			req.setAttribute("maxTotals", maxTotals);
		}

		req.setAttribute("curlang", lang);

		return new TemplateRetVal("wsactivities", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showNew(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		boolean canCreate = canAdmin || MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_CREATE_WORKSPACE.getPermissonName());

		if(!canCreate) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		List<KeyValue> entries = new ArrayList<>();
		entries.add(new KeyValue("", ""));
		req.setAttribute("translations1", entries);
		
		List<KeyValue> entries2 = new ArrayList<>();
		entries2.add(new KeyValue("", ""));
		req.setAttribute("translations2", entries2);

		req.setAttribute("joinPolicies", Arrays.asList(WorkspaceJoinPolicy.values()).stream().map(Object::toString).collect(Collectors.toList()));
		req.setAttribute("joinPolicy", WorkspaceJoinPolicy.AUTOMATIC.toString());
		req.setAttribute("archived", false);
		req.setAttribute("wsid", null);
		return new TemplateRetVal("wseditform", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showEdit(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		String sid = StringUtil.trim(variables.get("wid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		boolean canEdit = canAdmin || ws.getOwner().equals(user);
		if(!canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<KeyValue> entries = ws.getTitles().entrySet().stream().sorted((t1,t2)->t1.getKey().compareTo(t2.getKey())).map(t->new KeyValue(t.getKey(), t.getValue())).collect(Collectors.toList());
		if(entries.isEmpty()) entries.add(new KeyValue("", ""));
		req.setAttribute("translations1", entries);
		
		List<KeyValue> entries2 = ws.getDescriptions().entrySet().stream().sorted((t1,t2)->t1.getKey().compareTo(t2.getKey())).map(t->new KeyValue(t.getKey(), t.getValue())).collect(Collectors.toList());
		if(entries2.isEmpty()) entries2.add(new KeyValue("", ""));
		req.setAttribute("translations2", entries2);

		req.setAttribute("joinPolicies", Arrays.asList(WorkspaceJoinPolicy.values()).stream().map(Object::toString).collect(Collectors.toList()));
		req.setAttribute("joinPolicy", ws.getJoinPolicy().toString());
		req.setAttribute("archived", ws.isArchived());
		req.setAttribute("wsid", ws.getId());
		return new TemplateRetVal("wseditform", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public TemplateRetVal processUpdate(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		boolean canCreate = canAdmin || MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_CREATE_WORKSPACE.getPermissonName());

		if(!canCreate) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		String sid = StringUtil.trim(req.getParameter("wsid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(sid!=null && id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = null;
		if(id != null) {
			ws = dao.findWorkspaceByID(id);
			if(ws==null ) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
		
		boolean canEdit = canAdmin || id==null || ws.getOwner().equals(user);
		if(!canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<String> errors = new ArrayList<>();

		List<KeyValue> entries1 = readTranslations(req, "1", errors);
		List<KeyValue> entries2 = readTranslations(req, "2", errors);

		WorkspaceJoinPolicy jp = null;
		try {
			jp = WorkspaceJoinPolicy.valueOf(req.getParameter("joinPolicy"));
		} catch(Exception ex) {
			errors.add("JoinPolicy is invalid.");
		}
		boolean archived = "1".equals(req.getParameter("archived"));
		
		req.setAttribute("translations1", entries1);
		req.setAttribute("translations2", entries2);
		req.setAttribute("joinPolicies", Arrays.asList(WorkspaceJoinPolicy.values()).stream().map(Object::toString).collect(Collectors.toList()));
		req.setAttribute("joinPolicy", jp==null ? null : jp.toString());
		req.setAttribute("archived", archived);
		req.setAttribute("wsid", id);

		Set<String> langs1 = entries1.stream().filter(kv->kv.getKey()!=null).map(kv->kv.getKey()).collect(Collectors.toSet());
		Set<String> langs2 = entries2.stream().filter(kv->kv.getKey()!=null).map(kv->kv.getKey()).collect(Collectors.toSet());
		if(!langs1.equals(langs2)) {
			errors.add("Not same set of languages was defined for titles and descriptions. Please correct this.");
		}
		if(!errors.isEmpty()) {
			req.setAttribute("errors", errors);
			return new TemplateRetVal("wseditform", Technology.THYMELEAF);
		}

		boolean addnew = ws==null;
		
		if(addnew) {
			ws = new Workspace();
			ws.setCreatedAt(new Date());
			ws.setOwner(user);
		}
		
		ws.setArchived(archived);
		ws.setJoinPolicy(jp);
		
		final Workspace fws = ws;
		ws.getTitles().keySet().removeIf(k->!langs1.contains(k));
		entries1.forEach(e->fws.getTitles().put(e.getKey(), e.getValue()));
		ws.getDescriptions().keySet().removeIf(k->!langs2.contains(k));
		entries2.forEach(e->fws.getDescriptions().put(e.getKey(), e.getValue()));

		if(addnew) dao.saveWorkspace(ws);
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+ws.getId());
		return null;
	}

	
	//------------------------------------------------

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showNewGrouping(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		
		String sid = StringUtil.trim(variables.get("wsid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Workspace ws = dao.findWorkspaceByID(id);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		boolean canCreate = canAdmin || ws.getOwner().equals(user);

		if(!canCreate) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		List<KeyValue> entries = new ArrayList<>();
		entries.add(new KeyValue("", ""));
		req.setAttribute("translations1", entries);
		
		String lang = MLCurrentContext.getLocale().getLanguage();
		req.setAttribute("managementEnabled", false);
		req.setAttribute("minMembers", "");
		req.setAttribute("maxMembers", "");
		req.setAttribute("wsid", ws.getId());
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));
		return new TemplateRetVal("wsgroupingeditform", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showViewGrouping(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		String sid = StringUtil.trim(variables.get("gid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		WSGrouping g = dao.findWSGroupingByID(id);
		if(g==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		Workspace ws = g.getWorkspace();
		
		boolean canEdit = canAdmin || ws.getOwner().equals(user);
		if(!canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		List<WSGroup> groups = dao.findWSGroupsForWSGrouping(g, true);
		List<DWSGroup> dgroups = groups.stream().map(x->DWSGroup.fromDAOModelWithUsers(x)).sorted().collect(Collectors.toList());

		String lang = MLCurrentContext.getLocale().getLanguage();
		req.setAttribute("groups", dgroups);
		req.setAttribute("managementEnabled", g.isManagementEnabled());
		req.setAttribute("minMembers", g.getMinMembers()==null ? "" : g.getMinMembers().toString());
		req.setAttribute("maxMembers", g.getMaxMembers()==null ? "" : g.getMaxMembers().toString());
		req.setAttribute("wsid", ws.getId());
		req.setAttribute("wsgid", g.getId());
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));
		return new TemplateRetVal("wsgroupingview", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showEditGrouping(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		String sid = StringUtil.trim(variables.get("gid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		WSGrouping g = dao.findWSGroupingByID(id);
		if(g==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		Workspace ws = g.getWorkspace();
		
		boolean canEdit = canAdmin || ws.getOwner().equals(user);
		if(!canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<KeyValue> entries = g.getTitles().entrySet().stream().sorted((t1,t2)->t1.getKey().compareTo(t2.getKey())).map(t->new KeyValue(t.getKey(), t.getValue())).collect(Collectors.toList());
		if(entries.isEmpty()) entries.add(new KeyValue("", ""));
		req.setAttribute("translations1", entries);
		
		String lang = MLCurrentContext.getLocale().getLanguage();
		req.setAttribute("managementEnabled", g.isManagementEnabled());
		req.setAttribute("minMembers", g.getMinMembers()==null ? "" : g.getMinMembers().toString());
		req.setAttribute("maxMembers", g.getMaxMembers()==null ? "" : g.getMaxMembers().toString());
		req.setAttribute("wsid", ws.getId());
		req.setAttribute("wsgid", g.getId());
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));
		return new TemplateRetVal("wsgroupingeditform", Technology.THYMELEAF);
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=false) @Transactional
	public TemplateRetVal validateGroup(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return commonValidateGroup(req, resp, variables, true);
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=false) @Transactional
	public TemplateRetVal invalidateGroup(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return commonValidateGroup(req, resp, variables, false);
	}

	private TemplateRetVal commonValidateGroup(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, boolean validated) throws IOException {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		String sgid = StringUtil.trim(variables.get("gid"));
		Long gid = null;
		try {
			if(sgid!=null) gid = Long.valueOf(sgid);
		} catch(Exception ignorable) {
		}
		if(sgid==null || gid==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		DAO dao = DAOProvider.getDao();
		WSGroup g = dao.findWSGroupByID(gid);
		if(g==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		Workspace ws = g.getGrouping().getWorkspace();

		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		
		boolean canEdit = canAdmin || ws.getOwner().equals(user);
		if(!canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		g.setValidated(validated);
		
		resp.sendRedirect(req.getContextPath()+"/ml/admin/workspaces/wgrs/view/"+g.getGrouping().getId());
		return null;
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public TemplateRetVal processUpdateGrouping(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());

		String wsid = StringUtil.trim(req.getParameter("wsid"));
		Long wid = null;
		try {
			if(wsid!=null) wid = Long.valueOf(wsid);
		} catch(Exception ignorable) {
		}
		if(wsid==null || wid==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		DAO dao = DAOProvider.getDao();
		Workspace ws = dao.findWorkspaceByID(wid);
		if(ws==null ) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		
		boolean canEdit = canAdmin || ws.getOwner().equals(user);
		if(!canEdit) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		String sid = StringUtil.trim(req.getParameter("wsgid"));
		Long id = null;
		try {
			if(sid!=null) id = Long.valueOf(sid);
		} catch(Exception ignorable) {
		}
		if(sid!=null && id==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		WSGrouping g = null;
		if(id != null) {
			g = dao.findWSGroupingByID(id);
			if(g==null || !g.getWorkspace().equals(ws)) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
		
		List<String> errors = new ArrayList<>();

		List<KeyValue> entries1 = readTranslations(req, "1", errors);
		boolean managementEnabled = "1".equals(req.getParameter("managementEnabled"));
		String sMinMembers = StringUtil.trim(req.getParameter("minMembers"));
		String sMaxMembers = StringUtil.trim(req.getParameter("maxMembers"));

		Integer minMembers = null;
		if(sMinMembers != null) {
			try {
				minMembers = Integer.valueOf(sMinMembers);
				if(minMembers < 1) {
					errors.add("Minimalni broj studenata ili ne smije biti zadan, ili mora biti barem 1.");
				}
			} catch(Exception ex) {
				errors.add("Minimalni broj studenata nije ispravan broj.");
			}
		}
		Integer maxMembers = null;
		if(sMaxMembers != null) {
			try {
				maxMembers = Integer.valueOf(sMaxMembers);
				if(maxMembers < 1 || (minMembers!=null && maxMembers<minMembers)) {
					errors.add("Maksimalni broj studenata, ako je zadan, ne smije biti manji od 1 ili manji od minimalnog broja studenata (ako je isti zadan).");
				}
			} catch(Exception ex) {
				errors.add("Maksimalni broj studenata nije ispravan broj.");
			}
		}
		
		req.setAttribute("translations1", entries1);

		String lang = MLCurrentContext.getLocale().getLanguage();
		req.setAttribute("managementEnabled", managementEnabled);
		req.setAttribute("minMembers", minMembers==null ? "" : minMembers.toString());
		req.setAttribute("maxMembers", maxMembers==null ? "" : maxMembers.toString());
		req.setAttribute("wsid", ws.getId());
		req.setAttribute("wsgid", g==null ? null : g.getId());
		req.setAttribute("workspace", DWorkspace.fromDAOModel(ws, lang));

		Set<String> langs1 = entries1.stream().filter(kv->kv.getKey()!=null).map(kv->kv.getKey()).collect(Collectors.toSet());
		if(!errors.isEmpty()) {
			req.setAttribute("errors", errors);
			return new TemplateRetVal("wsgroupingeditform", Technology.THYMELEAF);
		}

		boolean addnew = g==null;
		
		if(addnew) {
			g = new WSGrouping();
			g.setWorkspace(ws);
		}
		
		g.setManagementEnabled(managementEnabled);
		g.setMinMembers(minMembers);
		g.setMaxMembers(maxMembers);

		final WSGrouping fg = g;
		g.getTitles().keySet().removeIf(k->!langs1.contains(k));
		entries1.forEach(e->fg.getTitles().put(e.getKey(), e.getValue()));

		if(addnew) {
			dao.saveWSGrouping(g);
			ws.getGroupings().add(g);
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+ws.getId());
		return null;
	}	
	// -----------------------------------------------
	
	private List<KeyValue> readTranslations(HttpServletRequest req, String key, List<String> errors) {
		List<KeyValue> entries = new ArrayList<>();
		Set<String> langs = new HashSet<>();
		for(int i = 0; true; i++) {
			String l = req.getParameter("lang"+key+"_"+i);
			if(l==null) break; // first non-present will terminate loop
			String lang = StringUtil.trim(l);
			String trans = StringUtil.trim(req.getParameter("trans"+key+"_"+i));
			if(lang==null && trans==null) continue;
			if(lang==null) {
				errors.add("You can not have translation without having the language defined.");
			} else {
				if(!langs.add(lang)) {
					errors.add("Language "+lang+" is defined multiple times.");
				}
			}
			entries.add(new KeyValue(lang, trans==null ? "" : trans));
		}
		return entries;
	}
	
	
}
