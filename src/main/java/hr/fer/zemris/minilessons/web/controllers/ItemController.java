package hr.fer.zemris.minilessons.web.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONObject;

import hr.fer.zemris.minilessons.DescriptorParserUtil;
import hr.fer.zemris.minilessons.LangUtil;
import hr.fer.zemris.minilessons.MapVariableMapping;
import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.ItemStatus;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.MLUserStat;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonStatus;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserGrade;
import hr.fer.zemris.minilessons.dao.model.MinilessonUserSharedData;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.domain.model.DMinilesson;
import hr.fer.zemris.minilessons.domain.model.DOption;
import hr.fer.zemris.minilessons.logging.MLLogger;
import hr.fer.zemris.minilessons.logging.packets.MLCompletedEduLogPacket;
import hr.fer.zemris.minilessons.logging.packets.MLItemCompletedEduLogPacket;
import hr.fer.zemris.minilessons.logging.packets.MLItemRequestedEduLogPacket;
import hr.fer.zemris.minilessons.logging.packets.MLJSCAnswerEduLogPacket;
import hr.fer.zemris.minilessons.logging.packets.MLStartedEduLogPacket;
import hr.fer.zemris.minilessons.logging.packets.MLXMLAnswerEduLogPacket;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.MLDeployer;
import hr.fer.zemris.minilessons.service.MLVerificationException;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.WebConstants;
import hr.fer.zemris.minilessons.web.htmltempl.ContentProcessor;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;
import hr.fer.zemris.minilessons.web.htmltempl.commands.BranchLinkCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.BranchTitleCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.CommonIncludeCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.EchoCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.FileCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.PageIncludeCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.RawTextCmd;
import hr.fer.zemris.minilessons.web.htmltempl.commands.VideoCmd;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;
import hr.fer.zemris.minilessons.web.scripttempl.ScriptProcessor;
import hr.fer.zemris.minilessons.xmlmodel.MLDescriptorException;
import hr.fer.zemris.minilessons.xmlmodel.XMLBranch;
import hr.fer.zemris.minilessons.xmlmodel.XMLFile;
import hr.fer.zemris.minilessons.xmlmodel.XMLItem;
import hr.fer.zemris.minilessons.xmlmodel.XMLMinilesson;
import hr.fer.zemris.minilessons.xmlmodel.XMLOffer;
import hr.fer.zemris.minilessons.xmlmodel.XMLOption;
import hr.fer.zemris.minilessons.xmlmodel.XMLPage;
import hr.fer.zemris.minilessons.xmlmodel.XMLQuestion;
import hr.fer.zemris.minilessons.xmlmodel.XMLString;
import hr.fer.zemris.minilessons.xmlmodel.XMLView;

public class ItemController {

	private static final String MAIN_BRANCH_ID = "#";
	
	private static class MLData {
		private XMLMinilesson xmlMinilesson;
		// Maps branchID to XMLItems in that branch and in correct order
		private Map<String, List<XMLItem>> branchItems = new HashMap<>();
		// Set of declared branch IDs
		private Set<String> declaredBranchIDs = new HashSet<>();
		// Map branchID to branch
		private Map<String,XMLBranch> branches = new HashMap<>();
		// Maps itemID to XMLItem
		private Map<String,XMLItem> itemByID = new HashMap<>();
		// Maps branchID to item which offers it: it's parent
		private Map<String,XMLItem> branchParent = new HashMap<>();
		
		public XMLItem findNextItem(XMLItem item) {
			String bid = branchID(item.getBranchID());

			// Get branch items:
			List<XMLItem> items = branchItems.get(bid);
			
			// Find this item there:
			int pos = items.indexOf(item);
			if(pos==-1) {
				throw new RuntimeException("Item "+item.getId()+" not found in expected branch.");
			}

			// If not last, return the next one:
			if(pos < items.size()-1) {
				return items.get(pos+1);
			}
			
			// Else, are we in main branch or some other?
			if(MAIN_BRANCH_ID.equals(bid)) {
				// If in main branch, then we are truly done:
				return null;
			}
			
			// Else find parent for this branch and redirect control flow there:
			XMLItem continueWith = branchParent.get(bid);
			if(continueWith==null) {
				throw new MLDescriptorException("Finished with branch "+bid+" but no parent was found to continue.");
			}
			return continueWith;
		}
	}

	private static MLData wrap(XMLMinilesson xmlMinilesson) {
		MLData mldata = new MLData();
		mldata.xmlMinilesson = xmlMinilesson;
		
		if(xmlMinilesson.getBranches() != null) {
			for(XMLBranch b : xmlMinilesson.getBranches()) {
				checkBranchID(b.getId());
				if(!mldata.declaredBranchIDs.add(b.getId())) {
					throw new MLDescriptorException("Branch with ID "+b.getId()+" is declared multiple times.");
				}
				mldata.branches.put(b.getId(), b);
			}
		}
		
		for(XMLItem it : xmlMinilesson.getItems()) {
			checkItemID(it.getId());
			String bid = it.getBranchID();
			if(bid==null) {
				bid = MAIN_BRANCH_ID;
			} else {
				if(!mldata.declaredBranchIDs.contains(bid)) {
					throw new MLDescriptorException("Item with ID="+it.getId()+" references non-existent branch with ID="+bid+".");
				}
			}
			List<XMLItem> items = mldata.branchItems.get(bid);
			if(items==null) {
				items = new ArrayList<>();
				mldata.branchItems.put(bid, items);
			}
			items.add(it);
			mldata.itemByID.put(it.getId(), it);
			if(it.getOfferBranches()!=null) {
				for(String obid : it.getOfferBranches()) {
					if(mldata.branchParent.containsKey(obid)) {
						throw new MLDescriptorException("There are multiple parents for branch with ID="+obid+". One is item with ID="+it.getId()+".");
					}
					mldata.branchParent.put(obid, it);
				}
			}
		}
		
		for(String bid : mldata.declaredBranchIDs) {
			if(!mldata.branchItems.containsKey(bid)) {
				throw new MLDescriptorException("Branch with ID="+bid+" is declared but has no items. Empty branches are not allowed.");
			}
		}
		
		if(!mldata.branchItems.containsKey(MAIN_BRANCH_ID)) {
			throw new MLDescriptorException("Main thread has no items. You must define at least one item in main thread.");
		}
		
		return mldata;
	}
	
	/**
	 * Checks if branch ID has valid format. Valid characters are letters, digits, '-', '_' and '.'.
	 * Returns nothing if ID is correctly formatted.
	 * 
	 * @param id id to check
	 * @throws MLDescriptorException if id is invalid
	 */
	private static void checkBranchID(String id) {
		try {
			MLDeployer.checkBranchID("", id);
		} catch(MLVerificationException ex) {
			throw new MLDescriptorException("Branch ID "+id+" contains invalid characters.");
		}
	}
	
	/**
	 * Checks if item ID has valid format. Valid characters are letters, digits, '-', '_' and '.'.
	 * Returns nothing if ID is correctly formatted.
	 * 
	 * @param id id to check
	 * @throws MLDescriptorException if id is invalid
	 */
	private static void checkItemID(String id) {
		try {
			MLDeployer.checkItemID("", id);
		} catch(MLVerificationException ex) {
			throw new MLDescriptorException("Branch ID "+id+" contains invalid characters or is 'done' which is forbidden.");
		}
	}

	/**
	 * Loads XMLMinilesson descriptor from its storage.
	 *  
	 * @param minilessonID minilesson ID for which descriptor is requested
	 * @param dao dao object
	 * @return object representing minilesson descriptor
	 * @throws IOException if file could not be accessed
	 */
	private static XMLMinilesson load(String minilessonID, DAO dao) throws IOException {
		Path path = dao.getRealPathFor(minilessonID, "descriptor.xml");
		if(path!=null) {
			XMLMinilesson ml = DescriptorParserUtil.parseInputStream(Files.newInputStream(path));
			List<XMLOption> optkeys = ml.getOptionKeys();
			Map<String,List<XMLString>> parentOptKeys = optkeys==null ? new HashMap<>() : optkeys.stream().collect(Collectors.toMap(o->o.getKey(), o->o.getTranslations()));
			ml.getItems().forEach(item->{
				if(item instanceof XMLQuestion) {
					XMLQuestion q = (XMLQuestion)item;
					if(q.getOptionKeys()!=null) {
						q.getOptionKeys().forEach(o->{
							if(o.getInherit()) {
								List<XMLString> list = parentOptKeys.get(o.getKey());
								if(list==null) {
									throw new RuntimeException("Option '"+o.getKey()+"' can not inherit: no parent option found.");
								}
								o.setTranslations(list);
							}
						});
					}
				}
			});
			return ml;
		}
		throw new IOException("Unable to obtain real path for minilesson "+minilessonID+".");
	}

	/**
	 * Loads XMLMinilesson descriptor from its storage and wraps it into {@link MLData}.
	 *  
	 * @param minilessonID minilesson ID for which descriptor is requested
	 * @param dao dao object
	 * @return object representing minilesson descriptor
	 * @throws IOException if file could not be accessed
	 * @throws MLDescriptorException if there are problems with data in descriptor
	 */
	private static MLData loadMLData(String minilessonID, DAO dao) throws IOException {
		return wrap(load(minilessonID, dao));
	}
	
	// Pronalazi prvu stranicu za zadani minilesson $uid i redirekta na tu stranicu...
	@DAOProvided @Transactional @Authenticated(redirectAllowed=true)
	public StreamRetVal openItem(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		
		DAO dao = DAOProvider.getDao();

		String uid = variables.get("uid");
		Minilesson ml = dao.findMinilessonByID(uid);
		if(ml==null) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}
		XMLMinilesson xmlml = load(ml.getId(), dao);

		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		if(xmlml.getItems()!=null && !xmlml.getItems().isEmpty()) {
			resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/"+xmlml.getItems().get(0).getId()+"/start");
		}
		
		return null;
	}
	
	// Za zadani minilesson $uid, item $iid i view $vid prikazuje stranicu 
	@DAOProvided @Transactional @Authenticated(redirectAllowed=true)
	public Object openItemView(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String lang = MLCurrentContext.getLocale().getLanguage();
		DAO dao = DAOProvider.getDao();

		String uid = StringUtil.trim(variables.get("uid"));
		String iid = StringUtil.trim(variables.get("iid"));
		String vid = StringUtil.trim(variables.get("vid"));
		
		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		
		if(uid==null || iid==null || vid==null) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}
		
		Minilesson ml = dao.findMinilessonByID(uid);
		if(ml==null) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}
		
		MLData mldata = loadMLData(ml.getId(), dao);
		XMLMinilesson xmlml = mldata.xmlMinilesson;

		XMLItem item = null;
		
		if(!"done".equals(iid)) {
			item = mldata.itemByID.get(iid);
			if(item==null) return null;
		}

		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Set<String> completed = new HashSet<>(dao.listCompletedItems(ml, user));

		// Ako je ovdje item==null, tražen je kraj minilekcije.
		
		// Neovisno o tome, provjeri najprije ima li korisnik pravo gledati ovu stranicu:
		if(!checkPrerequisites(mldata.branchItems.get(item==null ? MAIN_BRANCH_ID : branchID(item.getBranchID())), completed, item==null ? null : item.getId())) {
			System.out.println("Nemate pravo pristupiti ovoj stranici - preduvjeti nisu zadovoljeni.");
			return null;
		}
		
		buildItemNavigation(req, item, mldata, lang, completed);
		
		// Ako je zatražen kraj minilekcije...
		if(item==null) {
			MLLogger.getEdulogger().log(new MLItemRequestedEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), uid, iid, vid));
			req.setAttribute("apctx", apctx);
			req.setAttribute("ml", ml);
			return new TemplateRetVal("mllessondone", Technology.THYMELEAF);
		}
		
		Set<String> visibleBranchIDs = new HashSet<>();
		if(item.getOfferBranches() != null) {
			for(String bid : item.getOfferBranches()) {
				visibleBranchIDs.add(bid);
			}
		}

		MLLogger.getEdulogger().log(new MLItemRequestedEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), uid, iid, vid));
		
		if("start".equals(vid)) {
			ItemStatus itStat = dao.findItemStatus(ml, user, iid);
			if(itStat==null) {
				itStat = new ItemStatus(ml, user, iid);
				itStat.setOpenedOn(new Date());
				dao.saveItemStatus(itStat);
			}
		}
		
		req.setAttribute("cmntMinilessonID", ml.getId());
		req.setAttribute("cmntItemID", iid);
		
		if(item instanceof XMLPage) {
			// XMLPage supports only two vid: "start" and "done".
			XMLPage page = (XMLPage)item;
			
			if("done".equals(vid)) {
				ItemStatus status = dao.findItemStatus(ml, user, iid);
				if(status==null) {
					status = new ItemStatus(ml, user, iid);
					status.setCompletedOn(new Date());
					dao.saveItemStatus(status);
					System.out.println("Bilježim da je item " + iid + " odrađen.");
					MLLogger.getEdulogger().log(new MLItemCompletedEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), uid, iid));
				} else if(status.getCompletedOn()==null) {
					status.setCompletedOn(new Date());
					System.out.println("Bilježim da je item " + iid + " odrađen.");
					MLLogger.getEdulogger().log(new MLItemCompletedEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), uid, iid));
				}

				item = mldata.findNextItem(item);
				if(item==null) {
					// Lekcija je gotova. Prikaži neku završnu stranicu.
					markMinilessonCompleted(dao, ml, user);
					resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/done/start");
					return null;
				}
				resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/"+item.getId()+"/start");
				return null;
			}
			
			if(!"start".equals(vid)) {
				return null;
			}

			markMinilessonAccessed(dao, ml, user);
			
			Optional<XMLFile> maybeFile = page.getFiles().stream().filter(f -> f.getLang().equals(lang)).findAny();
			if(!maybeFile.isPresent()) return null;

			Optional<XMLString> maybeTitle = page.getTitle()==null ? Optional.empty() : page.getTitle().stream().filter(f -> f.getLang().equals(lang)).findAny();

			XMLFile file = maybeFile.get();
			String content = null;
			try {
				content = prepareBasicContentProcessor(lang, visibleBranchIDs, mldata, dao, uid, req.getContextPath(), null, null, apctx).load(file.getName());
			} catch(Exception ex) {
				ex.printStackTrace();
				return null;
			}
			//String content = new BasicContentProcessor(lang, visibleBranchIDs, mldata, dao, uid, req.getContextPath()).loadPage(file.getName());
			req.setAttribute("mlIID", iid);
			req.setAttribute("mlPageContent", content);
			req.setAttribute("mlPageTitle", maybeTitle.isPresent() ? maybeTitle.get().getText() : "???");
			req.setAttribute("ml", DMinilesson.fromDAOModel(ml, lang));
			req.setAttribute("apctx", apctx);
			
			return new TemplateRetVal("mlpage", Technology.THYMELEAF);
		}

		if(item instanceof XMLQuestion) {
			XMLQuestion question = (XMLQuestion)item;
			if("static".equals(question.getType())) {
				return handleStaticQuestion(req, resp, lang, dao, uid, iid, vid, ml, xmlml, question, mldata, visibleBranchIDs, apctx);
			}
			if("js-custom".equals(question.getType())) {
				return handleJSCustomQuestion(req, resp, lang, dao, uid, iid, vid, ml, xmlml, question, mldata, visibleBranchIDs, apctx);
			}
		}
		
		return null;
	}

	/**
	 * Helper method which translated <code>null</code>-branchIDs into {@link #MAIN_BRANCH_ID}.
	 * If non-<code>null</code> ID is passed, it will be returned.
	 * 
	 * @param branchID branch id
	 * @return translated branch id
	 */
	private static String branchID(String branchID) {
		return branchID==null ? MAIN_BRANCH_ID : branchID;
	}

	/**
	 * Method checks if prerequisites for given item (argument <code>itemID</code>) are completed.
	 * 
	 * @param threadItems list of items for thread in which item belongs
	 * @param completedUtemIDs IDs of items which user has completed
	 * @param itemID ID of item for which check is performed; can be <code>null</code> in which case it is expected that all items in thread must be completed
	 * @return <code>true</code> if prerequisites are met, <code>false</code> otherwise
	 */
	private boolean checkPrerequisites(List<XMLItem> threadItems, Set<String> completedUtemIDs, String itemID) {
		for(XMLItem it : threadItems) {
			// If we reached requested item in this thread, access is granted:
			if(itemID != null && it.getId().equals(itemID)) return true;
			// If we are at some earlier item, then check if we completed it:
			if(!completedUtemIDs.contains(it.getId())) return false;
		}
		// If all prerequisited were completed, access is granted...
		return true;
	}

	private MinilessonStatus markMinilessonAccessed(DAO dao, Minilesson ml, User user) {
		MinilessonStatus status = dao.findMinilessonStatus(ml, user);
		if(status==null) {
			status = new MinilessonStatus(ml, user);
			status.setOpenedAt(new Date());
			dao.saveMinilessonStatus(status);
			MLLogger.getEdulogger().log(new MLStartedEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), ml.getId()));
			System.out.println("Bilježim da je započeta lekcija: " + ml.getId());
		}
		return status;
	}

	private void markMinilessonCompleted(DAO dao, Minilesson ml, User user) {
		MinilessonStatus status = dao.findMinilessonStatus(ml, user);
		if(status!=null) {
			if(status.getCompletedOn() == null) {
				status.setCompletedOn(new Date());
				System.out.println("Bilježim da je završena lekcija: " + ml.getId());
				MLLogger.getEdulogger().log(new MLCompletedEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), ml.getId()));
			}
		} else {
			System.out.println("Greška. Ne mogu bilježiti da je završena lekcija: " + ml.getId() + " kad nije niti započela.");
		}
	}
	
	public static ContentProcessor prepareBasicContentProcessor(String lang, Set<String> visibleBranchIDs, MLData mldata, DAO dao, String uid, String contextPath, Map<String, String> varMap, Map<String, Object> infoMap, AppRequestContext apctx) {
		if(varMap==null) varMap = new HashMap<>();
		if(infoMap==null) infoMap = new HashMap<>();
		Map<String, HtmlTemplateCommand> commands = new HashMap<>();
		commands.put("file", new FileCmd(() -> contextPath+"/ml/minilesson/"+apctx.getContextData()+"/"+uid+"/file/"));
		commands.put("file-url", new FileCmd(() -> contextPath+"/ml/minilesson/"+apctx.getContextData()+"/"+uid+"/file/"));
		commands.put("branch-title", new BranchTitleCmd(mldata.branches, lang));
		commands.put("branch-link", new BranchLinkCmd(() -> contextPath+"/ml/minilesson/"+apctx.getContextData()+"/"+uid+"/item/", mldata.branches, mldata.branchParent, visibleBranchIDs));
		commands.put("include", new PageIncludeCmd());
		commands.put("page-include", new PageIncludeCmd());
		commands.put("common-include", new CommonIncludeCmd());
		commands.put("raw-text", new RawTextCmd(new MapVariableMapping(varMap)));
		commands.put("echoIfTrue", new EchoCmd(Boolean.TRUE, new MapVariableMapping(infoMap)));
		commands.put("echoIfFalse", new EchoCmd(Boolean.FALSE, new MapVariableMapping(infoMap)));
		commands.put("echo", new EchoCmd(null, new MapVariableMapping(infoMap)));
		commands.put("video", new VideoCmd(contextPath, uid));
		return new ContentProcessor(path->dao.getRealPathFor(uid, path), c->commands.containsKey(c), commands, "pages");
	}

	private Object handleStaticQuestion(HttpServletRequest req, HttpServletResponse resp,
			String lang, DAO dao, String uid, String iid, String vid, Minilesson ml, XMLMinilesson xmlml,
			XMLQuestion question, MLData mldata, Set<String> visibleBranchIDs, AppRequestContext apctx) throws IOException {
		
		if("done".equals(vid)) { // Ako nije completed, onda ovo ne možeš vidjeti...
			User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
			ItemStatus status = dao.findItemStatus(ml, user, iid);
			if(status == null || status.getCompletedOn()==null) {
				System.out.println("Pozvan /done nad pitanjem koje nije odgovoreno točno.");
				return null;
			}
		}
		
		Optional<XMLView> maybeView = question.getViews().stream().filter(v -> v.getName().equals(vid)).findAny();
		if(!maybeView.isPresent()) return null;
		XMLView view = maybeView.get();
		
		Optional<XMLPage> maybePage = question.getTemplates().stream().filter(p -> p.getId().equals(view.getRenderPageId())).findAny();
		if(!maybePage.isPresent()) return null;
		XMLPage page = maybePage.get();
		
		Optional<XMLFile> maybeFile = page.getFiles().stream().filter(f -> lang.equals(f.getLang())).findAny();
		if(!maybeFile.isPresent()) return null;
		XMLFile file = maybeFile.get();

		Optional<XMLString> maybeTitle =  question.getTitle()==null ? Optional.empty() : question.getTitle().stream().filter(f -> f.getLang().equals(lang)).findAny();

		markMinilessonAccessed(dao, ml, dao.findUserByID(MLCurrentContext.getAuthUser().getId()));
		
		List<DOption> options = new ArrayList<>();
		if(view.getOffers()!=null) {
			view.getOffers().forEach(o -> {
				options.add(new DOption(o.getKey(), LangUtil.findLocalization(findOption(o.getKey(), question.getOptionKeys()).getTranslations(), lang)));
			});
		}

		String content = null;
		try {
			content = prepareBasicContentProcessor(lang, visibleBranchIDs, mldata, dao, uid, req.getContextPath(), null, null, apctx).load(file.getName());
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		//String content = new BasicContentProcessor(lang, visibleBranchIDs, mldata, dao, uid, req.getContextPath()).loadPage(file.getName());
		req.setAttribute("mlIID", iid);
		req.setAttribute("mlVID", vid);
		req.setAttribute("mlPageContent", content);
		req.setAttribute("mlPageTitle", maybeTitle.isPresent() ? maybeTitle.get().getText() : "???");
		req.setAttribute("ml", DMinilesson.fromDAOModel(ml, lang));
		req.setAttribute("mlDOptions", options);
		req.setAttribute("apctx", apctx);
		
		return new TemplateRetVal("mlquestion", Technology.THYMELEAF);
	}

	private Object handleJSCustomQuestion(HttpServletRequest req, HttpServletResponse resp,
			String lang, DAO dao, String uid, String iid, String vid, Minilesson ml, XMLMinilesson xmlml,
			XMLQuestion question, MLData mldata, Set<String> visibleBranchIDs, AppRequestContext apctx) throws IOException {

		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		ItemStatus status = dao.findItemStatus(ml, user, iid);

		String infoParam = req.getParameter("info");
		if(infoParam!=null) infoParam = infoParam.trim();
		if(infoParam!=null && infoParam.isEmpty()) infoParam=null;
		if(infoParam==null) infoParam = "{}";
		
		Path jsfilePath = dao.getRealPathFor(uid, "jscustom/"+question.getHandler());
		if(jsfilePath == null) return null;
		Map<String, String> scriptLoaderMap = new HashMap<>();
		scriptLoaderMap.put("uid", uid);
		scriptLoaderMap.put("iid", iid);
		String script = ScriptProcessor.loadScript(jsfilePath, scriptLoaderMap);
		String headerScript = "function questionRenderViewJSON(vid,info) { return JSON.stringify(questionRenderView(vid, JSON.parse(info)));} function setUserDataFromJSON(u) {userData = JSON.parse(u);} function getUserDataAsJSON() {return JSON.stringify(userData);} function setSharedUserDataFromJSON(u) {sharedUserData = JSON.parse(u);} function getSharedUserDataAsJSON() {return JSON.stringify(sharedUserData);}\r\n";

		script = headerScript + script;
		
		ScriptEngineManager scEngMan = new ScriptEngineManager(null);
		ScriptEngine jsengine = scEngMan.getEngineByName("nashorn");
		Invocable invocable = (Invocable)jsengine;
		
		Bindings bindings = jsengine.createBindings();

		if(status==null) {
			status = new ItemStatus(ml, user, iid);
			dao.saveItemStatus(status);
		}

		MinilessonStatus mlStatus = markMinilessonAccessed(dao, ml, dao.findUserByID(MLCurrentContext.getAuthUser().getId()));
		
		if(!status.isInitialized()) {
			bindings.put("questionCompleted", false);
			jsengine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
			try {
				jsengine.eval(script);
			} catch (ScriptException e) {
				e.printStackTrace();
				return null;
			}
			try {
				invocable.invokeFunction("setUserDataFromJSON", "{}");
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			try {
				String ival = "{}";
				if(mlStatus.getSharedData()!=null && mlStatus.getSharedData().getSharedData()!=null && !mlStatus.getSharedData().getSharedData().trim().isEmpty()) {
				 ival = mlStatus.getSharedData().getSharedData().trim();
				}
				invocable.invokeFunction("setSharedUserDataFromJSON", ival);
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			try {
				invocable.invokeFunction("questionInitialize");
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			Object rawData = null;
			try {
				rawData = invocable.invokeFunction("getUserDataAsJSON");
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			String data = rawData==null ? "{}" : rawData.toString();
			status.setUserData(data);
			status.setInitialized(true);
			
			Object rawSharedData = null;
			try {
				rawSharedData = invocable.invokeFunction("getSharedUserDataAsJSON");
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			String sharedData = null;
			if(rawSharedData!=null) {
				String val = rawSharedData.toString();
				if(!("{}".equals(val))) {
					sharedData = val;
				}
			}
			if(mlStatus.getSharedData()==null) {
				if(sharedData != null) {
					mlStatus.setSharedData(new MinilessonUserSharedData());
					mlStatus.getSharedData().setSharedData(sharedData);
					dao.saveMinilessonUserSharedData(mlStatus.getSharedData());
				}
			} else {
				mlStatus.getSharedData().setSharedData(sharedData);
			}
		} else {
			bindings.put("questionCompleted", status.getCompletedOn()!=null);
			jsengine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
			try {
				jsengine.eval(script);
			} catch (ScriptException e) {
				e.printStackTrace();
				return null;
			}
			try {
				invocable.invokeFunction("setUserDataFromJSON", status.getUserData()==null ? "{}" : status.getUserData());
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
			try {
				String ival = "{}";
				if(mlStatus.getSharedData()!=null && mlStatus.getSharedData().getSharedData()!=null && !mlStatus.getSharedData().getSharedData().trim().isEmpty()) {
				 ival = mlStatus.getSharedData().getSharedData().trim();
				}
				invocable.invokeFunction("setSharedUserDataFromJSON", ival);
			} catch (NoSuchMethodException | ScriptException e) {
				e.printStackTrace();
				return null;
			}
		}

		Object res = null;
		try {
			res = invocable.invokeFunction("questionRenderViewJSON", vid, infoParam);
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}

		JSONArray objOptionsArray = null;
		String objQuestionState = null;
		JSONObject objView = null;
		String action = null;
		JSONObject objVariables = null;
		try {
			JSONObject obj = new JSONObject(res.toString());
			objOptionsArray = obj.getJSONArray("options");
			objQuestionState = obj.getString("questionState");
			objView = obj.getJSONObject("view");
			action = objView.getString("action");
			if(obj.has("variables")) objVariables = obj.getJSONObject("variables");
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
		List<String> optionKeys = new ArrayList<>();
		objOptionsArray.forEach(e -> optionKeys.add(e==null ? "" : e.toString()));
		
		if(optionKeys.isEmpty()) return null;
		String jsonStateData = objQuestionState;
		if(action==null) return null;

		List<DOption> options = new ArrayList<>();
		optionKeys.forEach(ok -> {
			options.add(new DOption(ok, LangUtil.findLocalization(findOption(ok, question.getOptionKeys()).getTranslations(), lang)));
		});
		
		if(action.startsWith("page:")) {
			String pageID = action.substring(5);
			
			Optional<XMLPage> maybePage = question.getTemplates().stream().filter(p -> p.getId().equals(pageID)).findAny();
			if(!maybePage.isPresent()) return null;
			XMLPage page = maybePage.get();
			
			Optional<XMLString> maybeTitle =  question.getTitle()==null ? Optional.empty() : question.getTitle().stream().filter(f -> f.getLang().equals(lang)).findAny();
			
			Optional<XMLFile> maybeFile = page.getFiles().stream().filter(f -> lang.equals(f.getLang())).findAny();
			if(!maybeFile.isPresent()) return null;
			XMLFile file = maybeFile.get();

			Map<String, String> varMap = new HashMap<>();
			varMap.put("jsinitdata", jsonStateData);
			String content = null;
			try {
				content = prepareBasicContentProcessor(lang, visibleBranchIDs, mldata, dao, uid, req.getContextPath(), varMap, createInfoParamMap(objVariables), apctx).load(file.getName());
			} catch(Exception ex) {
				ex.printStackTrace();
				return null;
			}
			// String content = new BasicContentProcessor(lang, visibleBranchIDs, mldata, dao, uid, req.getContextPath(), varMap, createInfoParamMap(objVariables)).loadPage(file.getName());
			
			req.setAttribute("mlIID", iid);
			req.setAttribute("mlVID", vid);
			req.setAttribute("mlPageContent", content);
			req.setAttribute("mlPageTitle", maybeTitle.isPresent() ? maybeTitle.get().getText() : "???");
			req.setAttribute("ml", DMinilesson.fromDAOModel(ml, lang));
			req.setAttribute("mlDOptions", options);
			req.setAttribute("apctx", apctx);
			
			return new TemplateRetVal("mljscquestion", Technology.THYMELEAF);
		}

		return null;
	}

	private Map<String, Object> createInfoParamMap(JSONObject objVariables) {
		Map<String, Object> map = new HashMap<>();
		if(objVariables==null) return map;
		for(String key : objVariables.keySet()) {
			if(objVariables.isNull(key)) {
				map.put(key, null);
				continue;
			}
			Object obj = objVariables.get(key);
			if(obj instanceof Number || obj instanceof Character || obj instanceof String || obj instanceof Boolean) {
				map.put(key, obj);
			} else {
				map.put(key, obj.toString());
			}
		}
		return map;
	}

	private XMLOption findOption(String key, List<XMLOption> optionKeys) {
		Optional<XMLOption> opt = optionKeys.stream().filter(o -> o.getKey().equals(key)).findAny();
		return opt.isPresent() ? opt.get() : null;
	}

	// Za zadani minilesson $uid, item $iid, view $vid i ključ $key prikazuje stranicu 
	@DAOProvided @Transactional @Authenticated
	public StreamRetVal openItemViewKey(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String lang = MLCurrentContext.getLocale().getLanguage();
		DAO dao = DAOProvider.getDao();

		String uid = variables.get("uid");
		String iid = variables.get("iid");
		String vid = variables.get("vid");
		String kid = variables.get("key");
		
		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		
		Minilesson ml = dao.findMinilessonByID(uid);
		if(ml==null) {
			System.out.println("Nisam ga pronašao!");
			return null;
		}
		MLData mldata = loadMLData(ml.getId(), dao);
		XMLMinilesson xmlml = mldata.xmlMinilesson;

		XMLItem item = mldata.itemByID.get(iid);
		if(item==null) return null;

		// Inače vidi ima li zadovoljene preduvjete:
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		Set<String> completed = new HashSet<>(dao.listCompletedItems(ml, user));
		if(!checkPrerequisites(mldata.branchItems.get(branchID(item.getBranchID())), completed, item.getId())) {
			System.out.println("Nemate pravo pristupiti ovoj stranici - preduvjeti nisu zadovoljeni.");
			return null;
		}
		
		if(item instanceof XMLQuestion) {
			XMLQuestion question = (XMLQuestion)item;
			if("static".equals(question.getType())) {
				String key = "_answer".equals(kid) ? req.getParameter("key") : kid;
				return handleStaticQuestionAnswer(req, resp, lang, dao, uid, iid, vid, key, ml, xmlml, question, mldata, apctx);
			}
			if("js-custom".equals(question.getType())) {
				String key = "_answer".equals(kid) ? req.getParameter("key") : kid;
				String sentUserData = req.getParameter("userData");
				return handleJSCustomQuestionAnswer(req, resp, lang, dao, uid, iid, vid, key, ml, xmlml, question, sentUserData, mldata, apctx);
			}
		}
		
		return null;
	}

	private StreamRetVal handleStaticQuestionAnswer(HttpServletRequest req, HttpServletResponse resp,
			String lang, DAO dao, String uid, String iid, String vid, String key, Minilesson ml,
			XMLMinilesson xmlml, XMLQuestion question, MLData mldata, AppRequestContext apctx) throws IOException {

		Long currentUserID = (Long)req.getSession().getAttribute("currentUserID");

		Optional<XMLView> maybeView = question.getViews().stream().filter(v -> v.getName().equals(vid)).findAny();
		if(!maybeView.isPresent()) return null;
		XMLView view = maybeView.get();

		Optional<XMLOffer> maybeOffer = view.getOffers().stream().filter(o -> o.getKey().equals(key)).findAny();
		if(!maybeOffer.isPresent()) return null;
		XMLOffer offer = maybeOffer.get();

		String action = offer.getAction();

		User user = dao.findUserByID(currentUserID);
		ItemStatus status = dao.findItemStatus(ml, user, iid);

		MLLogger.getEdulogger().log(new MLXMLAnswerEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), uid, iid, vid, key));
		if(offer.isCompleted()) {
			System.out.println("Evidentiram pitanje točno riješeno.");
			boolean marked = false;
			if(status == null) {
				status = new ItemStatus(ml, user, iid);
				status.setCompletedOn(new Date());
				dao.saveItemStatus(status);
				marked = true;
			} else {
				if(status.getCompletedOn()==null) {
					status.setCompletedOn(new Date());
					marked = true;
				}
			}
			if(marked) {
				MLLogger.getEdulogger().log(new MLItemCompletedEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), uid, iid));
			}
		}
		
		if(action.startsWith("view:")) {
			resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/"+iid+"/"+action.substring(5));
			return null;
		}

		if(action.equals("next")) {
			XMLItem item = mldata.findNextItem(question);
			if(item==null) {
				// Lekcija je gotova. Prikaži neku završnu stranicu.
				markMinilessonCompleted(dao, ml, user);
				resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/done/start");
				return null;
			}
			resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/"+item.getId()+"/start");
			return null;
		}

		return null;
	}
	
	private StreamRetVal handleJSCustomQuestionAnswer(HttpServletRequest req, HttpServletResponse resp,
			String lang, DAO dao, String uid, String iid, String vid, String key, Minilesson ml,
			XMLMinilesson xmlml, XMLQuestion question, String sentUserData, MLData mldata, AppRequestContext apctx) throws IOException {

		Long currentUserID = (Long)req.getSession().getAttribute("currentUserID");
		User user = dao.findUserByID(currentUserID);
		ItemStatus status = dao.findItemStatus(ml, user, iid);

		Path jsfilePath = dao.getRealPathFor(uid, "jscustom/"+question.getHandler());
		if(jsfilePath == null) return null;
		
		Map<String, String> scriptLoaderMap = new HashMap<>();
		scriptLoaderMap.put("uid", uid);
		scriptLoaderMap.put("iid", iid);
		String script = ScriptProcessor.loadScript(jsfilePath, scriptLoaderMap);
		
		String headerScript = 
				"function questionProcessKeyJSON(vid,key,sentData) { return JSON.stringify(questionProcessKey(vid,key,JSON.parse(sentData)));}\r\n" +
				"function setUserDataFromJSON(u) {userData = JSON.parse(u);} function getUserDataAsJSON() {return JSON.stringify(userData);}\r\n" +
				"function setSharedUserDataFromJSON(u) {sharedUserData = JSON.parse(u);} function getSharedUserDataAsJSON() {return JSON.stringify(sharedUserData);}\r\n";

		script = headerScript + script;

		MinilessonStatus mlStatus = markMinilessonAccessed(dao, ml, user);
		
		ScriptEngineManager scEngMan = new ScriptEngineManager(null);
		ScriptEngine jsengine = scEngMan.getEngineByName("nashorn");
		Invocable invocable = (Invocable)jsengine;
		
		Bindings bindings = jsengine.createBindings();

		if(status==null) { // Ne smije se dogoditi!
			return null;
		}

		bindings.put("questionCompleted", status.getCompletedOn()!=null);
		jsengine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
		try {
			jsengine.eval(script);
		} catch (ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			invocable.invokeFunction("setUserDataFromJSON", status.getUserData());
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		try {
			String ival = "{}";
			if(mlStatus.getSharedData()!=null && mlStatus.getSharedData().getSharedData()!=null && !mlStatus.getSharedData().getSharedData().trim().isEmpty()) {
			 ival = mlStatus.getSharedData().getSharedData().trim();
			}
			invocable.invokeFunction("setSharedUserDataFromJSON", ival);
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		Object res = null;
		try {
			res = invocable.invokeFunction("questionProcessKeyJSON", vid, key, sentUserData);
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		Object rawUserData = null;
		try {
			rawUserData = invocable.invokeFunction("getUserDataAsJSON");
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}
		Object rawSharedUserData = null;
		try {
			rawSharedUserData = invocable.invokeFunction("getSharedUserDataAsJSON");
		} catch (NoSuchMethodException | ScriptException e) {
			e.printStackTrace();
			return null;
		}

		JSONObject obj = new JSONObject(res==null ? "{}" : res.toString());
		JSONObject infoObject = null;
		boolean completed = false;
		String action = null;
		try {
			completed = obj.has("completed") && obj.getBoolean("completed");
			action = obj.getString("action");
			if(obj.has("info")) infoObject = obj.getJSONObject("info");
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}

		String sharedData = null;
		if(rawSharedUserData!=null) {
			String val = rawSharedUserData.toString();
			if(!("{}".equals(val))) {
				sharedData = val;
			}
		}
		if(mlStatus.getSharedData()==null) {
			if(sharedData != null) {
				mlStatus.setSharedData(new MinilessonUserSharedData());
				mlStatus.getSharedData().setSharedData(sharedData);
				dao.saveMinilessonUserSharedData(mlStatus.getSharedData());
			}
		} else {
			mlStatus.getSharedData().setSharedData(sharedData);
		}
		
		status.setUserData(rawUserData==null ? "{}" : rawUserData.toString());
		
		MLLogger.getEdulogger().log(new MLJSCAnswerEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), uid, iid, vid, key, sentUserData));
		if(completed) {
			if(status.getCompletedOn()==null) {
				status.setCompletedOn(new Date());
				System.out.println("Bilježim da je zadatak riješen.");
				MLLogger.getEdulogger().log(new MLItemCompletedEduLogPacket(MLCurrentContext.getAuthUser().getAuthDomain()+"\\"+MLCurrentContext.getAuthUser().getUsername(), uid, iid));
			}
		}

		if(action.startsWith("view:")) {
			String infoParam = infoObject==null ? "" : "?info="+URLEncoder.encode(infoObject.toString(), "UTF-8");
			resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/"+iid+"/"+action.substring(5)+infoParam);
			return null;
		}

		if(action.equals("next")) {
			XMLItem item = mldata.findNextItem(question);
			if(item==null) {
				// Lekcija je gotova. Prikaži neku završnu stranicu.
				markMinilessonCompleted(dao, ml, user);
				resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/done/start");
				return null;
			}
			resp.sendRedirect(req.getContextPath()+"/ml/minilesson/"+apctx.getContextData()+"/"+ml.getId()+"/item/"+item.getId()+"/start");
			return null;
		}

		return null;
	}

	/**
	 * Helper method which builds item-branches navigation...
	 * 
	 * @param req http request
	 * @param item current item; can be <code>null</code> in which case a main thread will be added into navigation structure
	 * @param mldata data object
	 */
	private void buildItemNavigation(HttpServletRequest req, XMLItem item, MLData mldata, String lang, Set<String> completed) {
		NavBranches nbrs = new NavBranches();
		XMLItem currentItem = item!=null ? item : mldata.branchItems.get(branchID(null)).get(0);
		do {
			int itemCounter=0;
			String bid = branchID(currentItem.getBranchID());
			XMLBranch branch = mldata.branches.get(bid); // This will be null for main-thread item!
			List<XMLItem> items = mldata.branchItems.get(bid);
			NavBranch nb = new NavBranch(bid, branch==null ? "Glavni tekst" : LangUtil.findLocalization(branch.getTitle(), lang));
			nbrs.branches.add(0, nb);
			if(currentItem.equals(item)) {
				nb.current = true;
				nbrs.activeNavBranch = nb;
			}
			for(XMLItem it : items) {
				itemCounter++;
				NavItem nit = new NavItem(it.getId(), LangUtil.findLocalization(it.getTitle(), lang), completed.contains(it.getId()));
				if(itemCounter==1) {
					nb.firstItemID = it.getId();
					nit.firstInBranch = true;
				}
				nb.allNavItems.add(nit);
				if(it.equals(currentItem)) {
					nb.currentItem = nit;
					nb.currentIndex = itemCounter;
				}
				if(it.equals(item)) {
					nit.active = true;
				}
			}
			currentItem = mldata.branchParent.get(bid);
		} while(currentItem != null);
		
		if(item!=null && item.getOfferBranches()!=null && !item.getOfferBranches().isEmpty()) {
			List<NavBranch> offered = new ArrayList<>();
			for(String bid : item.getOfferBranches()) {
				XMLBranch b = mldata.branches.get(bid);
				String title = LangUtil.findLocalization(b.getTitle(), lang);
				offered.add(new NavBranch(bid, title, mldata.branchItems.get(bid).get(0).getId()));
			}
			nbrs.offersBranches = offered;
		}
		
		req.setAttribute(WebConstants.ML_ITEMS_NAVIG, nbrs);
	}

	public static class NavBranches {
		private List<NavBranch> branches = new ArrayList<>();
		private NavBranch activeNavBranch;
		private List<NavBranch> offersBranches;
		
		public List<NavBranch> getBranches() {
			return branches;
		}
		public NavBranch getActiveNavBranch() {
			return activeNavBranch;
		}
		public List<NavBranch> getOffersBranches() {
			return offersBranches;
		}
	}
	
	public static class NavBranch {
		private String id;
		private String firstItemID;
		private String title;
		private NavItem currentItem;
		private List<NavItem> allNavItems = new ArrayList<>();
		private boolean current;  // je li ovo grana od itema koji je prikazan korisniku...
		private int currentIndex; // 1-based index representing the position of current item in item list

		public NavBranch(String id, String title) {
			this(id, title, null);
		}
		
		public NavBranch(String id, String title, String firstItemID) {
			super();
			this.id = id;
			this.title = title;
			this.firstItemID = firstItemID;
		}
		public String getId() {
			return id;
		}
		public String getFirstItemID() {
			return firstItemID;
		}
		public String getTitle() {
			return title;
		}
		public NavItem getCurrentItem() {
			return currentItem;
		}
		public List<NavItem> getAllNavItems() {
			return allNavItems;
		}
		public boolean isCurrent() {
			return current;
		}
		public int getCurrentIndex() {
			return currentIndex;
		}
	}
	
	public static class NavItem {
		private String id;
		private String title;
		private boolean completed;
		private boolean active;
		private boolean firstInBranch;
		
		public NavItem(String id, String title, boolean completed) {
			super();
			this.id = id;
			this.title = title;
			this.completed = completed;
		}
		public String getId() {
			return id;
		}
		public String getTitle() {
			return title;
		}
		public boolean isCompleted() {
			return completed;
		}
		public boolean isActive() {
			return active;
		}
		public boolean isFirstInBranch() {
			return firstInBranch;
		}
	}

	// Pronalazi podatke o glasanju trenutnog korisnika za minilesson $uid i vraća JSON zapis natrag:
	@DAOProvided @Transactional @Authenticated(attemptLogin=false)
	public StreamRetVal getUserMLVotes(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		
		String minilessonID = variables.get("uid");
		
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		
		MinilessonUserGrade mug = DAOProvider.getDao().findMinilessonUserGrades(minilesson, user);
		String ret = null;
		if(mug==null) {
			ret = "{\"quality\": 0, \"difficulty\": 0, \"duration\": 0}";
		} else {
			ret = "{\"quality\": "+mug.getQualityGrade()+", \"difficulty\": "+mug.getDifficultyGrade()+", \"duration\": "+mug.getDuration()+"}";
		}
		
		return new StreamRetVal(ret, StandardCharsets.UTF_8, "application/json", null);
	}
	
	// Snima podatke o glasanju trenutnog korisnika za minilesson $uid i vraća JSON zapis natrag:
	@DAOProvided @Transactional @Authenticated(attemptLogin=false)
	public StreamRetVal saveUserMLVotes(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		
		String minilessonID = variables.get("uid");
		String qualStr = variables.get("qual");
		String difStr = variables.get("dif");
		String durStr = variables.get("dur");
		
		int qual = -1;
		try {
			qual = Integer.parseInt(qualStr);
			if(qual<0 || qual>5) qual = -1;
		} catch(Exception ex) {
			// Ignore intended!
		}
		
		int dif = -1;
		try {
			dif = Integer.parseInt(difStr);
			if(dif<0 || dif>5) dif = -1;
		} catch(Exception ex) {
			// Ignore intended!
		}
		
		int dur = -1;
		try {
			dur = Integer.parseInt(durStr);
			if(dur<0 || dur>1440) dur = -1;
		} catch(Exception ex) {
			// Ignore intended!
		}
		
		if(qual==-1 || dif==-1 || dur==-1) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		
		MinilessonUserGrade mug = DAOProvider.getDao().findMinilessonUserGrades(minilesson, user);
		boolean isnew = false;
		if(mug==null) {
			mug = new MinilessonUserGrade(minilesson, user);
			isnew = true;
		}
		
		boolean anyDif = false;
		if(mug.getQualityGrade()!=qual) { mug.setQualityGrade(qual); anyDif = true; }
		if(mug.getDifficultyGrade()!=dif) { mug.setDifficultyGrade(dif); anyDif = true; }
		if(mug.getDuration()!=dur) { mug.setDuration(dur); anyDif = true; }

		if(anyDif) {
			mug.setUnprocessed(true);
		}
		
		if(isnew) {
			DAOProvider.getDao().saveMinilessonUserGrade(mug);
		}
		
		return new StreamRetVal("{\"error\": false}", StandardCharsets.UTF_8, "application/json", null);
	}
	
	// Vraća statistiku za zadane studente - kada su pristupili minilekciji, kada su je završili.
	@DAOProvided @Transactional @Authenticated(attemptLogin=false)
	public StreamRetVal dlStats(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		
		String minilessonID = variables.get("uid");
		if(minilessonID!=null) minilessonID = minilessonID.trim();
		
		if(user==null || !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName()) || minilessonID==null || minilessonID.isEmpty()) {
			resp.sendError(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		
		Minilesson minilesson = DAOProvider.getDao().findMinilessonByID(minilessonID);
		if(minilesson==null) {
			resp.sendError(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		
		String jmbagsStr = StringUtil.trim(req.getParameter("jmbagfilter"));
		Set<String> jmbags = new HashSet<>();
		if(jmbagsStr!=null) {
			try(Scanner sc = new Scanner(jmbagsStr)) {
				String s = sc.next();
				jmbags.add(s);
			}
		}
		
		List<MLUserStat> list = DAOProvider.getDao().getUserMinilessonBasicStats(minilesson.getId());
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Workbook wb = new HSSFWorkbook();
		Sheet s = wb.createSheet();
		Row r = null;
		Cell c = null;
		
		CellStyle cs = wb.createCellStyle();
		CellStyle cs2 = wb.createCellStyle();
		
		Font f = wb.createFont();
		f.setFontHeightInPoints((short) 12);
		f.setColor((short)0xc); // blue
		f.setBold(true);
		
		Font f2 = wb.createFont();
		f2.setFontHeightInPoints((short) 10);
		f2.setColor((short)Font.COLOR_NORMAL);

		cs.setFont(f);
		cs.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
		
		cs2.setFont(f2);
		cs2.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
		
		wb.setSheetName(0, "Statistika");
		
		r = s.createRow(0);
		c = r.createCell(0); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue("JMBAG");
		c = r.createCell(1); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue("Prezime");
		c = r.createCell(2); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue("Ime");
		c = r.createCell(3); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue("Započeo");
		c = r.createCell(4); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue("Završio");
		
		int rowNo = 0;
		
		for(MLUserStat u : list) {
			if(!jmbags.isEmpty() && !jmbags.contains(u.getJmbag())) continue;
			rowNo++;
			
			r = s.createRow(rowNo);
			c = r.createCell(0); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue(u.getJmbag());
			c = r.createCell(1); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue(u.getLastName());
			c = r.createCell(2); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue(u.getFirstName());
			c = r.createCell(3); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue(u.getStartedOn()==null ? "-" : sdf.format(u.getStartedOn()));
			c = r.createCell(4); c.setCellStyle(cs); c.setCellType(Cell.CELL_TYPE_STRING); c.setCellValue(u.getCompletedOn()==null ? "-" : sdf.format(u.getCompletedOn()));
		}

		wb.write(bos);
		bos.close();
		wb.close();
		return new StreamRetVal(bos.toByteArray(), "application/vnd.ms-excel", minilesson.getId()+".xls");
	}
	
}
