package hr.fer.zemris.minilessons.web.filters;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.minilessons.AuthDomain;
import hr.fer.zemris.minilessons.Configuration;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.DAOUtil;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.web.BasicConfiguration;
import hr.fer.zemris.minilessons.web.WebConstants;

/**
 * This filter will fix request encoding and generate {@link Configuration} object and attach it
 * in request attributes under name {@link WebConstants#ML_CONFIG_KEY}.
 * 
 * @author marcupic
 */
@WebFilter(urlPatterns={"/ml/*"})
public class MLFilter implements Filter {

	private final String autoLoginUser;
	
	public MLFilter() {
		String configKey = null;
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-basic.properties");
			if(is==null) {
				throw new IOException("File not found: ml-basic.properties. Could not configure MLFilter.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			configKey = prop.getProperty("ml.autologin.user");
		} catch(IOException ex) {
			throw new RuntimeException(ex);
		}
		
		if(configKey != null) configKey = configKey.trim();
		if(configKey!=null && configKey.isEmpty()) configKey=null;
		
		autoLoginUser = configKey;
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		request.setCharacterEncoding("UTF-8");
		request.setAttribute(WebConstants.ML_CONFIG_KEY, new BasicConfiguration(request.getServletContext(), (HttpServletRequest)request));
		
		HttpServletRequest req = (HttpServletRequest)request;
		if(autoLoginUser!=null && req.getSession().getAttribute("currentUserID") == null) {
			Long currentUserID = DAOUtil.transactional(()->{
				User user = DAOProvider.getDao().findUserByUsername(autoLoginUser, AuthDomain.LOCAL.getKey());
				return user==null ? null : user.getId();
			});
			if(currentUserID!=null) {
				System.out.println("*************************************************");
				System.out.println("Automatically logging in user ID: " + currentUserID);
				System.out.println("*************************************************");
				req.getSession().setAttribute("currentUserID", currentUserID);
			} else {
				System.out.println("***************************************************************");
				System.out.println("Automatical log-in of user " + autoLoginUser +" failed.");
				System.out.println("***************************************************************");
			}
		}
		
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
