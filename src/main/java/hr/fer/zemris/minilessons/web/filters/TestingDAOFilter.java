package hr.fer.zemris.minilessons.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.minilessons.dao.testing.TestingDAOData;

/**
 * A filter which ensures that {@link TestingDAOData} will be able to retrieve users HTTP session.
 * 
 * @author marcupic
 */
@WebFilter(urlPatterns={"/ml/*"})
public class TestingDAOFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest)request;
		TestingDAOData.set(req.getSession());
		try {
			chain.doFilter(request, response);
		} finally {
			TestingDAOData.remove();
		}
	}

	@Override
	public void destroy() {
	}

}
