package hr.fer.zemris.minilessons.web.retvals;

/**
 * This class models controller response which ask to generate content using some
 * template engine.
 * 
 * @author marcupic
 */
public class TemplateRetVal {
	
	/**
	 * Template engine types.
	 * 
	 * @author marcupic
	 */
	public static enum Technology {
		/**
		 * Template engine which can process JSP-files.
		 */
		JSP,
		/**
		 * Template engine which can process ThymeLeaf templates.
		 */
		THYMELEAF
	}
	
	/**
	 * Name of template to process.
	 */
	private String templateName;
	/**
	 * Which engine to use for processing.
	 */
	private Technology technology;
	
	/**
	 * Constructor. If template technology is {@link Technology#JSP}, <code>templateName</code> must be
	 * full servlet-context absolute path (e.g. /WEB-INF/pages/somePage.jsp).
	 * 
	 * @param templateName name of template
	 * @param technology template technology
	 */
	public TemplateRetVal(String templateName, Technology technology) {
		super();
		this.templateName = templateName;
		this.technology = technology;
	}
	
	/**
	 * Getter for technology.
	 * 
	 * @return technology
	 */
	public Technology getTechnology() {
		return technology;
	}
	
	/**
	 * Getter for template name.
	 * @return template name
	 */
	public String getTemplateName() {
		return templateName;
	}
}
