package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.json.JSONObject;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonApproval;
import hr.fer.zemris.minilessons.domain.model.DMinilesson;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.MLDeployer;
import hr.fer.zemris.minilessons.service.MLVerificationException;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;
import hr.fer.zemris.minilessons.web.video.VideoURLProducer;

/**
 * Implementation of home functionality. Used as one step of indirection to easier 
 * change what is the home page. Simply redirect user to currently configured home 
 * page.

 * @author marcupic
 */
public class MLAdministration {

	/**
	 * Show list of minilessons for given user.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return template to render
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=true)
	public TemplateRetVal showList(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		if(!MLCurrentContext.canManageMinilessons()) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<Minilesson> minilessons = DAOProvider.getDao().listAllMinilessonsForOwner(DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId()));
		List<DMinilesson> dminilessons = minilessons.stream().sorted((m1,m2)->m1.getPublicID().compareTo(m2.getPublicID())).map(m->DMinilesson.fromDAOModel(m, MLCurrentContext.getLocale().getLanguage())).collect(Collectors.toList());
		VideoURLProducer vuprod = MLCurrentContext.getVideoURLProducer();
		dminilessons.forEach(m->{
			m.setVideoURLInfo(vuprod.info(req.getContextPath(), m.getId()));
		});
		
		req.setAttribute("minilessons", dminilessons);
		return new TemplateRetVal("mlsadmin", Technology.THYMELEAF);
	}

	/**
	 * Toggle visibility for given minilesson.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=false)
	public StreamRetVal toggleVisibility(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String mid = variables.get("uid");
		mid = mid.trim();
		if(mid.isEmpty()) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		Minilesson ml = DAOProvider.getDao().findMinilessonByID(mid);
		if(ml==null) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		if(!ml.getOwner().getId().equals(MLCurrentContext.getAuthUser().getId()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		ml.setVisibility(!ml.isVisible());
		
		return new StreamRetVal("{\"error\": false, \"visible\": "+ml.isVisible()+"}", StandardCharsets.UTF_8, "application/json", null);
	}
	
	/**
	 * Set approval status for given minilesson.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=false)
	public StreamRetVal setApproval(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String mid = variables.get("uid");
		String stat = variables.get("status");
		mid = mid.trim();
		stat = stat.trim();
		if(mid.isEmpty() || stat.isEmpty()) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		MinilessonApproval newStatus = null;
		try {
			newStatus = MinilessonApproval.valueOf(stat);
		} catch(Exception ex) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_APPROVE_MINILESSONS.getPermissonName()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}

		Minilesson ml = DAOProvider.getDao().findMinilessonByID(mid);
		if(ml==null) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		if(ml.getApproval() != newStatus) {
			ml.setApproval(newStatus);
			ml.setApprovedBy(DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId()));
			ml.setApprovedOn(new Date());
		}
		
		return new StreamRetVal("{\"error\": false, \"status\": \""+ml.getApproval().name()+"\"}", StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Upload of minilesson.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return template to render
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=false)
	public StreamRetVal upload(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws ServletException, IOException {
		if(!MLCurrentContext.canManageMinilessons()) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		Part part = req.getPart("file");
		if(part==null) {
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		boolean treatAsSimple = isSet(req.getParameter("treatAsSimple"));
		boolean mlIsVisible = isSet(req.getParameter("mlIsVisible"));
		
		System.out.println("Submitted treatAsSimple = "+req.getParameter("treatAsSimple"));
		System.out.println("Submitted mlIsVisible = "+req.getParameter("mlIsVisible"));
		System.out.println("Submitted file name = "+part.getSubmittedFileName());
		System.out.println("Submitted content type = "+part.getContentType());
		System.out.println("Submitted size = "+part.getSize());

		Path tmp = null;
		try {
			tmp = Files.createTempFile("mnl", ".tmp");
		} catch(Exception ex) {
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		try {
			Files.copy(part.getInputStream(), tmp, StandardCopyOption.REPLACE_EXISTING);
			Map<String,String> options = new HashMap<>();
			options.put("create", "false");
			options.put("encoding", "UTF-8");
			try(FileSystem fs = FileSystems.newFileSystem(tmp, null)) {
				MLDeployer.deploy(fs.getPath("/"), DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId()), mlIsVisible, treatAsSimple);
			} catch(MLVerificationException ex) {
				JSONObject obj = new JSONObject();
				obj.put("error", true);
				obj.put("msg", ex.getMessage());
				return new StreamRetVal(obj.toString(), StandardCharsets.UTF_8, "application/json", null);
			} catch(Exception ex) {
				ex.printStackTrace();
				return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		} finally {
			try {
				Files.delete(tmp);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}

		return new StreamRetVal("{\"error\": false, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
	}

	private boolean isSet(String value) {
		return value!=null && (value.equalsIgnoreCase("on") || value.equalsIgnoreCase("true") || value.equals("1"));
	}
	
}
