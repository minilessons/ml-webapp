package hr.fer.zemris.minilessons.web.mappings;

import java.io.IOException;

import javax.servlet.ServletException;

/**
 * A callback object which will be notified with generated result.
 * 
 * @author marcupic
 */
public interface ResultCallback {

	/**
	 * Method which will be called when result is produced.
	 * 
	 * @param result result
	 * @throws IOException
	 * @throws ServletException
	 */
	public void accept(Object result) throws IOException, ServletException;
	
}
