package hr.fer.zemris.minilessons.web.listeners;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import hr.fer.zemris.minilessons.dao.DAOBoot;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.logging.MLLogger;
import hr.fer.zemris.minilessons.periodicals.PeriodicalManager;
import hr.fer.zemris.minilessons.service.AuthenticationService;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.WebConstants;
import hr.fer.zemris.minilessons.web.video.VideoURLProducer;

/**
 * Listener which is used to boot application.
 * 
 * @author marcupic
 */
@WebListener
public class AppLifecycleListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		initCommonData(sce);
		initTemplateEngine(sce);
		initDAO(sce);
		AuthenticationService.initialize();
		fireInitializers(sce);
		initPeriodicalService(sce);
	}

	/**
	 * Initialization of common data.
	 * 
	 * @param sce servlet context event
	 */
	private void initCommonData(ServletContextEvent sce) {
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-basic.properties");
			if(is==null) {
				throw new IOException("File not found: ml-basic.properties. Could not initialize common data.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			String tmp = StringUtil.trim(prop.getProperty("ml.registration.activation.smtp"));
			if(tmp!=null) sce.getServletContext().setAttribute(WebConstants.ML_MAIL_HOST, tmp);
			tmp = StringUtil.trim(prop.getProperty("ml.registration.activation.web.host"));
			if(tmp!=null) sce.getServletContext().setAttribute(WebConstants.ML_MAIL_ACT_SERVER, tmp);
			tmp = StringUtil.trim(prop.getProperty("ml.registration.activation.web.path"));
			if(tmp!=null) sce.getServletContext().setAttribute(WebConstants.ML_MAIL_ACT_PATH, tmp);
			tmp = StringUtil.trim(prop.getProperty("ml.registration.activation.from"));
			if(tmp!=null) sce.getServletContext().setAttribute(WebConstants.ML_MAIL_FROM, tmp);
		} catch(IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	/**
	 * Initialization of periodicals execution subsystem.
	 * 
	 * @param sce servlet context event
	 */
	private void initPeriodicalService(ServletContextEvent sce) {
		String configKey = null;
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-basic.properties");
			if(is==null) {
				throw new IOException("File not found: ml-basic.properties. Could not start periodicals executor service.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			configKey = prop.getProperty("ml.periodicals");
			if(configKey==null) {
				System.out.println("ml.periodicals not found. Periodicals execution service will not be started.");
			}
		} catch(IOException ex) {
			throw new RuntimeException(ex);
		}
		if(configKey!=null) {
			sce.getServletContext().setAttribute(WebConstants.ML_PERIODICALS_MANAGER_KEY, new PeriodicalManager(configKey));
		}
	}

	/**
	 * Initialization of DAO subsystem.
	 * 
	 * @param sce servlet context event
	 */
	private void initDAO(ServletContextEvent sce) {
		DAOBoot daoBoot = null;
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("dao.properties");
			if(is==null) {
				throw new IOException("File not found: dao.properties. Could not initialize DAO layer.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			String dbi = prop.getProperty("dao.boot.impl");
			if(dbi==null || dbi.isEmpty()) {
				throw new IOException("dao.boot.impl is not set.");
			}
			daoBoot = (DAOBoot)DAOProvider.class.getClassLoader().loadClass(dbi).newInstance();
		} catch (IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		
		daoBoot.startup();
		
		sce.getServletContext().setAttribute(WebConstants.ML_DAO_BOOT_KEY, daoBoot);
	}

	/**
	 * Run declared initializers, in declaration order.
	 * 
	 * @param sce servlet context event
	 */
	private void fireInitializers(ServletContextEvent sce) {
		List<String> initializerClasses = new ArrayList<>();
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-basic.properties");
			if(is==null) {
				throw new IOException("File not found: ml-basic.properties. Could not run initializes.");
			}
			try {
				prop.load(is);
			} finally {
				is.close();
			}
			String initList = prop.getProperty("ml.initializers");
			if(initList != null) initList = initList.trim();
			if(initList == null || initList.isEmpty()) return;
			
			for(String s : initList.split(" ")) {
				if(s.isEmpty()) continue;
				initializerClasses.add(s);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		System.out.println("Found " + initializerClasses.size() +" initializer(s).");
		
		for(String className : initializerClasses) {
			try {
				System.out.println("Running initializer: " + className);
				Runnable task = (Runnable)this.getClass().getClassLoader().loadClass(className).newInstance();
				task.run();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				System.err.println("Exception while running initializer: " + className);
				e.printStackTrace();
			}
		}
	}

	/**
	 * Initialization of template engine. Engine is bound into servlet context attributes under
	 * name {@link WebConstants#ML_TEMPLATE_ENGINE_KEY}.
	 * 
	 * @param sce servlet context event
	 */
	private void initTemplateEngine(ServletContextEvent sce) {
		boolean cacheable = false;
		long cacheTTL = 3600000L;
		String videoProvClass = null;
		String videoProvConfig = null;
		
		try {
			Properties prop = new Properties();
			InputStream is = DAOProvider.class.getClassLoader().getResourceAsStream("ml-basic.properties");
			if(is!=null) {
				try {
					prop.load(is);
				} finally {
					is.close();
				}
				String cacheableStr = prop.getProperty("ml.thymeleaf.cacheable");
				String cacheTTLStr = prop.getProperty("ml.thymeleaf.cacheTTL");
				if("true".equals(cacheableStr)) cacheable = true;
				if(cacheTTLStr!=null) {
					try {
						cacheTTL = Long.parseLong(cacheTTLStr);
					} catch(Exception ex) {
						throw new RuntimeException("ml.thymeleaf.cacheTTL is invalid.");
					}
					if(cacheTTL < 0) {
						throw new RuntimeException("ml.thymeleaf.cacheTTL is invalid (less than zero).");
					}
				}
			}
			
			videoProvClass = prop.getProperty("ml.video.url.producer.className");
			if(videoProvClass!=null) videoProvClass = videoProvClass.trim();
			videoProvConfig = prop.getProperty("ml.video.url.producer.configuration");
			if(videoProvConfig!=null) videoProvConfig = videoProvConfig.trim();
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver(sce.getServletContext());
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setPrefix("/WEB-INF/templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheTTLMs(cacheTTL);
		templateResolver.setCacheable(cacheable);

		TemplateEngine engine = new TemplateEngine();
		engine.setTemplateResolver(templateResolver);
		
		sce.getServletContext().setAttribute(WebConstants.ML_TEMPLATE_ENGINE_KEY, engine);
		
		if(videoProvClass==null || videoProvClass.isEmpty()) {
			throw new RuntimeException("Property ml.video.url.producer.className is not set in ml-basic.properties.");
		}
		try {
			VideoURLProducer prod = (VideoURLProducer)this.getClass().getClassLoader().loadClass(videoProvClass).getConstructor(String.class).newInstance(videoProvConfig);
			sce.getServletContext().setAttribute(WebConstants.ML_VIDEO_URL_PRODUCER_KEY, prod);
		} catch(Exception ex) {
			throw new RuntimeException("Could not initialize video URL provider "+videoProvClass+".", ex);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		MLLogger.shutdownAutoflusher();
		MLLogger.getAccesslogger().flush();
		MLLogger.getAccesslogger().close();
		MLLogger.getEdulogger().flush();
		MLLogger.getEdulogger().close();
		PeriodicalManager pman = (PeriodicalManager)sce.getServletContext().getAttribute(WebConstants.ML_PERIODICALS_MANAGER_KEY);
		if(pman != null) {
			pman.shutdown(true, 20000);
		}
		
		sce.getServletContext().removeAttribute(WebConstants.ML_TEMPLATE_ENGINE_KEY);
		sce.getServletContext().removeAttribute(WebConstants.ML_VIDEO_URL_PRODUCER_KEY);
		
		DAOBoot daoBoot = (DAOBoot)sce.getServletContext().getAttribute(WebConstants.ML_DAO_BOOT_KEY);
		if(daoBoot!=null) {
			daoBoot.destroy();
		}
	}

}
