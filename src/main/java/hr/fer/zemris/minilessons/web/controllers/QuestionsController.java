package hr.fer.zemris.minilessons.web.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.CommonQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.JSQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.JSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.QIStatus;
import hr.fer.zemris.minilessons.dao.model.QuestionApproval;
import hr.fer.zemris.minilessons.dao.model.QuestionConfigVariability;
import hr.fer.zemris.minilessons.dao.model.QuestionControlResult;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.dao.model.WSQDWorkflow;
import hr.fer.zemris.minilessons.dao.model.WSQDWorkflowState;
import hr.fer.zemris.minilessons.dao.model.WSQuestionInstance;
import hr.fer.zemris.minilessons.dao.model.Workspace;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionDefinitionAttachment;
import hr.fer.zemris.minilessons.dao.model.XMLQuestionInstance;
import hr.fer.zemris.minilessons.domain.model.DQuestion;
import hr.fer.zemris.minilessons.domain.model.DQuestionInstance;
import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.questions.quizzes.AbcSingleAnswer;
import hr.fer.zemris.minilessons.questions.quizzes.AbcSingleQuestion;
import hr.fer.zemris.minilessons.questions.quizzes.AbcSingleQuestionGroup;
import hr.fer.zemris.minilessons.questions.quizzes.QuestionFactory;
import hr.fer.zemris.minilessons.questions.quizzes.QuestionGroupBase;
import hr.fer.zemris.minilessons.questions.quizzes.SupportedXMLQuestions;
import hr.fer.zemris.minilessons.questions.quizzes.TextQuestion;
import hr.fer.zemris.minilessons.questions.quizzes.TextQuestionGroup;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.QDeployer;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.WebUtil;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

public class QuestionsController {

	@DAOProvided @Authenticated(optional=true, redirectAllowed=true)
	public TemplateRetVal listQuestions(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String lang = MLCurrentContext.getLocale().getLanguage();
		DAO dao = DAOProvider.getDao();
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		
		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		String qid = StringUtil.trim(variables.get("qid"));
		String givenConfig = StringUtil.trim(variables.get("config"));

		User currentUser = MLCurrentContext.getAuthUser()==null ? null : dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		
		if(qid!=null) {
			CommonQuestionDefinition qd = dao.findCommonQuestionDefinitionByID(qid);
			boolean err = qd == null;
			if(!err && (!qd.isPublicQuestion() || !qd.isVisible()) && !canAdmin && !qd.getOwner().equals(currentUser)) {
				err = true;
			}
			if(err) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			String idprefixx = null;
			if(qd instanceof JSQuestionDefinition) {
				idprefixx = "jsi";
				//System.out.println("Pitanje je primjerak od JSQuestionDefinition.");
			} else if(qd instanceof XMLQuestionDefinition) {
				idprefixx = "xmli";
				//System.out.println("Pitanje je primjerak od XMLQuestionDefinition.");
			} else {
				System.out.println("Pitanje nije primjerak niti od JSQuestionDefinition niti od XMLQuestionDefinition.");
				return null;
			}
			String idprefix = idprefixx;
			
			if(qd.getApproval()==QuestionApproval.REJECTED) {
				req.setAttribute("errorMsgKey", "questionDenied.rejected");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			} else if(qd.getApproval()==QuestionApproval.SOFT_WAITING || qd.getApproval()==QuestionApproval.WAITING) {
				req.setAttribute("errorMsgKey", "questionDenied.approval");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}

			// Želim prikazati detalje pitanja, konfiguracije i slično...
			
			DQuestion q = DQuestion.fromDAOModel(qd, lang);
			req.setAttribute("qd", q);

			List<DQuestionInstance> list = null;
			if(qd instanceof JSQuestionDefinition) {
				list = dao.findPublicUsersJSQuestionInstances(currentUser, (JSQuestionDefinition)qd);
			} else if(qd instanceof XMLQuestionDefinition) {
				list = dao.findPublicUsersXMLQuestionInstances(currentUser, (XMLQuestionDefinition)qd);
			}
			
			Map<String, QDConfigData> map = new LinkedHashMap<>();
			for(Map.Entry<String, QuestionConfigVariability> e : qd.getConfigVariability().entrySet()) {
				if(givenConfig!=null && !givenConfig.equals(e.getKey())) continue;
				QDConfigData data = new QDConfigData();
				data.name = e.getKey();
				data.variability = e.getValue();
				data.showCreateNewQuestion = false;
				data.createNewQuestionLocked = qd.getApproval()==QuestionApproval.REJECTED;
				data.instances = new ArrayList<>();
				map.put(data.name, data);
			}
			
			if(list!=null) {
				list.forEach(qi->{
					QDConfigData data = map.get(qi.getConfigName());
					if(data==null && givenConfig==null) {
						data = new QDConfigData();
						data.name = qi.getConfigName();
						data.variability = null;
						data.showCreateNewQuestion = false;
						data.createNewQuestionLocked = true;
						data.instances = new ArrayList<>();
						map.put(data.name, data);
					}
					if(data!=null) {
						qi.setIdprefix(idprefix);
						data.instances.add(qi);
					}
				});
				map.values().forEach(d->{
					d.instances.sort(Comparator.naturalOrder());
					d.showCreateNewQuestion = !d.createNewQuestionLocked && !d.instances.stream().filter(qi-> qi.getStatus().equals(QIStatus.SOLVABLE)).findAny().isPresent();
				});
			}
			
			req.setAttribute("data", map);
			req.setAttribute("apctx", apctx);

			return new TemplateRetVal("questionDetails", Technology.THYMELEAF);

			//req.setAttribute("errorMsgKey", "notImplemented.soon");
			//return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<CommonQuestionDefinition> list = dao.findVisiblePublicCommonQuestionDefinitions(currentUser);
		req.setAttribute("curlang", lang);
		req.setAttribute("questions", list.stream().map(q->DQuestion.fromDAOModel(q, lang)).collect(Collectors.toList()));
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("apctx", apctx);
		
		return new TemplateRetVal("questionsBrowser",Technology.THYMELEAF);
	}
	
	public static class QDConfigData {
		private String name;
		private QuestionConfigVariability variability;
		private boolean showCreateNewQuestion;
		private boolean createNewQuestionLocked;
		private List<DQuestionInstance> instances;
		public String getName() {
			return name;
		}
		public QuestionConfigVariability getVariability() {
			return variability;
		}
		public boolean isShowCreateNewQuestion() {
			return showCreateNewQuestion;
		}
		public List<DQuestionInstance> getInstances() {
			return instances;
		}
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public TemplateRetVal showUpload(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		req.setAttribute("canAdmin", canAdmin);

		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());

		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		String wsid = apctx.getParts().get("ws");
		if(wsid != null) {
			Long id = null;
			try {
				id = Long.parseLong(wsid);
			} catch(Exception ex) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			Workspace ws = dao.findWorkspaceByID(id);
			boolean canEdit = ws!=null && user.equals(ws.getOwner());
			if(!canEdit && !canAdmin) {
				req.setAttribute("errorMsgKey", "noPermission");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			req.setAttribute("workspace", ws);
		} else {
			if(!canAdmin) {
				req.setAttribute("errorMsgKey", "noPermission");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
		return new TemplateRetVal("quploadform", Technology.THYMELEAF);
	}

	public static class XMLQuestionUploadData {
		private String xml;
		private List<XMLQuestionDefinitionAttachment> attachments;
		private boolean publicQuestion;
		private boolean visible;
		
		public List<XMLQuestionDefinitionAttachment> getAttachments() {
			return attachments;
		}
		
		public String getXml() {
			return xml;
		}

		public boolean isPublicQuestion() {
			return publicQuestion;
		}
		
		public boolean isVisible() {
			return visible;
		}
		
		public static XMLQuestionUploadData fromString(String xml, boolean publicQuestion, boolean visible) {
			if(xml==null) throw new IllegalArgumentException("xml was not provided.");
			XMLQuestionUploadData data = new XMLQuestionUploadData();
			data.xml = xml;
			data.attachments = Collections.emptyList();
			data.publicQuestion = publicQuestion;
			data.visible = visible;
			return data;
		}
		
		public static XMLQuestionUploadData fromMultipart(HttpServletRequest req, String mpName, boolean publicQuestion, boolean visible) throws IOException {
			XMLQuestionUploadData data = new XMLQuestionUploadData();
			
			Part part = null;
			try {
				part = req.getPart(mpName);
			} catch (ServletException e) {
				throw new IllegalArgumentException("invalidParameters");
			}
			if(part.getSize()>10*1024*1024) {
				throw new IllegalArgumentException("invalidParameters (archive too big)");
			}
			
			String xml = null;

			List<XMLQuestionDefinitionAttachment> attachments = new ArrayList<>();
			
			try(ZipInputStream zis = new ZipInputStream(part.getInputStream())) {
				ZipEntry currentZE = null;
				while((currentZE=zis.getNextEntry()) != null) {
					if(currentZE.getName().equals("question.xml")) {
						if(xml!=null) {
							throw new IllegalArgumentException("invalidParameters (multiple question.xml found))");
						}
						byte[] xmldata = inputStreamToByteArray(zis);
						zis.closeEntry();
						xml = new String(xmldata, StandardCharsets.UTF_8);
					} else {
						String name = currentZE.getName();
						if(name.indexOf('/') != -1 || name.indexOf('\\') != -1) {
							throw new IllegalArgumentException("invalidParameters (folders are not supported))");
						}
						XMLQuestionDefinitionAttachment att = new XMLQuestionDefinitionAttachment();
						att.setBody(inputStreamToByteArray(zis));
						zis.closeEntry();
						att.setName(name);
						if(attachments.stream().filter(a->a.getName().equals(name)).findAny().isPresent()) {
							throw new IllegalArgumentException("invalidParameters (multiple files with same name found))");
						}
						attachments.add(att);
					}
				}
			}
			if(xml==null) {
				throw new IllegalArgumentException("invalidParameters (no question.xml found in archive))");
			}
			data.xml = xml;
			data.attachments = attachments;
			data.publicQuestion = publicQuestion;
			data.visible = visible;
			return data;
		}
		
	}
	
	private TemplateRetVal doUploadSimpleXML(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		req.setAttribute("canAdmin", canAdmin);

		// TODO: popravi da uzima u obzir publicID, version i minorVersion; vidi što je s ostalim podatcima tipa approved i slično
		String source = StringUtil.trim(req.getParameter("source"));
		if(!"xml".equals(source) && !"zip".equals(source)) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		
		Workspace ws = null;

		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		String wsid = apctx.getParts().get("ws");
		if(wsid != null) {
			Long id = null;
			try {
				id = Long.parseLong(wsid);
			} catch(Exception ex) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			ws = dao.findWorkspaceByID(id);
			boolean canEdit = ws!=null && user.equals(ws.getOwner());
			if(!canEdit && !canAdmin) {
				req.setAttribute("errorMsgKey", "noPermission");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			req.setAttribute("workspace", ws);
		} else {
			if(!canAdmin) {
				req.setAttribute("errorMsgKey", "noPermission");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
		
		boolean ispublic = "1".equals(req.getParameter("publicQuestion"));
		
		XMLQuestionUploadData data = null;
		try {
			if("xml".equals(source)) {
				data = XMLQuestionUploadData.fromString(StringUtil.trim(req.getParameter("xmlspec")), ispublic, true);
			} else {
				data = XMLQuestionUploadData.fromMultipart(req, "zip", ispublic, true);
			}	
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidXML");
			req.setAttribute("errorDetails", ex.getMessage());
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		try {
			QDeployer.deployXMLQuestion(user, data);
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidXML");
			req.setAttribute("errorDetails", ex.getMessage());
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(ws!=null) {
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+ws.getId());		
			return null;
		}
		
		req.setAttribute("feedbackMsg", "Zadatak je uspješno učitan.");
		return new TemplateRetVal("quploadform", Technology.THYMELEAF);
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public Object serveQuestionFile(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		req.setAttribute("canAdmin", canAdmin);

		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());

		String rqi = StringUtil.trim(variables.get("rqi"));
		if(rqi==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String filename = StringUtil.trim(variables.get("filename"));
		if(filename==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(rqi.startsWith("xmli-")) {
			Optional<Long> id = WorkspaceController.stringToLong(rqi.substring(5));
			if(id==null || !id.isPresent()) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			return commonServeFileXML(req, resp, variables, dao, user, id.get(), filename);
		} else {
			// Vrsta zadatka trenutno nije podržana...
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
	}
	
	private Object commonServeFileXML(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables,
			DAO dao, User user, Long rqid, String filename) {
		XMLQuestionInstance xmlqi = dao.findXMLQuestionInstanceByID(rqid);
		if(xmlqi==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String ctx = StringUtil.trim(xmlqi.getContext());
		if(ctx==null) {
			if(!user.equals(xmlqi.getUser())) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		} else {
			if(ctx.startsWith("wsqi=")) {
				Long wsqiid = Long.valueOf(ctx.substring(5));
				WSQuestionInstance wsqi = dao.findWSQuestionInstanceByID(wsqiid);
				if(wsqi==null) {
					req.setAttribute("errorMsgKey", "invalidParameters");
					return new TemplateRetVal("errorPage", Technology.THYMELEAF);
				}
				if(wsqi.getGroup() != null) {
					if(!wsqi.getGroup().getUsers().contains(user)) {
						req.setAttribute("errorMsgKey", "invalidParameters");
						return new TemplateRetVal("errorPage", Technology.THYMELEAF);
					}
				} else {
					if(!wsqi.getUser().equals(user)) {
						req.setAttribute("errorMsgKey", "invalidParameters");
						return new TemplateRetVal("errorPage", Technology.THYMELEAF);
					}
				}
			} else {
				// nepodržan kontekst...
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
		
		XMLQuestionDefinitionAttachment att = dao.findXMLQuestionDefinitionAttachment(xmlqi.getQuestion(), filename);
		if(att==null) {
			return new StreamRetVal(404, "File not found");
		}
		return new StreamRetVal(att.getBody(), WebUtil.guessMime(att.getName()), att.getName());
	}

	private static byte[] inputStreamToByteArray(ZipInputStream zis) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[4096];
		while(true) {
			int r =  zis.read(buf);
			if(r<1) break;
			bos.write(buf, 0, r);
		}
		bos.close();
		return bos.toByteArray();
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=false) @Transactional
	public TemplateRetVal doUpload(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return doUploadSimpleXML(req,resp, variables);
	}

	public static enum SS_ACTION {SHOW_FOR_SOLVING, SHOW_FOR_INSPECT, SHOW_FOR_VERIFY, SAVE, SAVE_EVAL, SAVE_VERIFY}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public Object inspectSolving(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return solvingCommon(req, resp, variables, SS_ACTION.SHOW_FOR_INSPECT);
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public Object verifySolving(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return solvingCommon(req, resp, variables, SS_ACTION.SHOW_FOR_VERIFY);
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true)
	public Object showSolving(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		return solvingCommon(req, resp, variables, SS_ACTION.SHOW_FOR_SOLVING);
	}
	
	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public Object createNewQuestionInstance(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());
		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));

		String qdid = StringUtil.trim(variables.get("qid"));
		if(qdid==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String config = StringUtil.trim(variables.get("cid"));
		if(config==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		CommonQuestionDefinition cqd = dao.findCommonQuestionDefinitionByID(qdid);
		if(cqd==null || cqd.getApproval()==QuestionApproval.REJECTED || !cqd.isVisible() || !cqd.isPublicQuestion()) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		List<DQuestionInstance> list = null;
		if(cqd instanceof JSQuestionDefinition) {
			list = dao.findPublicUsersJSQuestionInstances(user, (JSQuestionDefinition)cqd);
		} else if(cqd instanceof XMLQuestionDefinition) {
			list = dao.findPublicUsersXMLQuestionInstances(user, (XMLQuestionDefinition)cqd);
		}
		
		boolean canCreate = list==null || !list.stream().filter(q->q.getStatus()==QIStatus.SOLVABLE && config.equals(q.getConfigName())).findAny().isPresent();
		if(!canCreate || !cqd.getConfigVariability().containsKey(config)) {
			req.setAttribute("errorMsgKey", "solvableInstanceExists");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(cqd instanceof JSQuestionDefinition) {
			JSQuestionInstance jsqi = createJSQuestionInstance(dao, (JSQuestionDefinition)cqd, user, config);
			return commonSolvingJS(req, resp, variables, apctx, dao, user, jsqi.getId(), SS_ACTION.SHOW_FOR_SOLVING);
		} else if(cqd instanceof XMLQuestionDefinition) {
			XMLQuestionInstance xmlqi = createXMLQuestionInstance(dao, (XMLQuestionDefinition)cqd, user, config);
			return commonSolvingXML(req, resp, variables, apctx, dao, user, xmlqi.getId(), SS_ACTION.SHOW_FOR_SOLVING);
		}
		
		return null;
	}
	
	private JSQuestionInstance createJSQuestionInstance(DAO dao, JSQuestionDefinition qd, User user, String config) {
		return QTester.createJSQuestionInstance(dao, qd, user, config);
	}

	private static XMLQuestionInstance createXMLQuestionInstance(DAO dao, XMLQuestionDefinition qd, User user, String config) {
		QuestionGroupBase qgb = QuestionFactory.fromText(qd.getXml());

		if(qgb.getQuestionType()==SupportedXMLQuestions.ABCSINGLE) {
			AbcSingleQuestion selected = null;
			for(int i = 0, n = qgb.size(); i < n; i++) {
				if(qgb.getQuestion(i).getId().equals(config)) {
					selected = (AbcSingleQuestion)qgb.getQuestion(i);
					break;
				}
			}
			if(selected==null) throw new RuntimeException("No question with selected configuration found.");

			// Trenutno radimo bez permutacija opcija, pa fiksno pišemo da biramo opcije 0,1,2,...
			// Kasnije se i ovo može mijenjati / konfigurirati...
			String ordering = IntStream.range(0, selected.getAnswers().size()).mapToObj(Integer::toString).collect(Collectors.joining(","));
			XMLQuestionInstance realqi = new XMLQuestionInstance();
			realqi.setContext(null);
			realqi.setQuestion(qd);
			realqi.setqData(ordering);
			realqi.setConfigName(config);
			realqi.setCreatedOn(new Date());
			realqi.setStatus(QIStatus.SOLVABLE);
			realqi.setUser(user);
			dao.saveXMLQuestionInstance(realqi);
			return realqi;
		} else if(qgb.getQuestionType()==SupportedXMLQuestions.TEXTUAL) {
			TextQuestion selected = null;
			for(int i = 0, n = qgb.size(); i < n; i++) {
				if(qgb.getQuestion(i).getId().equals(config)) {
					selected = (TextQuestion)qgb.getQuestion(i);
					break;
				}
			}
			if(selected==null) throw new RuntimeException("No question with selected configuration found.");
			XMLQuestionInstance realqi = new XMLQuestionInstance();
			realqi.setContext(null);
			realqi.setQuestion(qd);
			realqi.setqData(null);
			realqi.setConfigName(config);
			realqi.setCreatedOn(new Date());
			realqi.setStatus(QIStatus.SOLVABLE);
			realqi.setUser(user);
			dao.saveXMLQuestionInstance(realqi);
			return realqi;
		} else {
			throw new RuntimeException("Unsupported question type.");
		}
	}

	@DAOProvided @Authenticated(optional=false, redirectAllowed=true) @Transactional
	public Object saveSolving(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		String act = StringUtil.trim(req.getParameter("action"));
		if("save".equals(act)) {
			return solvingCommon(req, resp, variables, SS_ACTION.SAVE);
		} else if("saveeval".equals(act)) {
			return solvingCommon(req, resp, variables, SS_ACTION.SAVE_EVAL);
		} else if("saveverify".equals(act)) {
			return solvingCommon(req, resp, variables, SS_ACTION.SAVE_VERIFY);
		} else {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
	}
	
	public Object solvingCommon(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, SS_ACTION action) throws IOException {
		boolean canAdmin = MLCurrentContext.getAuthUser() != null && MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
		req.setAttribute("canAdmin", canAdmin);
		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));

		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());

		String rqi = StringUtil.trim(variables.get("rqi"));
		if(rqi==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		if(rqi.startsWith("xmli-")) {
			Optional<Long> id = WorkspaceController.stringToLong(rqi.substring(5));
			if(id==null || !id.isPresent()) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			return commonSolvingXML(req, resp, variables, apctx, dao, user, id.get(), action);
		} else if(rqi.startsWith("jsi-")) {
			Optional<Long> id = WorkspaceController.stringToLong(rqi.substring(4));
			if(id==null || !id.isPresent()) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			return commonSolvingJS(req, resp, variables, apctx, dao, user, id.get(), action);
		} else {
			// Vrsta zadatka trenutno nije podržana...
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
	}

	private Object commonSolvingJS(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, Long rqid, SS_ACTION action) throws IOException {
		JSQuestionInstance jsqi = dao.findJSQuestionInstanceByID(rqid);
		if(jsqi==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String ctx = StringUtil.trim(jsqi.getContext());
		if(ctx==null) {
			if(!user.equals(jsqi.getUser()) || (jsqi.getStatus()!=QIStatus.SOLVABLE && jsqi.getStatus()!=QIStatus.EVALUATED) || action==SS_ACTION.SHOW_FOR_VERIFY || action==SS_ACTION.SAVE_VERIFY) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			if(jsqi.getStatus()!=QIStatus.SOLVABLE && action!=SS_ACTION.SHOW_FOR_INSPECT) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			return renderForSolvingJS(req,resp,variables,apctx,dao,user,jsqi,null,true,action,null,null);
		} else {
			if(ctx.startsWith("wsqi=")) {
				Long wsqiid = Long.valueOf(ctx.substring(5));
				WSQuestionInstance wsqi = dao.findWSQuestionInstanceByID(wsqiid);
				if(wsqi==null) {
					req.setAttribute("errorMsgKey", "invalidParameters");
					return new TemplateRetVal("errorPage", Technology.THYMELEAF);
				}
				if(wsqi.getGroup() != null) {
					if(!wsqi.getGroup().getUsers().contains(user)) {
						req.setAttribute("errorMsgKey", "invalidParameters");
						return new TemplateRetVal("errorPage", Technology.THYMELEAF);
					}
				} else {
					if(!wsqi.getUser().equals(user)) {
						req.setAttribute("errorMsgKey", "invalidParameters");
						return new TemplateRetVal("errorPage", Technology.THYMELEAF);
					}
				}
				if(wsqi.getDefinition().getWorkflow()==WSQDWorkflow.WF3) {
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.OPEN && wsqi.getStatus()==QIStatus.SOLVABLE) {
						return renderForSolvingJS(req,resp,variables,apctx,dao,user,jsqi,wsqi,false,action,null,null);
					}
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.INSPECT) {
						return renderForSolvingJS(req,resp,variables,apctx,dao,user,jsqi,wsqi,false,action,null,null);
					}
					resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idlee");
					return null;
				} else if(wsqi.getDefinition().getWorkflow()==WSQDWorkflow.WF4) {
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.OPEN && wsqi.getStatus()==QIStatus.SOLVABLE) {
						return renderForSolvingJS(req,resp,variables,apctx,dao,user,jsqi,wsqi,false,action,null,null);
					}
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.INSPECT) {
						return renderForSolvingJS(req,resp,variables,apctx,dao,user,jsqi,wsqi,false,action,null,null);
					}
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.VERIFY && wsqi.getToControl() != null) {
						WSQuestionInstance wsqic = wsqi.getToControl();
						if(!wsqic.getActualInstance().startsWith("jsi=")) {
							req.setAttribute("errorMsgKey", "invalidParameters");
							return new TemplateRetVal("errorPage", Technology.THYMELEAF);
						}
						JSQuestionInstance jsqic = dao.findJSQuestionInstanceByID(Long.valueOf(wsqic.getActualInstance().substring(4)));
						if(jsqic==null) {
							req.setAttribute("errorMsgKey", "invalidParameters");
							return new TemplateRetVal("errorPage", Technology.THYMELEAF);
						}
						return renderForSolvingJS(req,resp,variables,apctx,dao,user,jsqi,wsqi,false,action,jsqic,wsqic);
					}
					resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idlee");
					return null;
				}
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			} else {
				// nepodržan kontekst...
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
	}
	
	private Object commonSolvingXML(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, Long rqid, SS_ACTION action) throws IOException {
		XMLQuestionInstance xmlqi = dao.findXMLQuestionInstanceByID(rqid);
		if(xmlqi==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		String ctx = StringUtil.trim(xmlqi.getContext());
		if(ctx==null) {
			if(!user.equals(xmlqi.getUser()) || (xmlqi.getStatus()!=QIStatus.SOLVABLE && xmlqi.getStatus()!=QIStatus.EVALUATED) || action==SS_ACTION.SHOW_FOR_VERIFY || action==SS_ACTION.SAVE_VERIFY) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			if(xmlqi.getStatus()!=QIStatus.SOLVABLE && action!=SS_ACTION.SHOW_FOR_INSPECT) {
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
			return renderForSolvingXML(req,resp,variables,apctx,dao,user,xmlqi,null,true,action,null,null);
		} else {
			if(ctx.startsWith("wsqi=")) {
				Long wsqiid = Long.valueOf(ctx.substring(5));
				WSQuestionInstance wsqi = dao.findWSQuestionInstanceByID(wsqiid);
				if(wsqi==null) {
					req.setAttribute("errorMsgKey", "invalidParameters");
					return new TemplateRetVal("errorPage", Technology.THYMELEAF);
				}
				if(wsqi.getGroup() != null) {
					if(!wsqi.getGroup().getUsers().contains(user)) {
						req.setAttribute("errorMsgKey", "invalidParameters");
						return new TemplateRetVal("errorPage", Technology.THYMELEAF);
					}
				} else {
					if(!wsqi.getUser().equals(user)) {
						req.setAttribute("errorMsgKey", "invalidParameters");
						return new TemplateRetVal("errorPage", Technology.THYMELEAF);
					}
				}
				if(wsqi.getDefinition().getWorkflow()==WSQDWorkflow.WF3) {
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.OPEN && wsqi.getStatus()==QIStatus.SOLVABLE) {
						return renderForSolvingXML(req,resp,variables,apctx,dao,user,xmlqi,wsqi,false,action,null,null);
					}
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.INSPECT) {
						return renderForSolvingXML(req,resp,variables,apctx,dao,user,xmlqi,wsqi,false,action,null,null);
					}
					resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idlee");
					return null;
				} else if(wsqi.getDefinition().getWorkflow()==WSQDWorkflow.WF4) {
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.OPEN && wsqi.getStatus()==QIStatus.SOLVABLE) {
						return renderForSolvingXML(req,resp,variables,apctx,dao,user,xmlqi,wsqi,false,action,null,null);
					}
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.INSPECT) {
						return renderForSolvingXML(req,resp,variables,apctx,dao,user,xmlqi,wsqi,false,action,null,null);
					}
					if(wsqi.getDefinition().getWorkflowCurrentState()==WSQDWorkflowState.VERIFY && wsqi.getToControl() != null) {
						WSQuestionInstance wsqic = wsqi.getToControl();
						if(!wsqic.getActualInstance().startsWith("xmli=")) {
							req.setAttribute("errorMsgKey", "invalidParameters");
							return new TemplateRetVal("errorPage", Technology.THYMELEAF);
						}
						XMLQuestionInstance xmlqic = dao.findXMLQuestionInstanceByID(Long.valueOf(wsqic.getActualInstance().substring(5)));
						if(xmlqic==null) {
							req.setAttribute("errorMsgKey", "invalidParameters");
							return new TemplateRetVal("errorPage", Technology.THYMELEAF);
						}
						return renderForSolvingXML(req,resp,variables,apctx,dao,user,xmlqi,wsqi,false,action,xmlqic,wsqic);
					}
					resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idlee");
					return null;
				}
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			} else {
				// nepodržan kontekst...
				req.setAttribute("errorMsgKey", "invalidParameters");
				return new TemplateRetVal("errorPage", Technology.THYMELEAF);
			}
		}
	}

	private Object renderForSolvingXML(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, XMLQuestionInstance xmlqi, WSQuestionInstance wsqi, boolean showEvalButton, SS_ACTION action, XMLQuestionInstance xmlqic, WSQuestionInstance wsqic) throws IOException {
		QuestionGroupBase qgb = QuestionFactory.fromText((xmlqic!=null ? xmlqic : xmlqi).getQuestion().getXml());
		if(qgb.getQuestionType()==SupportedXMLQuestions.ABCSINGLE) {
			if(action==SS_ACTION.SHOW_FOR_SOLVING || action==SS_ACTION.SHOW_FOR_INSPECT || action==SS_ACTION.SHOW_FOR_VERIFY) {
				return renderForSolvingXMLABCS(req,resp,variables,apctx,dao,user,xmlqic!=null ? xmlqic:xmlqi,wsqic!=null ? wsqic : wsqi,(AbcSingleQuestionGroup)qgb, showEvalButton, action, xmlqi, wsqi);
			} else if(action==SS_ACTION.SAVE || action==SS_ACTION.SAVE_EVAL) {
				return saveForSolvingXMLABCS(req,resp,variables,apctx,dao,user,xmlqi,wsqi,(AbcSingleQuestionGroup)qgb, showEvalButton, action);
			} else if(action==SS_ACTION.SAVE_VERIFY) {
				return saveVerifyForSolvingXMLABCS(req,resp,variables,dao,user,xmlqi,wsqi);
			} else {
				return null;
			}
		} else if(qgb.getQuestionType()==SupportedXMLQuestions.TEXTUAL) {
			if(action==SS_ACTION.SHOW_FOR_SOLVING || action==SS_ACTION.SHOW_FOR_INSPECT || action==SS_ACTION.SHOW_FOR_VERIFY) {
				return renderForSolvingXMLTextual(req,resp,variables,apctx,dao,user,xmlqic!=null ? xmlqic:xmlqi,wsqic!=null ? wsqic : wsqi,(TextQuestionGroup)qgb, showEvalButton, action, xmlqi, wsqi);
			} else if(action==SS_ACTION.SAVE || action==SS_ACTION.SAVE_EVAL) {
				return saveForSolvingXMLTextual(req,resp,variables,apctx,dao,user,xmlqi,wsqi,(TextQuestionGroup)qgb, showEvalButton, action);
			} else if(action==SS_ACTION.SAVE_VERIFY) {
				return saveVerifyForSolvingXMLTextual(req,resp,variables,dao,user,xmlqi,wsqi);
			} else {
				return null;
			}
		} else {
			throw new RuntimeException("Unsupported question type.");
		}
	}

	private Object renderForSolvingJS(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, JSQuestionInstance jsqi, WSQuestionInstance wsqi, boolean showEvalButton, SS_ACTION action, JSQuestionInstance jsqic, WSQuestionInstance wsqic) throws IOException {
		if(action==SS_ACTION.SHOW_FOR_SOLVING || action==SS_ACTION.SHOW_FOR_INSPECT || action==SS_ACTION.SHOW_FOR_VERIFY) {
			return renderForSolvingJS(req,resp,variables,apctx,dao,user,jsqi,wsqi,jsqic,wsqic,showEvalButton,action);
		} else if(action==SS_ACTION.SAVE || action==SS_ACTION.SAVE_EVAL) {
			return saveForSolvingJS(req,resp,variables,apctx,dao,user,jsqi,wsqi,showEvalButton,action);
		} else if(action==SS_ACTION.SAVE_VERIFY) {
			return saveVerifyForSolvingJS(req,resp,variables,dao,user,jsqi,wsqi);
		} else {
			return null;
		}
	}

	private Object renderForSolvingJS(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables,
			AppRequestContext apctx, DAO dao, User user, JSQuestionInstance jsqi, WSQuestionInstance wsqi,
			JSQuestionInstance jsqic, WSQuestionInstance wsqic, boolean showEvalButton, SS_ACTION action) {
		return QTester.renderForSolvingJS(req, resp, variables, apctx, dao, user, jsqi, wsqi, jsqic, wsqic, showEvalButton, action);
	}

	private Object saveForSolvingJS(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, JSQuestionInstance jsqi, WSQuestionInstance wsqi, boolean showEvalButton, SS_ACTION action) throws IOException {
		QTester.saveJSQuestionInstanceState(req, dao, jsqi);

		if(action==SS_ACTION.SAVE) {
			if(wsqi==null) {
				resp.sendRedirect(req.getContextPath()+"/ml/questions/"+apctx.getContextData()+"/details/"+jsqi.getQuestion().getId());
				return null;
			}
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
			return null;
		}
		
		if(action==SS_ACTION.SAVE_EVAL) {
			QTester.evaluateJSQuestionInstanceState(req, dao, jsqi);
			if(wsqi==null) {
				resp.sendRedirect(req.getContextPath()+"/ml/questions/"+apctx.getContextData()+"/details/"+jsqi.getQuestion().getId());
				return null;
			}
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
			return null;
		}

		// Inače:
		return null;
	}
	
	private Object saveVerifyForSolvingJS(HttpServletRequest req, HttpServletResponse resp,
			Map<String, String> variables, DAO dao, User user, JSQuestionInstance xmlqi, WSQuestionInstance wsqi) throws IOException {
		String data = StringUtil.trim(req.getParameter("data"));
		
		Long index = null;
		try {
			if(data!=null) index = Long.valueOf(data);
		} catch(Exception ex) {
		}

		wsqi.setControllerCorrect(null);
		if(index==null) {
			wsqi.setControlResult(null);
		} else if(index==0) {
			wsqi.setControlResult(QuestionControlResult.CORRECT);
		} else if(index==1) {
			wsqi.setControlResult(QuestionControlResult.INCORRECT);
		} else if(index==2) {
			wsqi.setControlResult(QuestionControlResult.NOTSOLVED);
		} else {
			// nešto ne štima!
			return null;
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
		return null;
	}

	private Object saveForSolvingXMLABCS(HttpServletRequest req, HttpServletResponse resp,
			Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, XMLQuestionInstance xmlqi, WSQuestionInstance wsqi,
			AbcSingleQuestionGroup qg, boolean showEvalButton, SS_ACTION action) throws IOException {
		String data = StringUtil.trim(req.getParameter("data"));
		
		Long index = null;
		
		try {
			if(data!=null) index = Long.valueOf(data);
		} catch(Exception ex) {
		}
		
		xmlqi.setuData(index==null || index < 0 ? null : index.toString());

		if(action==SS_ACTION.SAVE) {
			if(wsqi==null) {
				resp.sendRedirect(req.getContextPath()+"/ml/questions/"+apctx.getContextData()+"/details/"+xmlqi.getQuestion().getId());
				return null;
			}
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
			return null;
		}
		
		if(action==SS_ACTION.SAVE_EVAL) {
			WorkspaceController.evalXMLABCSQuestion(dao, user, xmlqi.getQuestion(), qg, xmlqi);
			if(wsqi==null) {
				resp.sendRedirect(req.getContextPath()+"/ml/questions/"+apctx.getContextData()+"/details/"+xmlqi.getQuestion().getId());
				return null;
			}
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
			return null;
		}

		// Inače:
		return null;
	}
	
	private Object saveVerifyForSolvingXMLABCS(HttpServletRequest req, HttpServletResponse resp,
			Map<String, String> variables, DAO dao, User user, XMLQuestionInstance xmlqi, WSQuestionInstance wsqi) throws IOException {
		String data = StringUtil.trim(req.getParameter("data"));
		
		Long index = null;
		
		try {
			if(data!=null) index = Long.valueOf(data);
		} catch(Exception ex) {
		}

		wsqi.setControllerCorrect(null);
		if(index==null) {
			wsqi.setControlResult(null);
		} else if(index==0) {
			wsqi.setControlResult(QuestionControlResult.CORRECT);
		} else if(index==1) {
			wsqi.setControlResult(QuestionControlResult.INCORRECT);
		} else if(index==2) {
			wsqi.setControlResult(QuestionControlResult.NOTSOLVED);
		} else {
			// nešto ne štima!
			return null;
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
		return null;
	}
	
	private Object renderForSolvingXMLABCS(HttpServletRequest req, HttpServletResponse resp,
			Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, XMLQuestionInstance xmlqi, WSQuestionInstance wsqi,
			AbcSingleQuestionGroup qg, boolean showEvalButton, SS_ACTION action, XMLQuestionInstance xmlqiOrig, WSQuestionInstance wsqiOrig) {
		//String[] data = xmlqi.getqData().split("#");
		String[] sordering = xmlqi.getqData().split(",");
		int[] indexes = new int[sordering.length];
		for(int i = 0; i < sordering.length; i++) {
			indexes[i] = Integer.parseInt(sordering[i]);
		}
		String conf = xmlqi.getConfigName();
		AbcSingleQuestion q = qg.getForID(conf);
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		String qtext = q.getText().getTranslations().containsKey(lang) ? q.getText().getTranslation(lang) : "????";
		req.setAttribute("qtext", qtext);
		List<String> options = new ArrayList<>();
		for(int i = 0, n = q.getAnswers().size(); i < n; i++) {
			AbcSingleAnswer a = q.getAnswers().get(indexes[i]);
			options.add(a.getText().getTranslations().containsKey(lang) ? a.getText().getTranslation(lang) : "????");
		}
		req.setAttribute("qoptions", options);
		
		StringBuilder sb = new StringBuilder(1024);
		sb.append("<html>\n<head>\n");
		sb.append("<base href=\"").append(req.getRequestURL().append("/").toString()).append("\">\n");
		
		sb.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
		sb.append("<link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
		sb.append("<script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>");
		sb.append("<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
		sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/common.css\" data-th-href=\"@{/css/common.css}\">");
		sb.append("<link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css\">");
		sb.append("<script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js\"></script>");
		sb.append("<script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/vhdl.min.js\"></script>");
		sb.append("<script type=\"text/javascript\" async src=\"//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML\"></script>");
		sb.append("<script type=\"text/javascript\">hljs.initHighlightingOnLoad();</script>");
		sb.append("<link rel=\"stylesheet\" href=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/base/jquery-ui.css\">");
		sb.append("<script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js\"></script>");
		
		sb.append("<style> DIV.qsel {background-color: green; border: 1px solid black;} DIV.qnosel {background-color: white; border: 1px dotted #CCCCCC;} </style>\n</head>\n<body>");
		if(action==SS_ACTION.SHOW_FOR_INSPECT) {
			if(xmlqi.getStatus()==QIStatus.ERROR) {
				sb.append("<div>Vrednovanje zadatka rezultiralo je pogreškom.<br><hr></div>\n");
			}
			if(xmlqi.isSolved()) {
				sb.append("<div>Zadatak ste riješili. Mjera točnosti rješenja je: ").append(xmlqi.getCorrectness()).append("/1.0.");
			} else {
				sb.append("<div>Zadatak niste riješili.");
			}
			if((xmlqi.isSolved() && xmlqi.getCorrectness()<1) || !xmlqi.isSolved()) {
				int correct = -1;
				for(int i = 0, n = q.getAnswers().size(); i<n; i++) {
					if(q.getAnswers().get(indexes[i]).isCorrect()) {
						correct = i; break;
					}
				}
				sb.append("Točan odgovor je ").append((char)('A'+correct)).append(".");
			}
			sb.append("<br><hr></div>\n");
		}

		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			if(xmlqi.getStatus()==QIStatus.ERROR) {
				sb.append("<div>Vrednovanje zadatka je rezultiralo pogreškom. Označite da zadatak nije rješavan.<br><hr></div>\n");
			} else if(xmlqi.getStatus()!=QIStatus.EVALUATED) {
				return null;
			}
		}
		
		sb.append("<div>").append(qtext).append("</div>\n");
		sb.append("<ol type=\"A\">\n");
		for(String o : options) {
			sb.append("<li>").append(o).append("</li>\n");
		}
		sb.append("</ol>\n");

		Optional<Long> selIndex = WorkspaceController.stringToLong(StringUtil.trim(xmlqi.getuData()));
		int index = selIndex!=null && selIndex.isPresent() ? selIndex.get().intValue() : -1;
		int index2 = -1;
		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			index2 = wsqiOrig.getControlResult()==null ? -1 : (wsqiOrig.getControlResult()==QuestionControlResult.CORRECT ? 0 : (wsqiOrig.getControlResult()==QuestionControlResult.INCORRECT ? 1 : 2));
			sb.append("\n<script>\nvar selectedOptionIndex = ").append(index2).append(";\nfunction qtoggle(e,i) {\n   if(i==selectedOptionIndex) {\n      selectedOptionIndex=-1; qRemoveClass(e,'qsel'); qAddClass(e,'qnosel');\n   } else if(selectedOptionIndex==-1) {\n      selectedOptionIndex=i; qRemoveClass(e,'qnosel'); qAddClass(e,'qsel');\n   } else {\n      var e2=document.getElementById('qid'+selectedOptionIndex); qRemoveClass(e2,'qsel'); qAddClass(e2,'qnosel'); selectedOptionIndex=i; qRemoveClass(e,'qnosel'); qAddClass(e,'qsel');\n   }\n   console.log('selektirana opcija je: '+selectedOptionIndex);\n}\nfunction qAddClass(e,c) {\n    e.className += ' '+c;\n}\nfunction qRemoveClass(e,c) {\n   e.className = c=='qsel' ? e.className.replace( /(?:^|\\s)qsel(?!\\S)/g , '' ) : e.className.replace( /(?:^|\\s)qnosel(?!\\S)/g , '' );\n}</script>");
		} else {
			sb.append("\n<script>\nvar selectedOptionIndex = ").append(index).append(";\nfunction qtoggle(e,i) {\n   if(i==selectedOptionIndex) {\n      selectedOptionIndex=-1; qRemoveClass(e,'qsel'); qAddClass(e,'qnosel');\n   } else if(selectedOptionIndex==-1) {\n      selectedOptionIndex=i; qRemoveClass(e,'qnosel'); qAddClass(e,'qsel');\n   } else {\n      var e2=document.getElementById('qid'+selectedOptionIndex); qRemoveClass(e2,'qsel'); qAddClass(e2,'qnosel'); selectedOptionIndex=i; qRemoveClass(e,'qnosel'); qAddClass(e,'qsel');\n   }\n   console.log('selektirana opcija je: '+selectedOptionIndex);\n}\nfunction qAddClass(e,c) {\n    e.className += ' '+c;\n}\nfunction qRemoveClass(e,c) {\n   e.className = c=='qsel' ? e.className.replace( /(?:^|\\s)qsel(?!\\S)/g , '' ) : e.className.replace( /(?:^|\\s)qnosel(?!\\S)/g , '' );\n}</script>");
		}

		for(int i = 0, n = options.size(); i < n; i++) {
			sb.append("<div");
			if(action==SS_ACTION.SHOW_FOR_SOLVING) {
				sb.append(" id=\"qid").append(i).append("\"");
				sb.append(" onclick=\"qtoggle(this,").append(i).append(");\"");
			}
			sb.append(" style=\"text-align: center; font-size: 2em; padding: 5px; width: 100%; margin-top: 5px; margin-bottom: 5px;\" class=\"").append(i==index ? "qsel":"qnosel").append("\">").append((char)('A'+i)).append("</div>\n");
		}

		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			sb.append("<div><hr></div>");
			sb.append("<div>");
			if(wsqi.getUser()!=null) {
				sb.append("Pred Vama je zadatak koji je dobio korisnik ");
				WebUtil.htmlEncode(sb, wsqi.getUser().getFirstName());
				sb.append(" ");
				WebUtil.htmlEncode(sb, wsqi.getUser().getLastName());
				sb.append(". Provjerite je li njegovo rješenje točno. Odluku unesite putem gumbi koji su prikazani ispod.");
			} else {
				sb.append("Pred Vama je zadatak koji je dobila grupa ");
				WebUtil.htmlEncode(sb, wsqi.getGroup().getName());
				sb.append(". Provjerite je li njihovo rješenje točno. Odluku unesite putem gumbi koji su prikazani ispod.");
			}
			sb.append("</div>");
			String[] opcije = {"Rješenje je točno", "Rješenje nije točno", "Zadatak nije riješen"};
			for(int i = 0; i < opcije.length; i++) {
				sb.append("<div");
				sb.append(" id=\"qid").append(i).append("\"");
				sb.append(" onclick=\"qtoggle(this,").append(i).append(");\"");
				sb.append(" style=\"text-align: center; font-size: 2em; padding: 5px; width: 100%; margin-top: 5px; margin-bottom: 5px;\" class=\"").append(i==index2 ? "qsel":"qnosel").append("\">").append(opcije[i]).append("</div>\n");
			}
		}
		if(action==SS_ACTION.SHOW_FOR_INSPECT && wsqi!=null && wsqi.getToControl() != null) {
			sb.append("<hr>");
			sb.append("<div style=\"text-align: center;\">");
			sb.append("Dodatno, trebali ste provjeriti je li rješenje studenta koje Vam je sustav prikazao točno, netočno ili nerješavano. ");
			if(wsqi.getControlResult()==null) {
				sb.append("Na to niste odgovorili. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.CORRECT) {
				sb.append("Odgovorili ste da je rješenje točno. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.INCORRECT) {
				sb.append("Odgovorili ste da je rješenje netočno. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.NOTSOLVED) {
				sb.append("Odgovorili ste da zadatak nije riješen. ");
			};
			if(wsqi.getControllerCorrect()==null) {
				sb.append("Je li Vaš odgovor točan, u ovom trenutku nije poznato.");
			} else if(wsqi.getControllerCorrect().booleanValue()==true) {
				sb.append("I to je točan odgovor.");
			} else if(wsqi.getControllerCorrect().booleanValue()==false) {
				sb.append("To nije točan odgovor.");
			} 
			sb.append("</div>");
			sb.append("<hr>");
		}
		sb.append("<div style=\"text-align: center; padding-top: 10px; padding-bottom: 20px;\">");
		if(action==SS_ACTION.SHOW_FOR_SOLVING) {
			if(!showEvalButton) sb.append("<button onclick=\"document.getElementById('qaction').value='save';     document.getElementById('qhid').value=selectedOptionIndex; document.getElementById('qform1').submit(); return false;\" class='btn btn-warning'>Pohrani</button>"); 
			if(showEvalButton)  sb.append("<button onclick=\"document.getElementById('qaction').value='saveeval'; document.getElementById('qhid').value=selectedOptionIndex; document.getElementById('qform1').submit(); return false;\" class='btn btn-warning'>Pohrani i ocijeni</button>"); 
		} else if(action==SS_ACTION.SHOW_FOR_INSPECT) {
			sb.append("<button onclick=\"document.getElementById('qform1').submit(); return false;\" class='btn btn-warning'>Povratak</button>");
		} else if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			sb.append("<button onclick=\"document.getElementById('qaction').value='saveverify'; document.getElementById('qhid').value=selectedOptionIndex; document.getElementById('qform1').submit(); return false;\" class='btn btn-warning'>Pohrani i ocijeni</button>"); 
		}
		sb.append("</div>");
		if(action==SS_ACTION.SHOW_FOR_SOLVING || action==SS_ACTION.SHOW_FOR_VERIFY) {
			sb.append("\n<form action=\"").append(req.getContextPath()).append("/ml/questions/").append(apctx.getContextData()).append("/solving/xmli-").append(action==SS_ACTION.SHOW_FOR_VERIFY ? xmlqiOrig.getId() : xmlqi.getId()).append("\" method=\"post\" id=\"qform1\"><input id=\"qhid\" type=\"hidden\" name=\"data\" value=\"\"><input id=\"qaction\" name=\"action\" type=\"hidden\" value=\"\"></form>");
		} else if(action==SS_ACTION.SHOW_FOR_INSPECT) {
			if(wsqi==null) {
				sb.append("\n<form action=\"").append(req.getContextPath()).append("/ml/questions/").append(apctx.getContextData()).append("/details/").append(xmlqi.getQuestion().getId()).append("\" method=\"get\" id=\"qform1\"></form>");
			} else {
				sb.append("\n<form action=\"").append(req.getContextPath()).append("/ml/workspaces/").append(wsqi.getDefinition().getWorkspace().getId()).append("/room/").append(wsqi.getDefinition().getSubpage().getNumID()).append("\" method=\"get\" id=\"qform1\"><input type='hidden' name='op' value='idle'></form>");
			}
		}

		sb.append("</body></html>");
		
		return new StreamRetVal(sb.toString(), StandardCharsets.UTF_8, "text/html; charset=utf-8", null);
	}

	private Object saveForSolvingXMLTextual(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, XMLQuestionInstance xmlqi, WSQuestionInstance wsqi, TextQuestionGroup qg, boolean showEvalButton, SS_ACTION action) throws IOException {
		String data = StringUtil.trim(req.getParameter("data"));
		
		xmlqi.setuData(data);

		if(action==SS_ACTION.SAVE) {
			if(wsqi==null) {
				resp.sendRedirect(req.getContextPath()+"/ml/questions/"+apctx.getContextData()+"/details/"+xmlqi.getQuestion().getId());
				return null;
			}
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
			return null;
		}
		
		if(action==SS_ACTION.SAVE_EVAL) {
			WorkspaceController.evalXMLTextualQuestion(dao, user, xmlqi.getQuestion(), qg, xmlqi);
			if(wsqi==null) {
				resp.sendRedirect(req.getContextPath()+"/ml/questions/"+apctx.getContextData()+"/details/"+xmlqi.getQuestion().getId());
				return null;
			}
			resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
			return null;
		}

		// Inače:
		return null;
	}

	private Object saveVerifyForSolvingXMLTextual(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables, DAO dao, User user, XMLQuestionInstance xmlqi, WSQuestionInstance wsqi) throws IOException {
		String data = StringUtil.trim(req.getParameter("data"));
		
		Long index = null;
		
		try {
			if(data!=null) index = Long.valueOf(data);
		} catch(Exception ex) {
		}

		wsqi.setControllerCorrect(null);
		if(index==null) {
			wsqi.setControlResult(null);
		} else if(index==0) {
			wsqi.setControlResult(QuestionControlResult.CORRECT);
		} else if(index==1) {
			wsqi.setControlResult(QuestionControlResult.INCORRECT);
		} else if(index==2) {
			wsqi.setControlResult(QuestionControlResult.NOTSOLVED);
		} else {
			// nešto ne štima!
			return null;
		}
		
		resp.sendRedirect(req.getContextPath()+"/ml/workspaces/"+wsqi.getDefinition().getWorkspace().getId()+"/room/"+wsqi.getDefinition().getSubpage().getNumID()+"?op=idle");
		return null;
	}

	private Object renderForSolvingXMLTextual(HttpServletRequest req, HttpServletResponse resp,
			Map<String, String> variables, AppRequestContext apctx, DAO dao, User user, XMLQuestionInstance xmlqi, WSQuestionInstance wsqi,
			TextQuestionGroup qg, boolean showEvalButton, SS_ACTION action, XMLQuestionInstance xmlqiOrig, WSQuestionInstance wsqiOrig) throws IOException {
		String conf = xmlqi.getConfigName();
		TextQuestion q = qg.getForID(conf);
		String lang = MLCurrentContext.getLocale().getLanguage();
		
		String qtext = q.getText().getTranslations().containsKey(lang) ? q.getText().getTranslation(lang) : "????";
		req.setAttribute("qtext", qtext);
		
		StringBuilder sb = new StringBuilder(1024);
		sb.append("<html>\n<head>\n");
		sb.append("<base href=\"").append(req.getRequestURL().append("/").toString()).append("\">\n");
		
		sb.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
		sb.append("<link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
		sb.append("<script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>");
		sb.append("<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
		sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/common.css\" data-th-href=\"@{/css/common.css}\">");
		sb.append("<link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css\">");
		sb.append("<script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js\"></script>");
		sb.append("<script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/vhdl.min.js\"></script>");
		sb.append("<script type=\"text/javascript\" async src=\"//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML\"></script>");
		sb.append("<script type=\"text/javascript\">hljs.initHighlightingOnLoad();</script>");
		sb.append("<link rel=\"stylesheet\" href=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/base/jquery-ui.css\">");
		sb.append("<script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js\"></script>");
		
		sb.append("<style> DIV.qsel {background-color: green; border: 1px solid black;} DIV.qnosel {background-color: white; border: 1px dotted #CCCCCC;} </style>\n</head>\n<body>");
		if(action==SS_ACTION.SHOW_FOR_INSPECT) {
			if(xmlqi.getStatus()==QIStatus.ERROR) {
				sb.append("<div>Vrednovanje zadatka rezultiralo je pogreškom.<br><hr></div>\n");
			}
			if(xmlqi.isSolved()) {
				sb.append("<div>Zadatak ste riješili. Mjera točnosti rješenja je: ").append(xmlqi.getCorrectness()).append("/1.0.");
			} else {
				sb.append("<div>Zadatak niste riješili.");
			}
			if((xmlqi.isSolved() && xmlqi.getCorrectness()<1) || !xmlqi.isSolved()) {
				sb.append("Točan odgovor je ");
				String str = q.getAnswers().get(0).getText().getTranslations().getOrDefault(lang, new I18NText("???", "hr")).getText();
				WebUtil.htmlEncode(sb, str);
				sb.append(".");
			}
			sb.append("<br><hr></div>\n");
		}

		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			if(xmlqi.getStatus()==QIStatus.ERROR) {
				sb.append("<div>Vrednovanje zadatka je rezultiralo pogreškom. Označite da zadatak nije rješavan.<br><hr></div>\n");
			} else if(xmlqi.getStatus()!=QIStatus.EVALUATED) {
				return null;
			}
		}
		
		sb.append("<div>").append(qtext).append("</div>\n");
		sb.append("<div>Odgovor: ");
		sb.append("<input name=\"qfanswer\" id=\"qfanswer\" type=\"text\" size=\"80\" value=\"");
		WebUtil.htmlAttribEncode(sb, StringUtil.trim(xmlqi.getuData()));
		sb.append("\"");
		if(action!=SS_ACTION.SHOW_FOR_SOLVING) {
			sb.append(" readonly");
		}
		sb.append(">");
		sb.append("</div>");
		
		int index2 = -1;
		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			index2 = wsqiOrig.getControlResult()==null ? -1 : (wsqiOrig.getControlResult()==QuestionControlResult.CORRECT ? 0 : (wsqiOrig.getControlResult()==QuestionControlResult.INCORRECT ? 1 : 2));
			sb.append("\n<script>\nvar selectedOptionIndex = ").append(index2).append(";\nfunction qtoggle(e,i) {\n   if(i==selectedOptionIndex) {\n      selectedOptionIndex=-1; qRemoveClass(e,'qsel'); qAddClass(e,'qnosel');\n   } else if(selectedOptionIndex==-1) {\n      selectedOptionIndex=i; qRemoveClass(e,'qnosel'); qAddClass(e,'qsel');\n   } else {\n      var e2=document.getElementById('qid'+selectedOptionIndex); qRemoveClass(e2,'qsel'); qAddClass(e2,'qnosel'); selectedOptionIndex=i; qRemoveClass(e,'qnosel'); qAddClass(e,'qsel');\n   }\n   console.log('selektirana opcija je: '+selectedOptionIndex);\n}\nfunction qAddClass(e,c) {\n    e.className += ' '+c;\n}\nfunction qRemoveClass(e,c) {\n   e.className = c=='qsel' ? e.className.replace( /(?:^|\\s)qsel(?!\\S)/g , '' ) : e.className.replace( /(?:^|\\s)qnosel(?!\\S)/g , '' );\n}</script>");
		}

		if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			sb.append("<div><hr></div>");
			sb.append("<div>");
			if(wsqi.getUser()!=null) {
				sb.append("Pred Vama je zadatak koji je dobio korisnik ");
				WebUtil.htmlEncode(sb, wsqi.getUser().getFirstName());
				sb.append(" ");
				WebUtil.htmlEncode(sb, wsqi.getUser().getLastName());
				sb.append(". Provjerite je li njegovo rješenje točno. Odluku unesite putem gumbi koji su prikazani ispod.");
			} else {
				sb.append("Pred Vama je zadatak koji je dobila grupa ");
				WebUtil.htmlEncode(sb, wsqi.getGroup().getName());
				sb.append(". Provjerite je li njihovo rješenje točno. Odluku unesite putem gumbi koji su prikazani ispod.");
			}
			sb.append("</div>");
			String[] opcije = {"Rješenje je točno", "Rješenje nije točno", "Zadatak nije riješen"};
			for(int i = 0; i < opcije.length; i++) {
				sb.append("<div");
				sb.append(" id=\"qid").append(i).append("\"");
				sb.append(" onclick=\"qtoggle(this,").append(i).append(");\"");
				sb.append(" style=\"text-align: center; font-size: 2em; padding: 5px; width: 100%; margin-top: 5px; margin-bottom: 5px;\" class=\"").append(i==index2 ? "qsel":"qnosel").append("\">").append(opcije[i]).append("</div>\n");
			}
		}
		if(action==SS_ACTION.SHOW_FOR_INSPECT && wsqi!=null && wsqi.getToControl() != null) {
			sb.append("<hr>");
			sb.append("<div style=\"text-align: center;\">");
			sb.append("Dodatno, trebali ste provjeriti je li rješenje studenta koje Vam je sustav prikazao točno, netočno ili nerješavano. ");
			if(wsqi.getControlResult()==null) {
				sb.append("Na to niste odgovorili. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.CORRECT) {
				sb.append("Odgovorili ste da je rješenje točno. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.INCORRECT) {
				sb.append("Odgovorili ste da je rješenje netočno. ");
			} else if(wsqi.getControlResult()==QuestionControlResult.NOTSOLVED) {
				sb.append("Odgovorili ste da zadatak nije riješen. ");
			};
			if(wsqi.getControllerCorrect()==null) {
				sb.append("Je li Vaš odgovor točan, u ovom trenutku nije poznato.");
			} else if(wsqi.getControllerCorrect().booleanValue()==true) {
				sb.append("I to je točan odgovor.");
			} else if(wsqi.getControllerCorrect().booleanValue()==false) {
				sb.append("To nije točan odgovor.");
			} 
			sb.append("</div>");
			sb.append("<hr>");
		}
		sb.append("<div style=\"text-align: center; padding-top: 10px; padding-bottom: 20px;\">");
		if(action==SS_ACTION.SHOW_FOR_SOLVING) {
			if(!showEvalButton) sb.append("<button onclick=\"document.getElementById('qaction').value='save';     document.getElementById('qhid').value=document.getElementById('qfanswer').value; document.getElementById('qform1').submit(); return false;\" class='btn btn-warning'>Pohrani</button>"); 
			if(showEvalButton)  sb.append("<button onclick=\"document.getElementById('qaction').value='saveeval'; document.getElementById('qhid').value=document.getElementById('qfanswer').value; document.getElementById('qform1').submit(); return false;\" class='btn btn-warning'>Pohrani i ocijeni</button>"); 
		} else if(action==SS_ACTION.SHOW_FOR_INSPECT) {
			sb.append("<button onclick=\"document.getElementById('qform1').submit(); return false;\" class='btn btn-warning'>Povratak</button>");
		} else if(action==SS_ACTION.SHOW_FOR_VERIFY) {
			sb.append("<button onclick=\"document.getElementById('qaction').value='saveverify'; document.getElementById('qhid').value=selectedOptionIndex; document.getElementById('qform1').submit(); return false;\" class='btn btn-warning'>Pohrani i ocijeni</button>"); 
		}
		sb.append("</div>");
		if(action==SS_ACTION.SHOW_FOR_SOLVING || action==SS_ACTION.SHOW_FOR_VERIFY) {
			sb.append("\n<form action=\"").append(req.getContextPath()).append("/ml/questions/").append(apctx.getContextData()).append("/solving/xmli-").append(action==SS_ACTION.SHOW_FOR_VERIFY ? xmlqiOrig.getId() : xmlqi.getId()).append("\" method=\"post\" id=\"qform1\"><input id=\"qhid\" type=\"hidden\" name=\"data\" value=\"\"><input id=\"qaction\" name=\"action\" type=\"hidden\" value=\"\"></form>");
		} else if(action==SS_ACTION.SHOW_FOR_INSPECT) {
			if(wsqi==null) {
				sb.append("\n<form action=\"").append(req.getContextPath()).append("/ml/questions/").append(apctx.getContextData()).append("/details/").append(xmlqi.getQuestion().getId()).append("\" method=\"get\" id=\"qform1\"></form>");
			} else {
				sb.append("\n<form action=\"").append(req.getContextPath()).append("/ml/workspaces/").append(wsqi.getDefinition().getWorkspace().getId()).append("/room/").append(wsqi.getDefinition().getSubpage().getNumID()).append("\" method=\"get\" id=\"qform1\"><input type='hidden' name='op' value='idle'></form>");
			}
		}

		sb.append("</body></html>");
		
		return new StreamRetVal(sb.toString(), StandardCharsets.UTF_8, "text/html; charset=utf-8", null);
	}


}
