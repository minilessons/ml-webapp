package hr.fer.zemris.minilessons.web.retvals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import hr.fer.zemris.minilessons.web.WebUtil;

/**
 * This class models controller response which ask to pass content which is already generated
 * and is available as input-stream. When content is streamed back to client, it will be automatically
 * closed.
 * 
 * @author marcupic
 */
public class StreamRetVal {
	
	/**
	 * Content length.
	 */
	private long length;
	/**
	 * File name to send back to client.
	 */
	private String name;
	/**
	 * Mime-type for content.
	 */
	private String contentType;
	/**
	 * Input-stream containing content.
	 */
	private InputStream stream;
	/**
	 * What status to send to client. If not set, status will be 200.
	 */
	private Integer status = null;
	/**
	 * What status message to send to client. If not set, status text will be OK.
	 */
	private String statusMessage = null;
	/**
	 * ETag for caching purposes.
	 */
	private String etag = null;

	/**
	 * Constructs {@link StreamRetVal} from byte array.
	 * 
	 * @param data data to send back; can be <code>null</code>
	 * @param contentType mime-type to send with data; must not be <code>null</code>
	 * @param name file name to send with data; can be <code>null</code>
	 */
	public StreamRetVal(byte[] data, String contentType, String name) {
		this.stream = new ByteArrayInputStream(data==null ? new byte[0] : data);
		this.contentType = Objects.requireNonNull(contentType,"contentType must not be null.");
		this.length = data==null ? 0 : data.length;
		this.name = name;
	}

	/**
	 * Constructs {@link StreamRetVal} from string data.
	 * 
	 * @param data string content to return; can be <code>null</code>
	 * @param charset charset to use for string conversion into bytes; must not be <code>null</code>
	 * @param contentType mime-type to send with data
	 * @param name file name to send with data
	 */
	public StreamRetVal(String data, Charset charset, String contentType, String name) {
		byte[] bytes = data==null ? new byte[0] : data.getBytes(charset);
		this.stream = new ByteArrayInputStream(bytes);
		this.contentType = Objects.requireNonNull(contentType,"contentType must not be null.");
		this.length = bytes.length;
		this.name = name;
	}

	/**
	 * Constructs {@link StreamRetVal} from input stream data.
	 * 
	 * @param stream input stream containing data
	 * @param contentType mime-type to send with data
	 * @param length length of data; can be set to -1 if unknown
	 * @param name file name to send with data
	 */
	public StreamRetVal(InputStream stream, String contentType, long length, String name) {
		super();
		this.stream = stream;
		this.contentType = Objects.requireNonNull(contentType,"contentType must not be null.");
		this.length = length;
		this.name = name;
	}

	/**
	 * Constructs {@link StreamRetVal} from path to file containing data.
	 * 
	 * @param path path to file with data
	 * @throws IOException when file size could not be determined or file could not be opened
	 */
	public StreamRetVal(Path path) throws IOException {
		this.name = path.getFileName().toString();
		this.contentType = WebUtil.guessMime(this.name);
		this.length = Files.size(path);
		this.stream = Files.newInputStream(path);
	}

	/**
	 * Constructs {@link StreamRetVal} from path to file containing data.
	 * 
	 * @param path path to file with data
	 * @param etag etag value for resource
	 * @throws IOException when file size could not be determined or file could not be opened
	 */
	public StreamRetVal(Path path, String etag) throws IOException {
		this.name = path.getFileName().toString();
		this.etag = etag;
		this.contentType = WebUtil.guessMime(this.name);
		this.length = Files.size(path);
		this.stream = Files.newInputStream(path);
	}

	/**
	 * Constructs {@link StreamRetVal} with given status and status text and no data.
	 * 
	 * @param status status to send
	 * @param statusMessage status text to send
	 */
	public StreamRetVal(Integer status, String statusMessage) {
		super();
		this.status = status;
		this.statusMessage = statusMessage;
	}

	/**
	 * Getter for input stream. If {@link StreamRetVal#length} is non-negative, only that much
	 * bytes will be sent to client.
	 * 
	 * @return input stream
	 */
	public InputStream getStream() {
		return stream;
	}
	
	/**
	 * Getter for content type
	 * 
	 * @return content type
	 */
	public String getContentType() {
		return contentType;
	}
	
	/**
	 * Getter for data length. Can be -1 to indicate that this information is not available.
	 * 
	 * @return length
	 */
	public long getLength() {
		return length;
	}
	
	/**
	 * Getter for file name.
	 * 
	 * @return file name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Getter for status.
	 * 
	 * @return status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Getter for status text.
	 * 
	 * @return status text
	 */
	public String getStatusMessage() {
		return statusMessage;
	}
	
	/**
	 * Getter for etag value; can be <code>null</code> if etag value was not set.
	 * 
	 * @return etag
	 */
	public String getEtag() {
		return etag;
	}
}
