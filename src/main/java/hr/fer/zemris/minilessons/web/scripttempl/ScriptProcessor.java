package hr.fer.zemris.minilessons.web.scripttempl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class ScriptProcessor {

	public static String loadScript(Path jsfilePath, Map<String, String> scriptLoaderMap) throws IOException {
		String script = new String(Files.readAllBytes(jsfilePath), StandardCharsets.UTF_8);
		StringBuilder sb = null;
		int pos = 0;
		while(true) {
			int cur = script.indexOf("/*@#", pos);
			if(cur==-1) {
				if(pos==0) return script; // ako uopće nema posebnih znakova, vrati original...
				sb.append(script.substring(pos));
				break;
			}
			int cur2 = script.indexOf("#(", cur+4);
			if(cur2==-1) {
				throw new IOException("Invalid syntax of SSI-command found in script " + jsfilePath.getFileName()+".");
			}

			String commandName = script.substring(cur+4, cur2);
						
			if(sb==null) sb = new StringBuilder();
			if(cur > pos) {
				sb.append(script.substring(pos, cur));
			}
			pos = cur2 + 2;
			cur = script.indexOf(")*/", pos);
			if(cur==-1) throw new IOException("Format skripte nije ispravan.");
			String body = script.substring(pos, cur);
			pos = cur + 3;
			String fileName = processSubstitutions(body, scriptLoaderMap);
			
			Path filePath = null;
			if(commandName.equals("import") || commandName.equals("include")) {
				filePath = jsfilePath.getParent().resolve(fileName);
			} else if(commandName.equals("common-include")) {
				filePath = jsfilePath.getParent().getParent().resolve("common").resolve(fileName);
			} else if(commandName.equals("file-include")) {
				filePath = jsfilePath.getParent().getParent().resolve("files").resolve(fileName);
			} else {
				throw new IOException("Invalid SSI-command found in script " + jsfilePath.getFileName()+": command '"+commandName+"'.");
			}
			filePath = jsfilePath.getParent().resolve(fileName);
			sb.append(new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8).trim());
		}
		return sb.toString();
	}

// TODO: test new implementation given above, and then delete this one below
//	public static String loadScript(Path jsfilePath, Map<String, String> scriptLoaderMap) throws IOException {
//		String script = new String(Files.readAllBytes(jsfilePath), StandardCharsets.UTF_8);
//		StringBuilder sb = null;
//		int pos = 0;
//		while(true) {
//			int cur = script.indexOf("/*@#import#(", pos);
//			if(cur==-1) {
//				if(pos==0) return script; // ako uopće nema posebnih znakova, vrati original...
//				sb.append(script.substring(pos));
//				break;
//			}
//			if(sb==null) sb = new StringBuilder();
//			if(cur > pos) {
//				sb.append(script.substring(pos, cur));
//			}
//			pos = cur + 12;
//			cur = script.indexOf(")*/", pos);
//			if(cur==-1) throw new IOException("Format skripte nije ispravan.");
//			String body = script.substring(pos, cur);
//			pos = cur + 3;
//			String fileName = processSubstitutions(body, scriptLoaderMap);
//			Path filePath = jsfilePath.getParent().resolve(fileName);
//			sb.append(new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8).trim());
//		}
//		return sb.toString();
//	}
//
	private static String processSubstitutions(String body, Map<String, String> scriptLoaderMap) {
		StringBuilder sb = new StringBuilder();
		int pos = 0;
		while(true) {
			int cur = body.indexOf("${", pos);
			if(cur==-1) {
				sb.append(body.substring(pos));
				break;
			}
			if(cur>pos) {
				sb.append(body.substring(pos, cur));
			}
			pos = cur+2;
			cur = body.indexOf('}', pos);
			if(cur==-1) throw new RuntimeException("Invalid substitution variable declaration.");
			String varName = body.substring(pos, cur).trim();
			if(varName.length()>0 && varName.charAt(0)=='$') {
				varName = varName.substring(1);
			}
			if(!scriptLoaderMap.containsKey(varName)) {
				throw new RuntimeException("Invalid substitution variable request: '"+varName+"' is not given.");
			}
			sb.append(scriptLoaderMap.get(varName));
			pos = cur+1;
		}
		return sb.toString();
	}

}
