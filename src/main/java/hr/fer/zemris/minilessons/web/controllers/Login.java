package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.DAOUtil;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.service.AuthUserInfo;
import hr.fer.zemris.minilessons.service.AuthenticationResult;
import hr.fer.zemris.minilessons.service.AuthenticationService;
import hr.fer.zemris.minilessons.service.AuthenticationServiceProvider;
import hr.fer.zemris.minilessons.service.AuthenticationStatus;
import hr.fer.zemris.minilessons.web.WebConstants;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Implementation of login/logout functionality which generates responses in JSON format.

 * @author marcupic
 *
 */
public class Login {

	/**
	 * If username or password is not given.
	 */
	public static final int ERROR_PARAMS_MISSING = 1;
	/**
	 * If username or password is wrong.
	 */
	public static final int ERROR_INVALID = 2;
	/**
	 * If username is wrongly formatted.
	 */
	public static final int ERROR_USERNAME_FORMAT = 3;
	/**
	 * If there was error with authentication service itself.
	 */
	public static final int ERROR_INTERNAL = 4;
	/**
	 * If user is already logged in.
	 */
	public static final int ERROR_ALREADY_LOGGED_IN = 5;

	
	/**
	 * Implementation of login page.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return login page
	 */
	public TemplateRetVal showLogin(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		if(!("true".equals(req.getParameter("auto")))) {
			req.getSession().removeAttribute(WebConstants.ML_POSTLOGIN_REDIRECT);
			req.getSession().removeAttribute(WebConstants.ML_POSTLOGIN_EXPIRES);
		}
		return new TemplateRetVal("loginPage",Technology.THYMELEAF);
	}
	
	/**
	 * Implementation of login procedure.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return result object
	 */
	public StreamRetVal login(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		Long uid = (Long)req.getSession().getAttribute("currentUserID");
		if(uid!=null) {
			return error(ERROR_ALREADY_LOGGED_IN);
		}
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		if(username!=null) {
			username = username.trim();
		}
		if(password!=null) {
			password = password.trim();
		}
		
		if(username==null || username.isEmpty() || password==null || password.isEmpty()) return error(ERROR_PARAMS_MISSING);
		
		int pos = username.indexOf('\\');
		String authDomain = pos==-1 ? AuthenticationService.getDefaultAuthDomain() : username.substring(0, pos).trim();
		String authUsername = pos==-1 ? username : username.substring(pos+1).trim();

		if(authDomain.isEmpty() || username.isEmpty()) {
			return error(ERROR_USERNAME_FORMAT);
		}
		
		User user = DAOUtil.transactional(()->{
			return DAOProvider.getDao().findUserByUsername(authUsername, authDomain);
		});

		if(user!=null && user.isAccountDisabled()) {
			return error(ERROR_INVALID);
		}
		
		AuthenticationServiceProvider prov = AuthenticationService.getProvider(authDomain);

		try {
			AuthenticationResult result = prov.authenticate(user, authUsername, password, true);
			AuthenticationStatus status = result.getStatus();
			if(status==AuthenticationStatus.SUCCESSFULL) {
				if(user==null) {
					if(result.getUserInfo()==null) {
						return error(ERROR_INVALID);
					}
					user = autocreateUser(result.getUserInfo(), authUsername, authDomain);
					if(user==null) {
						return error(ERROR_INVALID);
					}
				}
				req.getSession().setAttribute("currentUserID", user.getId());
				return success(req, resp);
			} else if(status==AuthenticationStatus.INVALID) {
				return error(ERROR_INVALID);
			} else {
				return error(ERROR_INTERNAL);				
			}
		} catch(Exception ex) {
			return error(ERROR_INTERNAL);
		}
	}

	private User autocreateUser(AuthUserInfo userInfo, String authUsername, String authDomain) {
		return DAOUtil.transactional(()->{
			User u = new User();
			u.setAuthDomain(authDomain);
			u.setCreatedOn(new Date());
			u.setEmail(userInfo.getEmail());
			u.setFirstName(userInfo.getFirstName());
			u.setJmbag(userInfo.getJmbag());
			u.setLastName(userInfo.getLastName());
			u.setPasswordHash("");
			u.setUsername(authUsername);
			u.setUploadPolicy(userInfo.getUploadPolicy());
			u.setPermissions(new HashSet<>(userInfo.getPermissions()));
			DAOProvider.getDao().saveUser(u);
			return u;
		});
	}

	/**
	 * Helper method which generates result if authentication was successful.
	 * 
	 * @param req request
	 * @param resp response
	 * @return result JSON object
	 */
	private StreamRetVal success(HttpServletRequest req, HttpServletResponse resp) {
		String url = (String)req.getSession().getAttribute(WebConstants.ML_POSTLOGIN_REDIRECT);
		LocalDateTime expires = (LocalDateTime)req.getSession().getAttribute(WebConstants.ML_POSTLOGIN_EXPIRES);
		
		req.getSession().removeAttribute(WebConstants.ML_POSTLOGIN_REDIRECT);
		req.getSession().removeAttribute(WebConstants.ML_POSTLOGIN_EXPIRES);
		
		if(url!=null && url.endsWith("/ml/login")) {
			// Prevent circular redirections...
			url = null;
		}
		if(url!=null && expires!=null && LocalDateTime.now().isBefore(expires)) {
			JSONObject result = new JSONObject();
			result.put("error", 0);
			result.put("redirect", url);
			return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
		}
		// Construct /ml/home URL from /ml/login which got us here
		StringBuffer sb = req.getRequestURL();
		sb.delete(sb.length()-5, sb.length());
		sb.append("home");
		
		JSONObject result = new JSONObject();
		result.put("error", 0);
		result.put("redirect", sb.toString());
		
		return new StreamRetVal(result.toString(), StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Helper method which generates result if authentication failed.
	 * 
	 * @param errorType code of error
	 * @return result JSON object
	 */
	private StreamRetVal error(int errorType) {
		if(errorType==ERROR_INVALID || errorType==ERROR_INTERNAL || errorType==ERROR_ALREADY_LOGGED_IN) {
			try {
				Thread.sleep(2500);
			} catch (InterruptedException ignorable) {
			}
		}
		return new StreamRetVal("{\"error\": "+errorType+",\"redirect\": null}", StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Implementation of logout procedure.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return result object
	 */
	public StreamRetVal logout(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		req.getSession().invalidate();
		resp.sendRedirect(req.getContextPath()+"/ml/home");
		return null;
	}
}
