package hr.fer.zemris.minilessons.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.MinilessonStats;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.domain.model.DMinilesson;
import hr.fer.zemris.minilessons.domain.model.DUser;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Implementation of page with information about specific user.

 * @author marcupic
 */
public class UserInfo {

	/**
	 * Implementation of page.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 */
	@DAOProvided @Authenticated(redirectAllowed=true)
	public TemplateRetVal infoPage(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String userID = variables.get("uid");
		if(userID!=null) {
			userID = userID.trim();
		}
		if(userID==null || userID.isEmpty()) {
			req.setAttribute("errorMsgKey", "missingParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		long id = 0;
		try {
			id = Long.parseLong(userID);
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		User user = DAOProvider.getDao().findUserByID(id);
		if(user==null) {
			req.setAttribute("errorMsgKey", "invalidParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		DUser duser = DUser.fromUser(user, false);
		req.setAttribute("user", duser);
		
		List<DMinilesson> minilessons = new ArrayList<>();
		List<MinilessonStats> mlslist = DAOProvider.getDao().listAllVisibleMLStatsForOwner(user);
		String lang = MLCurrentContext.getLocale().getLanguage();
		mlslist.forEach(st -> minilessons.add(DMinilesson.fromDAOModel(st, lang)));
		req.setAttribute("minilessons", minilessons);
		
		return new TemplateRetVal("userinfo", Technology.THYMELEAF);
	}

}
