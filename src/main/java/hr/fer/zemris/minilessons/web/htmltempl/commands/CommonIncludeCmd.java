package hr.fer.zemris.minilessons.web.htmltempl.commands;

import java.io.IOException;

import hr.fer.zemris.minilessons.web.htmltempl.CommandEnvironment;
import hr.fer.zemris.minilessons.web.htmltempl.ContentProcessorUtil;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;

public class CommonIncludeCmd implements HtmlTemplateCommand {

	@Override
	public void execute(CommandEnvironment env) throws IOException {
		if(!ContentProcessorUtil.checkFileName(env.getCommandArgs())) {
			throw new RuntimeException("Invalid file name found: " + env.getCommandArgs());
		}
		String includedContent = env.getProcessor().loadCommon(env.getCommandArgs());
		env.getWriter().append(includedContent);
	}

}
