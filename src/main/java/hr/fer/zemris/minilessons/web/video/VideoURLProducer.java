package hr.fer.zemris.minilessons.web.video;

/**
 * This service is used to generate an URL for video content in given minilesson.
 *  
 * @author marcupic
 */
public interface VideoURLProducer {
	/**
	 * Generate URL.
	 * @param contextPath web-application context path
	 * @param uid minilesson ID
	 * @param videoFileName video file name
	 * @return url
	 */
	String produce(String contextPath, String uid, String videoFileName);
	/**
	 * Retrieves information on video storage. This will be written to the
	 * author of minilesson after minilesson upload. It should contain enough
	 * information that the author knows where to copy video materials in order
	 * for them to be usable with deployed minilesson.
	 *  
	 * @param contextPath web-application context path
	 * @param uid minilesson ID
	 * @return information for author
	 */
	String info(String contextPath, String uid);
}
