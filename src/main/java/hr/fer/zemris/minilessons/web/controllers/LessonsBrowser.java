package hr.fer.zemris.minilessons.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.Course;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.MLStatWithUserGrade;
import hr.fer.zemris.minilessons.dao.model.Minilesson;
import hr.fer.zemris.minilessons.dao.model.MinilessonStats;
import hr.fer.zemris.minilessons.domain.model.DMinilesson;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

public class LessonsBrowser {

	@DAOProvided @Transactional @Authenticated(optional=true)
	public TemplateRetVal process(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String lang = MLCurrentContext.getLocale().getLanguage();
		AppRequestContext apctx = new AppRequestContext(variables.get("apctx"));
		
		String mluid = StringUtil.trim(variables.get("uid"));
		
		DAO dao = DAOProvider.getDao();

		boolean canAdmin = false;
		
		List<DMinilesson> minilessons = new ArrayList<>();

		if(mluid != null) {
			Minilesson ml = dao.findMinilessonByID(mluid);
			if(ml != null) {
				// TODO: tu popuni listu s minilekcijama i statistikom za minilekcije koje su povezane s ovim kolegijem...
				if(MLCurrentContext.getAuthUser()!=null) {
					canAdmin = MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
					List<MLStatWithUserGrade> statsWUG = dao.listMLStatsExForMinilesson(MLCurrentContext.getAuthUser().getId(), ml);
					statsWUG.forEach(swug -> minilessons.add(
							DMinilesson.fillWithUserStatus(
								DMinilesson.fillWithUserGrades(
										DMinilesson.fromDAOModel(swug.getStats(), lang),
										swug.getUserGrade()
								),
								swug.getStatus()
							)
						)
					);
				} else {
					List<MinilessonStats> mlstats = dao.listMLStatsForMinilesson(ml);
					mlstats.forEach(st -> minilessons.add(DMinilesson.fromDAOModel(st, lang)));
				}
				req.setAttribute("specificML", ml);
			}
		} else if(apctx.getParts().containsKey("c")) {
			Course c = dao.findCourseByID(apctx.getParts().get("c"));
			if(c!=null) {
				// TODO: tu popuni listu s minilekcijama i statistikom za minilekcije koje su povezane s ovim kolegijem...
				if(MLCurrentContext.getAuthUser()!=null) {
					canAdmin = MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
					List<MLStatWithUserGrade> statsWUG = dao.listMLStatsExForCourseMinilessons(MLCurrentContext.getAuthUser().getId(), c);
					statsWUG.forEach(swug -> minilessons.add(
							DMinilesson.fillWithUserStatus(
								DMinilesson.fillWithUserGrades(
										DMinilesson.fromDAOModel(swug.getStats(), lang),
										swug.getUserGrade()
								),
								swug.getStatus()
							)
						)
					);
				} else {
					List<MinilessonStats> mlstats = dao.listMLStatsForCourseMinilessons(c);
					mlstats.forEach(st -> minilessons.add(DMinilesson.fromDAOModel(st, lang)));
				}
			}
		} else {
			if(MLCurrentContext.getAuthUser()!=null) {
				canAdmin = MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName());
				List<MLStatWithUserGrade> statsWUG = dao.listAllVisibleMLStatsEx(MLCurrentContext.getAuthUser().getId());
				statsWUG.forEach(swug -> minilessons.add(
						DMinilesson.fillWithUserStatus(
							DMinilesson.fillWithUserGrades(
									DMinilesson.fromDAOModel(swug.getStats(), lang),
									swug.getUserGrade()
							),
							swug.getStatus()
						)
					)
				);
			} else {
				List<MinilessonStats> mlstats = dao.listAllVisibleMLStats();
				mlstats.forEach(st -> minilessons.add(DMinilesson.fromDAOModel(st, lang)));
			}
		}
		
		String order = req.getParameter("order");
		if("publicID".equals(order)) {
			minilessons.sort((m1,m2)->m1.getPublicID().compareTo(m2.getPublicID()));
		}
		
		req.setAttribute("minilessons", minilessons);
		req.setAttribute("canAdmin", canAdmin);
		req.setAttribute("apctx", apctx);
		
		return new TemplateRetVal("lessonsBrowser",Technology.THYMELEAF);
	}
	
}
