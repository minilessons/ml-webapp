package hr.fer.zemris.minilessons.web.controllers;

import java.net.URLEncoder;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import hr.fer.zemris.minilessons.util.StringUtil;

/**
 * Application Request Context is used for tracking from which part of the system user arrived to some
 * resource. It is used, among other things, to generate proper backlinks.
 * 
 * @author marcupic
 */
public class AppRequestContext {

	private final Map<String,String> parts;
	private String contextData = null;
	
	public AppRequestContext(String data) {
		Map<String,String> parts = new LinkedHashMap<>();
		data = StringUtil.trim(data);
		if(data!=null && !"-".equals(data)) {
			String[] elems = data.split(",");
			for(String elem : elems) {
				elem = StringUtil.trim(elem);
				if(elem==null) continue;
				int pos = elem.indexOf('=');
				if(pos<1 || pos>=elem.length()-1) continue;
				String name = elem.substring(0, pos);
				String value = elem.substring(pos+1);
				parts.put(name, value);
			}
		}
		this.parts = Collections.unmodifiableMap(parts);
	}

	public String getContextData() {
		if(contextData==null) {
			try {
				contextData = parts.entrySet().stream()
					.map(e->{
						try {
							return URLEncoder.encode(e.getKey(),"UTF-8")+"="+URLEncoder.encode(e.getValue(),"UTF-8");
						} catch(Exception ex) {
							throw new RuntimeException(ex);
						}
					})
					.collect(Collectors.joining(","));
			} catch(Exception ignorable) {
			}
			if(contextData==null || contextData.isEmpty()) contextData = "-";
		}
		return contextData;
	}
	
	public Map<String, String> getParts() {
		return parts;
	}

	private AppRequestContext(Map<String,String> map) {
		this.parts = Collections.unmodifiableMap(map);
	}
	
	public AppRequestContext newWith(String key, String value) {
		Map<String,String> template = new LinkedHashMap<>(parts);
		template.put(key, value);
		return new AppRequestContext(template);
	}
	
	public AppRequestContext newWithout(String key) {
		Map<String,String> template = new LinkedHashMap<>(parts);
		if(key!=null) template.remove(key);
		return new AppRequestContext(template);
	}
}
