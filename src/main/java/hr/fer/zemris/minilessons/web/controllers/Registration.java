package hr.fer.zemris.minilessons.web.controllers;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;

import hr.fer.zemris.minilessons.AuthDomain;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.dao.model.UserRegistration;
import hr.fer.zemris.minilessons.service.AuthenticationService;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.PasswordUtil;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.WebConstants;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

public class Registration {

	/**
	 * Success.
	 */
	public static final int NO_ERROR = 0;
	/**
	 * If user is already logged in.
	 */
	public static final int ERROR_ALREADY_LOGGED_IN = 1;
	/**
	 * Username is not available.
	 */
	public static final int ERROR_EXISTS = 2;
	/**
	 * Too many registration requests. Try later.
	 */
	public static final int ERROR_LIMIT = 3;
	/**
	 * Error while sending e-mail.
	 */
	public static final int ERROR_SENDMAIL = 4;
	/**
	 * Any other error.
	 */
	public static final int ERROR_OTHER = 5;

	/**
	 * Implementation of registration procedure.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return result object
	 */
	@DAOProvided @Transactional
	public StreamRetVal register(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		
		if(!AuthenticationService.isRegistrationEnabled()) {
			return error(ERROR_OTHER);
		}
		
		Long uid = (Long)req.getSession().getAttribute("currentUserID");
		if(uid!=null) {
			return error(ERROR_ALREADY_LOGGED_IN);
		}
		
		String username = StringUtil.trim(req.getParameter("username"));
		String password = StringUtil.trim(req.getParameter("password"));
		String password2 = StringUtil.trim(req.getParameter("password2"));
		String firstName = StringUtil.trim(req.getParameter("firstname"));
		String lastName = StringUtil.trim(req.getParameter("lastname"));
		String email = StringUtil.trim(req.getParameter("email"));

		if(username==null || password==null || password2==null || firstName==null || lastName==null || email==null || username.indexOf('\\')!=-1) {
			return error(ERROR_OTHER);
		}

		if(!password.equals(password2) || password.length()<8) {
			return error(ERROR_OTHER);
		}
		
		if(!checkEmail(email)) {
			return error(ERROR_OTHER);
		}
		
		if(username!=null) {
			username = username.trim();
		}
		if(password!=null) {
			password = password.trim();
		}

		User user = DAOProvider.getDao().findUserByUsername(username, AuthDomain.PUBLIC.getKey());
		if(user!=null) {
			return error(ERROR_EXISTS);
		}
		
		String ip = req.getRemoteAddr();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date now = new Date();
		String dateKey = sdf.format(now);
		int n1 = DAOProvider.getDao().countRegistrationsForDate(dateKey);
		int n2 = DAOProvider.getDao().countRegistrationsForDate(dateKey,ip);
		if(n1>1000 || n2>500) {
			return error(ERROR_LIMIT);
		}
		
		String authToken = buildAuthToken();
		
		UserRegistration ureg = new UserRegistration();
		ureg.setActivationKey(authToken);
		ureg.setDateKey(dateKey);
		ureg.setEmail(email);
		ureg.setFirstName(firstName);
		ureg.setLastName(lastName);
		ureg.setRequestDate(now);
		ureg.setRequestIP(ip);
		ureg.setUsername(username);
		ureg.setPasswordHash(PasswordUtil.encodePassword(password));

		DAOProvider.getDao().saveUserRegistration(ureg);

		try {
			sendEmail(req.getServletContext(), username, email, firstName, lastName, ureg.getId(), authToken);
		} catch(Exception ex) {
			ex.printStackTrace();
			return error(ERROR_SENDMAIL);
		}
		return error(NO_ERROR);
	}

	private void sendEmail(ServletContext context, String username, String email, String firstName, String lastName, Long id, String authToken) throws Exception {
		String smtpServer = (String)context.getAttribute(WebConstants.ML_MAIL_HOST);
		String fromField = (String)context.getAttribute(WebConstants.ML_MAIL_FROM);
		String webServer = (String)context.getAttribute(WebConstants.ML_MAIL_ACT_SERVER);
		String webPath = (String)context.getAttribute(WebConstants.ML_MAIL_ACT_PATH);
		String url = "http://"+webServer+webPath+"?key="+id+"-"+authToken;
		String subject = "[Minilessons] Aktivacija korisničkog računa";
		String messageBody =  "Bok "+ firstName+"!\r\n\r\n"
							+ "Ova poruka je poslana jer ste pokrenuli registraciju korisničkog računa na sustavu Minilessons za korisničko ime "+username+".\r\n"
							+ "Da biste dovršili postupak, račun je potrebno aktivirati što možete učiniti klikom na sljedeći link.\r\n\r\n"
							+ url + "\r\n\r\n"
							+ "Nakon što provedete aktivaciju, na sustav ćete se moći prijaviti koristeći 'public\\"+username+"' kao korisničko ime te šifru koju ste unijeli prilikom registracije.\r\n\r\n"
							+ "Vaš Minilessons";
		Properties mailProp = new Properties();
		mailProp.put("mail.smtp.host", smtpServer);
		Session session = Session.getInstance(mailProp, null);
		Message msg = new MimeMessage(session);
		InternetAddress from = new InternetAddress(fromField);
		msg.setFrom(from);
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
		msg.setSubject(subject);
		msg.setContent(messageBody, "text/plain; charset=utf-8");
		Transport.send(msg);

	}

	/**
	 * Private helper method for building random activation tokens of 64 HEX digits.
	 * @return activation token
	 */
	private String buildAuthToken() {
		byte[] data = new byte[32];
		Random rand = new Random();
		rand.nextBytes(data);
		return Hex.encodeHexString(data);
	}

	/**
	 * Implementation of activation page.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return login page
	 */
	@DAOProvided @Transactional
	public TemplateRetVal activate(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String key = StringUtil.trim(req.getParameter("key"));
		if(key==null) {
			req.setAttribute("errorMsgKey", "invalidActivationParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		int p = key.indexOf('-');
		if(p<1) {
			req.setAttribute("errorMsgKey", "invalidActivationParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		String part1 = key.substring(0, p).trim();
		String part2 = key.substring(p+1).trim();

		if(part1.isEmpty() || part2.isEmpty()) {
			req.setAttribute("errorMsgKey", "invalidActivationParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		Long id = null;
		try {
			id = Long.valueOf(part1);
		} catch(Exception ex) {
			req.setAttribute("errorMsgKey", "invalidActivationParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		UserRegistration ureg = DAOProvider.getDao().findUserRegistration(id);
		if(ureg==null) {
			req.setAttribute("errorMsgKey", "invalidActivationParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		if(!ureg.getActivationKey().equals(part2)) {
			req.setAttribute("errorMsgKey", "invalidActivationParameters");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		User user = DAOProvider.getDao().findUserByUsername(ureg.getUsername(), AuthDomain.PUBLIC.getKey());
		if(user!=null) {
			req.setAttribute("errorMsgKey", "activationUserExists");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}

		user = new User();
		user.setUsername(ureg.getUsername());
		user.setAuthDomain(AuthDomain.PUBLIC.getKey());
		user.setEmail(ureg.getEmail());
		user.setFirstName(ureg.getFirstName());
		user.setLastName(ureg.getLastName());
		user.setJmbag(String.format("P%010d", ureg.getId()));
		user.setUploadPolicy(AuthenticationService.getRegUsersUploadPolicy());
		user.setPermissions(AuthenticationService.getRegUsersPermissions());
		user.setPasswordHash(ureg.getPasswordHash());
		user.setCreatedOn(new Date());

		ureg.setActivationDate(user.getCreatedOn());

		DAOProvider.getDao().saveUser(user);
		
		req.setAttribute("username", user.getUsername());
		
		return new TemplateRetVal("activationPage",Technology.THYMELEAF);
	}
	
	/**
	 * Simple helper method for e-mail syntax verification (very very basic).
	 * @param email email to check
	 * @return <code>true</code> if email is OK, <code>false</code> otherwise
	 */
	private boolean checkEmail(String email) {
		int l = email.length();
		int a = email.indexOf('@');
		if(l<3 || a<1 || a>=l-1) return false;
		int d = email.lastIndexOf('.');
		if(d<0 || d < a || d==a+1 || d>=l-1) return false;
		return true;
	}


	/**
	 * Helper method which generates result if authentication failed.
	 * 
	 * @param errorType code of error
	 * @return result JSON object
	 */
	private StreamRetVal error(int errorType) {
		if(errorType != NO_ERROR) {
			try {
				Thread.sleep(2500);
			} catch (InterruptedException ignorable) {
			}
		}
		return new StreamRetVal("{\"error\": "+errorType+"}", StandardCharsets.UTF_8, "application/json", null);
	}	
}
