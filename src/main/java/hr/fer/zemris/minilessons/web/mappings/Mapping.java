package hr.fer.zemris.minilessons.web.mappings;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.Configuration;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.DAOUtil;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.locking.LockingTree;
import hr.fer.zemris.minilessons.logging.MLLogger;
import hr.fer.zemris.minilessons.service.AuthUser;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.CallAfterLockException;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.web.WebConstants;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.video.VideoURLProducer;

/**
 * This class specifies a single URL mapping.
 * 
 * @author marcupic
 */
public class Mapping {
	
	/**
	 * Http method which must be set in http headers to trigger this mapping.
	 */
	private HttpMethod httpMethod;
	/**
	 * Pattern parts.
	 */
	private String[] parts;
	/**
	 * Controller which should be used for request handling.
	 */
	private Class<?> controllerClass;
	/**
	 * Method in controller which should be called.
	 */
	private Method methodHandle;
	/**
	 * Indexes of parts which must match (they are literal strings).
	 */
	private int[] exactMatchIndexes;
	/**
	 * Indexes of parts which must be bound to defined variables.
	 */
	private int[] variableIndexes;
	/**
	 * Should DAO provider be open automatically on method call and closed on method exit (see {@link DAOProvided})?
	 */
	private boolean daoProvided;
	/**
	 * Should transaction be opened automatically on method call and committed on method exit (see {@link Transactional})?
	 */
	private boolean transactional;
	/**
	 * Is call of this method allowed only for authenticated users (see {@link Authenticated})?
	 */
	private boolean authenticated;
	/**
	 * Is redirection to this controller allowed after login procedure?
	 */
	private boolean postLoginRedirectEnabled;
	/**
	 * Is authentication optional flag set?
	 */
	private boolean authIsOptional;
	/**
	 * Should login be attempted if user is not logged in?
	 */
	private boolean shouldAttemptLogin;
	
	
	/**
	 * Constructor.
	 * 
	 * @param httpMethod http method of request
	 * @param pattern URL pattern to match
	 * @param controllerName name of controller to execute
	 * @param methodName name of method in controller to execute; if <code>null</code> or empty, "process" will be used
	 */
	public Mapping(HttpMethod httpMethod, String pattern, String controllerName, String methodName) {
		super();
		
		this.httpMethod = Objects.requireNonNull(httpMethod, "HttpMethod must not be null.");
		this.parts = pattern.split("/");
		
		try {
			controllerClass = getClass().getClassLoader().loadClass("hr.fer.zemris.minilessons.web.controllers."+controllerName);
			if(methodName==null || methodName.equals("")) {
				methodName = "process";
			}
			methodHandle = controllerClass.getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class, Map.class);
			
			daoProvided = methodHandle.isAnnotationPresent(DAOProvided.class);
			transactional = methodHandle.isAnnotationPresent(Transactional.class);
			
			Authenticated authAnno = methodHandle.getAnnotation(Authenticated.class);
			
			authenticated = authAnno != null;
			postLoginRedirectEnabled = authAnno != null && authAnno.redirectAllowed();
			authIsOptional = authAnno != null && authAnno.optional();
			shouldAttemptLogin = authAnno != null && authAnno.attemptLogin();
			
			if(!daoProvided && transactional) throw new RuntimeException("@Transactional annotation found on method without @DAOProvided annotation.");
		} catch(Exception ex) {
			throw new RuntimeException("Could not create mapping for controller "+controllerName+" and method "+methodName+".", ex);
		}
		
		int emiCount = 0;
		for(int i = 0; i < parts.length; i++) {
			if(parts[i].charAt(0) != '$') emiCount++;
		}
		exactMatchIndexes = new int[emiCount];
		variableIndexes = new int[parts.length - emiCount];
		
		emiCount = 0;
		int varCount = 0;
		for(int i = 0; i < parts.length; i++) {
			if(parts[i].charAt(0) != '$') {
				exactMatchIndexes[emiCount] = i;
				emiCount++;
			} else {
				variableIndexes[varCount] = i;
				varCount++;
			}
		}
	}

	/**
	 * Try to match given path to this mapping. If match is unsuccessful, <code>null</code>
	 * will be returned. Otherwise, defined variables will be bound and a map containing
	 * variable mappings will be returned. If no variables were defined, empty map will
	 * be returned.
	 * 
	 * @param currentPath path to match
	 * @param currentMethod http request method 
	 * @return map of bound variables if match is successful or <code>null</code> otherwise
	 */
	public Map<String,String> matches(String[] currentPath, HttpMethod currentMethod) {
		// Vidi uvjet po metodi kojom je stigao zahtjev...
		if(httpMethod!=HttpMethod.ANY) {
			if(httpMethod != currentMethod) return null;
		}
		
		// Ako sam ja staza "" koju tretiramo kao staza koja sve hvata, prihvati:
		if(parts.length==1 && parts[0].isEmpty()) return new HashMap<>();
	
		// Inace, ja nesto trazim, a dobio sam null kao trenutnu stazu - sigurno ne prihvaćam:
		if(currentPath == null) {
			return null;
		}
		
		// Inace idemo vidjeti prvo fiksne dijelove, pa ako je to OK, onda procitamo varijable:
		if(currentPath.length != parts.length) return null;
		for(int i : exactMatchIndexes) {
			if(!currentPath[i].equals(parts[i])) return null;
		}
		
		Map<String, String> map = new HashMap<>();
		for(int i : variableIndexes) {
			map.put(parts[i].substring(1), currentPath[i]);
		}
		return map;
	}
	
	/**
	 * Use this method to invoke controller defined for this mapping in order to process the request.
	 * 
	 * @param req http request
	 * @param resp http response
	 * @param vars bound variables when URL matching was performed
	 * @return result of controller processing
	 * @throws IOException
	 * @throws ServletException
	 */
	public Object invoke(HttpServletRequest req, HttpServletResponse resp, Map<String,String> vars) throws IOException, ServletException {
		Long currentUserID = (Long)req.getSession().getAttribute("currentUserID");
		if(authenticated) {
			if(!authIsOptional && currentUserID==null) {
				if(shouldAttemptLogin) {
					if(postLoginRedirectEnabled) {
						String url = req.getRequestURL().toString();
						req.getSession().setAttribute(WebConstants.ML_POSTLOGIN_REDIRECT, url);
						req.getSession().setAttribute(WebConstants.ML_POSTLOGIN_EXPIRES, LocalDateTime.now().plusMinutes(10));
						resp.sendRedirect(""+req.getContextPath()+"/ml/login?auto=true");
					} else {
						req.getSession().removeAttribute(WebConstants.ML_POSTLOGIN_REDIRECT);
						req.getSession().removeAttribute(WebConstants.ML_POSTLOGIN_EXPIRES);
						resp.sendRedirect(""+req.getContextPath()+"/ml/login");
					}
					return null;
				} else {
					return new StreamRetVal(403, "Forbidden.");
				}
			}
		}

		Object c = null;
		try {
			c = controllerClass.newInstance();
		} catch(Exception ex) {
			throw new RuntimeException("Could not create an instance of controller "+controllerClass.getName()+".", ex);
		}
		
		Configuration config = (Configuration)req.getAttribute(WebConstants.ML_CONFIG_KEY);
		if(config!=null) {
			MLCurrentContext.setLocale(config.getLocale());
		}
		MLCurrentContext.setVideoURLProducer((VideoURLProducer)req.getServletContext().getAttribute(WebConstants.ML_VIDEO_URL_PRODUCER_KEY));
		
		Object result = null;
		try {
			String[] lockingPath = null;
			int callCounter = 0;
			boolean lockObtained = false;
			while(true) {
				callCounter++;
				if(callCounter>2) {
					break;
				} else if(callCounter==2){
					// If we do not have to repeat call after locking, we are done...
					if(lockingPath==null) break;
					// Else we must acquire the lock and try controller method again...
					System.out.println("Retrying service method after obtaining the lock for: " + Arrays.toString(lockingPath));
					LockingTree.getDefaultInstance().lock(lockingPath);
					lockObtained = true;
				}
				try {
					if(daoProvided) {
						//System.out.println("Calling DAOProvider.getDAOManager().open();");
						DAOProvider.getDAOManager().open();
					}
					boolean transactionOpened = false;
					try {
						if(transactional) {
							//System.out.println("Calling DAOProvider.getDAOManager().beginTransaction();");
							DAOProvider.getDAOManager().beginTransaction();
							transactionOpened = true;
						}
						String username = "";
						if(authenticated && currentUserID!=null && MLCurrentContext.getAuthUser()==null) {
							if(daoProvided) {
								loadCurrentUser(currentUserID);
							} else {
								DAOUtil.transactional(()->{
									loadCurrentUser(currentUserID);
									return null;
								});
							}
							req.setAttribute(WebConstants.ML_CURRENT_USER, MLCurrentContext.getAuthUser());
						}
						AuthUser au = MLCurrentContext.getAuthUser();
						if(au!=null) username = au.getAuthDomain()+"\\"+au.getUsername();
						
						//System.out.println("Calling "+methodHandle);
						if(callCounter==1) MLLogger.getAccesslogger().log(req.getRemoteAddr(), username, req.getPathInfo(), req.getHeader("Referer"));
						Map<String,String> varsMap = vars;
						if(lockObtained) {
							varsMap = new HashMap<>(vars);
							varsMap.put("@lockObtained", lockObtained ? "1" : "0");
							varsMap.put("@lockPath", Arrays.asList(lockingPath).stream().collect(Collectors.joining("/")));
						}
						result = methodHandle.invoke(c, req, resp, varsMap);
						if(transactional) {
							//System.out.println("Calling DAOProvider.getDAOManager().commitTransaction();");
							DAOProvider.getDAOManager().commitTransaction();
						}
					} catch(InvocationTargetException ex) {
						if(transactional && transactionOpened) {
							//System.out.println("Calling DAOProvider.getDAOManager().rollbackTransaction();");
							DAOProvider.getDAOManager().rollbackTransaction();
						}
						if(ex.getCause()!=null && ex.getCause() instanceof CallAfterLockException) {
							CallAfterLockException cale = (CallAfterLockException)ex.getCause();
							lockingPath = cale.getLockPath();
						} else {
							throw ex;
						}
					} catch(Exception ex) {
						if(transactional && transactionOpened) {
							//System.out.println("Calling DAOProvider.getDAOManager().rollbackTransaction();");
							DAOProvider.getDAOManager().rollbackTransaction();
						}
						throw ex;
					}
				} finally {
					try {
						if(daoProvided) {
							//System.out.println("Calling DAOProvider.getDAOManager().close();");
							DAOProvider.getDAOManager().close();
						}
					} finally {
						if(lockObtained) LockingTree.getDefaultInstance().unlock(lockingPath);
					}
				}
			}
		} catch(Exception ex) {
			throw new RuntimeException("Exception while executing an instance of controller "+controllerClass.getName()+" and method "+methodHandle.toGenericString()+".", ex);
		} finally {
			MLCurrentContext.clear();
		}

		return result;
	}

	/**
	 * Helper method for automatic loading of authenticated user.
	 * 
	 * @param currentUserID ID of user to load
	 */
	private void loadCurrentUser(Long currentUserID) {
		User user = DAOProvider.getDao().findUserByID(currentUserID);
		if(user==null) {
			throw new RuntimeException("Current user could not be loaded.");
		}
		MLCurrentContext.setAuthUser(AuthUser.fillFromUser(user));
	}
	
}