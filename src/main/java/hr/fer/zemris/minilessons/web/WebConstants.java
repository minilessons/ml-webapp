package hr.fer.zemris.minilessons.web;

import hr.fer.zemris.minilessons.Configuration;

/**
 * Constants used in web part of this application.
 * 
 * @author marcupic
 */
public class WebConstants {

	/**
	 * This key is used to store {@link Configuration} object for current
	 * http-request in its attributes.
	 */
	public static final String ML_CONFIG_KEY = "ml-configuration";
	
	/**
	 * This key is used to bound template engine implementation into
	 * servlet context global attributes.
	 */
	public static final String ML_TEMPLATE_ENGINE_KEY = "ml-template-engine";
	
	/**
	 * This key is used to bound video URL producer into
	 * servlet context global attributes.
	 */
	public static final String ML_VIDEO_URL_PRODUCER_KEY = "ml-video-url-producer";
	
	/**
	 * This key is used to bound navigation for item into http request
	 * attributes.
	 */
	public static final String ML_ITEMS_NAVIG = "mlItemsNavig";

	/**
	 * This key is used to bound DAOBoot object into servlet context global 
	 * attributes.
	 */
	public static final String ML_DAO_BOOT_KEY = "ml-dao-boot";
	
	/**
	 * This key is used to bound Periodicals execution subsystem object into servlet context global 
	 * attributes.
	 */
	public static final String ML_PERIODICALS_MANAGER_KEY = "ml-periodicals-manager";
	/**
	 * This key is used to bound URL which was denied for authentication reasons while user
	 * was redirected to login page into current HTTP Session. Once the login is completed, 
	 * user can be redirected to this address.
	 */
	public static final String ML_POSTLOGIN_REDIRECT = "ml-postlogin-redirect";
	/**
	 * Works in conjunction with {@link #ML_POSTLOGIN_REDIRECT}. Bounds time after which redirect
	 * will be discarded and the user will be redirected to the home page instead.
	 */
	public static final String ML_POSTLOGIN_EXPIRES = "ml-postlogin-expires";
	
	/**
	 * This key is used to bound current (authenticated) user into http request
	 * attributes.
	 */
	public static final String ML_CURRENT_USER = "mlCurrentUser";
	/**
	 * Key for storing smtp server name into servlet context attributes.
	 */
	public static final String ML_MAIL_HOST = "mlMailHost";
	/**
	 * Key for storing email for From: field when sending emails, into servlet context attributes.
	 */
	public static final String ML_MAIL_FROM = "mlMailFrom";
	/**
	 * Key for storing web server name when constructing URLs in mail, into servlet context attributes.
	 */
	public static final String ML_MAIL_ACT_SERVER = "mlMailActServer";
	/**
	 * Key for storing URL-path when constructing URL for activation mail, into servlet context attributes.
	 * Path should start with /, so that http://${ML_MAIL_ACT_SERVER}/${ML_MAIL_ACT_PATH} forms valid URL.
	 */
	public static final String ML_MAIL_ACT_PATH = "mlMailActPath";
}
