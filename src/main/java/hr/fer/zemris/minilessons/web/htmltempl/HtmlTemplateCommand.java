package hr.fer.zemris.minilessons.web.htmltempl;

import java.io.IOException;

public interface HtmlTemplateCommand {

	void execute(CommandEnvironment env) throws IOException;
	
}
