package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.json.JSONObject;

import hr.fer.zemris.minilessons.dao.DAO;
import hr.fer.zemris.minilessons.dao.DAOProvider;
import hr.fer.zemris.minilessons.dao.model.CommonQuestionDefinition;
import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.dao.model.QuestionApproval;
import hr.fer.zemris.minilessons.dao.model.User;
import hr.fer.zemris.minilessons.domain.model.DQuestion;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.MLVerificationException;
import hr.fer.zemris.minilessons.service.QDeployer;
import hr.fer.zemris.minilessons.service.Transactional;
import hr.fer.zemris.minilessons.util.StringUtil;
import hr.fer.zemris.minilessons.web.controllers.QuestionsController.XMLQuestionUploadData;
import hr.fer.zemris.minilessons.web.retvals.StreamRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Administration of JS questions.

 * @author marcupic
 */
public class QJSAdministration {

	/**
	 * Show list of JSQuestions for given user.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return template to render
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=true)
	public TemplateRetVal showList(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		if(!MLCurrentContext.canManageMinilessons()) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new TemplateRetVal("errorPage", Technology.THYMELEAF);
		}
		
		List<CommonQuestionDefinition> qdefs = DAOProvider.getDao().listAllCommonQuestionsForOwner(DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId()));
		List<DQuestion> dqdefs = qdefs.stream().sorted((m1,m2)->m1.getPublicID().compareTo(m2.getPublicID())).map(m->DQuestion.fromDAOModel(m, MLCurrentContext.getLocale().getLanguage())).collect(Collectors.toList());
		dqdefs.forEach(q->q.setApprovalManageable(true));
		req.setAttribute("qdefs", dqdefs);
		return new TemplateRetVal("qjssadmin", Technology.THYMELEAF);
	}

	/**
	 * Toggle visibility for given common question.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=false)
	public StreamRetVal toggleVisibility(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String mid = variables.get("uid");
		mid = mid.trim();
		if(mid.isEmpty()) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		CommonQuestionDefinition qd = DAOProvider.getDao().findCommonQuestionDefinitionByID(mid);
		if(qd==null) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		if(!qd.getOwner().getId().equals(MLCurrentContext.getAuthUser().getId()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		qd.setVisible(!qd.isVisible());
		
		return new StreamRetVal("{\"error\": false, \"visible\": "+qd.isVisible()+"}", StandardCharsets.UTF_8, "application/json", null);
	}
	
	/**
	 * Set approval status for given common question.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return JSON result
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=false)
	public StreamRetVal setApproval(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		String mid = variables.get("uid");
		String stat = variables.get("status");
		mid = mid.trim();
		stat = stat.trim();
		if(mid.isEmpty() || stat.isEmpty()) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		QuestionApproval newStatus = null;
		try {
			newStatus = QuestionApproval.valueOf(stat);
		} catch(Exception ex) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}

		if(!MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.CAN_APPROVE_MINILESSONS.getPermissonName()) && !MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}

		CommonQuestionDefinition qd = DAOProvider.getDao().findCommonQuestionDefinitionByID(mid);
		if(qd==null) {
			return new StreamRetVal("{\"error\": true}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		if(qd.getApproval() != newStatus) {
			qd.setApproval(newStatus);
			qd.setApprovedBy(DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId()));
			qd.setApprovedOn(new Date());
		}
		
		return new StreamRetVal("{\"error\": false, \"status\": \""+qd.getApproval().name()+"\"}", StandardCharsets.UTF_8, "application/json", null);
	}

	/**
	 * Upload of jsquestion.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return template to render
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=false)
	public StreamRetVal uploadjs(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws ServletException, IOException {
		if(!MLCurrentContext.canManageMinilessons()) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		Part part = req.getPart("file");
		if(part==null) {
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		boolean mlIsVisible = isSet(req.getParameter("mlIsVisible"));
		boolean publicQuestion = isSet(req.getParameter("publicQuestion"));
		
		System.out.println("Submitted mlIsVisible = "+req.getParameter("mlIsVisible"));
		System.out.println("Submitted file name = "+part.getSubmittedFileName());
		System.out.println("Submitted content type = "+part.getContentType());
		System.out.println("Submitted size = "+part.getSize());

		Path tmp = null;
		try {
			tmp = Files.createTempFile("mnl", ".tmp");
		} catch(Exception ex) {
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		}
		
		try {
			Files.copy(part.getInputStream(), tmp, StandardCopyOption.REPLACE_EXISTING);
			Map<String,String> options = new HashMap<>();
			options.put("create", "false");
			options.put("encoding", "UTF-8");
			try(FileSystem fs = FileSystems.newFileSystem(tmp, null)) {
				QDeployer.deployJSQuestion(fs.getPath("/"), DAOProvider.getDao().findUserByID(MLCurrentContext.getAuthUser().getId()), mlIsVisible, publicQuestion);
			} catch(MLVerificationException ex) {
				JSONObject obj = new JSONObject();
				obj.put("error", true);
				obj.put("msg", ex.getMessage());
				return new StreamRetVal(obj.toString(), StandardCharsets.UTF_8, "application/json", null);
			} catch(Exception ex) {
				ex.printStackTrace();
				return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		} finally {
			try {
				Files.delete(tmp);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}

		return new StreamRetVal("{\"error\": false, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
	}

	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=false)
	public StreamRetVal uploadxml(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws ServletException, IOException {
		if(!MLCurrentContext.canManageMinilessons()) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		}

		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());

		boolean mlIsVisible = isSet(req.getParameter("mlIsVisible"));
		boolean publicQuestion = isSet(req.getParameter("publicQuestion"));
		String xml = StringUtil.trim(req.getParameter("xml"));
		try {
			XMLQuestionUploadData data = XMLQuestionUploadData.fromString(xml, publicQuestion, mlIsVisible);
			QDeployer.deployXMLQuestion(user, data);
		} catch(Exception ex) {
			return new StreamRetVal("{\"error\": true, \"msg\": \""+stringEscape(ex.getMessage())+"\"}", StandardCharsets.UTF_8, "application/json", null);
		}

		return new StreamRetVal("{\"error\": false, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
	}

	private String stringEscape(String message) {
		if(message==null) return "";
		StringBuilder sb = new StringBuilder();
		for(char c : message.toCharArray()) {
			switch(c) {
			case '\\': sb.append("\\\\"); break;
			case '\t': sb.append("\\t"); break;
			case '\r': sb.append("\\r"); break;
			case '\n': sb.append("\\n"); break;
			default:
				if(c<32) {
					sb.append('?');
				} else {
					sb.append(c);
				}
			}
		}
		return sb.toString();
	}

	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=false)
	public StreamRetVal uploadxmlz(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws ServletException, IOException {
		if(!MLCurrentContext.canManageMinilessons()) {
			req.setAttribute("errorMsgKey", "noPermission");
			return new StreamRetVal("{\"error\": true, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
		}

		DAO dao = DAOProvider.getDao();
		User user = dao.findUserByID(MLCurrentContext.getAuthUser().getId());

		boolean mlIsVisible = isSet(req.getParameter("mlIsVisible"));
		boolean publicQuestion = isSet(req.getParameter("publicQuestion"));

		try {
			XMLQuestionUploadData data = XMLQuestionUploadData.fromMultipart(req, "file", publicQuestion, mlIsVisible);
			QDeployer.deployXMLQuestion(user, data);
		} catch(Exception ex) {
			return new StreamRetVal("{\"error\": true, \"msg\": \""+stringEscape(ex.getMessage())+"\"}", StandardCharsets.UTF_8, "application/json", null);
		}

		return new StreamRetVal("{\"error\": false, \"msg\": null}", StandardCharsets.UTF_8, "application/json", null);
	}

	private boolean isSet(String value) {
		return value!=null && (value.equalsIgnoreCase("on") || value.equalsIgnoreCase("true") || value.equals("1"));
	}
	
}
