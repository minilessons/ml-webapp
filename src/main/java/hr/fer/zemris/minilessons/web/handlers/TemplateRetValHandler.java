package hr.fer.zemris.minilessons.web.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.context.WebContext;

import hr.fer.zemris.minilessons.Configuration;
import hr.fer.zemris.minilessons.web.WebConstants;
import hr.fer.zemris.minilessons.web.mappings.ResponseHandler;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Response handler for {@link TemplateRetVal} results.
 * 
 * @author marcupic
 *
 */
public class TemplateRetValHandler implements ResponseHandler {
	
	@Override
	public void handle(HttpServletRequest req, HttpServletResponse resp, Object result) throws IOException, ServletException {
		TemplateRetVal res = (TemplateRetVal)result;
		
		if(res.getTechnology()==Technology.JSP) {
			req.getRequestDispatcher(res.getTemplateName()).forward(req, resp);
			return;
		}
		
		if(res.getTechnology()==Technology.THYMELEAF) {
			resp.setContentType("text/html; charset=UTF-8");
			resp.setHeader("Pragma", "no-cache");
			resp.setHeader("Cache-Control", "no-cache");
			resp.setDateHeader("Expires", 0);
			
			Configuration config = (Configuration)req.getAttribute(WebConstants.ML_CONFIG_KEY);
			config.getTemplateEngine().process(res.getTemplateName(), new WebContext(req, resp, req.getServletContext(), config.getLocale()),resp.getWriter());
		}
	}
	
}