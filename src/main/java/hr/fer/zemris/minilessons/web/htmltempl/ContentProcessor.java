package hr.fer.zemris.minilessons.web.htmltempl;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

public class ContentProcessor {
	
	private Set<String> inclusionSet = new HashSet<>();
	private Function<String, Path> pathResolver;
	private Predicate<String> commandVerifier;
	private Map<String, HtmlTemplateCommand> commands;
	private String defaultFolder;
	private Set<String> registeredPageScripts = new LinkedHashSet<>();
	
	public ContentProcessor(Function<String, Path> pathResolver, Predicate<String> commandVerifier, Map<String, HtmlTemplateCommand> commands, String defaultFolder) {
		this.pathResolver = Objects.requireNonNull(pathResolver);
		this.commandVerifier = commandVerifier;
		this.commands = new HashMap<>(commands);
		this.defaultFolder = defaultFolder;
	}

	public void registerPageScript(String url) {
		registeredPageScripts.add(url);
	}
	
	public Set<String> getRegisteredPageScripts() {
		return registeredPageScripts;
	}
	
	public String load(String fileName) throws IOException {
		if(!inclusionSet.add(fileName)) {
			throw new RuntimeException("Recursive inclusion detected. Offender: " + fileName + ".");
		}
		
		Path filePath = pathResolver.apply(defaultFolder + "/" + fileName);
		if(filePath == null) throw new RuntimeException("Can not include page " + fileName + ".");
		
		String content = new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);
		content = processContent(content);
		return content;
	}

	public String loadCommon(String fileName) throws IOException {
		if(!inclusionSet.add("common/"+fileName)) {
			throw new RuntimeException("Recursive inclusion detected. Offender: " + fileName + ".");
		}
		
		Path filePath = pathResolver.apply("common/"+fileName);
		if(filePath == null) throw new RuntimeException("Can not include common " + fileName + ".");
		
		String content = new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);
		content = processContent(content);
		return content;
	}

	private String processContent(String content) throws IOException {
		StringWriter writer = new StringWriter();
		
		Lexer lex = new Lexer(content.toCharArray());
		lex.nextToken();
		while(lex.getTokenType()!=TokenType.EOF) {
			if(lex.getTokenType()==TokenType.STRING) {
				writer.append(lex.getTokenString());
			} else if(lex.getTokenType()==TokenType.COMMAND) {
				String commandName = lex.getCommandName();
				String commandArgs = lex.getCommandArgs();
				if(commandVerifier != null && !commandVerifier.test(commandName)) {
					throw new RuntimeException("Command "+commandName+" is not allowed here.");
				}
				HtmlTemplateCommand command = commands.get(commandName);
				if(command==null) {
					throw new RuntimeException("Command implementation for "+commandName+" is not available.");
				}
				command.execute(new CommandEnvironment(commandName, commandArgs, this, writer));
			}
			lex.nextToken();
		}
		writer.flush();
		writer.close();
		return writer.toString();
	}
	
}
