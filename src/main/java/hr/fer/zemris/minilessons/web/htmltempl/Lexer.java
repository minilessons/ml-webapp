package hr.fer.zemris.minilessons.web.htmltempl;

public class Lexer {

	int current = 0;
	char[] data;
	TokenType tokenType;
	String tokenString;
	String commandName;
	String commandArgs;
	
	public Lexer(char[] data) {
		super();
		this.data = data;
	}

	public String getCommandArgs() {
		return commandArgs;
	}
	
	public String getCommandName() {
		return commandName;
	}
	
	public String getTokenString() {
		return tokenString;
	}
	
	public TokenType getTokenType() {
		return tokenType;
	}
	
	public void nextToken() {
		if(tokenType==TokenType.EOF) return;
		if(current >= data.length) {
			tokenType  =TokenType.EOF;
			return;
		}
		int start = current;
		while(true) {
			while(current < data.length && data[current] != '@' && data[current] != '/') current++;
			if(current>=data.length) {
				tokenType = TokenType.STRING;
				tokenString = new String(data, start, current-start);
				return;
			}
			
			// inače sam na @ ili /: vidi slijedi li nakon toga valjan tag
			int tagStartOffset;
			boolean jsTypeTag = false;
			if(data[current] == '@') {
				tagStartOffset = checkTagStart(current+1);
				if(tagStartOffset==-1) {
					current++;
					continue;
				}
			} else {
				tagStartOffset = checkTagStart2(current+1);
				if(tagStartOffset==-1) {
					current++;
					continue;
				}
				jsTypeTag = true;
			}
			
			int tagEndOffset;
			if(jsTypeTag) {
				tagEndOffset = getTagEndOffset2(tagStartOffset);
			} else {
				tagEndOffset = getTagEndOffset(tagStartOffset);
			}
			if(tagEndOffset == -1) {
				throw new RuntimeException("Unterminated tag.");
			}
			
			// Imam tag. Ako ispred toga imam tekst, vrati ga
			if(start != current) {
				tokenType = TokenType.STRING;
				tokenString = new String(data, start, current-start);
				return;
			}

			if(jsTypeTag) {
				commandName = new String(data, current+4, tagStartOffset-current-6);
			} else {
				commandName = new String(data, current+2, tagStartOffset-current-4);
			}
			if(jsTypeTag) {
				commandArgs = new String(data, tagStartOffset, tagEndOffset-tagStartOffset-3);
			} else {
				commandArgs = new String(data, tagStartOffset, tagEndOffset-tagStartOffset-1);
			}
			tokenType = TokenType.COMMAND;
			current = tagEndOffset;
			return;
		}
	}

	private int checkTagStart(int pos) {
		if(pos >= data.length || data[pos]!='#') return -1;
		pos++;
		int start = pos;
		if(pos >= data.length || !Character.isLetter(data[pos])) return -1;
		pos++;
		while(pos < data.length && (Character.isLetter(data[pos]) || Character.isDigit(data[pos]) || data[pos]=='-')) pos++;
		if(start == pos) return -1;
		if(pos >= data.length || data[pos] != '#') return -1;
		pos++;
		if(pos >= data.length || data[pos] != '(') return -1;
		pos++;
		return pos;
	}
	
	private int checkTagStart2(int pos) {
		if(pos >= data.length || data[pos]!='*') return -1;
		pos++;
		if(pos >= data.length || data[pos]!='@') return -1;
		pos++;
		if(pos >= data.length || data[pos]!='#') return -1;
		pos++;
		int start = pos;
		if(pos >= data.length || !Character.isLetter(data[pos])) return -1;
		pos++;
		while(pos < data.length && (Character.isLetter(data[pos]) || Character.isDigit(data[pos]) || data[pos]=='-')) pos++;
		if(start == pos) return -1;
		if(pos >= data.length || data[pos] != '#') return -1;
		pos++;
		if(pos >= data.length || data[pos] != '(') return -1;
		pos++;
		return pos;
	}
	
	private int getTagEndOffset(int pos) {
		while(pos < data.length && data[pos]!=')') pos++;
		if(pos >= data.length) return -1;
		return pos+1;
	}
	
	private int getTagEndOffset2(int pos) {
		while(pos < data.length && data[pos]!=')') pos++;
		if(pos >= data.length) return -1;
		pos++;
		if(pos >= data.length || data[pos]!='*') return -1;
		pos++;
		if(pos >= data.length || data[pos]!='/') return -1;
		pos++;
		return pos;
	}
	
}
