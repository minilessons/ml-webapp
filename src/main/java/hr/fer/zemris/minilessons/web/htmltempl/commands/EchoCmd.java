package hr.fer.zemris.minilessons.web.htmltempl.commands;

import java.io.IOException;

import hr.fer.zemris.minilessons.VariableMapping;
import hr.fer.zemris.minilessons.web.WebUtil;
import hr.fer.zemris.minilessons.web.htmltempl.CommandEnvironment;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;

public class EchoCmd implements HtmlTemplateCommand {

	public static enum EchoEncode {
		HTML,
		RAW,
		JS,
		HTML_ATTRIB
	}
	
	private VariableMapping mapping;
	private Boolean expectedCondition;
	
	public EchoCmd(Boolean expectedCondition, VariableMapping mapping) {
		super();
		this.mapping = mapping;
		this.expectedCondition = expectedCondition;
	}

	@Override
	public void execute(CommandEnvironment env) throws IOException {
		String varValue = env.getCommandArgs().trim();
		EchoEncode encode = EchoEncode.HTML;
		
		// Utvrdi je li prisutan encoding; encoding se piše između dvije zvjezdice: *raw*
		char[] chars = varValue.toCharArray();
		int curr = 0;
		while(curr < chars.length && (chars[curr]==' '||chars[curr]=='\t'||chars[curr]=='\r'||chars[curr]=='\n')) curr++;
		if(curr<chars.length && chars[curr]=='*') {
			curr++;
			int start = curr;
			while(curr < chars.length && chars[curr]!='*') curr++;
			if(curr < chars.length) {
				// Imam encode!
				String encName = new String(chars, start, curr-start).trim();
				encode = determineEncodeFromName(encName);
				curr++;
				while(curr < chars.length && (chars[curr]==' '||chars[curr]=='\t'||chars[curr]=='\r'||chars[curr]=='\n')) curr++;
				if(curr >= chars.length) {
					varValue = "";
				} else {
					varValue = new String(chars, curr, chars.length-curr);
				}
			}
		}

		if(expectedCondition!=null) {
			int i = varValue.indexOf(',');
			String varName = i==-1 ? "" : varValue.substring(0, i).trim();
			varValue = i==-1 ? "?" : varValue.substring(i+1).trim();
			Object obj = resolveEchoParameter1(varName);
			if(!toBoolean(obj).equals(expectedCondition)) return;
		}
		String value = resolveEchoParameter2(varValue);
		switch(encode) {
		case HTML: WebUtil.htmlEncode(env.getWriter(), value); break;
		case HTML_ATTRIB: WebUtil.htmlAttribEncode(env.getWriter(), value); break;
		case JS: WebUtil.jsEncode(env.getWriter(), value); break;
		case RAW: env.getWriter().append(value); break;
		}
	}

	private EchoEncode determineEncodeFromName(String encName) {
		if(encName.equalsIgnoreCase("html")) return EchoEncode.HTML;
		if(encName.equalsIgnoreCase("html-attrib")) return EchoEncode.HTML_ATTRIB;
		if(encName.equalsIgnoreCase("js")) return EchoEncode.JS;
		if(encName.equalsIgnoreCase("raw")) return EchoEncode.RAW;
		throw new RuntimeException("Invalid echo encoding found: "+encName+".");
	}

	private Object resolveEchoParameter1(String varName) {
		if(varName.equals("true")) return Boolean.TRUE;
		if(varName.equals("false")) return Boolean.FALSE;
		if(varName.startsWith("$")) {
			return mapping.get(varName.substring(1));
		}
		
		System.out.println("WARNING: EchoCmd has condition set to '"+varName+"'; if variable was expected, add $ at the beginning of variable name.");
		
		return varName;
	}

	private String resolveEchoParameter2(String var) {
		if(var.length()>=2 && var.charAt(0)=='"' && var.charAt(var.length()-1)=='"') return var.substring(1, var.length()-1);
		if(var.startsWith("$")) {
			String name = var.substring(1);
			Object obj = mapping.get(name);
			return obj==null ? "" : obj.toString();
		} 
		return var;
	}
	
	private Boolean toBoolean(Object obj) {
		if(obj==null) return Boolean.FALSE;
		if(obj instanceof Boolean) return (Boolean)obj;
		if(obj instanceof Number) {
			Number num = (Number)obj;
			return num.doubleValue()!=0.0;
		}
		if(obj instanceof String) {
			String str = (String)obj;
			return !str.isEmpty();
		}
		return false;
	}
	
}
