package hr.fer.zemris.minilessons.web.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.dao.model.MLBasicPermission;
import hr.fer.zemris.minilessons.logging.MLLogger;
import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.service.MLCurrentContext;
import hr.fer.zemris.minilessons.service.Transactional;

/**
 * Implementation of logs functionality.

 * @author marcupic
 */
public class Logs {

	/**
	 * Flushes logs
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return template to render
	 */
	@DAOProvided @Transactional @Authenticated(optional=false,redirectAllowed=true)
	public Void logsFlush(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		if(MLCurrentContext.getAuthUser().getPermissions().contains(MLBasicPermission.ADMINISTRATION.getPermissonName())) {
			MLLogger.getAccesslogger().flush();
			MLLogger.getEdulogger().flush();
		}
		return null;
	}
}
