package hr.fer.zemris.minilessons.web.htmltempl.commands;

import java.io.IOException;
import java.util.function.Supplier;

import hr.fer.zemris.minilessons.web.htmltempl.CommandEnvironment;
import hr.fer.zemris.minilessons.web.htmltempl.ContentProcessorUtil;
import hr.fer.zemris.minilessons.web.htmltempl.HtmlTemplateCommand;

public class RegisterPageScriptCmd implements HtmlTemplateCommand {

	private Supplier<String> urlSupplier;
	
	public RegisterPageScriptCmd(Supplier<String> urlSupplier) {
		this.urlSupplier = urlSupplier;
	}

	@Override
	public void execute(CommandEnvironment env) throws IOException {
		if(!ContentProcessorUtil.checkFileName(env.getCommandArgs())) {
			throw new RuntimeException("Invalid file name found: " + env.getCommandArgs());
		}
		env.getProcessor().registerPageScript(urlSupplier.get()+env.getCommandArgs());
	}

}
