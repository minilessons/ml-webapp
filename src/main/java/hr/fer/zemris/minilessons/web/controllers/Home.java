package hr.fer.zemris.minilessons.web.controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.minilessons.service.Authenticated;
import hr.fer.zemris.minilessons.service.DAOProvided;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal;
import hr.fer.zemris.minilessons.web.retvals.TemplateRetVal.Technology;

/**
 * Implementation of home functionality. Used as one step of indirection to easier 
 * change what is the home page. Simply redirect user to currently configured home 
 * page.

 * @author marcupic
 */
public class Home {

	/**
	 * Implementation of home-redirect.
	 * 
	 * @param req request
	 * @param resp response
	 * @param variables variables
	 * @return <code>null</code>
	 * @throws IOException if redirection fails
	 */
	public Void home(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) throws IOException {
		resp.sendRedirect(req.getContextPath()+"/ml/index");
		return null;
	}

	@DAOProvided @Authenticated(optional=true)
	public TemplateRetVal mainIndex(HttpServletRequest req, HttpServletResponse resp, Map<String, String> variables) {
		return new TemplateRetVal("mainIndex",Technology.THYMELEAF);
	}
}
