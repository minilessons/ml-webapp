package hr.fer.zemris.minilessons;

/**
 * Represents a localized string. It is composed from a string value and a language tag.
 * 
 * @author marcupic
 *
 */
public interface ILocalizedString {

	/**
	 * Getter for language tag, such as "en", "de", "hr".
	 * 
	 * @return language tag
	 */
	public String getLang();
	
	/**
	 * Getter for string value.
	 * 
	 * @return string value
	 */
	public String getText();

}
