package hr.fer.zemris.minilessons.questions.quizzes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.minilessons.localization.I18NTexts;

public class TextQuestion extends AbstractQuestion {

	private I18NTexts text;
	private List<TextAnswer> answers;
	
	public TextQuestion(String id, I18NTexts text, List<TextAnswer> answers) {
		super(id);
		if(answers==null || answers.isEmpty()) {
			throw new IllegalArgumentException("TextQuestion answers must be nonempty collection.");
		}
		this.text = Objects.requireNonNull(text);
		this.answers = Collections.unmodifiableList(new ArrayList<>(answers));
	}

	public List<TextAnswer> getAnswers() {
		return answers;
	}
	
	public I18NTexts getText() {
		return text;
	}
	
	@Override
	public void accept(QuestionVisitor v) {
		v.visit(this);
	}
}
