package hr.fer.zemris.minilessons.questions.quizzes;

import java.util.Objects;

public abstract class AbstractQuestion {

	private String id;
	
	public AbstractQuestion(String id) {
		this.id = Objects.requireNonNull(id);
	}

	public String getId() {
		return id;
	}

	public abstract void accept(QuestionVisitor v);
}
