package hr.fer.zemris.minilessons.questions.quizzes;

import hr.fer.zemris.minilessons.localization.I18NTexts;

public class AbcSingleAnswer {

	private boolean correct;
	private I18NTexts text;
	
	public AbcSingleAnswer(boolean correct, I18NTexts text) {
		this.correct = correct;
		this.text = text;
	}

	public boolean isCorrect() {
		return correct;
	}
	
	public I18NTexts getText() {
		return text;
	}
	
}
