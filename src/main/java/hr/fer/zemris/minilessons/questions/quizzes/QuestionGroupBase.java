package hr.fer.zemris.minilessons.questions.quizzes;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.localization.I18NTexts;

public abstract class QuestionGroupBase {

	private String publicID;
	private int version;
	private int minorVersion;
	private I18NTexts title;
	private Set<String> supportedLanguages;
	private Set<I18NText> tags;
	
	public QuestionGroupBase(String publicID, int version, int minorVersion, Set<String> supportedLanguages, I18NTexts title, Set<I18NText> tags) {
		this.publicID = publicID;
		this.version = version;
		this.minorVersion = minorVersion;
		this.supportedLanguages = Collections.unmodifiableSet(new HashSet<>(supportedLanguages));
		this.title = Objects.requireNonNull(title);
		this.tags = Collections.unmodifiableSet(tags==null ? Collections.emptySet() : tags);
	}
	
	public Set<String> getSupportedLanguages() {
		return supportedLanguages;
	}

	public I18NTexts getTitle() {
		return title;
	}

	public Set<I18NText> getTags() {
		return tags;
	}
	
	public void setTags(Set<I18NText> tags) {
		this.tags = tags;
	}
	
	public abstract int size();
	
	public abstract AbstractQuestion getQuestion(int index);
	
	public abstract void accept(QuestionGroupVisitor v);

	public abstract SupportedXMLQuestions getQuestionType();
	
	public abstract AbstractQuestion getForID(String id);
	
	public String getPublicID() {
		return publicID;
	}
	
	public int getVersion() {
		return version;
	}
	
	public int getMinorVersion() {
		return minorVersion;
	}
}
