package hr.fer.zemris.minilessons.questions.quizzes;

public interface QuestionGroupVisitor {
	void visit(AbcSingleQuestionGroup question);
	void visit(TextQuestionGroup question);
}
