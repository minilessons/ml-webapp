package hr.fer.zemris.minilessons.questions.quizzes;

import java.util.Objects;

import hr.fer.zemris.minilessons.localization.I18NTexts;

public class TextAnswer {

	private boolean caseSensitive;
	private I18NTexts text;
	
	public TextAnswer(boolean caseSensitive, I18NTexts text) {
		this.caseSensitive = caseSensitive;
		this.text = Objects.requireNonNull(text);
	}

	public boolean isCaseSensitive() {
		return caseSensitive;
	}
	
	public I18NTexts getText() {
		return text;
	}
	
}
