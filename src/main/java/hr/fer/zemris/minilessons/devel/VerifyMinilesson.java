package hr.fer.zemris.minilessons.devel;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.minilessons.DescriptorParserUtil;
import hr.fer.zemris.minilessons.service.MLDeployer;
import hr.fer.zemris.minilessons.service.MLVerificationException;
import hr.fer.zemris.minilessons.xmlmodel.XMLMinilesson;

public class VerifyMinilesson {

	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Očekivao sam jedan argument: putanju do korijenskog direktorija minilekcije.");
			return;
		}
		Path rootPath = Paths.get(args[0]);
		
		if(!Files.exists(rootPath)) {
			System.out.println(rootPath+" ne postoji!");
			return;
		}
		if(!Files.isDirectory(rootPath)) {
			System.out.println(rootPath+" nije direktorij!");
			return;
		}
		Path descriptorPath = rootPath.resolve("descriptor.xml");
		XMLMinilesson xmlml = null;
		try {
			xmlml = DescriptorParserUtil.parseInputStream(Files.newInputStream(descriptorPath));
		} catch(Exception ex) {
			System.out.println("Učitavanje datoteke "+descriptorPath+" nije uspjelo. Pogreška je ispisana u sljedećem retku.");
			System.out.println(ex.getMessage());
			return;
		}
		
		boolean ok = false;
		try {
			System.out.println("Započinjemo s provjerama...");
			MLDeployer.checkMinilessonDescriptor(rootPath, xmlml);
			ok = true;
			System.out.println("Minilekcija je prošla sve provjere.");
		} catch(MLVerificationException ex) {
			System.out.println(" ==> Postoji problem. Detalji su ispisani u sljedećem retku.");
			System.out.println("     " + ex.getMessage());
		} catch(Exception ex) {
			System.out.println(" ==> Postoji problem. Iznimka je ispisana u nastavku.");
			ex.printStackTrace(System.out);
		}
		
		if(ok) {
			System.out.println("Je li minilekcija ograničena?");
			try {
				MLDeployer.checkMinilessonIsRestricted(rootPath, xmlml);
				System.out.println(" ==> JE");
			} catch(MLVerificationException ex) {
				System.out.println(" ==> NIJE. Razlog je ispisan u sljedećem retku.");
				System.out.println("     " + ex.getMessage());
			} catch(Exception ex) {
				System.out.println(" ==> Postoji problem. Iznimka je ispisana u nastavku.");
				ex.printStackTrace(System.out);
			}
		}
	}

}
