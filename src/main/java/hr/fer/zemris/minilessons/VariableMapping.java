package hr.fer.zemris.minilessons;

public interface VariableMapping {
	boolean containsKey(String key);
	Object get(String key);
}
