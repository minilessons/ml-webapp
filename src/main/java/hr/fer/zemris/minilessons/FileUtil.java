package hr.fer.zemris.minilessons;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileUtil {

	private static interface Operation {
		void execute() throws IOException;
	}

	private static class DeleteFile implements Operation {
		private Path file;

		public DeleteFile(Path file) {
			super();
			this.file = file;
		}
		
		@Override
		public void execute() throws IOException {
			Files.delete(file);
		}
	}
	
	private static class CopyVisitor implements FileVisitor<Path> {
		private Path src;
		private Path dest;
		private boolean errors;
		private List<Operation> operations = new ArrayList<>();
		
		public CopyVisitor(Path src, Path dest) {
			super();
			this.src = src;
			this.dest = dest;
		}
		
		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			Path rel = src.relativize(dir);
			Path dstDir = dest.resolve(toFileSystem(dest.getFileSystem(), rel));
			if(!Files.exists(dstDir)) {
				Files.createDirectories(dstDir);
				operations.add(new DeleteFile(dstDir));
			}
			return FileVisitResult.CONTINUE;
		}
		
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			Path rel = src.relativize(file);
			Path dstFile = dest.resolve(toFileSystem(dest.getFileSystem(), rel));
			Files.createDirectories(dstFile.getParent());
			Files.copy(file, dstFile, StandardCopyOption.REPLACE_EXISTING);
			operations.add(new DeleteFile(dstFile));
			return FileVisitResult.CONTINUE;
		}
		
		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
			errors = true;
			return FileVisitResult.TERMINATE;
		}
		
		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			return FileVisitResult.CONTINUE;
		}
		
	}

	private static class DeleteVisitor implements FileVisitor<Path> {

		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			return FileVisitResult.CONTINUE;
		}
		
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			Files.delete(file);
			return FileVisitResult.CONTINUE;
		}
		
		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
			return FileVisitResult.TERMINATE;
		}
		
		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			Files.delete(dir);
			return FileVisitResult.CONTINUE;
		}
		
	}

	public static void deleteTree(Path src) throws IOException {
		Files.walkFileTree(src, new DeleteVisitor());
	}
	
	public static void copyTree(Path src, Path dest) throws IOException {
		CopyVisitor cv = new CopyVisitor(src.normalize().toAbsolutePath(), dest);
		try {
			Files.walkFileTree(src, cv);
		} catch(IOException ex) {
			// we must revert what was partially copied...
			cv.errors = true;
		}
		if(cv.errors) {
			Collections.reverse(cv.operations);
			for(Operation op : cv.operations) {
				try {
					op.execute();
				} catch(IOException ignorable) {
				}
			}
		}
		if(cv.errors) {
			throw new IOException("Could not copy package tree.");
		}
	}
	
	/**
	 * Helper method which converts path from one filesystem to other.
	 * 
	 * @param targetFileSystem to which filesystem to convert?
	 * @param path path to convert
	 * @return converted path
	 */
	private static Path toFileSystem(FileSystem targetFileSystem, Path path) {
		if(path.getFileSystem().equals(targetFileSystem)) {
			return path;
		}
	    Path ret = targetFileSystem.getPath(path.isAbsolute() ? targetFileSystem.getSeparator() : "");
	    for(Path component: path) {
	        ret = ret.resolve(component.getFileName().toString());
	    }
	    return ret;
	}
}
