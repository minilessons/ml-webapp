package hr.fer.zemris.minilessons;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.NoTypePermission;
import com.thoughtworks.xstream.security.NullPermission;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;

import hr.fer.zemris.minilessons.dao.model.QuestionConfigVariability;
import hr.fer.zemris.minilessons.xmlmodel.XMLAbout;
import hr.fer.zemris.minilessons.xmlmodel.XMLAuthor;
import hr.fer.zemris.minilessons.xmlmodel.XMLConfiguration;
import hr.fer.zemris.minilessons.xmlmodel.XMLLanguage;
import hr.fer.zemris.minilessons.xmlmodel.XMLQuestionDescriptor;
import hr.fer.zemris.minilessons.xmlmodel.XMLString;

/**
 * Utility class for construction of parsers for parsing <code>descriptor.xml</code> of questions
 * as well as for parsing.
 * 
 * @author marcupic
 *
 */
public class QDescriptorParserUtil {

	/**
	 * Parse <code>descriptor.xml</code> which is made available as provided input stream and returns
	 * {@link XMLQuestionDescriptor} object which represents the content of provided <code>descriptor.xml</code>.
	 * 
	 * @param is input stream from which the <code>descriptor.xml</code> can be read
	 * @return object which represents the read and parsed file
	 */
	public static XMLQuestionDescriptor parseInputStream(InputStream is) {
		try {
			return (XMLQuestionDescriptor)getParser().fromXML(new InputStreamReader(new BufferedInputStream(is), StandardCharsets.UTF_8));
		} finally {
			try { is.close(); } catch(Exception ignorable) {}
		}
	}
	
	/**
	 * Creates and returns a new parser for processing of <code>descriptor.xml</code>.
	 * 
	 * @return new parser
	 */
	private static XStream getParser() {
		XStream xs = new XStream(new StaxDriver());

		xs.addPermission(NoTypePermission.NONE);
		xs.addPermission(NullPermission.NULL);
		xs.addPermission(PrimitiveTypePermission.PRIMITIVES);
		xs.allowTypeHierarchy(Collection.class);
		xs.allowTypes(new Class[] {String.class, QuestionConfigVariability.class});
		xs.allowTypesByWildcard(new String[] {XMLQuestionDescriptor.class.getPackage().getName()+".*"});
		
		xs.alias("id", String.class);
		
		xs.alias("question", XMLQuestionDescriptor.class);
		xs.useAttributeFor(XMLQuestionDescriptor.class, "uid");
		xs.useAttributeFor(XMLQuestionDescriptor.class, "version");
		xs.useAttributeFor(XMLQuestionDescriptor.class, "minorVersion");
		xs.useAttributeFor(XMLQuestionDescriptor.class, "apiLevel");

		xs.alias("language", XMLLanguage.class);
		xs.useAttributeFor(XMLLanguage.class, "lang");

		xs.alias("about", XMLAbout.class);

		xs.alias("string", XMLString.class);
		xs.useAttributeFor(XMLString.class, "text");
		xs.useAttributeFor(XMLString.class, "lang");

		xs.alias("author", XMLAuthor.class);
		xs.useAttributeFor(XMLAuthor.class, "firstName");
		xs.useAttributeFor(XMLAuthor.class, "lastName");
		xs.useAttributeFor(XMLAuthor.class, "email");

		xs.alias("configuration", XMLConfiguration.class);
		xs.useAttributeFor(XMLConfiguration.class, "name");
		xs.useAttributeFor(XMLConfiguration.class, "variability");
		
//		ClassAliasingMapper mapper = new ClassAliasingMapper(xs.getMapper());
//		mapper.addClassAlias("config", String.class);
//		xs.registerLocalConverter(XMLQuestionDescriptor.class, "configurations", new CollectionConverter(mapper));
		
		return xs;
	}
}
