package hr.fer.zemris.minilessons.testspec.model;

/**
 * Model objekta koji na temelju informacije je li zadatak uopće rješavan te ako je,
 * koja je mjera točnosti generira prikladan broj bodova. Primjeri implementacija
 * bi moglo biti proporcionalno bodovanje mjeri točnosti, bodovanje koje daje
 * različit broj bodova ovisno o tome je li zadatak rješavan, je li točan ili nije točan,
 * itd.
 * 
 * @author marcupic
 *
 */
public interface ProblemGrader {

	double calculateScore(boolean solved, double correctness);
	
}
