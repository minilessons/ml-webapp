package hr.fer.zemris.minilessons.testspec.model;

public interface Condition {

	boolean test(CheckersEnvironment env);

	default Condition not() {
		return e->!test(e);
	}
	
	default Condition and(Condition c) {
		return e->test(e) && c.test(e);
	}
	
	default Condition or(Condition c) {
		return e->test(e) || c.test(e);
	}
}
