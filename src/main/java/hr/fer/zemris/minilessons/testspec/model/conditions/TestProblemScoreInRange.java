package hr.fer.zemris.minilessons.testspec.model.conditions;

import hr.fer.zemris.minilessons.testspec.model.CheckersEnvironment;
import hr.fer.zemris.minilessons.testspec.model.Condition;
import hr.fer.zemris.minilessons.testspec.model.conditions.intervals.IntervalChecker;

public class TestProblemScoreInRange implements Condition {

	// TODO: dovrši
	
	private int problemIndex;
	private IntervalChecker intervalChecker;
	
	public TestProblemScoreInRange(int problemIndex, IntervalChecker intervalChecker) {
		this.problemIndex = problemIndex;
		this.intervalChecker = intervalChecker;
	}

	@Override
	public boolean test(CheckersEnvironment env) {
		double score = 0; // kasnije - dohvati iz zadatka i vidi je li isti uopće rješavan!
		return intervalChecker.isInInterval(score);
	}
	
}
