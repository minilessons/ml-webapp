package hr.fer.zemris.minilessons.testspec.lexer;

public class LexerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public LexerException() {
	}

	public LexerException(String message) {
		super(message);
	}

	public LexerException(Throwable cause) {
		super(cause);
	}

	public LexerException(String message, Throwable cause) {
		super(message, cause);
	}

	public LexerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
