package hr.fer.zemris.minilessons.testspec.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.localization.I18NTexts;
import hr.fer.zemris.minilessons.testspec.lexer.Lexer;
import hr.fer.zemris.minilessons.testspec.model.Condition;
import hr.fer.zemris.minilessons.testspec.model.FinishAcceptChecker;
import hr.fer.zemris.minilessons.testspec.model.ProblemGrader;
import hr.fer.zemris.minilessons.testspec.model.ProblemMessageProducer;
import hr.fer.zemris.minilessons.testspec.model.ProblemMessageProducerImpl;
import hr.fer.zemris.minilessons.testspec.model.TSMTestQuestion;
import hr.fer.zemris.minilessons.testspec.model.TestQuestionTarget;
import hr.fer.zemris.minilessons.testspec.model.conditions.TestProblemCorrectness;
import hr.fer.zemris.minilessons.testspec.model.conditions.TestProblemFinished;
import hr.fer.zemris.minilessons.testspec.model.conditions.TestProblemUnsolved;
import hr.fer.zemris.minilessons.testspec.model.conditions.intervals.IntervalChecker;
import hr.fer.zemris.minilessons.testspec.model.conditions.intervals.IntervalCheckerClosedClosed;
import hr.fer.zemris.minilessons.testspec.model.conditions.intervals.IntervalCheckerClosedOpen;
import hr.fer.zemris.minilessons.testspec.model.conditions.intervals.IntervalCheckerOpenClosed;
import hr.fer.zemris.minilessons.testspec.model.conditions.intervals.IntervalCheckerOpenOpen;
import hr.fer.zemris.minilessons.testspec.model.graders.ProportionalGrader;
import hr.fer.zemris.minilessons.testspec.model.graders.TFBGrader;
import hr.fer.zemris.minilessons.testspec.model.translations.TranslationProviderFromI18NTexts;
import hr.fer.zemris.minilessons.testspec.model.translations.TranslationProviderFromMap;

public class TestSpecParser {

	private Lexer lexer;
	private List<TSMTestQuestion> questions;
	private Map<String,I18NTexts> globalTranslations = new HashMap<>();
	private List<I18NTexts> allTranslations = new ArrayList<>();
	private Set<String> usedMessageKeys = new HashSet<>();
	
	public TestSpecParser(String src) {
		lexer = new Lexer(src);
		questions = parse();
		if(!lexer.getToken().isEOF()) {
			throw new TestSpecParserException("Tokens left after parsing. Stopped at: " + lexer.getToken());
		}
	}

	public List<TSMTestQuestion> getQuestions() {
		return questions;
	}
	
	private List<TSMTestQuestion> parse() {
		List<TSMTestQuestion> list = new ArrayList<>();
		int expectedQuestionNo = 1;
		lexer.nextToken();
		while(true) {
			if(lexer.getToken().isEOF()) break;
			if(lexer.getToken().isIdentifier("question")) {
				TSMTestQuestion q = parseQuestion();
				if(q.getIndex() != expectedQuestionNo) {
					throw new TestSpecParserException("Expected definition for question "+expectedQuestionNo+" but found for question "+q.getIndex()+".");
				}
				list.add(q);
				expectedQuestionNo++;
				continue;
			}
			if(lexer.getToken().isIdentifier("translations")) {
				parseGlobalTranslations();
				continue;
			}
			throw_utf();
		}
		return list;
	}

	private TSMTestQuestion parseQuestion() {
		lexer.nextToken();
		if(!lexer.getToken().isInteger()) throw_utf();
		int index = lexer.getToken().asInteger();
		lexer.nextToken();
		checkSymbolOrThrow('{');
		lexer.nextToken();
		
		Condition enabled_if = null;
		Condition score_condition = null;
		String uid = null;
		String config = null;
		FinishAcceptChecker finishAcceptChecker = null;
		ProblemGrader grader = null;
		List<ProblemMessageProducer> messageProducers = new ArrayList<>();
		TestQuestionTarget target = null;
		while(true) {
			if(lexer.getToken().isSymbol('}')) {
				lexer.nextToken();
				break;
			}
			if(lexer.getToken().isIdentifier("for")) {
				if(target!=null) throw new TestSpecParserException("Multiple occurence of target in question "+index+".");
				lexer.nextToken();
				if(lexer.getToken().isIdentifier("user")) {
					lexer.nextToken();
					target = TestQuestionTarget.USER;
				} else if(lexer.getToken().isIdentifier("group")) {
					lexer.nextToken();
					if(lexer.getToken().isIdentifier("user")) {
						lexer.nextToken();
						target = TestQuestionTarget.GROUP_USER;
					} else {
						target = TestQuestionTarget.GROUP;
					}
				} else throw_utf();
				checkSymbolOrThrow(';');
				lexer.nextToken();
				continue;
			}
			if(lexer.getToken().isIdentifier("enabled_if")) {
				if(enabled_if!=null) throw new TestSpecParserException("Multiple occurence of enabled_if in question "+index+".");
				lexer.nextToken();
				checkSymbolOrThrow(':');
				lexer.nextToken();
				Condition c = parseCondition();
				checkSymbolOrThrow(';');
				lexer.nextToken();
				enabled_if = c;
				continue;
			}
			if(lexer.getToken().isIdentifier("score_condition")) {
				if(score_condition!=null) throw new TestSpecParserException("Multiple occurence of score_condition in question "+index+".");
				lexer.nextToken();
				checkSymbolOrThrow(':');
				lexer.nextToken();
				Condition c = parseCondition();
				checkSymbolOrThrow(';');
				lexer.nextToken();
				score_condition = c;
				continue;
			}
			if(lexer.getToken().isIdentifier("id")) {
				if(uid!=null) throw new TestSpecParserException("Multiple occurence of id in question "+index+".");
				lexer.nextToken();
				if(!lexer.getToken().isString()) throw_utf();
				String str = lexer.getToken().asString();
				lexer.nextToken();
				checkSymbolOrThrow(';');
				lexer.nextToken();
				int p = str.indexOf(':');
				if(p==-1) {
					uid = str.trim();
					config = "default";
				} else {
					uid = str.substring(0, p).trim();
					config = str.substring(p+1).trim();
				}
				continue;
			}
			if(lexer.getToken().isIdentifier("finish_accept")) {
				if(finishAcceptChecker!=null) throw new TestSpecParserException("Multiple occurence of finish_accept in question "+index+".");
				lexer.nextToken();
				checkSymbolOrThrow(':');
				lexer.nextToken();
				if(!lexer.getToken().isIdentifier()) throw_utf();
				FinishAcceptChecker c = null;
				switch(lexer.getToken().asString()) {
					case "if_correct": c = FinishAcceptChecker.IF_CORRECT; break;
					case "if-not-empty": c = FinishAcceptChecker.IF_SOLVED; break;
					case "always": c = FinishAcceptChecker.ALWAYS; break;
					default:
						throw new TestSpecParserException("Invalid finish_accept specification found: "+lexer.getToken().asString());
				}
				lexer.nextToken();
				checkSymbolOrThrow(';');
				lexer.nextToken();
				finishAcceptChecker = c;
				continue;
			}
			if(lexer.getToken().isIdentifier("score")) {
				if(grader!=null) throw new TestSpecParserException("Multiple occurence of score in question "+index+".");
				lexer.nextToken();
				checkSymbolOrThrow(':');
				lexer.nextToken();
				if(lexer.getToken().isIdentifier("proportional")) {
					lexer.nextToken();
					if(!lexer.getToken().isNumber()) throw_utf();
					double max = lexer.getToken().asNumber().doubleValue();
					lexer.nextToken();
					grader = new ProportionalGrader(max);
				} else if(lexer.getToken().isSymbol('{')) {
					lexer.nextToken();
					if(!lexer.getToken().isNumber()) throw_utf();
					double b1 = lexer.getToken().asNumber().doubleValue();
					lexer.nextToken();
					checkSymbolOrThrow(',');
					lexer.nextToken();
					if(!lexer.getToken().isNumber()) throw_utf();
					double b2 = lexer.getToken().asNumber().doubleValue();
					lexer.nextToken();
					checkSymbolOrThrow(',');
					lexer.nextToken();
					if(!lexer.getToken().isNumber()) throw_utf();
					double b3 = lexer.getToken().asNumber().doubleValue();
					lexer.nextToken();
					checkSymbolOrThrow('}');
					lexer.nextToken();
					grader = new TFBGrader(b1, b2, b3, 0.00001);
				} else throw_utf();
				checkSymbolOrThrow(';');
				lexer.nextToken();
				continue;
			}
			if(lexer.getToken().isIdentifier("message_if_unsolved")) {
				lexer.nextToken();
				checkSymbolOrThrow(':');
				lexer.nextToken();
				Condition cond = new TestProblemUnsolved(index);
				if(lexer.getToken().isMessageKey()) {
					String key = lexer.getToken().asString();
					lexer.nextToken();
					checkSymbolOrThrow(';');
					lexer.nextToken();
					messageProducers.add(new ProblemMessageProducerImpl(cond, new TranslationProviderFromMap(key, globalTranslations)));
					usedMessageKeys.add(key);
					continue;
				}
				if(lexer.getToken().isSymbol('{')) {
					I18NTexts translations = parseTranslation();
					checkSymbolOrThrow(';');
					lexer.nextToken();
					messageProducers.add(new ProblemMessageProducerImpl(cond, new TranslationProviderFromI18NTexts(translations)));
					continue;
				}
				throw_utf();
			}
			if(lexer.getToken().isIdentifier("message_if_correctness_between")) {
				lexer.nextToken();
				checkSymbolOrThrow(':');
				lexer.nextToken();
				boolean firstOpen = true;
				if(lexer.getToken().isSymbol('(')) {
				} else if(lexer.getToken().isSymbol('[')) {
					firstOpen = false;
				} else {
					throw_utf();
				}
				lexer.nextToken();
				if(!lexer.getToken().isNumber()) throw_utf();
				double min = lexer.getToken().asNumber().doubleValue();
				lexer.nextToken();
				checkSymbolOrThrow(',');
				lexer.nextToken();
				if(!lexer.getToken().isNumber()) throw_utf();
				double max = lexer.getToken().asNumber().doubleValue();
				lexer.nextToken();
				boolean secondOpen = true;
				if(lexer.getToken().isSymbol(')')) {
				} else if(lexer.getToken().isSymbol(']')) {
					secondOpen = false;
				} else {
					throw_utf();
				}
				lexer.nextToken();
				IntervalChecker ic = null;
				if(firstOpen) {
					if(secondOpen) {
						ic = new IntervalCheckerOpenOpen(min, max);
					} else {
						ic = new IntervalCheckerOpenClosed(min, max);
					}
				} else {
					if(secondOpen) {
						ic = new IntervalCheckerClosedOpen(min, max);
					} else {
						ic = new IntervalCheckerClosedClosed(min, max);
					}
				}
				Condition cond = new TestProblemCorrectness(index,ic);
				if(lexer.getToken().isMessageKey()) {
					String key = lexer.getToken().asString();
					lexer.nextToken();
					checkSymbolOrThrow(';');
					lexer.nextToken();
					messageProducers.add(new ProblemMessageProducerImpl(cond, new TranslationProviderFromMap(key, globalTranslations)));
					usedMessageKeys.add(key);
					continue;
				}
				if(lexer.getToken().isSymbol('{')) {
					I18NTexts translations = parseTranslation();
					checkSymbolOrThrow(';');
					lexer.nextToken();
					messageProducers.add(new ProblemMessageProducerImpl(cond, new TranslationProviderFromI18NTexts(translations)));
					continue;
				}
				throw_utf();
			}
			throw_utf();
		}
		return new TSMTestQuestion(index,target,uid,config,finishAcceptChecker,grader,score_condition,messageProducers,enabled_if);
	}

	private Condition parseCondition() {
		checkSymbolOrThrow('#');
		lexer.nextToken();
		if(!lexer.getToken().isInteger()) throw_utf();
		int problemIndex = lexer.getToken().asInteger();
		lexer.nextToken();
		checkSymbolOrThrow(':');
		lexer.nextToken();
		if(!lexer.getToken().isIdentifier()) throw_utf();
		String verb = lexer.getToken().asString();
		String filter = "any";
		lexer.nextToken();
		if(lexer.getToken().isSymbol(':')) {
			lexer.nextToken();
			if(!lexer.getToken().isIdentifier()) throw_utf();
			filter = lexer.getToken().asString();
			lexer.nextToken();
		}
		if(verb.equals("finished")) {
			boolean all = true;
			if(filter.equals("all")) {
			} else if(filter.equals("any")) {
				all = false;
			} else {
				throw new TestSpecParserException("Invalid subcondition: "+verb+":"+filter+".");
			}
			return new TestProblemFinished(problemIndex, all);
		} else {
			throw new TestSpecParserException("Invalid condition: "+verb+".");
		}
	}

	private void parseGlobalTranslations() {
		lexer.nextToken();
		checkSymbolOrThrow('{');
		lexer.nextToken();
		while(true) {
			if(lexer.getToken().isMessageKey()) {
				String messageKey = lexer.getToken().asString();
				if(globalTranslations.containsKey(messageKey)) {
					throw new TestSpecParserException("Message key @"+messageKey+" was defined more than once. This is not allowed.");
				}
				lexer.nextToken();
				globalTranslations.put(messageKey, parseTranslation());
				continue;
			}
			if(lexer.getToken().isSymbol('}')) {
				lexer.nextToken();
				break;
			}
			throw_utf();
		}
	}

	private I18NTexts parseTranslation() {
		Map<String,I18NText> map = new HashMap<>();
		checkSymbolOrThrow('{');
		lexer.nextToken();
		while(true) {
			if(!lexer.getToken().isIdentifier()) break;
			String lang = lexer.getToken().asString();
			lexer.nextToken();
			if(!lexer.getToken().isString()) throw_utf();
			String val = lexer.getToken().asString();
			lexer.nextToken();
			if(map.containsKey(lang)) throw new TestSpecParserException("Translation found with multiple language "+lang+".");
			map.put(lang, new I18NText(val, lang));
		}
		checkSymbolOrThrow('}');
		lexer.nextToken();
		if(map.isEmpty()) throw new TestSpecParserException("Translation must offer at least one language.");
		I18NTexts translation = new I18NTexts(map);
		allTranslations.add(translation);
		return translation;
	}

	private void checkSymbolOrThrow(char c) {
		if(!lexer.getToken().isSymbol(c)) {
			throw new TestSpecParserException("Unexpected token found: "+lexer.getToken());
		}
	}
	
	private void throw_utf() {
		throw new TestSpecParserException("Unexpected token found: "+lexer.getToken());
	}
}
