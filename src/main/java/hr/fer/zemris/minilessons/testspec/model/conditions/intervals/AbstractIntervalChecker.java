package hr.fer.zemris.minilessons.testspec.model.conditions.intervals;

public abstract class AbstractIntervalChecker implements IntervalChecker {

	protected double lower;
	protected double upper;
	
	public AbstractIntervalChecker(double lower, double upper) {
		this.lower = lower;
		this.upper = upper;
	}

}
