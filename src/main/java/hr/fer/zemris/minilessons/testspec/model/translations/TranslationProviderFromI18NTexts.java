package hr.fer.zemris.minilessons.testspec.model.translations;

import java.util.Optional;

import hr.fer.zemris.minilessons.localization.I18NText;
import hr.fer.zemris.minilessons.localization.I18NTexts;

public class TranslationProviderFromI18NTexts extends AbstractTranslationProvider {

	private I18NTexts texts;
	
	public TranslationProviderFromI18NTexts(I18NTexts texts) {
		this.texts = texts;
	}

	@Override
	public Optional<I18NText> getTranslation(String languageTag, boolean fallBackToDefault) {
		return getTranslation(texts, languageTag, fallBackToDefault);
	}
	
}
