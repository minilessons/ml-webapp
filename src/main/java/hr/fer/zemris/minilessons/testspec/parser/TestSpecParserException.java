package hr.fer.zemris.minilessons.testspec.parser;

public class TestSpecParserException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TestSpecParserException() {
	}

	public TestSpecParserException(String message) {
		super(message);
	}

	public TestSpecParserException(Throwable cause) {
		super(cause);
	}

	public TestSpecParserException(String message, Throwable cause) {
		super(message, cause);
	}

	public TestSpecParserException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
